INCLUDEPATH += $$PWD/files/fdb/fdbLogic/Qt/JIAYUUIComponent

win32-msvc2010 {
    CONFIG(debug,debug|release) {
        LIBS += -lFDBUiPluginsd.2010.dll
    } else {
        LIBS += -lFDBUiPlugins.2010.dll
    }
    contains(DEFINES,_WIN64) {
    } else {
        QMAKE_LFLAGS += /LIBPATH:$$PWD/files/fdb/fdbLogic/Qt/JIAYUUiComponent/lib/win32/i386/msvc/2010
    }
}
win32-msvc2013 {
    CONFIG(debug,debug|release) {
        LIBS += -lFDBUiPluginsd.2013.dll
    } else {
        LIBS += -lFDBUiPlugins.2013.dll
    }
    contains(DEFINES,_WIN64) {
    } else {
        QMAKE_LFLAGS += /LIBPATH:$$PWD/files/fdb/fdbLogic/Qt/JIAYUUiComponent/lib/win32/i386/msvc/2013
    }
}

#HEADERS += \
#    $$PWD/files/fdb/fdbLogic/Qt/JIAYUUIComponent/company_contacts.h \
#    $$PWD/files/fdb/fdbLogic/Qt/JIAYUUIComponent/flipbutton.h \
#    $$PWD/files/fdb/fdbLogic/Qt/JIAYUUIComponent/dragtargetcontainer.h
