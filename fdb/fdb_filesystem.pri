win32 {
    win32-msvc* {
        CONFIG(debug,debug|release) {
            LIBS += -llibFDBFileSystemd
            QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/msvc2010/i386/debug/shared
        } else {
            LIBS += -llibFDBFileSystem
            QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/msvc2010/i386/release/shared
        }
    }
}
