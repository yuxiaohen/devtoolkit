DEFINES += WIN32_LEAN_AND_MEAN NOMINMAX

linux | mac {
    DEFINES += _FDB_POSIX
}

INCLUDEPATH += $$PWD/files

HEADERS += \
    $$PWD/files/fdb/fdb.h \
    $$PWD/files/fdb/fdbglobal.h \
    $$PWD/files/fdb/fdblib.h \
    $$PWD/files/fdb/cfgmgr32.h

#Primitive layer
include($$PWD/files/fdb/fdbPrimitive/fdbPrimitiveHeaders.pri)
#Logic layer
include($$PWD/files/fdb/fdbLogic/fdbLogicHeaders.pri)
