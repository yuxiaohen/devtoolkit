SOURCES += \
    $$PWD/files/fdb/fdblib.cpp \
    $$PWD/files/fdb/fdbglobal.cpp

#Primitive layer
include($$PWD/files/fdb/fdbPrimitive/fdbPrimitiveSources.pri)
#Logic layer
include($$PWD/files/fdb/fdbLogic/fdbLogicSources.pri)
