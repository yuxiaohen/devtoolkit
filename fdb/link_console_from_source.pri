message("           ----------------->link fdb console from source")

DEFINES += LINK_FDB_FROM_SOURCE

INCLUDEPATH += $$PWD/files

include($$PWD/files/fdb/fdbPrimitive/fdbPrimitive.pri)
include($$PWD/files/fdb/fdbLogic/fdbLogic_console.pri)

DEFINES += __FDB_CONSOLE_LINK__

win32-msvc* {
    QMAKE_LFLAGS += /DEBUG
}

message("           <-----------------link fdb console from source")
