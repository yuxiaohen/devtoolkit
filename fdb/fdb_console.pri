DEFINES += __FDB_CONSOLE_LINK__ WIN32_LEAN_AND_MEAN

INCLUDEPATH += $$PWD/files

SOURCES += \
    $$PWD/files/fdb/fdblib.cpp

#linux {
#    linux-g++ | linux-g++-32 {
#        CONFIG(debug,debug|release) {
#            message("link fdb console on linux g++ i386 debug-mode")
#            QMAKE_LFLAGS += -L$$PWD/fdb/lib/linux/gcc/i386/debug/static
#            LIBS += -lFDBconsoled
#        } else {
#            message("link fdb console on linux g++ i386 release-mode")
#            QMAKE_LFLAGS += -L$$PWD/fdb/lib/linux/gcc/i386/release/static
#            LIBS += -lFDBconsole
#        }
#    }
#}

include($$PWD/files/fdb/fdbPrimitive/Text/Text.pri)
include($$PWD/files/fdb/fdbPrimitive/IOKit/IOKit.pri)
include($$PWD/files/fdb/fdbPrimitive/Exception/Exception.pri)
include($$PWD/files/fdb/fdbPrimitive/MemoryManagement/MemoryManagement.pri)
