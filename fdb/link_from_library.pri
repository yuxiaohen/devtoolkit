CONFIG += qt
QT += gui core widgets quick

INCLUDEPATH += $$PWD/files

win32 {
    win32-msvc* {
        contains(DEFINES,WIN64) { #amd64
            CONFIG(debug,debug|release) { #debug
                LIBS += -lFDBd
                QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/msvc2010/amd64/debug/shared
            } else { #release
                LIBS += -lFDB
                QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/msvc2010/amd64/release/shared
            }
        } else { #i386
            CONFIG(debug,debug|release) { #debug
                LIBS += -lFDBd
                QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/msvc2010/i386/debug/shared
            } else { #release
                LIBS += -lFDB
                QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/msvc2010/i386/release/shared
            }
        }
    }
}
