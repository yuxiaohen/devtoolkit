CONFIG += qt
TEMPLATE = lib
QT += gui core widgets webkitwidgets concurrent

win32 {
    CONFIG += dll
}

win32-msvc2010 {
  contains(CONFIG,static) {
    CONFIG(debug,debug|release) {
      TARGET = FDBd.2010.s
    } else {
      TARGET = FDB.2010.s
    }
  } else {
    CONFIG(debug,debug|release) {
      TARGET = FDBd.2010.dll
    } else {
      TARGET = FDB.2010.dll
    }
  }
}

win32-msvc2013 {
  contains(CONFIG,static) {
    CONFIG(debug,debug|release) {
      TARGET = FDBd.2013.s
    } else {
      TARGET = FDB.2013.s
    }
  } else {
    CONFIG(debug,debug|release) {
      TARGET = FDBd.2013.dll
    } else {
      TARGET = FDB.2013.dll
    }
  }
}

QMAKE_CXXFLAGS += -std=c++0x

DEFINES += COMPILE_FDB_AS_LIBRARY
#DEFINES += FDB_RECORD_ERR
#DEFINES += FDB_USE_LOG LOG_TO_FILE

include($$PWD/../../../fdbHeaders.pri)
include($$PWD/../../../fdbSources.pri)

include($$PWD/../../../../openssl/openssl.pri)
include($$PWD/../../../../boost/boost.pri)
include($$PWD/../../../../zlib/zlib.pri)
include($$PWD/../../../../regex/regex.pri)
include($$PWD/../../../../serialization/jsonite/jsonite.pri)
win32 {
    include($$PWD/../../../../SmartDog/SmartDog.pri)
}

win32 {
  LIBS += -lUser32 -lGdi32 -lWs2_32
  win32-msvc* {
    QMAKE_CFLAGS += /DEBUG
    QMAKE_CXXFLAGS += /DEBUG
    QMAKE_LFLAGS += /DEBUG
  }
  win32-msvc2013 {
    contains(DEFINES,WIN64) {
      QMAKE_LFLAGS += /SUBSYSTEM:CONSOLE",5.02"
    } else {
      QMAKE_LFLAGS += /SUBSYSTEM:CONSOLE",5.01"
    }
  }
}

mac {
    LIBS += -lcrypto
    LIBS += $$BOOST_LIB_PATH/libboost_system.a $$BOOST_LIB_PATH/libboost_thread.a
}
