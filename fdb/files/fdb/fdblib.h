#pragma once

#include <stdio.h>
#include <time.h>

#if !defined(LINK_FDB_FROM_SOURCE)
    #if defined(COMPILE_FDB_AS_LIBRARY)
        #if defined(_MSC_VER)
            #define FDBInterface __declspec(dllexport)
        #else
            #define FDBInterface
        #endif
    #else
        #if defined(_MSC_VER)
            #define FDBInterface __declspec(dllimport)
        #else
            #define FDBInterface
        #endif
    #endif
#else
    #define FDBInterface
#endif


#if defined(FDB_USE_LOG)
    #if defined(LOG_TO_FILE)
        FDBInterface extern FILE* g_pf_log_file;
        #define FDB_LOG_FILE g_pf_log_file
    #else
        #define FDB_LOG_FILE stdout
    #endif

    #define FDBLOG(logfile,format) \
        if(logfile)  \
        {       \
            time_t s_now; \
            time(&s_now); \
            struct tm* s_now_info; \
            s_now_info = localtime(&s_now); \
            fprintf(logfile,"\r\n\r\ntime:%s,file:%s,function:%s,line:%d|" format "\r\n\r\n",asctime(s_now_info),__FILE__,__FUNCTION__,__LINE__); \
            fflush(logfile); \
        }
    #define FDBLOG_ONE_PARAM(logfile,format,param1) \
        if(logfile)  \
        {       \
            time_t s_now; \
            time(&s_now); \
            struct tm* s_now_info; \
            s_now_info = localtime(&s_now); \
            fprintf(logfile,"\r\n\r\ntime:%s,file:%s,function:%s,line:%d|" format "\r\n\r\n",asctime(s_now_info),__FILE__,__FUNCTION__,__LINE__,param1); \
            fflush(logfile); \
        }
    #define FDBLOG_TWO_PARAM(logfile,format,param1,param2) \
        if(logfile)  \
        {       \
            time_t s_now; \
            time(&s_now); \
            struct tm* s_now_info; \
            s_now_info = localtime(&s_now); \
            fprintf(logfile,"\r\n\r\ntime:%s,file:%s,function:%s,line:%d|" format "\r\n\r\n",asctime(s_now_info),__FILE__,__FUNCTION__,__LINE__,param1,param2); \
            fflush(logfile); \
        }
    #define FDBLOG_THREE_PARAM(logfile,format,param1,param2,param3) \
        if(logfile)  \
        {       \
            time_t s_now; \
            time(&s_now); \
            struct tm* s_now_info; \
            s_now_info = localtime(&s_now); \
            fprintf(logfile,"\r\n\r\ntime:%s,file:%s,function:%s,line:%d|" format "\r\n\r\n",asctime(s_now_info),__FILE__,__FUNCTION__,__LINE__,param1,param2,param3); \
            fflush(logfile); \
        }
    #define FDBLOG_FOUR_PARAM(logfile,format,param1,param2,param3,param4) \
        if(logfile)  \
        {       \
            time_t s_now; \
            time(&s_now); \
            struct tm* s_now_info; \
            s_now_info = localtime(&s_now); \
            fprintf(logfile,"\r\n\r\ntime:%s,file:%s,function:%s,line:%d|" format "\r\n\r\n",asctime(s_now_info),__FILE__,__FUNCTION__,__LINE__,param1,param2,param3,param4); \
            fflush(logfile); \
        }
    #define FDBLOG_FIVE_PARAM(logfile,format,param1,param2,param3,param4,param5) \
        if(logfile)  \
        {       \
            time_t s_now; \
            time(&s_now); \
            struct tm* s_now_info; \
            s_now_info = localtime(&s_now); \
            fprintf(logfile,"\r\n\r\ntime:%s,file:%s,function:%s,line:%d|" format "\r\n\r\n",asctime(s_now_info),__FILE__,__FUNCTION__,__LINE__,param1,param2,param3,param4,param5); \
            fflush(logfile); \
        }
#else
    #define FDB_LOG_FILE

    #define FDBLOG(logfile,format)
    #define FDBLOG_ONE_PARAM(logfile,format,param1)
    #define FDBLOG_TWO_PARAM(logfile,format,param1,param2)
    #define FDBLOG_THREE_PARAM(logfile,format,param1,param2,param3)
    #define FDBLOG_FOUR_PARAM(logfile,format,param1,param2,param3,param4)
    #define FDBLOG_FIVE_PARAM(logfile,format,param1,param2,param3,param4,param5)
#endif

//supported thread minimum version
#define FDB_SUPPORT_POSIX_VERSION 200112L //IEEE 1003.1 2001
