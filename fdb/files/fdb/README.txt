﻿FDB -- Full Data Box Foudation Library

|--Requirement: Qt 
|--要求:Qt库

|--Best: Qt 4.8.4 + C++ 11 Complier
|--最佳环境:Qt 4.8.4库加上支持C++11的编译器(包括GCC,Clang,MSVC,ICC等...) 
|--要比较良好地观察此文档你可能需要至少1440*900分辨率的显示器和sublime text2

|--Component: 
	|--usb相关:usbInfo.h
	|--aes加密解密相关:aes.h
	|--验证License文件相关：verify.h
	|--解析加密license文件相关：utilities.h
|--组件

|--使用FDB注意事项：
	|--1.FDB中大部分的库在使用时，如果出错，程序并不会提示错误信息，而是直接退出程序，这样一来就为我们获取错误信息带来不便。通过#define __WRITE_ERROR_LOG__，当程序出错直接退出时，会在程序当前目录下生成一个日志文件FDBErrorLog.log，文件中有错误的信息，这样你就对出错原因一目了然了。

(标注*号的表示此文件为一类组件的入口头文件,命名空间为FDB)
|--File Hierarchy
	|--fdb
		|--fdb.h    一一一 |定义了一些常用的宏
		*|--usbInfo.h   一一一一一一一一一| API List API列表:
										  |	 QVector<quint16> mfVIDVector() const;   获取USB系统VID的vector
                                          |	 QVector<quint16> mfPIDVector() const；  获取USB系统PID的vector
                                          |  QVector<QString> mfSNVector() const；   获取USB系统Serial Number的vector	
                                          |  QVector<QString> mfDeviceDescVector() const；   获取USB系统设备描述符的vector
                                          |  QVector<QString> mfVendorName() const；   获取USB系统厂商名称的vector
                                          |
										  |	获取系统usb系统的入口文件
										  | 包含此头文件后得到FDB::USB::CUSBTier类的定义,
										  | 你需要创建一个实例才能访问当前系统USB设备的信息
										  | 如:
										  | FDB::CUSBTier oUSBTier;
										  | 当前适用于所有可用平台可获得的信息有VID,PID,SerialNumber
										  | 可用平台:Windows/Linux/Mac OS X
										  |
										  | **注意**:如果在Windows系统上需要获得usb设备的描述符信息
										  |          需要使用msvc编译器及在包含此头文件之前定义__GET_USB_DESC__
										  |			 #define __GET_USB_DESC__
										  |			 #include "../fdb/usbInfo.h"
										  | 获取的USB系统信息使用一系列的vector存储,如下图示:
										  |      			   	USB1            USB2              USB3
										  | VendorIDVector:    	1872			456				  8732
										  | ProductIDVector:   	763             32                323	
										  | SerialNumberVector: "Fulldata"      "software"        "jiayutec"
										  | 
										  | 代码示例:
										  | ----------C++:---------
										  | FDB::USB::CUSBTier oUSBTier;
										  | for(int i = 0;i<oUSBTier.mfVIDVector().size();++ i ) {
										  |		std::cout << "the " << i << "nd usb's vid: " << oUSBTier.mfVIDVector().at(i) << std::endl;
										  | 	std::cout << "the " << i << "nd usb's pid: " << oUSBTier.mfPIDVector().at(i) << std::endl;
										  |		std::cout << "the " << i << "nd usb's serial number: " << oUSBTier.mfSNVector().at(i) << std::endl;
										  | }
		|--cfgmgr32.h    一一一|
		|--usb100        一一一|
		|--usbioctl.h    一一一|
		|--usbiodef.h    一一一|
						       |
						       |
						       -------Microsoft Edition,这些文件在MSVC环境中才能使用,依靠
						       |      Q_CC_MSVC这个宏是否被定义可以进行条件编译
						       |		#ifdef Q_CC_MSVC
		|--cfgmgr32.lib  一一一|	    #endif
		|--setupapi.lib  一一一|
		|--usbVendorList.h  一一一一|   usb Vendor ID转为string的字典头文件
									| API List API列表:
							        |   QString fGetVendorString(quint16 uVendorID);
	   	
	   	*|--aes.h       一一一一一一一一一| API List API列表:
										  |	 1.QByteArray fEncryptAES128CBC(const QByteArray& baPlainText,const QByteArray& baKey
                             			  |	    	,long long int llPlainTextLength = 0); 
                             			  |  函数功能：AES加密函数   
                             			  |  参数说明：
                             			  |  baPlainText:输入参数，你需要加密的明文
                             			  |  baKey:加密需要的key
                             			  |  llPlainTextLength:默认为0，程序会使用baPlainText.size()获取明文长度
                                          |　返回值：加密后的密文 
                                          |  
                                          |  2.QByteArray fDecryptAES128CBC(const QByteArray& baCipherText,const QByteArray& baKey
                             			  |			,long long int llCipherTextLength = 0); 
                             			  |  函数功能：AES解密函数
                                          |  参数说明：
                             			  |  baCipherText:输入参数，你需要解密的密文
                             			  |  baKey:解密需要的key
                             			  |  llPlainTextLength:默认为0，程序会使用baCipherText.size()获取密文长度
                                          |　返回值：解密后的明文 
                                          |	 
                                          |  3.QByteArray fGetPublicAES128Key(const QByteArray& baFakeKey=QByteArray("ixoogdwdvriw"));
                                          |  函数功能: 获取公司的共有加密解密key，将ixoogdwdvriw进行运算后返回fulldatasoft。防止公司公有key从程序中泄露出去
										  |	 参数说明：
										  |  baFakeKey:默认为"ixoogdwdvriw",返回"fuldatasoft"
										  |
										  | 此头文件包含数据AES加密、解密的方法
										  | 包含此头文件后得到FDB::AES的定义
										  | 调用函数时需要在函数前面加上FDB::AES::
										  | 当前适用于所有可用平台可获得的信息有VID,PID,SerialNumber
										  | 可用平台:Windows/Linux/Mac OS X
										  |
										  | **注意**:使用此头文件需要加入openssl库，相关库文件可向相关同事索取
										  |          使用VS编译需在pro文件中加入：LIBS += -llibeay32MT
                                          |          使用MinGW编译需在pro文件中加入：LIBS += -lcrypto
										  | 
										  | 代码示例:
										  | ----------C++:---------
										  |  QByteArray oByteArray = FDB::AES::fEncryptAES128CBC(sUserInfo.toAscii(),oTrueKey.toAscii());
		

		*|--verify.h    一一一一一一一一一| API List API列表:
										  |	1.bool fVerifyLicense(const QString &strSoftwareReleaseTime 
                                          |     ,const QString &strLicensePath ,const QString& strProductName,const QString& strMatchLicenseVersion,QCoreApplication* pApp,const bool bAutoClose = true,const bool bAutoRegister = true,const int iLanguage = 0);
                                          | 函数功能：验证license文件的合法性
                                          | 参数说明：
                                          | strSoftwareReleaseTime:产品的发布日期，格式为"yyyy-mm-dd"
                                          | strLicensePath:License文件的路径，一般为当前路径，给"License.key"即可
                                          | strProductName:产品名称
     									  |	strMatchLicenseVersion:允许使用此版本软件的License版本集合，此版本对应的是软件的版本，比如客户的Lciense版本为1，而我们设定的版本集合为"1,2,3,4",则此客户可以使用软件，若客户的License类型为0，则不在版本集合范围内，就不能使用软件。此举是为了限制某些用户使用新版本软件。    
                                          | pApp：应用程序对象的地址。当使用Qt开发时，这个参数可以为你加载一个定时器用来规定用户的使用时间。没使用Qt则不理会此参数
                                          | 返回值：匹配到返回true,否则返回false 
                                          | bAutocClose:检测到失败后是否自动退出软件
                                          | bAutoRegister:检测到失败后是否自动弹出注册框
                                          | iLanguage:产品的语言，0为中文版，1为英文版
										  |	当我们的产品推向市场时，需要向用户收费，这时候会让用户使用我们提供的注册机注册软件，并生成一个License.key的文件，
										  | 注册机会将用户的U盘与软件绑定，如果用户不选择U盘注册，我们会自动与他的电脑硬件序列号绑定（硬盘、CPU、BIOS）, 
										  | License文件中会保存U盘(或者硬件序列号)的信息并且是加密的，用户只有在电脑上插入此U盘时(或者使用注册用的电脑)才
										  | 可以使用我们的软件。fVerifyLicense函数可以帮助你匹配License.key文件和电脑上的U盘(或者电脑的硬件序列号) 
										  |  ，以及其他相关信息，匹配上返回true,否则false
										  | 包含此头文件后得到FDB::Verify的定义
										  | 调用函数时需要在函数前面加上FDB::Verify::
										  | 可用平台:Windows/Linux/Mac OS X
										  |
										  | **注意**:使用此头文件需要加入openssl库，相关库文件可向相关同事索取
										  |          使用VS编译需在pro文件中加入：LIBS += -llibeay32MT
                                          |          使用MinGW编译需在pro文件中加入：LIBS += -lcrypto
										  | 
										  | 代码示例:
										  | ----------C++:---------
		                                  |  bool b = FDB::Verify::fVerifyLicense("2013-03-12","License.key","EliteiSCS"，"1,2,3,4",&app,true,true,0);

		*|--parse.h			一一一一一一一| API List API列表:
										  |	 1.QString fParsePeerMsg(const QString& strPeerMsg,const QString& strKey
                                          |             ,const QString& strKVFormat = QString("Key:Value|"));
                                          | 函数功能：从某一个键值对的格式提取出key对应的值,格式比如key:Value|。如从"age:18|length:175|sex:male|"中提取sex或者age等等对应的值
                                          | 
                                          | 参数说明：
                                          | strPeerMsg:输入参数，一串键值对
                                          | strKey:输入参数，需要查找的键
                                          | strKVFormat:键值对的格式，默认为"Key:Value|",可针对要提取的信息的格式自定义
                                          | 返回值：传入键对应的值 
                                          | 
                                          | 包含此头文件得到FDB::Parse的定义                                 
										  | 调用此函数时需要在函数前面加上FDB::Parse::         
										  | 
										  | 可用平台:Windows/Linux/Mac OS X
										  |
										  | 代码示例:
										  | ----------C++:---------
										  | QString strAge = FDB::Parse::fParsePeerMsg("age:18|length:175|sex:male|","age");				  			  

		*|--utilities.h		一一一一一一一| API List API列表:
										  |	1.QByteArray fParseEncryptedLicenseFile(const QString& strFilePath
                                          |    ,const QString& strBasicDecryptedKey = QString("ixoogdwdvriw") );
                                          | 函数功能：解析加密的license文件
                                          | 参数说明：
                                          | strFilePath:输入参数，需要解析的文件路径
                                          | strBasicDecryptedKey:输入参数，解析文件需要的Key,默认为"ixoogdwdvriw"
                                          | 返回值：解析后的明文
                                          | 2.QString fPickupLicenseInfo(const QString& strFilePath,const QString& strKey
                                          |    ,const QString& strBasicDecryptedKey = QString("ixoogdwdvriw"));
                                          | 函数功能：从加密的文件中的键值串中提取key对应的值。本函数内部调用了fParseEncryptedLicenseFile和verify.
                                          | h头文件中的fVerifyLicense两个函数
                                          | 参数说明：
                                          | strFilePath:输入参数，加密文件的路径
                                          | strKey:输入参数，需要查找的键
                                          | strBasicDecryptedKey:输入参数，解析文件需要的Key,默认为"ixoogdwdvriw"
                                          | 返回值：key对应的值
                                          |	3. bool fIsLeapYear(const int& iYear);
                                          | 函数功能：判断传入的参数年份是否为闰年
                                          | 参数说明：
                                          | iYear:输入参数，需要判断的年份
                                          | 返回值：是闰年返回true，否则返回false
                                          | 包含此头文件得到FDB::Utilities的定义                                 
										  | 调用此函数时需要在函数前面加上FDB::Utilities::  
										  | 可用平台:Windows/Linux/Mac OS X									
										  | 
										  | 代码示例:
										  | ----------C++:---------
										  | 1.QByteArray baPlaintext = FDB::Utilities::fParseEncryptedLicenseFile("License.key");
										  | 2.QString strValue = FDB::Utilities::fPickupLicenseInfo("License.key","age");

		*|--IOCircleSectorCache.h	一一一一一一一| API List API列表:
												  |	1.QByteArray mfReadSector(const qint64& i64StartSector,const int& iSectorNumberToRead);
		                                          | 函数功能：从缓冲中读取iSectorNumberToRead*扇区大小个字节的数据
		                                          | 参数说明：
		                                          | i64StartSector:读取数据的开始扇区
		                                          | iSectorNumberToRead:读多少个扇区
		                                          | 返回值：读取的数据
		                                          |	
		                                          | 包含此头文件得到FDB::IOKit::CIOCircleSectorCache::的定义                                 
												  | 调用函数时需要在函数前面加上FDB::IOKit::CIOCircleSectorCache::
												  | 可用平台:Windows/Linux/Mac OS X									
												  | 
												  | 代码示例:
												  | ----------C++:---------
												  | FDB::IOKit::CIOCircleSectorCache oIOCircleSectorCache(QString("\\\\.\\PhysicalDrive0"));
    											  |		for(qint64 i64=0;i64<0x9999999;i64+=512) {
        										  |			QByteArray ba = oIOCircleSectorCache.mfReadSector(i64,512);
    											  |		}

		*|--DriveInfo.h   一一一一一一一一| API List API列表：
							  		      | 
							  		      |	此头文件包含获取驱动器信息的方法
							  		      |	

		*|--MasterHDInfo.h  一一一一一| API List API列表：
									  | 1.FDB::IOKit::CMasterHardDiskSerial类的定义
									  | 功能：得到CMasterHardDiskSerial类的定义，获取主硬盘的序列号，可用于实例化
									  | 可用平台: Windows
									  | 代码示例：
									  | ---------------------C++:------------------
									  |  FDB::IOKit::CMasterHardDiskSerial o;
									  |  std::string strHDSN = o.GetSerialNo();
									  |  std::cout << strHDSN << std::endl;

		*|DriveReadWrite.h	 一一一一一一一一一一一| API List API列表：
		                                   		   |
		                                   		   | 此头文件包含读写驱动器的方法
                                           		   |

		*|DriveSpeedTest.h   一一一一一一一| API List API列表：
		                                   |
		                                   | 此头文件包含测试设备读取速度的方法
		|--README.txt     一一一|it's me

version:0.2.0     2013-3-15 took
version:0.2.1 	  2013-3-26 shawhen ： 增加对MasterHDInfo.h的说明
