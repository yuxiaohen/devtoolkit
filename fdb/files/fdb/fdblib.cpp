#include "fdblib.h"

#include <stdio.h>

#if defined(LOG_TO_FILE)
    #if defined(__cplusplus)
        FILE* g_pf_log_file = fopen("fdblog.log","a+");
    #else
        FILE* gpfLogFile = NULL;
    #endif
#endif
