#pragma once

#include "Updaterlib.h"

#include "../../fdbPrimitive/Type/int.h"

#include <QObject>

class FDBInterface FDBCUpdater : public QObject {
    Q_OBJECT
private:
    QString m_str_product_name;
    QString m_str_product_type;
    QString m_str_product_language;
    int32_t m_i32_verify_method;
    int m_i_alerted;
public:
    FDBCUpdater(const char* pc_product_name
                ,int32_t i32_product_type
                ,int32_t i32_product_language
                ,int32_t i32_product_verify_method);
public slots:
    void mfslCheckUpdate();
};
