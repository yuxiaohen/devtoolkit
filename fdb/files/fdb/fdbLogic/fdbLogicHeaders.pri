HEADERS += \
    $$PWD/fdbLogic.h \
    $$PWD/fdbLogiclib.h \
    $$PWD/Qt/FDBCWizard.h \
    $$PWD/Qt/FDBCWizardPage.h

#Verify
include($$PWD/Verify/VerifyHeaders.pri)
#Register
include($$PWD/Register/RegisterHeaders.pri)
#Network Logic
#include($$PWD/NetworkLogic/NetworkLogic.pri)
#Updater
#include($$PWD/Updater/UpdaterHeaders.pri)
