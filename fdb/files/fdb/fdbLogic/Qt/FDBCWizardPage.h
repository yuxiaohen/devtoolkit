#pragma once

#include <QWidget>

#include "FDBCWizard.h"

class FDBInterface FDBCWizardPage : public QWidget {
    friend class FDBCWizard;
    Q_OBJECT
protected:
    FDBCWizard* m_p_wizard; //wizard管理，这样使得page可以呼叫wizard进行某些行为
    bool m_inited;
public:
    FDBCWizardPage(QWidget* parent = nullptr)
    {
        this->m_p_wizard = nullptr;
        this->m_inited = false;
    }

    void setWizard(FDBCWizard* p_wizard)
    {
        this->m_p_wizard = p_wizard;
    }
    virtual void init() = 0;
    virtual void enter() {};
    virtual void leave() {};
signals:
    void sigGotoPage(const QString target_name_qstring
                     ,const bool remember_target
                     ,const bool register_target_in_map
                     ,const bool reinit_target
                     ,const bool reset_target
                     ,const FDBCWizard::FDBEWizardTrasaction e_transaction);
protected slots:
    void slGotoPage(const QString target_name_qstring
                  , const bool remember_target = false
                  , const bool register_target_in_map = false
                  , const bool reinit_target = true
                  , const bool reset_target = false
                  , const FDBCWizard::FDBEWizardTrasaction e_transaction = FDBCWizard::FDBEWizardTrasaction_None);
};
