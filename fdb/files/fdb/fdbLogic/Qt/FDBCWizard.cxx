﻿#pragma once

#include "FDBCWizard.h"
#include "FDBCwizardPage.h"

#include <fdb/fdbglobal.h>

QWidget* FDBCWizard::slGotoPage(const QString target_name_qstring
                                , const bool remember_target
                                , const bool register_target_in_map
                                , const bool reinit_target
                                , const bool reset_target
                                , const FDBEWizardTrasaction e_transaction)
{
    //获取目标页面
    string target_name = target_name_qstring.toStdString();
    FDBCWizardPage* target = nullptr;
    auto target_register_ite = this->m_created_pages.find(target_name);
    bool b_should_init = reinit_target;
    if(reset_target || target_register_ite == this->m_created_pages.end())
    {
        if(target_register_ite != this->m_created_pages.end())
        {
            FDBCWizardPage* page = target_register_ite->second;
            delete page;
        }
        target = qobject_cast<FDBCWizardPage*>(F_USE_REFLECT(target_name.data()));
        if(target == nullptr)
        {
            return (nullptr);
        }
        target->setWizard(this);
        if(this->m_wizard_stage->layout() == nullptr)
        {
            this->m_wizard_stage->setLayout(new QHBoxLayout());
        }
        this->m_wizard_stage->layout()->addWidget(target);
        QObject::connect( target,&FDBCWizardPage::sigGotoPage
                          ,this,&FDBCWizard::slGotoPage );
    } else {
        target = target_register_ite->second;
        b_should_init = 0;
    }
    //路线跟踪
    if(remember_target)
    {
        this->m_walk_widgets.push(target);
    }
    //创建跟踪
    if(register_target_in_map)
    {
        this->m_created_pages.erase(target_name);
        this->m_created_pages.insert(make_pair(target_name,target));
    }
    //隐藏当前,设置新的当前并展示
    if(this->m_current == nullptr)
    {
        this->m_current = target;
    }
    this->m_current->leave();
    this->m_current->setVisible(false);
    this->m_current = target;
    this->m_current->enter();
    this->m_current->setVisible(true);
    emit sigPageShowed(target_name_qstring);
    if(b_should_init)
    {
        target->init();
        target->m_inited = true;
    }

    qDebug() << "goto" << target_name_qstring << "over";
    return (target);
}

QWidget* FDBCWizard::slCurrentWidget()
{
    return (this->m_current);
}
