﻿#pragma once

#include <QAbstractItemModel>
#include <QDebug>

#ifndef Q_MOC_RUN
    #include <boost/shared_ptr.hpp>
#endif

#include <fdb/fdbPrimitive/Type/int.h>

#define __FDB_MODEL_PARENT_INDEX_COLUMN_NUMBER__ 9999

template<typename _Tchildren>
class FDBCAbstructItem {
private:
    std::vector< std::vector<_Tchildren*>* >* child_items;
    _Tchildren* parent_item;
    int i_column_num;

public:
    FDBCAbstructItem(_Tchildren* p_parent)
    {
        this->child_items = new std::vector< std::vector<_Tchildren*>* >();
        this->i_column_num = 0;
//        this->row_items = new std::vector<_Tchildren*>();
        this->parent_item = p_parent;
    }
    /*!
     * @brief 添加一行
     */
    void append_row(_Tchildren* p_child_item)
    {
        p_child_item->parent_item = static_cast<_Tchildren*>(this);
        std::vector<_Tchildren*>* p_vec_row = new std::vector<_Tchildren*>();
        p_vec_row->push_back( p_child_item );
        this->child_items->push_back( p_vec_row );
        this->i_column_num < 1 ? this->i_column_num = 1 : this->i_column_num;
    }
    void append_row(const std::vector<_Tchildren*>* p_vec_row)
    {
        std::vector<_Tchildren*>* p_vec_row_ = new std::vector<_Tchildren*>();
        for(int i=0;i < p_vec_row->size();++ i)
        {
            _Tchildren* p_child = p_vec_row->at(i);
            p_child->parent_item = static_cast<_Tchildren*>(this);
            p_vec_row_->push_back( p_child );
        }
        this->child_items->push_back( p_vec_row_ );
        if(p_vec_row->size() > this->i_column_num)
        {
            this->i_column_num = p_vec_row_->size();
        }
    }

    /*!
     * @brief 根据子结点的编号得到子节点
     * @param i_child_number 子节点编号
     * @return 指向子节点的指针
     */
    _Tchildren* childByNum(uint64_t u64_row_num,uint64_t u64_column_num = 0)
    {
        if(u64_row_num < this->child_items->size())
        {
            std::vector<_Tchildren*>* p_vec_row = this->child_items->at(u64_row_num);
            if(u64_column_num < p_vec_row->size())
            {
                return (p_vec_row->at(u64_column_num));
            }
        }
    }
    /*!
     * @brief 得到此节点的父亲结点
     */
    _Tchildren* parentItem() { return (this->parent_item); }
    /*!
     * @brief 得到此节点作为子节点时属于父节点的第几个子节点(行数)
     */
    int64_t act_as_child_of_number()
    {
        _Tchildren* p_parent_item = this->parent_item;
        for( int64_t i64_row= 0;i64_row < this->parent_item->child_items->size();++ i64_row )
        {
            std::vector<_Tchildren*>* p_vec_row = this->parent_item->child_items->at(i64_row);
            for(int64_t i64_column =0;i64_column < p_vec_row->size();++ i64_column)
            {
                if( p_vec_row->at(i64_column) == this )
                {
                    return (i64_row);
                }
            }
        }
        return (-1);
    }
    /*!
     * @brief 得到此节点下的子节点的个数
     * @return 此节点下的子节点个数
     */
    uint64_t row_count() { return (this->child_items->size()); }

    /*!
     * @brief 得到列数
     */
    uint64_t column_count() { return (this->i_column_num); }
};

template<typename _Titem>
class FDBCAbstractItemModel : public QAbstractItemModel {
protected:
    _Titem* m_p_root_item;
    QHash<int,QByteArray> m_obj_model_roles;
public:
    FDBCAbstractItemModel()
    {
        this->m_p_root_item = new _Titem(NULL);
    }

    void setRoleNames(const QHash<int,QByteArray>& ref_roles) { this->m_obj_model_roles = ref_roles; }
    /*!
     * @brief返回quick viewer的delegate的model角色名
     */
    virtual QHash<int,QByteArray> roleNames() const
    {
        return (this->m_obj_model_roles);
    }
    /*!
     * @brief返回隐藏的根节点
     */
    _Titem* rootItem()
    {
        return (this->m_p_root_item);
    }

    /*!
     * @brief 返回row,column的index,根据parent得到其internalPointer,创建一个子的index
     */
    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const
    {
        _Titem* p_parent_item = NULL;
        _Titem* p_child_item = NULL;

        if(hasIndex(row,column,parent) == false)
        {
            return (QModelIndex());
        }

        if(parent.isValid() == false)
        { //父亲索引不可用,隐藏的根节点
            p_parent_item = static_cast<_Titem*>(this->m_p_root_item);
        } else {
            p_parent_item = static_cast<_Titem*>(parent.internalPointer());
        }
        p_child_item = static_cast<_Titem*>(p_parent_item->childByNum(row,column));
        if(p_child_item) //确实找到了parent的row,column的子节点
        {
            return ( createIndex(row,column,p_child_item) );
        } else {
            return (QModelIndex());
        }

        return (QModelIndex());
    }
    virtual QModelIndex parent(const QModelIndex &child) const
    {
        if(child.isValid() == false)
        {
            return (QModelIndex());
        }

        _Titem* p_child_item = static_cast<_Titem*>(child.internalPointer());
        _Titem* p_parent_item = static_cast<_Titem*>(p_child_item->parentItem());
        if(p_parent_item == this->m_p_root_item) //隐藏的根节点
        {
            return (QModelIndex());
        } else {
            return ( createIndex(p_parent_item->act_as_child_of_number(),__FDB_MODEL_PARENT_INDEX_COLUMN_NUMBER__,p_parent_item) );
        }
    }
    //parent的row count
    virtual int rowCount(const QModelIndex &parent) const
    {
        if(parent.isValid() == false) //父亲是无用
        {
            int row_count = this->m_p_root_item->row_count();
            // qDebug() << "hidden root item's row count:" << row_count;
            return (row_count);
        }
        int i_ret = 0;
        _Titem* p_item = NULL;

        p_item = static_cast<_Titem*>( parent.internalPointer() );
        if(p_item != NULL)
        {
            i_ret = p_item->row_count();
        } else {
            i_ret = this->m_p_root_item->row_count();
        }

        // qDebug() << "model row count: " << i_ret;

        return (i_ret);
    }
    //返回parent的列数
    virtual int columnCount(const QModelIndex &parent) const
    {
        if(parent.isValid() == false) //父亲是无用
        {
            return (this->m_p_root_item->column_count());
        }
        _Titem* p_item = NULL;
        p_item = static_cast<_Titem*>( parent.internalPointer() );
        if(p_item != NULL)
        {
            return (p_item->column_count());
        } else {
            return (this->m_p_root_item->column_count());
        }
    }
};
