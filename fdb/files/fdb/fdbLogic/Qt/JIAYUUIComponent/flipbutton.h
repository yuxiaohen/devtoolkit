#ifndef FLIPBUTTON_H
#define FLIPBUTTON_H

#include <QWidget>
#include <QPropertyAnimation>

#include <fdb/fdblib.h>

#include "ui_flipbutton.h"

namespace Ui {
class FlipButton;
}

class FDBInterface FlipButton : public QWidget
{
    Q_OBJECT

public:
    explicit FlipButton(QWidget *parent = 0):
        QWidget(parent),
        ui(new Ui::FlipButton)
    {
        ui->setupUi(this);
    }
    ~FlipButton()
    {
        delete ui;
    }

    typedef enum EFlipButtonSurface {
        EFlipButtonSurface_positive
        ,EFlipButtonSurface_negative
    } EFlipButtonSurface;
    typedef enum EFlipTransaction {
        EFlipTransaction_none //无转场
        ,EFlipTransaction_left //向左滑动出场
    } EFlipTransaction;

//    void flip(EFlipButtonSurface e_surface,EFlipTransaction e_transaction,const QEasingCurve& easing);
//    void setPositiveSurface(QWidget* p_obj_widget);
//    void setNegativeSurface(QWidget* p_obj_widget);

private:
    Ui::FlipButton *ui;
    EFlipButtonSurface m_e_current_surface;

    QWidget* m_p_obj_positive_widget;
    QWidget* m_p_obj_negative_widget;

    QPropertyAnimation* m_p_obj_transaction_animation;

public:
//    void runFromApp();

protected:
//    virtual void enterEvent(QEvent*);
//    virtual void leaveEvent(QEvent*);
//    virtual void resizeEvent(QResizeEvent*);

signals:
    /*!
     * @brief 发送的point是光标的全局位置
     */
    void mouseEnter(const QPoint&);
    void mouseLeave(const QPoint&);
};

#endif // FLIPBUTTON_H
