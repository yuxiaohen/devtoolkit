#ifndef DRAGTARGETCONTAINER_H
#define DRAGTARGETCONTAINER_H

#include <QWidget>

#include <fdb/fdblib.h>

namespace Ui {
class DragTargetContainer;
}

class FDBInterface DragTargetContainer : public QWidget
{
    Q_OBJECT
private:
    QWidget* m_target;
    bool m_presseding; //鼠标正被按着
    bool m_double_clk_then_max;
    QPoint m_snap_pos;
public:
    explicit DragTargetContainer(QWidget *parent = 0);
    ~DragTargetContainer();
    void setTarget(QWidget* target);
    void setDoubleClkMax(bool max);

private:
    Ui::DragTargetContainer *ui;
protected:
    virtual void mouseMoveEvent(QMouseEvent* e);
    virtual void mouseDoubleClickEvent(QMouseEvent* e);
    virtual void mousePressEvent(QMouseEvent* e);
    virtual void mouseReleaseEvent(QMouseEvent* e);
};

#endif // DRAGTARGETCONTAINER_H
