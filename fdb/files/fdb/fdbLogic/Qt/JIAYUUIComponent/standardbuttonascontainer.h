#ifndef STANDARDBUTTONASCONTAINER_H
#define STANDARDBUTTONASCONTAINER_H

#include <QPushButton>

#include <fdb/fdblib.h>

class FDBInterface StandardButtonAsContainer : public QPushButton
{
    Q_OBJECT

public:
    explicit StandardButtonAsContainer(QWidget *parent = 0);
    ~StandardButtonAsContainer();

private:
};

#endif // STANDARDBUTTONASCONTAINER_H
