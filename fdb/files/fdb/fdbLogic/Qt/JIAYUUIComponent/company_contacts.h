﻿#ifndef COMPANY_CONTACTS_H
#define COMPANY_CONTACTS_H

#include <QWidget>
#include <QDesktopServices>
#include <QUrl>

#include <QDebug>

#include <fdb/fdblib.h>

namespace Ui {
class CompanyContacts;
}

class FDBInterface CompanyContacts : public QWidget
{
    Q_OBJECT

public:
    explicit CompanyContacts(QWidget *parent = NULL);
    ~CompanyContacts();

private:
    Ui::CompanyContacts* ui;
protected slots:
    void mfslVisitWebsite();
    void mfslTalkQQ();
};

#endif // COMPANY_CONTACTS_H
