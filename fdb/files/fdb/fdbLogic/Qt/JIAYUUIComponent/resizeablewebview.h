#ifndef RESIZEABLEWEBVIEW_H
#define RESIZEABLEWEBVIEW_H

#include <QWidget>
#include <QWebView>
#include <QWebFrame>

#include <fdb/fdblib.h>

#include <map>
using namespace std;

namespace Ui {
class ResizeableWebView;
}

class FDBInterface ResizeableWebView : public QWidget
{
    Q_OBJECT

public:
    explicit ResizeableWebView(QWidget *parent = 0);
    ~ResizeableWebView();
    QWebView* internalView();
    void addJSObject(const QString& name,QObject* object);

private:
    Ui::ResizeableWebView *ui;
    map<const QString,QObject*> m_js_objects;

private slots:
    void slAddJSObj();
    void slNewFrame(QWebFrame* frame);
    void slResizeViewer_ui_thread(const QString size);
signals:
    void sigResizeViewer(const QString size);
public slots:
    void slResizeViewer(const QString size);
};

#endif // RESIZEABLEWEBVIEW_H
