﻿#pragma once

#include <vector>
#include <map>
#include <stack>

#include <QWidget>
#include <QLayout>
#include <QJsonObject>
#include <QMessageBox>
#include <QDebug>

#include "../../fdblib.h"

#include <stack>
#include <map>
using namespace std;

class FDBCWizardPage;
class FDBInterface FDBCWizard : public QObject {
    Q_OBJECT
public:
    typedef enum FDBEWizardPageType {
        FDBEWizardPageType_invalid = 0xffffff10
        ,FDBEWizardPageType_previous = 0xffffff11
        ,FDBEWizardPageType_next
        ,FDBEWizardPageType_home
    } FDBEWizardPageType;
    typedef enum FDBEWizardTrasaction {
        FDBEWizardTrasaction_None
    } FDBEWizardTrasaction;
private:
    QWidget* m_wizard_stage;
    stack<FDBCWizardPage*> m_walk_widgets; //一路走过的控件路线
    FDBCWizardPage* m_current;
    map<string,FDBCWizardPage*> m_created_pages;
    int m_i_come_from; /*用户多路分派 =>->= 可以让分派器知道从哪来*/

    QJsonObject m_msgs;

    QMutex m_o_goto_page_locker;
public:
    FDBCWizard(QWidget* p_wizard_stage)
    {
        this->m_current = nullptr;
        this->m_wizard_stage = p_wizard_stage;
    }
    ~FDBCWizard()
    {

    }
    QJsonObject& msgs()
    {
        return (this->m_msgs);
    }
signals:
    void sigPageShowed(const QString page_name);
public slots:
    /*!
     * @brief 线程安全,但不可重入
     * @param remember_target 是否在控件路线中记住此控件(就算此为假,但在编号对应控件的散列表中还是会记录)
     * @param register_target_in_map 是否在编号<->控件散列表中记住此控件
     * @param b_reset_target 是否重置此编号对应的控件,即就算在编号<->控件散列表中记录了此控件,也给我重新生成
     * @param e_transaction 转场...现在为止,还未提供任何转场
     */
    QWidget* slGotoPage(const QString target_name_qstring
                  ,const bool remember_target = true
                  ,const bool register_target_in_map = true
                  ,const bool reinit_target = true
                  ,const bool reset_target = false
                  ,const FDBEWizardTrasaction e_transaction = FDBEWizardTrasaction_None);

    QWidget* slCurrentWidget();
};
