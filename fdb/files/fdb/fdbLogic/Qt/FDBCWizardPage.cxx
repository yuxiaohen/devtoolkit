#include "FDBCwizardPage.h"

#include <assert.h>

void FDBCWizardPage::slGotoPage(const QString target_name_qstring
                                ,const bool remember_target
                                ,const bool register_target_in_map
                                ,const bool reinit_target
                                ,const bool reset_target
                                ,const FDBCWizard::FDBEWizardTrasaction e_transaction)
{
    assert(this->m_p_wizard);
    emit sigGotoPage(target_name_qstring
                     ,remember_target,register_target_in_map
                     ,reinit_target,reset_target
                     ,e_transaction);
}
