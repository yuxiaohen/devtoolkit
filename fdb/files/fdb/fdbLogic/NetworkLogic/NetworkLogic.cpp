﻿#include "NetworkLogic.h"

#include <fdb/fdbPrimitive/Compress/Compress.h>
#include <fdb/fdbPrimitive/Crypto/Crypto.h>
#include <fdb/fdbPrimitive/Math/Math.h>

#ifndef Q_MOC_RUN
    #include <boost/bind.hpp>
#endif

boost::shared_ptr<std::vector<char> >
FDBfNetworkLogic_host_instruction_to_le_buffer(FDBSInstruction* p_s_host_instruction)
{
    boost::shared_ptr<std::vector<char> > sp_le_buffer(new std::vector<char>(__FDB_COMMUNICATION_INSTRUCTION_LEN__,'\x00'));
    if( ENDIANNESS == 'l' ) {
        memcpy(sp_le_buffer->data(),(void*)p_s_host_instruction,__FDB_COMMUNICATION_INSTRUCTION_LEN__);
    } else {
        std::runtime_error("this is big endian cpu,unsupport now");
    }
    return (sp_le_buffer);
}

boost::shared_ptr<FDBSInstruction>
FDBfNetworkLogic_le_buffer_to_host_instruction(const void* p_le_buffer)
{
    boost::shared_ptr<FDBSInstruction> sp_instruction( new FDBSInstruction() );
    if(ENDIANNESS == 'l') { //host littel_endian
       memcpy( sp_instruction.get(),p_le_buffer,__FDB_COMMUNICATION_INSTRUCTION_LEN__ );
    } else {//host big endian
        memcpy(sp_instruction->pc_addon,p_le_buffer,__FDB_COMMUNICATION_INSTRUCTION_ADDON_LEN__);
        sp_instruction->u8_general_switch = ((uint8_t*)p_le_buffer)[__FDB_COMMUNICATION_INSTRUCTION_ADDON_LEN__
                                                        +sizeof(sp_instruction->p_reserved)];
        FDBfMath_le_array_to_host_value((uint8_t*)p_le_buffer-sizeof(uint32_t),4,&sp_instruction->u32_instruction_data_size);
    }

    return (sp_instruction);
}

std::pair<boost::shared_ptr<std::vector<char> >,boost::shared_ptr<std::vector<char> > >
FDBfNetworkLogic_produce_send(FDBSInstruction* p_s_instrcution
                              , const void *p_instruction_content
                              , const uint32_t u32_instruction_content_len
                              , const uint8_t u8_general_switch/* = __FDB_COMMUNICATION_INSTRUCTION_DATA_COMPRESSED__*/)
{
    //压缩指令数据
    boost::shared_ptr<std::vector<char> > sp_compressed_instruction_data_buffer
            = FDBfCompress_compress_buffer(p_instruction_content
                                           ,u32_instruction_content_len
                                           ,9);
    //补充指令的压缩标志及数据长度
    p_s_instrcution->u8_general_switch |= u8_general_switch;
    p_s_instrcution->u32_instruction_data_size = sp_compressed_instruction_data_buffer->size();
    boost::shared_ptr<std::vector<char> > sp_instruction_buffer = FDBfNetworkLogic_host_instruction_to_le_buffer(p_s_instrcution);

    std::pair<boost::shared_ptr<std::vector<char> >,boost::shared_ptr<std::vector<char> > > pair_result
            = std::pair<boost::shared_ptr<std::vector<char> >,boost::shared_ptr<std::vector<char> > >(
                sp_instruction_buffer,sp_compressed_instruction_data_buffer);

    return (pair_result);
}

//! [1]
std::pair<boost::shared_ptr<FDBSInstruction>,boost::shared_ptr<std::vector<char> > >
FDBfNetworkLogic_restore_recv( std::pair<boost::shared_ptr<FDBSInstruction>
                               ,boost::shared_ptr<std::vector<char> > > sp_io_result)
{
    boost::shared_ptr<std::vector<char> > sp_origin_instruction_data_buffer = sp_io_result.second;
    if( sp_io_result.first->u8_general_switch & __FDB_COMMUNICATION_INSTRUCTION_DATA_COMPRESSED__ )
    {
        //对指令携带数据解压缩
        sp_origin_instruction_data_buffer
                = FDBfCompress_uncompress_buffer(sp_io_result.second->data(),sp_io_result.second->size());
    }
    return ( std::pair<boost::shared_ptr<FDBSInstruction>,boost::shared_ptr<std::vector<char> > >(
                 sp_io_result.first,sp_origin_instruction_data_buffer) );
}
//! [2]
std::pair<boost::shared_ptr<FDBSInstruction>,boost::shared_ptr<std::vector<char> > >
FDBfNetworkLogic_restore_recv(const void* p_buffer_from_remote_host,const uint32_t u32_buffer_len)
{
    boost::shared_ptr<FDBSInstruction> sp_instruction
            = FDBfNetworkLogic_le_buffer_to_host_instruction(p_buffer_from_remote_host);
    boost::shared_ptr<std::vector<char> > sp_origin_instruction_data_buffer;
    if( sp_instruction->u8_general_switch & __FDB_COMMUNICATION_INSTRUCTION_DATA_COMPRESSED__ )
    {
        //对指令携带数据解压缩
        sp_origin_instruction_data_buffer
                = FDBfCompress_uncompress_buffer((const char*)p_buffer_from_remote_host+__FDB_COMMUNICATION_INSTRUCTION_LEN__
                                                 ,u32_buffer_len-__FDB_COMMUNICATION_INSTRUCTION_LEN__);
    }
    return ( std::pair<boost::shared_ptr<FDBSInstruction>,boost::shared_ptr<std::vector<char> > >(
                 sp_instruction,sp_origin_instruction_data_buffer) );
}

void
FDBfNetworkLogic_recv_instruction_handler(boost::shared_ptr<std::vector<char> > sp_instruction_le_buffer
                                          , const int b_auto_unpack_instruction_data
                                          , boost::shared_ptr<boost::asio::ip::tcp::socket> sp_peer_socket
                                          , bfuncIOHandler_t io_handler
                                          , boost::system::error_code ec, size_t sz_bytes_transferred)
{
    if( ec )
    {
        printf("error occurred:%s\r\n",ec.message().data());
        return ;
    }
    if( sz_bytes_transferred != __FDB_COMMUNICATION_INSTRUCTION_LEN__  )
    {
        printf("transferred error\r\n");
        return ;
    }
//    printf("recved instruction(%d bytes:%s)\r\n",sz_bytes_transferred,sp_instruction_le_buffer->data());
    boost::shared_ptr<FDBSInstruction> sp_instruction
            = FDBfNetworkLogic_le_buffer_to_host_instruction(sp_instruction_le_buffer->data());
//    printf( "instruction data len: %d,switch: %d\r\n"
//            ,sp_instruction->u32_instruction_data_size
//            ,sp_instruction->u8_general_switch );

    boost::shared_ptr<std::vector<char> > sp_packed_instruction_data(
                new std::vector<char>(sp_instruction->u32_instruction_data_size,'\x00') );

    boost::asio::async_read(*sp_peer_socket
                            ,boost::asio::buffer(sp_packed_instruction_data->data(),sp_packed_instruction_data->size())
                            ,boost::bind(&FDBfNetworkLogic_recv_instruction_data_handler
                                         ,sp_instruction,sp_packed_instruction_data,b_auto_unpack_instruction_data,io_handler
                                         ,_1,_2) );

}

void
FDBfNetworkLogic_recv_instruction_data_handler(boost::shared_ptr<FDBSInstruction> sp_instruction
                                               ,boost::shared_ptr<std::vector<char> > sp_packed_instruction_data
                                               ,int b_auto_unpack_instruction_data
                                               ,bfuncIOHandler_t io_handler
                                               ,boost::system::error_code ec
                                               ,size_t sz_bytes_transferred)
{
    if(ec)
    {
        printf("recv instruction data failed\r\n");
        return ;
    }
    if(b_auto_unpack_instruction_data)
    {
        std::pair<boost::shared_ptr<FDBSInstruction>,boost::shared_ptr<std::vector<char> > > pair_instruction_with_unpacked_data
                = FDBfNetworkLogic_restore_recv(std::pair<boost::shared_ptr<FDBSInstruction>
                                                        ,boost::shared_ptr<std::vector<char> > >(sp_instruction,sp_packed_instruction_data));

        io_handler(pair_instruction_with_unpacked_data);
    } else {
        io_handler(std::pair<boost::shared_ptr<FDBSInstruction>,boost::shared_ptr<std::vector<char> > >(sp_instruction,sp_packed_instruction_data));
    }
}

void
FDBfNetworkLogic_send_data_handler_to_recv_next_instruction(std::pair<boost::shared_ptr<std::vector<char> >
                                                                    , boost::shared_ptr<std::vector<char> > > pair_io_data
                                   , boost::shared_ptr<boost::asio::ip::tcp::socket> sp_socket
                                   , int b_auto_unpack_instruction_data
                                   , bfuncIOHandler_t io_handler
                                   , boost::system::error_code ec
                                   , size_t sz_bytes_transferred)
{
    if( ec )
    {
        printf("error ocurred when send data\r\n");
        return ;
    }

//    printf("send %d bytes\r\n",sz_bytes_transferred);
    //控制流转向接收指令
    boost::shared_ptr<std::vector<char> > sp_instruction_le_buffer(new std::vector<char>(sizeof(FDBSInstruction),'\x00'));
    boost::asio::async_read(*sp_socket
                            ,boost::asio::buffer(sp_instruction_le_buffer->data(),sp_instruction_le_buffer->size())
                            ,boost::bind(&FDBfNetworkLogic_recv_instruction_handler
                                         ,sp_instruction_le_buffer
                                         ,b_auto_unpack_instruction_data
                                         ,sp_socket
                                         ,io_handler
                                         ,_1,_2));
}

void
FDBfNetworkLogic_send_data_handler_dummy(std::pair<boost::shared_ptr<std::vector<char> >
                                                , boost::shared_ptr<std::vector<char> > > pair_io_data
                                         , boost::system::error_code ec
                                         , size_t sz_bytes_transferred)
{
    if( ec )
    {
        printf("error ocurred when send data\r\n");
        return ;
    }
//    printf("send %d bytes\r\n",sz_bytes_transferred);
}
