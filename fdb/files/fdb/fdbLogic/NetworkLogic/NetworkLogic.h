﻿#ifndef __fdb_logic_network_h_2013_11_07_01_01__
#define __fdb_logic_network_h_2013_11_07_01_01__

#include "NetworkLogiclib.h"

#include "../../fdbPrimitive/Type/Type.h"

#include <utility>

#ifndef Q_MOC_RUN
    #include <boost/shared_ptr.hpp>
    #include <boost/function.hpp>
    #include <boost/system/error_code.hpp>
    #include <boost/asio.hpp>
    #include <boost/function.hpp>
#endif

/*!
 * @brief 将本机的指令数据结构转换为小端buffer
 */
FDBInterface boost::shared_ptr<std::vector<char> >
FDBfNetworkLogic_host_instruction_to_le_buffer(FDBSInstruction* p_s_host_instruction);

/*!
 * @brief 将一个小端buffer还原为一个本机的指令数据结构
 */
FDBInterface boost::shared_ptr<FDBSInstruction>
FDBfNetworkLogic_le_buffer_to_host_instruction(const void *p_le_buffer);

/*!
 * @brief 生成回应内容,将要发送给其他主机.
 *        压缩指令内容,把指令和压缩过的指令内容打包,并且置指令general_switch压缩标志.
 *        指令部分小端序
 * @param pc_instruction 回应指令
 * @param p_instruction_content 指令内容
 * @param u32_instruction_content_len 指令内容长度
 * @param p_u32_response_buffer_len 回应内容的长度
 * @return 回应内容,前者指令,后者内容,不粘合在一起主要是考虑内存拷贝的问题,可交给外层做分散写.null失败
 */
FDBInterface std::pair<boost::shared_ptr<std::vector<char> >, boost::shared_ptr<std::vector<char> > >
FDBfNetworkLogic_produce_send(FDBSInstruction* p_s_instrcution
                              , const void* p_instruction_content
                              , const uint32_t u32_instruction_content_len
                              , const uint8_t u8_general_switch = __FDB_COMMUNICATION_INSTRUCTION_DATA_COMPRESSED__);

/*! [1]
 * @brief 还原数据.根据指令,对携带的数据部分解压缩或者解密什么的
 * @param sp_io_result 使用asio分段接收时得到的io结果
 * @return 前者指令,后面指令携带的原始发送携带的数据
 */
FDBInterface std::pair<boost::shared_ptr<FDBSInstruction>, boost::shared_ptr<std::vector<char> > >
FDBfNetworkLogic_restore_recv(std::pair<boost::shared_ptr<FDBSInstruction>
                              , boost::shared_ptr<std::vector<char> > > sp_io_result);
/*! [2]
 * @brief 还原数据.根据指令,对携带的数据部分解压缩或者解密什么的
 * @param p_buffer_from_remote_host 从远程发送而来的数据,一次性全部读出了(包含指令和指令携带数据)
 * @return 前者指令,后面指令携带的原始发送携带的数据
 */
FDBInterface std::pair<boost::shared_ptr<FDBSInstruction>, boost::shared_ptr<std::vector<char> > >
FDBfNetworkLogic_restore_recv(const void* p_buffer_from_remote_host, const uint32_t u32_buffer_len);


/*!
 * @brief 接收i/o完成后的回调函数签名
 */
#define __FDB_NETWORKLOGIC_INSTRUCTION_IO_HANDLER_SIGNATURE__(func_name) void func_name(const int b_instruction_data_unpacked,boost::shared_ptr<boost::asio::ip::tcp::socket> sp_peer_socket,std::pair<boost::shared_ptr<FDBSInstruction>,boost::shared_ptr<std::vector<char> > > pair_io_result)
typedef boost::function< void(std::pair<boost::shared_ptr<FDBSInstruction>,boost::shared_ptr<std::vector<char> > >) > bfuncIOHandler_t;
/*!
 * @brief 接收指令的处理,然后发起接收指令数据的,最后转发到i/o完成的回调函数
 */
FDBInterface void
FDBfNetworkLogic_recv_instruction_handler(boost::shared_ptr<std::vector<char> > sp_instruction_le_buffer
                                          , const int b_auto_unpack_instruction_data
                                          , boost::shared_ptr<boost::asio::ip::tcp::socket> sp_peer_socket
                                          , bfuncIOHandler_t io_handler
                                          , boost::system::error_code ec
                                          , size_t sz_bytes_transferred);

/*!
 * @brief 接收指令数据的处理,并且决定是否unpack,接收一次数据已经完成.然后将控制转向到i/o用户自定义的完成函数
 * @param io_handler 异步读完指令数据,并且unpack后的回调函数
 */
FDBInterface void
FDBfNetworkLogic_recv_instruction_data_handler(boost::shared_ptr<FDBSInstruction> sp_instruction
                                               , boost::shared_ptr<std::vector<char> > sp_packed_instruction_data
                                               , int b_auto_unpack_instruction_data
                                               , bfuncIOHandler_t io_handler
                                               , boost::system::error_code ec
                                               , size_t sz_bytes_transferred);

/*!
 * @brief 发送完一次i/o后的回调函数,然后发起接收指令
 */
FDBInterface void
FDBfNetworkLogic_send_data_handler_to_recv_next_instruction(std::pair<boost::shared_ptr<std::vector<char> >
                                                                    ,boost::shared_ptr<std::vector<char> > > pair_io_data
                                                            , boost::shared_ptr<boost::asio::ip::tcp::socket> sp_socket
                                                            , int b_auto_unpack_instruction_data
                                                            , bfuncIOHandler_t io_handler
                                                            , boost::system::error_code ec
                                                            , size_t sz_bytes_transferred);

/*!
 * @brief 发送完一次i/o后的回调函数,此处终结
 */
FDBInterface void
FDBfNetworkLogic_send_data_handler_dummy(std::pair<boost::shared_ptr<std::vector<char> >
                                                    , boost::shared_ptr<std::vector<char> > > pair_io_data
                                         , boost::system::error_code ec
                                         , size_t sz_bytes_transferred);

#endif //header
