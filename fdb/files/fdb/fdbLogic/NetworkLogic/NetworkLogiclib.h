﻿#ifndef __fdb_logic_network_lib_h_2013_11_07_01_02__
#define __fdb_logic_network_lib_h_2013_11_07_01_02__

#include "../fdbLogiclib.h"

#include "../../fdbPrimitive/Type/Type.h"

#include <string.h>

/*8 bit flag*/
#define __FDB_COMMUNICATION_INSTRUCTION_DATA_COMPRESSED__ 0x01
#define __FDB_COMMUNICATION_INSTRUCTION_DATA_ENCRYPTED__ 0x02

/*!
 * @brief 一次网络交互中使用的指令,其中指示了本次通讯的附加(例如register|,accepted|...),以及指令携带的数据部分的大小
 * @note 可能在网络上传送,处理时注意大小端
 */
extern "C" {
#pragma pack(push)
#pragma pack(4)

typedef struct FDBSInstruction {
    char pc_addon[10];
    uint8_t p_reserved[5];
    uint8_t u8_general_switch; //xxxx xxxx(0123 4567),7-是否压缩
    uint32_t u32_instruction_data_size;
} FDBSInstruction;

#pragma pack(pop)
}

/*!
 * @brief 通信指令字节大小
 */
#define __FDB_COMMUNICATION_INSTRUCTION_LEN__ sizeof(FDBSInstruction)
#define __FDB_COMMUNICATION_INSTRUCTION_ADDON_LEN__ 10



#endif //header
