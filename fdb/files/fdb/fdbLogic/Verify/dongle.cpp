﻿#include "dongle.h"

#include "Verify.h"

#include <smartdog/SmartUKeyApp.h>
#include <smartdog/SmartUKeyFileSystem.h>

#include <QMessageBox>
#include <QByteArray>

#include <stdlib.h>

/*!
 * \brief fVerifyDongle
 * \param app_id
 * \param i32_dog_passwd_1
 * \param i32_dog_passwd_2
 * \param i32_dog_passwd_3
 * \param i32_dog_passwd_4
 * \param product_infos
 * \param extend_setting
 * \return 错误码
 */
EVerifyErrorCode fVerifyDongle(const char *product_name
                  , const int min_version
                  , const char *product_language
                  , const char *product_type
                  , const int32_t i32_dog_passwd_1/* = 0xEF3EFBA7L*/
                  , const int32_t i32_dog_passwd_2/* = 0xE3C779D1L*/
                  , const int32_t i32_dog_passwd_3/* = 0x23FF25FL*/
                  , const int32_t i32_dog_passwd_4/* = 0x1148995FL*/
                  ,vector<string>* corrupt_dongles_uid
                  , CJsonite *product_infos, CFDBExtendSetting **extend_setting)
{
    char appID[32] = {0};
    strncpy(appID,product_name,32);
    long keyHandles[8] = {0};
    long matched_dongle_handle = 0;
    long keyNumber = 0;
    long ret = 0;
    char license_file_path[1024] = {0};
    sprintf(license_file_path,"%s_License.key",product_name);
    //和产品名字相同的(之前的一个狗只能放一个产品的狗)
    //或者和EliteSerial相同的(一个狗可以验证多个产品)
    ret = SmartUKeyFind(appID, keyHandles, &keyNumber);
    if(ret != 0)
    {
        strncpy(appID,"EliteSerial",32);
        ret = SmartUKeyFind(appID, keyHandles, &keyNumber);
    }
    EVerifyErrorCode verify_ec = EVerifyErrorCode_dongle_not_found;
    if (0 == ret)
    {
        verify_ec = EVerifyErrorCode_dongle_error;
        //找到可用的dog
        for(int i_matched_dongle_num=0;i_matched_dongle_num < keyNumber;++ i_matched_dongle_num)
        {
            long requestFromKey = 0;
            long dongle_handle = keyHandles[i_matched_dongle_num];
            ret = SmartUKeyOpen(dongle_handle
                                 , i32_dog_passwd_1, i32_dog_passwd_2, i32_dog_passwd_3, i32_dog_passwd_4
                                 , &requestFromKey);
            if(ret == 0)
            {
                long response = requestFromKey*requestFromKey;
                ret = SmartUKeyVerify(dongle_handle,response);
                if(ret == 0)
                {
                    matched_dongle_handle = dongle_handle;
                    char uid[33] = {0};
                    ret = SmartUKeyGetUid(matched_dongle_handle,uid);
                    int b_can_insert = 1;
                    for(int i=0;i<corrupt_dongles_uid->size();++ i)
                    {
                        string& str_uid = corrupt_dongles_uid->at(i);
                        if(strcmp(str_uid.data(),uid)==0)
                        {
                            b_can_insert = 0;
                        }
                    }
                    if(b_can_insert)
                    {
                        corrupt_dongles_uid->push_back(uid);
                    }
                    //找到一个可用的软件狗了,构造license object之后传递给验证接口
                    ret = SmartFS_SetCurrentDirectory(matched_dongle_handle,"0:\\");
                    if(ret != 0)
                    {
                        continue;
                    }
                    KEY_FILE license_file = NULL;
                    ret = SmartFS_OpenFile(license_file_path,0,&license_file);
                    if(license_file == NULL)
                    {
                        continue;
                    }
                    unsigned long license_file_size = 0;
                    ret = SmartFS_GetSize(license_file,&license_file_size);
                    if(ret != 0 || license_file_size <= 0)
                    {
                        continue;
                    }
                    char* origin_license_buf = (char*)malloc(license_file_size);
                    long bytes_read = -1;
                    ret = SmartFS_ReadFile(license_file,origin_license_buf,license_file_size,&bytes_read);
                    if(ret != 0 || bytes_read <= 0)
                    {
                        continue;
                    }
                    SmartFS_CloseFile(license_file);

                    //尝试解密
                    uint32_t decrypted_license_file_buf_len = 0;
                    char* decrypted_license_file_buf = FDBfCrypto_aes_128cbc_decrypt(origin_license_buf,license_file_size
                                                                                     ,"cat /etc/profile",strlen("cat /etc/profile")
                                                                                     ,&decrypted_license_file_buf_len);
                    string pure_license_file_body_buf;
                    //如果能找到register_vesion,说明这是7代以后的许可证注册机制
                    if(FDBfText_strstr(decrypted_license_file_buf,license_file_size
                                       ,"register_version"))
                    {
                        //构造出许可证描述对象
                        char* license_file_head_spltter_pos = FDBfText_strstr(decrypted_license_file_buf,license_file_size
                                                                              ,__FDB_LICENSE_SPLITER__);
                        if(license_file_head_spltter_pos == NULL)
                        {
                            continue;
                        }
                        CJsonite* desc_object = CJsonite::parseFromBuf(decrypted_license_file_buf
                                                                       ,license_file_head_spltter_pos-decrypted_license_file_buf);
                        int register_version = desc_object->getPayload("register_version")->toInt();
                        switch(register_version)
                        {
                        case 7:
                        {
                            int license_file_head_len = (license_file_head_spltter_pos-decrypted_license_file_buf)+__FDB_LICENSE_SPLITER_LEN__;
                            auto uncompressed_license_file_body_buf = FDBfCompress_uncompress_buffer(decrypted_license_file_buf+license_file_head_len
                                                                                                     ,license_file_size-license_file_head_len);
                            pure_license_file_body_buf = string(uncompressed_license_file_body_buf->data()
                                                                ,uncompressed_license_file_body_buf->size());
                        }break;
                        }
                    } else {
                        //7代以前的许可证
                        auto uncompressed_license_file_body = qUncompress((uchar*)origin_license_buf,license_file_size);
                        pure_license_file_body_buf = string(uncompressed_license_file_body.constData()
                                                            ,uncompressed_license_file_body.size());
                    }
                    //下面就是許可證的內容的驗證了
                    {   //验证证书的结果和数据完整性
                        char* splitter_pos_ptr = FDBfText_strstr(pure_license_file_body_buf.data(),pure_license_file_body_buf.size()
                                                                 ,__FDB_LICENSE_SPLITER__);
                        if(splitter_pos_ptr == NULL)
                        {
                            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"no splitter in license file,%s",license_file_path);
                            continue;
                        }

                        //構建出許可證內容
                        char* license_infos_start_pos = splitter_pos_ptr + __FDB_LICENSE_SPLITER_LEN__;
                        auto license_infos_object
                                = boost::shared_ptr<CJsonite>(CJsonite::parseFromBuf(license_infos_start_pos
                                                                              ,pure_license_file_body_buf.size()
                                                                              -(license_infos_start_pos-pure_license_file_body_buf.data())));

                        //得到许可证中在服务端计算出的许可证信息部分的摘要
                        char* decrypted_digest = NULL;
                        auto decrypted_digest_len = FDBfCrypto_rsa_private_decrypt(pure_license_file_body_buf.data()
                                                                                   ,splitter_pos_ptr - pure_license_file_body_buf.data()
                                                                                   ,&decrypted_digest
                                                                                   ,"private.key");
                        if(decrypted_digest == NULL)
                        {
                            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"decrypt license digest failed,no key(%s) found","private.key");
                            continue;
                        }
                        //在本地计算许可证的信息部分的摘要
                        auto license_infos_buf = license_infos_object->toBuf();
                        auto local_license_infos_digest
                                = FDBfHash_md5(license_infos_buf->data()
                                               ,license_infos_buf->size());
                        delete license_infos_buf;
                        if( strncmp(decrypted_digest,local_license_infos_digest,32) != 0 )
                        {
                            FDBLOG_TWO_PARAM(FDB_LOG_FILE
                                             ,"license basis digest doesn't match license origin digest:origin(%s),local calculated(%s)"
                                             ,decrypted_digest
                                             ,local_license_infos_digest);
                            continue;
                        }
                        free(decrypted_digest);
                        free(local_license_infos_digest);

                        /*验证存储介质的合法性*/
                        auto license_medium_payload = license_infos_object->getPayload("license_medium");
                        if(license_medium_payload)
                        {
                            if(strcmp(license_medium_payload->toString().data(),"smartdog")!=0)
                            {
                                continue;
                            }
                        }
                        /*smartdog加密狗的硬件id可以验一验,服务端暂未填写smartdoguid*/
                        auto dog_id_payload_in_license = license_infos_object->getPayload("smartdog_uid");
                        if(dog_id_payload_in_license != NULL)
                        {
                            string dog_id_in_license = dog_id_payload_in_license->toString();
                            char dog_id[100] = {0};
                            ret = SmartUKeyGetUid(dongle_handle,dog_id);
                            if(ret != 0)
                            {
                                continue;
                            }
                            if(strcmp(dog_id_in_license.data(),dog_id) != 0)
                            {
                                continue;
                            }
                        }

                        /*验证许可证通用对象的合法性*/
                        auto verify_ec = fVerifyLicenseObject(license_infos_object.get()
                                                              ,product_name,min_version,product_language,product_type
                                                              ,product_infos,extend_setting);
                        if(verify_ec == EVerifyErrorCode_succeed)
                        {
                            SmartUKeyClose(dongle_handle);
                            return (verify_ec);
                        }
                    }
                }
            }
        }
    }
    return (verify_ec);
}

void fEraseSmartDogLicense(const char* product_name
                           ,const int dog_pw1,const int dog_pw2,const int dog_pw3,const int dog_pw4)
{
    char appID[100] = {0};
    strncpy(appID,product_name,100);
    long keyHandles[8] = {0};
    long matched_dongle_handle = 0;
    long keyNumber = 0;
    long ret = 0;
    char license_file_path[1024] = {0};
    sprintf(license_file_path,"%s_License.key",product_name);
    ret = SmartUKeyFind(appID, keyHandles, &keyNumber);
    if (0 == ret)
    {
        //找到可用的dog
        for(int i_matched_dongle_num=0;i_matched_dongle_num < keyNumber;++ i_matched_dongle_num)
        {
            long requestFromKey = 0;
            long dongle_handle = keyHandles[i_matched_dongle_num];
            ret = SmartUKeyOpen(dongle_handle
                                 , dog_pw1, dog_pw2, dog_pw3, dog_pw4
                                 , &requestFromKey);
            if(ret == 0)
            {
                long response = requestFromKey*requestFromKey;
                ret = SmartUKeyVerify(dongle_handle,response);
                if(ret == 0)
                {
                    matched_dongle_handle = dongle_handle;
                    //找到一个可用的软件狗了
                    ret = SmartFS_SetCurrentDirectory(matched_dongle_handle,"0:\\");
                    if(ret != 0)
                    {
                        continue;
                    }
                    SmartFS_DeleteFile(license_file_path);
                    SmartUKeyClose(matched_dongle_handle);
                }
            }
        }
    }
}

void fSaveLicenseSmartDog(const char *product_name, const void *license_buf, const int license_size,long dongle_handle)
{
    char license_file_path[1024] = {0};
    sprintf(license_file_path,"%s_License.key",product_name);
    auto ret = SmartFS_SetCurrentDirectory(dongle_handle,"0:\\");
    if(ret != 0)
    {
        return ;
    }
    KEY_FILE license_file = NULL;
    ret = SmartFS_OpenFile(license_file_path,1,&license_file);
    if(ret != 0 || license_file == NULL)
    {
        return ;
    }
    long bytes_write = -1;
    ret = SmartFS_WriteFile(license_file,(void*)license_buf,license_size,&bytes_write);
    if(ret != 0 || bytes_write <= 0)
    {
        return ;
    }
    ret = SmartFS_FlushFile(license_file);
    ret = SmartFS_CloseFile(license_file);
}

long fGetDongleHandle(const char* app_id
    , const long pw1, const long pw2, const long pw3, const long pw4
    , const string& str_uid)
{
    char appID[32] = { 0 };
    strncpy(appID, app_id, 32);
    long key_handles[8] = { 0 };
    long key_num = 0;
    auto ret = SmartUKeyFind(appID,key_handles,&key_num);
    if (ret != 0)
    {
        strncpy(appID,"EliteSerial",32);
        ret = SmartUKeyFind(appID,key_handles,&key_num);
    }
    if(ret != 0)
    {
        return (0);
    }
    for (int i = 0; i < key_num; ++i)
    {
        char uid[33] = {0};
        ret = SmartUKeyGetUid(key_handles[i],uid);
        if (strcmp(uid, str_uid.data())==0)
        {
			long dongle_handle = key_handles[0];
			long requestFromKey = 0;
			ret = SmartUKeyOpen(dongle_handle
				, pw1, pw2, pw3, pw4
				, &requestFromKey);
			if(ret == 0)
			{
				long response = requestFromKey*requestFromKey;
				ret = SmartUKeyVerify(dongle_handle,response);
				if(ret == 0)
				{
					return (key_handles[i]);
				}
			}
        }
    }
}
