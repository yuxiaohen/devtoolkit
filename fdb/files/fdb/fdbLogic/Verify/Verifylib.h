﻿#ifndef __fdb_verify_lib_2013_11_04_01_49__
#define __fdb_verify_lib_2013_11_04_01_49__

#include "../fdbLogiclib.h"
#include "../../fdbPrimitive/Type/int.h"

#define __FDB_LICENSE_AUTO_CLOSE__ 0x01L
#define __FDB_LICENSE_AUTO_REGISTER__ 0x01L

#define __FDB_LANGUAGE_CHINESE__ 0x01L
#define __FDB_LANGUAGE_ENGLISH__ 0x02L

#define __FDB_LICENSE_TYPE_PERSONAL__ 0x01L
#define __FDB_LICENSE_TYPE_ENTERPRISE__ 0x02L
#define __FDB_LICENSE_TYPE_PROFESSIONAL__ 0x04L

#define __FDB_LICENSE_VERIFY_LICENSE__ 0x01L
#define __FDB_LICENSE_VERIFY_SMART_DOG__ 0x02L

#define __FDB_LICENSE_VERIFY_INTERFACE_GUI__ 0x01L
#define __FDB_LICENSE_VERIFY_INTERFACE_CLI__ 0x02L

#define __FDB_LICENSE_SPLITER__ "<----fu11d0t0s0ft---->"
#define __FDB_LICENSE_SPLITER_LEN__ 22

typedef enum EVerifyErrorCode {
    EVerifyErrorCode_succeed /*无错*/
    ,EVerifyErrorCode_dongle_error /*软件错误*/
    ,EVerifyErrorCode_license_corrupt /*许可证错误*/
    ,EVerifyErrorCode_expire /*许可证过期*/
    ,EVerifyErrorCode_hardware_mismatch /*硬件不匹配*/
    ,EVerifyErrorCode_product_mismatch /*产品不匹配*/
    ,EVerifyErrorCode_language_mismatch /*语言不匹配*/
    ,EVerifyErrorCode_version_too_low /*版本过低*/
    ,EVerifyErrorCode_product_type_mismatch /*产品类型不匹配*/
    ,EVerifyErrorCode_dongle_not_found /*未找到加密狗*/
} EVerifyErrorCode;

#endif //header
