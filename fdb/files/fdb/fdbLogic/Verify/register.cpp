#include "register.h"
#include "../Register/registerDialog.h"
#include <fdb/fdb.h>

#include <QHostInfo>
#include <QTcpSocket>
#include <QEventLoop>

#include "dongle.h"

string g_one_available_server_ip;

string FDBfGetAvailableServerAddr(const int i_port)
{
    static string one_available_server_ip;
    if(one_available_server_ip.empty() == false)
    {
       return (one_available_server_ip);
    }
    const int i_server_host_name_num = sizeof(g_pp_server_host_names) / sizeof(char*);
    /*测试所有域名和ip*/
    for( int i=0;i < i_server_host_name_num;++ i )
    {
        auto host_info = QHostInfo::fromName(g_pp_server_host_names[i]);
        foreach(auto& address,host_info.addresses())
        {
            QTcpSocket obj_sock;
            obj_sock.connectToHost(address,i_port);
            if(obj_sock.waitForConnected())
            {
                char pc_ping_pkg_header[20] = {"ping_a"}; //ping请求
                obj_sock.write(pc_ping_pkg_header,sizeof(pc_ping_pkg_header));
                obj_sock.waitForBytesWritten();
                obj_sock.waitForReadyRead();
                auto ba_ping_r_pkg_header = obj_sock.read(20);
                if(ba_ping_r_pkg_header.indexOf("ping_r") != -1)
                {
                    //this ok
                    one_available_server_ip = address.toString().toStdString();
                    return (one_available_server_ip);
                }
            }
        }
    }
    return ("-1");
}

string FDBfGetAvailabelHttpDomain()
{
    static string one_available_http_domain;
    if(one_available_http_domain.empty() == false)
    {
        return (one_available_http_domain);
    }
    const int i_http_server_host_name_num = sizeof(g_pp_server_http_domains)/sizeof(char*);
    FDBCHttp http;
    http.slEnableHeader(FDBCHttp::EHTTPHeader_host);
    char buf[2048] = {0};
    for(int i=0;i<i_http_server_host_name_num;++ i)
    {
        sprintf(buf,"http://%s/handshake.html",g_pp_server_http_domains[i]);
        auto handshake_html_response = http.slGet(buf);
        if(handshake_html_response.second.find("OK!") != string::npos)
        {
            one_available_http_domain = g_pp_server_http_domains[i];
            return (one_available_http_domain);
        }
    }
}

pair<int,string> fSendRecvRegisterRequest(CJsonite* provider_infos)
{
    auto hardware_infos_buf = provider_infos->toBuf();
    //连接服务器
    try {
        boost::asio::io_service io_service;
        boost::asio::ip::tcp::endpoint endpoint(
                    boost::asio::ip::address::from_string(FDBfGetAvailableServerAddr(g_server_port)),g_server_port);
        boost::asio::ip::tcp::socket sock(io_service);
        sock.connect(endpoint);
        //发送注册
        string pkg_header_a(20,'\x00');
        strcpy((char*)pkg_header_a.data(),"register_a");
        sprintf((char*)pkg_header_a.data()+10,"%d",hardware_infos_buf->size());
        boost::asio::write(sock,boost::asio::buffer(pkg_header_a.data(),pkg_header_a.size()));
        boost::asio::write(sock,boost::asio::buffer(hardware_infos_buf->data(),hardware_infos_buf->size()));
        //收取应答
        auto pkg_header_r = boost::shared_ptr<string>(new string(20, '\x00'));
        boost::asio::read(sock, boost::asio::buffer((char*)pkg_header_r->data(), pkg_header_r->size()));
        auto pkg_payload_r_len = atoi(pkg_header_r->data() + 10);
        string error_reason("Server error");
        if (pkg_payload_r_len > 0)
        {
            auto pkg_payload_r = boost::shared_ptr<string>(new string(pkg_payload_r_len, '\x00'));
            boost::asio::read(sock, boost::asio::buffer((char*)pkg_payload_r->data(), pkg_payload_r->size()));
            if (pkg_header_r->find("register_r") != string::npos)
            {
                //注册成功,保存license文件内容即可
                if (pkg_payload_r->find("accepted|") != string::npos)
                {
                    string license_buf(pkg_payload_r->data() + strlen("accepted|")
                                       , pkg_payload_r->size() - strlen("accepted|"));
                    delete hardware_infos_buf;
                    return (make_pair(0,license_buf));
                }
                else {
                    error_reason = string(pkg_payload_r->data() + strlen("rejected|"));
                }
            }
        }
        delete hardware_infos_buf;
        return (make_pair(-1,error_reason));
    }
    catch (boost::system::system_error ec) {
        delete hardware_infos_buf;
        return (make_pair(-1,"Connect Host Failed"));
    }
}

string fAutoFetchLicense(const char* product_name
                         ,const int min_version
                         ,const char* product_language
                         ,const char* product_type
                         ,const string& dog_uid)
{
    string bios_sn = FDBfHardware_get_bios_sn();
    string cpu_id = FDBfHardware_get_cpu_id();
    string hd_sn = FDBfHardware_get_hd_sn();
    CJsonite* auto_fetch_infos = new CJsonite();
    auto_fetch_infos->addPayload_string("product_name",product_name);
    auto_fetch_infos->addPayload_int("license_version",min_version);
    auto_fetch_infos->addPayload_string("license_language",product_language);
    auto_fetch_infos->addPayload_string("license_type",product_type);
    auto_fetch_infos->addPayload_string("bios_sn",bios_sn.data());
    auto_fetch_infos->addPayload_string("cpu_id",cpu_id.data());
    auto_fetch_infos->addPayload_string("hd_sn",hd_sn.data());
    if(dog_uid.empty() == false)
    {
        auto_fetch_infos->addPayload_string("smartdog_uid",dog_uid.data(),dog_uid.size());
    }
    auto fetch_result = fSendRecvRegisterRequest(auto_fetch_infos);
    delete auto_fetch_infos;
    if(fetch_result.first == 0)
    {
        return (fetch_result.second);
    }
    return ("");
}

void fShowRegister(const char* product_name
                   , const int min_version
                   , const char* product_language
                   , const char* product_type
                   , const char* license_medium
                   , vector<long> *corrupt_dongles_handle)
{
    CRegisterDialog* register_dialog
            = new CRegisterDialog(product_name,product_language,product_type,min_version
            ,license_medium
			,corrupt_dongles_handle);
//    #if !defined(LINK_FDB_FROM_SOURCE)
    register_dialog->exec();
//    #else
//     register_dialog->setModal(false);
//     register_dialog->show();
//     QEventLoop event;
//     QObject::connect(register_dialog,&CRegisterDialog::close,&event,&QEventLoop::quit);
//     event.exec();
//     #endif
}
