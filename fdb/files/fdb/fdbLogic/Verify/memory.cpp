﻿#include "memory.h"

#include <fdb/fdb.h>

#include <QByteArray>

EVerifyErrorCode fVerifyMemory(const char* product_name
                               ,const int min_version
                               ,const char* product_language
                               ,const char* product_type
                               ,const char* license_buf,const int len
                               ,CJsonite* gp_product_infos,CFDBExtendSetting** gp_extend_setting)
{
    string pure_license_body_buf;
    {
        //尝试解密
        uint32_t decrypted_license_buf_len = 0;
        char* decrypted_license_buf = FDBfCrypto_aes_128cbc_decrypt(license_buf,len
                                                                    ,"cat /etc/profile",strlen("cat /etc/profile")
                                                                    ,&decrypted_license_buf_len);
        //如果能找到register_vesion,说明这是7代以后的许可证注册机制
        if(FDBfText_strstr(decrypted_license_buf,len
                           ,"register_version"))
        {
            //构造出许可证描述对象
            char* license_file_head_spltter_pos = FDBfText_strstr(decrypted_license_buf,len
                                                                  ,__FDB_LICENSE_SPLITER__);
            if(license_file_head_spltter_pos == NULL)
            {
                return (EVerifyErrorCode_license_corrupt);
            }
            CJsonite* desc_object = CJsonite::parseFromBuf(decrypted_license_buf
                                                           ,license_file_head_spltter_pos-decrypted_license_buf);
            int register_version = desc_object->getPayload("register_version")->toInt();
            switch(register_version)
            {
            case 7:
            {
                int license_file_head_len = (license_file_head_spltter_pos-decrypted_license_buf)+__FDB_LICENSE_SPLITER_LEN__;
                auto uncompressed_license_file_body_buf = FDBfCompress_uncompress_buffer(decrypted_license_buf+license_file_head_len
                                                                                         ,len-license_file_head_len);
                pure_license_body_buf = string(uncompressed_license_file_body_buf->data()
                                               ,uncompressed_license_file_body_buf->size());
            }break;
            }
        } else {
            //7代以前的许可证
            auto uncompressed_license_file_body = qUncompress((uchar*)license_buf,len);
            pure_license_body_buf = string(uncompressed_license_file_body.constData()
                                           ,uncompressed_license_file_body.size());
        }
    }
    //下面就是許可證的內容的驗證了
    {   //验证证书的结果和数据完整性
        char* splitter_pos_ptr = FDBfText_strstr(pure_license_body_buf.data(),pure_license_body_buf.size()
                                                 ,__FDB_LICENSE_SPLITER__);
        if(splitter_pos_ptr == NULL)
        {
            return (EVerifyErrorCode_license_corrupt);
        }

        //構建出許可證內容
        char* license_infos_start_pos = splitter_pos_ptr + __FDB_LICENSE_SPLITER_LEN__;
        auto license_infos_object
                = boost::shared_ptr<CJsonite>(CJsonite::parseFromBuf(license_infos_start_pos
                                                              ,pure_license_body_buf.size()
                                                              -(license_infos_start_pos-pure_license_body_buf.data())));

        //得到许可证中在服务端计算出的许可证信息部分的摘要
        char* decrypted_digest = NULL;
        auto decrypted_digest_len = FDBfCrypto_rsa_private_decrypt(pure_license_body_buf.data()
                                                                   ,splitter_pos_ptr - pure_license_body_buf.data()
                                                                   ,&decrypted_digest
                                                                   ,"private.key");
        if(decrypted_digest == NULL)
        {
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"decrypt license digest failed,no key(%s) found","private.key");
            return (EVerifyErrorCode_license_corrupt);
        }
        //在本地计算许可证的信息部分的摘要
        auto license_infos_buf = license_infos_object->toBuf();
        auto local_license_infos_digest
                = FDBfHash_md5(license_infos_buf->data()
                               ,license_infos_buf->size());
        delete license_infos_buf;
        if( strncmp(decrypted_digest,local_license_infos_digest,32) != 0 )
        {
            FDBLOG_TWO_PARAM(FDB_LOG_FILE
                             ,"license basis digest doesn't match license origin digest:origin(%s),local calculated(%s)"
                             ,decrypted_digest
                             ,local_license_infos_digest);
            return (EVerifyErrorCode_license_corrupt);
        }
        free(decrypted_digest);
        free(local_license_infos_digest);

        /*验证存储介质的合法性*/
        auto license_medium_payload = license_infos_object->getPayload("license_medium");
        if(license_medium_payload)
        {
            if(strcmp(license_medium_payload->toString().data(),"memory")!=0)
            {
                return (EVerifyErrorCode_license_corrupt);
            }
        }

        return (fVerifyLicenseObject(license_infos_object.get()
                                     ,product_name,min_version,product_language,product_type
                                     ,gp_product_infos,gp_extend_setting));
    }
    return (EVerifyErrorCode_succeed);
}
