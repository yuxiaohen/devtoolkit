#include "error_tip.h"

#include <QDebug>

ErrorTip::ErrorTip(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ErrorTip)
{
    ui->setupUi(this);
}

ErrorTip::~ErrorTip()
{
    delete ui;
}

void ErrorTip::mfslSetError(const QString &ref_str_error_image_path)
{
    qDebug() << "error tip image path:" << ref_str_error_image_path;
    this->ui->error_msg_widget->setStyleSheet(QString("QWidget {background-image:url(%1);}").arg(ref_str_error_image_path) );
}
