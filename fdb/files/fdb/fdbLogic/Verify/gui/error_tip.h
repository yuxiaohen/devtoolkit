#pragma once

#include <QDialog>

#include "ui_error_tip.h"

namespace Ui {
class ErrorTip;
}

class ErrorTip : public QDialog
{
    Q_OBJECT

public:
    explicit ErrorTip(QWidget *parent = 0);
    ~ErrorTip();

private:
    Ui::ErrorTip *ui;
public:
    void mfslSetError(const QString& ref_str_error_image_path);
};
