﻿#include "Verify.h"

#if defined(_WIN32) && defined(__HAVE_SMART_LIB__)
    #include <smartdog/SmartUKeyApp.h>
#endif

#include "dongle.h"
#include "license.h"
#include "memory.h"
#include "register.h"

#if defined(QT_GUI_LIB) && !defined(__FDB_CONSOLE_LINK__)
    #include <QMessageBox>
    #include <QTimer>
    #include "gui/error_tip.h"

    #include <QDebug>
    CCrashFeedbackDialog* g_p_obj_crash_dialog = nullptr;
#endif

#include <fdb/fdbPrimitive/DateTime/DateTime.h>

CFDBExtendSetting* gp_extend_setting = new CFDBExtendSetting();
CJsonite* gp_product_infos = new CJsonite();
string g_license_buf;

CFDBExtendSetting::CFDBExtendSetting()
{

}
void CFDBExtendSetting::constructSetting(const char* extend_setting_buf,const int len)
{
    this->m_extend_setting = CJsonite::parseFromBuf(extend_setting_buf,len);
}
void CFDBExtendSetting::refreshSetting()
{
    delete this->m_extend_setting;
    FDBfVerifyLicense(this->m_product_name.data(),this->m_min_version,this->m_product_language.data()
                      ,this->m_product_type.data(),this->m_auto_close,this->m_auto_register
                      ,this->m_license_medium.data());
}

string CFDBExtendSetting::pathSeparator()
{
    refreshSetting();

    return (this->m_extend_setting->getPayload("pathSeparator")->toString());
}
string CFDBExtendSetting::physicalDriverPath(int i)
{
    refreshSetting();

    string physical_driver_path;
    char buf[10] = {0};
#if defined(_WIN32)
    sprintf(buf,"%d",i);
    physical_driver_path = this->m_extend_setting->getPayload("WinPhysicalDrivePrefix")->toString()+buf;
#elif defined(__APPLE__)
    //physical_driver_path = this->m_extend_setting.value("MacPhysicalDrivePrefix").toString()+QString::number(i);
#elif defined(__linux__)
    //physical_driver_path = this->m_extend_setting.value("LinuxPhysicalDrivePrefix").toString()+('a'+i);
#endif
    return (physical_driver_path);
}
CJsonitePayload* CFDBExtendSetting::getSpecialValue(const char* key)
{
    refreshSetting();

    return (this->m_extend_setting->getPayload(key));
}

/*!
 * @brief 设置异常现场处理
 */
void fSetupCrashReport(const char* product_name)
{
    #if defined(QT_GUI_LIB)
        if(g_p_obj_crash_dialog == nullptr)
        {
            g_p_obj_crash_dialog = new CCrashFeedbackDialog(product_name);
            FDBfException_set_dump_trigger(FDBfException_dump);
        }
    #else
        #error "no terminal crash handle now"
    #endif
}

void fShowErr(const char *msg, FILE* out);

int FDBfVerifyLicense(const char* product_name
                      , const int min_version
                      , const char* product_language
                      , const char* product_type
                      , const int auto_close
                      , const int auto_register
                      , const char* license_medium
                      , const int32_t dog_pw1
                      , const int32_t dog_pw2
                      , const int32_t dog_pw3
                      , const int32_t dog_pw4)
{
    int first_enter = 1;
    /*! -- 现场处理 --*/
    fSetupCrashReport(product_name);

    EVerifyErrorCode verify_result = EVerifyErrorCode_succeed;
    int b_need_circle = 1;
    vector<string> corrupt_dongles_uid;
    VerifyBegin:
    //加密狗亦或证书验证
    if(strcmp(license_medium,"smartdog") == 0)
    {
        verify_result = fVerifyDongle(product_name
                                        ,min_version
                                        ,product_language
                                        ,product_type
                                        ,dog_pw1,dog_pw2,dog_pw3,dog_pw4
                                        ,&corrupt_dongles_uid
                                        ,gp_product_infos,&gp_extend_setting);
    } else if(strcmp(license_medium,"file") == 0) {
        verify_result = fVerifyLicenseFile(product_name
                                             ,min_version
                                             ,product_language
                                             ,product_type
                                             ,gp_product_infos,&gp_extend_setting);
    } else if(strcmp(license_medium,"memory") == 0) {
        //内存型证书
        verify_result = fVerifyMemory(product_name
                                      ,min_version
                                      ,product_language
                                      ,product_type
                                      ,g_license_buf.data(),g_license_buf.size()
                                      ,gp_product_infos,&gp_extend_setting);
    }
    /*下面是进行自动获取的,对于文件型许可证和软件狗都可以,但内存型的就没必要了*/
    if(verify_result != EVerifyErrorCode_succeed)
    {
        //证书损坏或者证书内的内容不正确,那擦除掉证书了
        fEraseLicense(product_name,license_medium,dog_pw1,dog_pw2,dog_pw3,dog_pw4);
        //先到服务器上自动检索下看看,看有没有现成的许可证
        if(first_enter && auto_register)
        {   //首次验证,有必要自动从服务器获取,但没必要多次尝试
            first_enter = 0;
            int b_need_recheck = 0;
            if(verify_result == EVerifyErrorCode_dongle_error || verify_result == EVerifyErrorCode_dongle_not_found)
            {   //针对软件狗的情况,循环所有的损坏软件,进行一一自动获取
                if (corrupt_dongles_uid.size() == 0)
                {   //软件狗的版本,你却连一个软件狗都没有,混毛
                    QMessageBox::warning(NULL,QString("NOTICE"),QStringLiteral("未找到加密狗"));
                    return (0);
                } else {
                    QMessageBox::warning(NULL,QString("NOTICE"),QStringLiteral("加密狗错误"));
                }
                for(int i=0;i<corrupt_dongles_uid.size();++ i)
                {
                    auto dongle_uid = corrupt_dongles_uid[i];
                    auto license_buf = fAutoFetchLicense(product_name,min_version,product_language,product_type
                        , dongle_uid);
                    if(license_buf.empty() == false)
                    {
                        long dongle_handle = fGetDongleHandle(product_name
                            , dog_pw1, dog_pw2, dog_pw3, dog_pw4
                            , dongle_uid.data());
                        //先存下来,然后调用验证接口验证,不通过再弹出注册界面
                        fSaveLicenseBuf(product_name,license_buf.data(),license_buf.size(),license_medium,dongle_handle);
                        SmartUKeyClose (dongle_handle);
                        b_need_recheck = 1;
                    }
                }
            } else {
                auto license_buf = fAutoFetchLicense(product_name,min_version,product_language,product_type);
                if(license_buf.empty() == false)
                {
                    //先存下来,然后调用验证接口验证,不通过再弹出注册界面
                    fSaveLicenseBuf(product_name,license_buf.data(),license_buf.size(),license_medium);
                    b_need_recheck = 1;
                }
            }
            if(b_need_recheck)
            {
                goto VerifyBegin;
            }
        }
        if(auto_register && b_need_circle)
        {
            std::vector<long> corrupt_dongles_handle;
            foreach(auto& uid,corrupt_dongles_uid)
            {
                auto dongle_handle = fGetDongleHandle(product_name
                                                      ,dog_pw1,dog_pw2,dog_pw3,dog_pw4
                                                      ,uid);
                if(dongle_handle != 0)
                {
                    corrupt_dongles_handle.push_back(dongle_handle);
                }
            }
            //显示注册界面,并进行许可证的存放,以便再次验证
            fShowRegister(product_name,min_version,product_language,product_type,license_medium
                , &corrupt_dongles_handle);
			foreach(long dongle_handle,corrupt_dongles_handle)
			{
				auto ret = SmartUKeyClose(dongle_handle);
			}
            b_need_circle = 0;
            goto VerifyBegin;
        }
        if(auto_close)
        {
            exit(-1);
        }
    } else {
        /*许可证合法,为全局配置设置刷新参数*/
        gp_extend_setting->m_product_name = product_name;
        gp_extend_setting->m_min_version = min_version;
        gp_extend_setting->m_product_language = product_language;
        gp_extend_setting->m_product_type = product_type;
        gp_extend_setting->m_auto_close = auto_close;
        gp_extend_setting->m_auto_register = auto_register;
        gp_extend_setting->m_license_medium = license_medium;
        return (1);
    }
    return (0);
}

int FDBfVerifyLicense(const char *pc_release_time
                      , const char *pc_valid_license_product_name
                      , const int32_t i32_valid_license_min_version
                      , const int32_t i32_valid_license_language
                      , const int32_t i32_valid_license_type
                      , const int32_t i32_verify_interface
                      , const int b_auto_close
                      , const int b_auto_register
                      , const int32_t i32_verify_method
                      , const int32_t i32_dog_passwd_1
                      , const int32_t i32_dog_passwd_2
                      , const int32_t i32_dog_passwd_3
                      , const int32_t i32_dog_passwd_4)
{
    string product_language = FDBfLangIndexToStr(i32_valid_license_language);
    string product_type = FDBfLicenseTypeIndexToStr(i32_valid_license_type);
    string license_medium = FDBfLicenseMediumIndexToStr(i32_verify_method);
    return (FDBfVerifyLicense(pc_valid_license_product_name
                              ,i32_valid_license_min_version
                              ,product_language.data()
                              ,product_type.data()
                              ,b_auto_close
                              ,b_auto_register
                              ,license_medium.data()) );
}

int
FDBfVerifyLicense(const char* product_name
                  , const int32_t min_version
                  , const int32_t product_language
                  , const int32_t product_type
                  , const int32_t i32_verify_interface/* = __FDB_LICENSE_VERIFY_INTERFACE_GUI__*/
                  , const int b_auto_close/* = __FDB_LICENSE_AUTO_CLOSE__*/
                  , const int b_auto_register/* = __FDB_LICENSE_AUTO_REGISTER__*/
                  , const int32_t i32_verify_method/* = __FDB_LICENSE_VERIFY_LICENSE__*/
                  , const int32_t i32_dog_passwd_1
                  , const int32_t i32_dog_passwd_2
                  , const int32_t i32_dog_passwd_3
                  , const int32_t i32_dog_passwd_4
                  )/* { return (0); }*/
{
    string product_language_str = FDBfLangIndexToStr(product_language);
    string product_type_str = FDBfLicenseTypeIndexToStr(product_type);
    string license_medium_str = FDBfLicenseMediumIndexToStr(i32_verify_method);
    return (FDBfVerifyLicense(product_name
                              ,min_version
                              ,product_language_str.data()
                              ,product_type_str.data()
                              ,b_auto_close
                              ,b_auto_register
                              ,license_medium_str.data()));
}

EVerifyErrorCode fVerifyLicenseObject(CJsonite* license_object
                                      ,const char* product_name,const int min_version
                                      ,const char* product_language,const char* product_type
                                      ,CJsonite* product_infos,CFDBExtendSetting** extend_setting)
{
    { //验证许可证的使用期限
        //1.软件的发布时间在证书的使用截止时间之后,说明此时证书已经过期了
        auto i64_use_expire_time_stamp = license_object->getPayload("use_expire_time")->toInt();
        auto i64_use_expire_milliseconds = i64_use_expire_time_stamp*1000;
        auto i64_milliseconds_of_compile_time = getCompileDateTime() * 1000;
        if(i64_milliseconds_of_compile_time > i64_use_expire_milliseconds)
        {
            FDBLOG_ONE_PARAM(FDB_LOG_FILE
                             ,"license expire(before release time) at %lld(unix timestamp*1000)"
                             ,i64_use_expire_milliseconds);
            return (EVerifyErrorCode_expire);
        }
        //2.现在时间大于使用时间,过期
        auto i64_milliseconds_of_current_time = time(NULL)*1000;
        if(i64_milliseconds_of_current_time > i64_use_expire_milliseconds)
        {
            FDBLOG_ONE_PARAM(FDB_LOG_FILE
                             ,"license expire(timeout) at %lld(unix timestamp*1000)"
                             ,i64_milliseconds_of_current_time);
            return (EVerifyErrorCode_expire);
        }
    }
    {   //验证许可证对应的软件
        auto product_name_in_license = license_object->getPayload("product_name")->toString();
        if(strcmp(product_name,product_name_in_license.data()) != 0)
        {
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"product name mismatch:product require is %s,but license is for %s"
                             ,product_name
                             ,product_name_in_license.data());
            return (EVerifyErrorCode_product_mismatch);
        }
    }
    {   //验证许可证对应的软件的版本
        if(min_version > license_object->getPayload("license_version")->toInt())
        {
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"version mismatch:product require version %d,but license max is %d"
                             ,min_version
                             ,license_object->getPayload("license_version")->toInt());
            return (EVerifyErrorCode_version_too_low);
        }
    }
    {//验证许可证对应的软件的语种
        auto str_license_language = license_object->getPayload("license_language")->toString();
        if(strcmp(product_language,str_license_language.data()) != 0)
        {
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"language mismatch:product require %s,but license is %s"
                             ,product_language
                             ,str_license_language.data());
            return (EVerifyErrorCode_language_mismatch);
        }
    }
    {//验证许可证的类型
        auto str_license_type = license_object->getPayload("license_type")->toString();
        if(strcmp(product_type,str_license_type.data()) != 0)
        {
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"type mismatch:product require %s,but license is %s"
                             ,product_type
                             ,str_license_type.data());
            return (EVerifyErrorCode_product_type_mismatch);
        }
    }
    {   //验证许可证对应的硬件
        //使用硬件进行注册,usb信息都是空的
        auto dog_id_in_license = license_object->getPayload("smartdog_uid");
        if(dog_id_in_license && dog_id_in_license->toString().empty() == false)
        {   //这是加密狗验证的,不用验证硬件了

        } else {
            auto i_usb_vid = license_object->getPayload("usb_vid")->toInt();
            auto i_usb_pid = license_object->getPayload("usb_pid")->toInt();
            auto str_usb_sn = license_object->getPayload("usb_sn")->toString();
            if(i_usb_vid == 0 && i_usb_pid == 0 && str_usb_sn.size() == 0)
            {
                int b_hardware_match = 0;
                string str_hd_sn = FDBfHardware_get_hd_sn();
                string str_cpu_sn = FDBfHardware_get_cpu_id();
                string str_bios_sn = FDBfHardware_get_bios_sn();
                auto str_hd_sn_in_license = license_object->getPayload("hd_sn")->toString();
                auto str_cpu_sn_in_license = license_object->getPayload("cpu_sn")->toString();
                auto str_bios_sn_in_license = license_object->getPayload("bios_sn")->toString();

                if(strcmp(str_hd_sn.data(),str_hd_sn_in_license.data())==0
                        &&strcmp(str_cpu_sn.data(),str_cpu_sn_in_license.data())==0
                        &&strcmp(str_bios_sn.data(),str_bios_sn_in_license.data())==0)
                {
                    b_hardware_match = 1;
                }

                if(b_hardware_match == 0)
                {
                    FDBLOG(FDB_LOG_FILE,"license hardware mismatch,detail below:");
                    FDBLOG_THREE_PARAM(FDB_LOG_FILE,"hd sn in license:%s,cpu sn in license:%s,bios sn in license:%s"
                                       ,str_hd_sn_in_license.data()
                                       ,str_cpu_sn_in_license.data()
                                       ,str_bios_sn_in_license.data());
                    FDBLOG_THREE_PARAM(FDB_LOG_FILE,"hd sn of host:%s,cpu sn of host:%s,bios sn of host:%s"
                                       ,str_hd_sn.data()
                                       ,str_cpu_sn.data()
                                       ,str_bios_sn.data());
                    return (EVerifyErrorCode_hardware_mismatch);
                }
            }
        }
    } //硬件匹配
    { //设置许可证对应的产品每次可启动的时长定时器
        auto i64_every_time_demo = license_object->getPayload("every_time_demo")->toInt();
        if(i64_every_time_demo != 0)
        {
            boost::asio::io_service* io_service = new boost::asio::io_service();
            boost::asio::deadline_timer deadline_quit(*io_service);
            deadline_quit.expires_from_now(boost::posix_time::seconds(i64_every_time_demo));
            deadline_quit.async_wait(boost::bind(exit,-1));
            boost::thread deadline_quit_io_service_thead(boost::bind(&boost::asio::io_service::run
                                                                     ,io_service));
        }
    }
    { //设置全局配置
        auto str_user_key = license_object->getPayload("user_key")->toString();
        auto extend_setting_buf = license_object->getPayload("extend_conf")->toBlob();
        product_infos->addPayload_string("user_key",str_user_key.data());
        (*extend_setting)->constructSetting(extend_setting_buf->data()
                                            , extend_setting_buf->size());
        delete extend_setting_buf;
    }
    return (EVerifyErrorCode_succeed);
}

void fSaveLicenseBuf(const char* product_name, const char* license_buf, const int len, const char* license_medium, long dongle_handle)
{
    if(strcmp(license_medium,"file")==0)
    {   //文件型许可证存储
        fSaveLicenseFile(product_name,license_buf,len);
    } else if(strcmp(license_medium,"smartdog")==0) {
        fSaveLicenseSmartDog(product_name,license_buf,len,dongle_handle);
    } else if(strcmp(license_medium,"memory")==0) {
        g_license_buf = string(license_buf,len);
    }
}

void fEraseLicense(const char* product_name,
                   const char* license_medium
                   ,const int dog_pw1,const int dog_pw2,const int dog_pw3,const int dog_pw4)
{
    if(strcmp(license_medium,"file")==0)
    {   //擦除文件型许可证
        char license_path[1024] = {0};
        sprintf(license_path,"%s_License.key",product_name);
        remove(license_path);
    } else if(strcmp(license_medium,"smartdog")==0) {
        fEraseSmartDogLicense(product_name,dog_pw1,dog_pw2,dog_pw3,dog_pw4);
    }
}

int FDBfLangStrToIndex(const char *pc_language)
{
    if( strcmp(pc_language,"chinese") == 0 )
    {
        return (__FDB_LANGUAGE_CHINESE__);
    } else if( strcmp(pc_language,"english") == 0 ) {
        return (__FDB_LANGUAGE_ENGLISH__);
    } else {
        return (__FDB_LANGUAGE_ENGLISH__);
    }
    return (__FDB_LANGUAGE_ENGLISH__);
}
string FDBfLangIndexToStr(const int i_lang_index)
{
    switch(i_lang_index)
    {
        case __FDB_LANGUAGE_CHINESE__:
        {
            return (std::string("chinese"));
        }break;
        case __FDB_LANGUAGE_ENGLISH__:
        {
            return (std::string("english"));
        }break;
        default:
        {
            return (std::string("english"));
        }break;
    }
}

int FDBfLicenseTypeStrToIndex(const char* pc_license_type)
{
    if( strcmp(pc_license_type,"personal") == 0 )
    {
        return (__FDB_LICENSE_TYPE_PERSONAL__);
    } else if( strcmp(pc_license_type,"enterprise") == 0 ) {
        return (__FDB_LICENSE_TYPE_ENTERPRISE__);
    } else if( strcmp(pc_license_type,"professional") == 0 ) {
        return (__FDB_LICENSE_TYPE_PROFESSIONAL__);
    } else {
        return (__FDB_LICENSE_TYPE_PERSONAL__);
    }
    return (__FDB_LICENSE_TYPE_PERSONAL__);
}
string FDBfLicenseTypeIndexToStr(const int i_license_type_index)
{
    switch(i_license_type_index)
    {
        case __FDB_LICENSE_TYPE_PERSONAL__:
        {
            return (string("personal"));
        }break;
        case __FDB_LICENSE_TYPE_ENTERPRISE__:
        {
            return (string("enterprise"));
        }break;
        case __FDB_LICENSE_TYPE_PROFESSIONAL__:
        {
            return (string("professional"));
        }break;
    }
}
int FDBfLicenseMediumStrToIndex(const char *medium_str)
{
    if(strcmp(medium_str,"file") == 0)
    {
        return (__FDB_LICENSE_VERIFY_LICENSE__);
    } else if(strcmp(medium_str,"smartdog") == 0) {
        return (__FDB_LICENSE_VERIFY_SMART_DOG__);
    }
}
string FDBfLicenseMediumIndexToStr(const int medium_index)
{
    switch(medium_index)
    {
        case __FDB_LICENSE_VERIFY_LICENSE__:
        {
            return (string("file"));
        }break;
        case __FDB_LICENSE_VERIFY_SMART_DOG__:
        {
            return (string("smartdog"));
        }break;
    }
}
