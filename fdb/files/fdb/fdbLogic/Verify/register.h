#pragma once

#include "Verifylib.h"

#include <string>

#include <jsonite/jsonite.h>

using namespace std;

/*包含所有可用服务器的域名和ip地址*/
static const char* g_pp_server_host_names[] = {
    "rs.na94.com"
    ,"fdsrd.xicp.net"
    ,"fulldatasoft.com"
    ,"fulldatasoft.oicp.net"
    ,"115.28.167.218"
    ,"199.182.233.71"
    ,"113.10.240.232"
};
static const char* g_pp_server_http_domains[] = {
    "rs.sysfix.cn" /*阿裡云oss*/
    ,"rs.na94.com"
    ,"fdsrd.xicp.net"
    ,"fulldatasoft.com"
    ,"fulldatasoft.oicp.net"
};
static const int g_server_port = 5573;

/*!
 * \brief 在所有我们自己的此端口的服务开着的服务器中返回一个可用的ip地址,憑證是指定端口有握手交換
 * \param i_port
 * \return
 */
FDBInterface string FDBfGetAvailableServerAddr(const int i_port);

/*!
 * @brief 獲取一個可用的http 域名,憑證是指定域名根目錄有handshake.html寫著OK!
 */
FDBInterface string FDBfGetAvailabelHttpDomain();

/*!
 * \brief fAutoFetchLicense 自动在服务端查找这个机器的注册过的痕迹,然后获取许可证
 * \return license的内容
 */
string fAutoFetchLicense(const char *product_name
                         , const int min_version
                         , const char *product_language
                         , const char *product_type
                         , const string &dog_uid = "");

/*!
 * \brief fSendRecvRegisterRequest
 * \param provider_infos
 * \return -1,错误原因.0,许可证内容
 */
pair<int,string> fSendRecvRegisterRequest(CJsonite* provider_infos);

void fShowRegister(const char* product_name
                   , const int min_version
                   , const char* product_language
                   , const char* product_type
                   , const char *license_medium
                   , vector<long> *corrupt_dongles_handle);
