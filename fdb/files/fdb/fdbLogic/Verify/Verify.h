﻿/*******************************************************************
 *  FDB - Full Data Box -- FullDataSoft Company Foudation Library
 *
 *  author:Took
 *  changer:shawhen
 *
 *  Requirement: Qt
 *
 *  aes.h -- function unit:
 *      \1.验证license文件的合法性
 *  copyright:ShangHai JiaYu 2013
 *
 * dev 0.2.1 2013-03-27
 * stable 0.2.2 2013-03-29 verify cpu,ide,bios
 * stable 0.3.0 2013-03-29 popup auto register window
 *******************************************************************/

#pragma once

#include "Verifylib.h"

#include <fdb/fdb.h>

#include <string>

using namespace std;

class CFDBExtendSetting;
FDBInterface extern CFDBExtendSetting* gp_extend_setting;
FDBInterface extern CJsonite* gp_product_infos; //许可证文件的一些信息

class FDBInterface CFDBExtendSetting {
private:
    CJsonite* m_extend_setting;
public:
    string m_product_name;
    int m_min_version;
    string m_product_language;
    string m_product_type;
    int m_auto_close;
    int m_auto_register;
    string m_license_medium;
    /*!
     * \brief CFDBExtendSetting
     * \param extend_setting_buf 服务器端的原始扩展配置文件内容
     * \param len
     */
    CFDBExtendSetting();
    void constructSetting(const char* extend_setting_buf,const int len);

    void refreshSetting();
    string pathSeparator(); /*路徑分隔符*/
    string physicalDriverPath(int i); /*傳入物理磁碟編號,返回他的路徑,平臺無關*/
    /*!
     * @brief 請不要在外部析構,在再次调用此间接口之后将不可用
     */
    CJsonitePayload* getSpecialValue(const char* key); /*獲取指定key的值*/
};

/*! #0
 * @brief 验证license文件的合法性,并且构建全局的license配置,所有验证函数的主入口
 * @param p_release_time 软件发行的时间,格式为yyyy-mm-dd,比如1991年10月5号(1991-10-05)
 *        license中的生效时间不能早于这个时间,主要用来在时间上淘汰旧版本license
 * @param pc_valid_license_product_name 允许运行产品的许可证产品名称
 * @param i32_valid_license_version 可以允许此版本软件运行的最低许可证版本,主要用来在版本上淘汰其他版本license
 * @param i32_valid_license_language 允许运行软件的许可证语言版本,__FDB_LANGUAGE_CHINESE,__FDB_LANGUAGE_ENGLISH__等
 *										可用|进行操作
 * @param i32_valid_license_type 此版本软件需要许可证是什么样的许可证类型 __FDB_LICENSE_TYPE_PERSONAL__,__FDB_LICENSE_TYPE_ENTERPRISE__等
 *										可用|进行操作
 * @param b_auto_close	验证失败后是否直接关闭程序
 * @param b_auto_register 验证失败后自动弹出注册对话框,但这种事情只会发生一次,然后就会根据b_auto_close决定是否
 *							关闭程序或者关闭注册对话框
 * @param i32_verify_method 验证手段,有license文件验证,smart dog验证,可使用|进行操作,即多重验证
 
 * @return 成功1,失败0
 */
FDBInterface int
FDBfVerifyLicense(const char* product_name
                  , const int min_version
                  , const char *product_language = "chinese"
                  , const char *product_type = "personal"
                  , const int auto_close = 0
                  , const int auto_register = 1
                  , const char *license_medium = "file"
                  , const int32_t i32_dog_passwd_1 = 0xEF3EFBA7L
        , const int32_t i32_dog_passwd_2 = 0xE3C779D1L
        , const int32_t i32_dog_passwd_3 = 0x23FF25FL
        , const int32_t i32_dog_passwd_4 = 0x1148995FL);

/*! #4
 * @brief 转发接口至#1
 */
FDBInterface int DEPRECATE_SUPPORTED
FDBfVerifyLicense(const char* pc_valid_license_product_name
                  , const int32_t i32_valid_license_min_version
                  , const int32_t i32_valid_license_language = __FDB_LANGUAGE_CHINESE__
        , const int32_t i32_valid_license_type = __FDB_LICENSE_TYPE_PERSONAL__
        , const int32_t i32_verify_interface = __FDB_LICENSE_VERIFY_INTERFACE_GUI__
        , const int b_auto_close = __FDB_LICENSE_AUTO_CLOSE__
        , const int b_auto_register = __FDB_LICENSE_AUTO_REGISTER__
        , const int32_t i32_verify_method = __FDB_LICENSE_VERIFY_LICENSE__
        , const int32_t i32_dog_passwd_1 = 0xEF3EFBA7L
        , const int32_t i32_dog_passwd_2 = 0xE3C779D1L
        , const int32_t i32_dog_passwd_3 = 0x23FF25FL
        , const int32_t i32_dog_passwd_4 = 0x1148995FL
        );

/*! #1
 * @brief 实际上这是一个转发接口(#0),已不受支持,release_time将由build自动指定
 */
FDBInterface int DEPRECATE_SUPPORTED
FDBfVerifyLicense(const char* pc_release_time
                  ,const char* pc_valid_license_product_name
                  , const int32_t i32_valid_license_min_version
                  , const int32_t i32_valid_license_language = __FDB_LANGUAGE_CHINESE__
        , const int32_t i32_valid_license_type = __FDB_LICENSE_TYPE_PERSONAL__
        , const int32_t i32_verify_interface = __FDB_LICENSE_VERIFY_INTERFACE_GUI__
        , const int b_auto_close = __FDB_LICENSE_AUTO_CLOSE__
        , const int b_auto_register = __FDB_LICENSE_AUTO_REGISTER__
        , const int32_t i32_verify_method = __FDB_LICENSE_VERIFY_LICENSE__
        , const int32_t i32_dog_passwd_1 = 0xEF3EFBA7L
        , const int32_t i32_dog_passwd_2 = 0xE3C779D1L
        , const int32_t i32_dog_passwd_3 = 0x23FF25FL
        , const int32_t i32_dog_passwd_4 = 0x1148995FL
        );

/*! #2
 * @brief 使用一个cjsonite对象指定参数,实际上将转发至#0
 * 受支持的参数有
 *  "language":"chinese"或者"english",表示产品的语种
 *  "license_type": "personal"或者"enterprise",表示产品的类型
 *  "auto_close_app":"yes"或者"no",验证许可证失败后自动关闭应用程式
 *  "auto_register":"yes"或者"no",验证许可证失败后自动弹出注册(是命令行还是gui,将由函数自行决定)
 *  "license_medium":"file"或者"smartdog",许可证的存储媒介(目前受支持的有文件或者smart狗)
 */
FDBInterface int
FDBfVerifyLicense(const char* product_name
                  ,const int min_version
                  ,CJsonite* args);

/*! #3
 * @brief 支持简便验证参数,将构成cjsonite参数对象转发至#2
 * args示例如下:
 *  "chinese(中文)|personal(个人版)|close(自动关闭)|register(自动注册)|smartdog(软件狗)"
 */
FDBInterface int
FDBfVerifyLicense(const char* product_name,const int license_min_version,const char* args);

/*!
 * \brief fVerifyLicenseBuf 验证一个许可证对象(由加密狗和文件许可证抽象出的对象)
 * \param pure_license_buf
 * \param buf_size
 * \param product_name
 * \param min_version
 * \param product_language
 */
EVerifyErrorCode fVerifyLicenseObject(CJsonite *license_object
                                      , const char* product_name, const int min_version
                                      , const char* product_language, const char *product_type
                                      , CJsonite* product_infos, CFDBExtendSetting **extend_setting);

/*!
 * \brief fShowErr 显示错误消息,命令行时消息写向out,默认为stderr
 * \param out
 * \param msg
 */
void fShowErr(const char *msg, FILE* out);

/*!
 * \brief fSaveLicenseBuf 存储一个许可证缓存到介质上(加密狗亦或文件...)
 * \param license_buf
 * \param len
 * \param license_medium
 */
void fSaveLicenseBuf(const char *product_name, const char* license_buf, const int len, const char* license_medium
                     ,long dongle_handle = 0);

void fEraseLicense(const char *product_name, const char* license_medium
                   , const int dog_pw1, const int dog_pw2, const int dog_pw3, const int dog_pw4);

FDBInterface int FDBfLangStrToIndex(const char* pc_language);
FDBInterface std::string FDBfLangIndexToStr(const int i_lang_index);

FDBInterface int FDBfLicenseTypeStrToIndex(const char* pc_license_type);
FDBInterface std::string FDBfLicenseTypeIndexToStr(const int i_license_type_index);

FDBInterface string FDBfLicenseMediumIndexToStr(const int medium_index);
FDBInterface int FDBfLicenseMediumStrToIndex(const char* medium_str);
