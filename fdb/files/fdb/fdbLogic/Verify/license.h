#pragma once

#include "Verify.h"

/*!
 * \brief fSaveLicenseFile
 * \param product_name
 * \param license_buf
 * \param len
 * \return 错误码,0正常,-1失败
 */
int fSaveLicenseFile(const char* product_name,const char* license_buf,const int len);

/*!
 * \brief fVerifyLicenseFile 验证文件型许可证
 * \return 错误码
 */
EVerifyErrorCode fVerifyLicenseFile(const char* product_name
                                    , const int min_version
                                    , const char *product_language
                                    , const char *product_type
                                    , CJsonite* product_infos, CFDBExtendSetting **extend_setting);
