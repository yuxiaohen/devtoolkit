#pragma once

#include "Verify.h"

EVerifyErrorCode fVerifyMemory(const char* product_name
                               , const int min_version
                               , const char* product_language
                               , const char* product_type
                               , const char* license_buf, const int len
                               , CJsonite* gp_product_infos, CFDBExtendSetting** gp_extend_setting);
