﻿/*******************************************************************
 *  FDB - Full Data Box -- FullDataSoft Company Foudation Library
 *
 *  author:shawhen
 *
 *  Requirement: ansi c
 *
 *  utilities.h -- function unit:
 *      \1.解析加密license文件
 *      \2.从加密的license中提取键对应的值
 *      \3.判断是否是闰年
 *      \4.大端buffer转为本机整型值
 *
 *  copyright:ShangHai JiaYu 2013
 *
 *  stable version:0.2.2 2013-03-20
 *  stable version:0.3.2 2013-05-27
 *  stable version:0.3.3 2013-06-17 replace MACROS to #pragma once
 *******************************************************************/


#include <stdint.h>

//fdb
#include "../../fdbPrimitive/Crypto/Crypto.h"
#include "../../fdb.h"

/*!
 * @brief 解密加密过的License.Key,并返回其中的内容
 */
//FDBInterface char* FDBfParseEncryptedLicenseFile(const char* p_license_file_path
//                                 ,const char* p_decrypt_key);
//{
//    qDebug() << "application path: " << QCoreApplication::applicationDirPath();
//    FILE* pLicenseFile = NULL;
//    pLicenseFile=fopen(strFilePath.toLocal8Bit().constData(),"rb");
//    if(pLicenseFile == NULL) {
//#ifdef __WRITE_ERROR_LOG__
//        FDB::Exception::fWriteErrorLog( QString("open license file:%1 failed").arg(strFilePath) );
//#endif
//        return (QByteArray(""));
//    }
//    //get license file size
//    fseek(pLicenseFile,0,SEEK_END);
//    int iLicenseFileSize = ftell(pLicenseFile);
//    rewind(pLicenseFile);
//    QByteArray baTotalMsg(iLicenseFileSize,0);
//    if( fread(baTotalMsg.data(),1,iLicenseFileSize,pLicenseFile) != iLicenseFileSize ) {
//        printf("read license file content in iLicenseFileSize:%d error\r\n",iLicenseFileSize);
//    }

//    //calculate real key
//    QString strRealKey; strRealKey.fill(0,16);  //the key is used to calculate aes-128-cbc,so now,keep it 16
//    for(int i=0;i<(16<strBasicDecryptedKey.size()?16:strBasicDecryptedKey.size());++ i) {
//        strRealKey[i] = strBasicDecryptedKey.at(i).toLatin1() - 3;
//    }

//    //decrypt license key wrapper and keyChain
//    const int iEncryptedKeyWrapperSize = 48;   //encrypted key size:48 bytes
//    QByteArray baEncryptedKeyWrapper = baTotalMsg.left(iEncryptedKeyWrapperSize);    //key:***-****-*****|
//    //strRealKey:fulldatasoft
//    QByteArray baDecryptedKeyWrapper
//            = FDB::AES::fDecryptAES128CBC(baEncryptedKeyWrapper,strRealKey.toLocal8Bit());
//    QString strSourceKeyChain = FDB::Parse::fParsePeerMsg(
//                QString(baDecryptedKeyWrapper)
//                ,QString("key") );

//    //decrypt license content
//    QByteArray baTwoTimeEncryptedLicenseContent = baTotalMsg.mid(iEncryptedKeyWrapperSize);
//    QByteArray baOneTimeEncryptedLicenseContent
//            = FDB::AES::fDecryptAES128CBC(baTwoTimeEncryptedLicenseContent,strRealKey.toLocal8Bit());
//    QByteArray baSourceLicenseContent = FDB::AES::fDecryptAES128CBC(baOneTimeEncryptedLicenseContent
//                                                                    ,strSourceKeyChain.toLocal8Bit() );
//#if __FDB_DEBUG__ == 1
//    qDebug() << __FILE__ << __FUNCTION__ << __LINE__
//             << "encrypted key wrapper: " << baEncryptedKeyWrapper.toHex();
//    qDebug() << __FILE__ << __FUNCTION__ << __LINE__
//             << "|decrypted key: " << strSourceKeyChain.toLocal8Bit().toHex();
//    qDebug() << __FILE__ << __FUNCTION__ << __LINE__
//             << "|two time encrypted license content:" << baTwoTimeEncryptedLicenseContent.toHex();
//    qDebug() << __FILE__ << __FUNCTION__ << __LINE__
//             << "|one time encrypted license content: " << baOneTimeEncryptedLicenseContent.toHex();
//    qDebug() << __FILE__ << __FUNCTION__ << __LINE__
//             << "|source license content: " << baSourceLicenseContent;
//#endif
//    return (QByteArray(baDecryptedKeyWrapper.constData())+baSourceLicenseContent);
//}
/////*从加密的License.Key中摘取出对应key的value*/
//inline QString fPickupLicenseInfo(const QString& strFilePath,const QString& strKey
//                                  ,const QString& strBasicDecryptedKey = QString("ixoogdwdvriw"))
//{
//    QByteArray baSourceLicense = FDB::Utilities::fParseEncryptedLicenseFile(strFilePath);
//    QString strValue = FDB::Parse::fParsePeerMsg(QString(baSourceLicense),strKey);
//    return (strValue);
//}
