#pragma once

#include "Verify.h"

/*!
 * \brief fVerifyDongle 验证软件狗许可证,若验证成功,将设定product_infos和extend_setting
 * \param app_id
 * \param i32_dog_passwd_1
 * \param i32_dog_passwd_2
 * \param i32_dog_passwd_3
 * \param i32_dog_passwd_4
 * \param product_infos
 * \param extend_setting
 * \return
 */
EVerifyErrorCode fVerifyDongle(const char* product_name
                  , const int min_version
                  , const char* product_language
                  , const char* product_type
                  , const int32_t i32_dog_passwd_1
                  , const int32_t i32_dog_passwd_2
                  , const int32_t i32_dog_passwd_3
                  , const int32_t i32_dog_passwd_4
                  , vector<string> *corrupt_dongles_uid, CJsonite* product_infos, CFDBExtendSetting **extend_setting);

void fEraseSmartDogLicense(const char* product_name
                           ,const int dog_pw1,const int dog_pw2,const int dog_pw3,const int dog_pw4);

void fSaveLicenseSmartDog(const char* product_name, const void* license_buf, const int license_size, long dongle_handle);

long fGetDongleHandle(const char* app_id
    , const long pw1, const long pw2, const long pw3, const long pw4
    , const string& str_uid);
