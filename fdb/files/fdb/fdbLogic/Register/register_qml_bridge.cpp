﻿#if !defined(__FDB_CONSOLE_LINK__)

#ifndef Q_MOC_RUN
    #include <boost/bind.hpp>
#endif //Q_MOC_RUN

#include "../../fdbPrimitive/HardwareInfo/HardwareInfo.h"
#include "../../fdbPrimitive/Crypto/Crypto.h"
#include "../Register/licenseGenerator/licenseGenerator.h"
#include "../../fdbPrimitive/Compress/Compress.h"
#include "../../fdbLogic/NetworkLogic/NetworkLogic.h"

#include "register_qml_bridge.h"

//Qt
#include <QMessageBox>
#include <QFile>
#include <QString>
#include <QWidget>
#include <QQuickView>
#include <QQmlEngine>
#include <QQmlContext>

#include <QDebug>
//system
#if defined(_WIN32)
    #define NOMINMAX
    #include <Windows.h>
    #include <Windowsx.h>
#endif


CRegisterQmlBridge::CRegisterQmlBridge(QQuickView *p_quick_view
                                       , const char *pc_require_product_name
                                       , const uint32_t u32_require_language
                                       , const uint32_t u32_require_license_type
                                       , const uint32_t u32_require_license_version)
    : FDBCExecutionEngine(p_quick_view)
{
    this->m_p_quick_view->engine()->rootContext()->setContextProperty("register_qml_bridge",this);
    switch(u32_require_language)
    {
        case __FDB_LANGUAGE_CHINESE__:
        {
            this->m_str_language = QString("chinese");
        }break;
        case __FDB_LANGUAGE_ENGLISH__:
        {
            this->m_str_language = QString("english");
        }break;
        default:
        {
            this->m_str_language = QString("chinese");
        }break;
    }
    this->m_i32_require_license_language = u32_require_language;
    this->m_str_language = QString("chinese");
    this->m_str_product_name = QString(pc_require_product_name);
    this->m_i32_require_license_type = u32_require_license_type;
    this->m_i32_require_license_version = u32_require_license_version;

    QObject::connect( this,SIGNAL(closeMe())
                       ,this,SLOT(mfslCancelClicked()));

    this->m_p_obj_io_service_pool = FDBfIOServicePool_new_not_run(1);
    boost::thread(&FDBfIOServicePool_run,this->m_p_obj_io_service_pool);
//    int b_hide_system_title = 1;
//    #if defined(_WIN32)
//        OSVERSIONINFOEXA s_os_version_info_ex = {0};
//        s_os_version_info_ex.dwOSVersionInfoSize = sizeof(s_os_version_info_ex);
//        int b_get_version_result = GetVersionExA((LPOSVERSIONINFOA)&s_os_version_info_ex);
//        if(b_get_version_result)
//        {
//            if( (s_os_version_info_ex.dwMajorVersion == 5 && s_os_version_info_ex.dwMinorVersion == 2) //windows 2003 /r2
//            )
//            {
//                b_hide_system_title = 0;
//            }
//        }
//    #endif
//    if(b_hide_system_title)
//    {
//        this->setWindowFlags(Qt::FramelessWindowHint);  //设置无边框
//        this->setAttribute(Qt::WA_TranslucentBackground,true);
//    }
}

CRegisterQmlBridge::~CRegisterQmlBridge()
{
    FDBfIOServicePool_delete(&this->m_p_obj_io_service_pool);
}

__FDB_NETWORKLOGIC_INSTRUCTION_IO_HANDLER_SIGNATURE__(CRegisterQmlBridge::mfResponseHandler)
{
    if(b_instruction_data_unpacked)
    {
        boost::shared_ptr<FDBSInstruction> sp_instruction_from_remote = pair_io_result.first;
        boost::shared_ptr<std::vector<char> > sp_instruction_data_from_remote = pair_io_result.second;
        if( strncmp(sp_instruction_from_remote->pc_addon,"rejected|",__FDB_COMMUNICATION_INSTRUCTION_ADDON_LEN__) == 0 )
        {
            emit errorMsgChanged( QString(sp_instruction_data_from_remote->data()) );
        } else if(strncmp(sp_instruction_from_remote->pc_addon,"accepted|",__FDB_COMMUNICATION_INSTRUCTION_ADDON_LEN__) == 0) {
            //注册成功了,保存license文件即可
            QString str_license_file_path = QString("%1_License.key").arg(this->m_str_product_name);
            QFile obj_license_file(str_license_file_path);
            if(obj_license_file.open(QIODevice::WriteOnly))
            {
                int64_t i64_write_bytes = obj_license_file.write(sp_instruction_data_from_remote->data(),sp_instruction_data_from_remote->size());
                if(i64_write_bytes != sp_instruction_data_from_remote->size())
                {
                    std::runtime_error("write license file failed\r\n");
                    return ;
                }
                obj_license_file.close();
            }
            emit closeMe();
        }
    } else {
        std::runtime_error("instruction data should auto unpack here");
        return ;
    }
}

void CRegisterQmlBridge::mfslRegisterClicked(const QVariantMap &ref_infos)
{
    FDBSRegisterProvideAndRequirement* p_s_register_provide_and_requirement
            = (FDBSRegisterProvideAndRequirement*)calloc(1,sizeof(FDBSRegisterProvideAndRequirement));

    strncpy(p_s_register_provide_and_requirement->pc_provide_user_name
            ,ref_infos.value("user_name").toByteArray().constData()
            ,__FDB_LICENSE_USER_NAME_LEN__);
    strncpy(p_s_register_provide_and_requirement->pc_provide_user_email
            ,ref_infos.value("user_email").toByteArray().constData()
            ,__FDB_LICENSE_USER_EMAIL_LEN__);
    strncpy( p_s_register_provide_and_requirement->pc_provide_key
            ,ref_infos.value("user_key").toByteArray().simplified().constData()
            ,__FDB_LICENSE_KEY_LEN__);
    std::string str_hd_sn = FDBfHardware_get_hd_sn();
    std::string str_cpu_sn = FDBfHardware_get_cpu_id();
    std::string str_bios_sn = FDBfHardware_get_bios_sn();
    strncpy(p_s_register_provide_and_requirement->pc_provide_hd_sn,str_hd_sn.data(),__FDB_LICENSE_HD_SN_LEN__);
    strncpy(p_s_register_provide_and_requirement->pc_provide_cpu_sn,str_cpu_sn.data(),__FDB_LICENSE_CPU_SN_LEN__);
    strncpy(p_s_register_provide_and_requirement->pc_provide_bios_sn,str_bios_sn.data(),__FDB_LICENSE_BIOS_SN_LEN__);
    const QByteArray ba_product_name = this->m_str_product_name.toUtf8();
    strncpy(p_s_register_provide_and_requirement->pc_require_product_name,ba_product_name.constData(),__FDB_LICENSE_PRODUCT_NAME_LEN__);
    p_s_register_provide_and_requirement->i32_require_license_language = this->m_i32_require_license_language;
    p_s_register_provide_and_requirement->i32_require_license_version = this->m_i32_require_license_version;
    p_s_register_provide_and_requirement->i32_require_license_type = this->m_i32_require_license_type;

    boost::shared_ptr<std::vector<char> > sp_instruction_data_buffer
            = FDBfRegister_provide_and_requirement_to_le_buffer(p_s_register_provide_and_requirement);
    free( p_s_register_provide_and_requirement );
    //处理发送内容:压缩
    FDBSInstruction s_instruction = {0};
    strncpy(s_instruction.pc_addon,"register|",__FDB_COMMUNICATION_INSTRUCTION_ADDON_LEN__);
    std::pair<boost::shared_ptr<std::vector<char> >,boost::shared_ptr<std::vector<char> > > pair_send_buffers
            = FDBfNetworkLogic_produce_send(&s_instruction
                                            ,sp_instruction_data_buffer->data()
                                            ,sp_instruction_data_buffer->size());



    uint32_t u32_total_send_buffer_len = pair_send_buffers.first->size() + pair_send_buffers.second->size();
    char* p_send_buffer = (char*)malloc( u32_total_send_buffer_len );
    memcpy(p_send_buffer,pair_send_buffers.first->data(),pair_send_buffers.first->size());
    memcpy(p_send_buffer+pair_send_buffers.first->size(),pair_send_buffers.second->data(),pair_send_buffers.second->size());

    boost::shared_ptr<boost::asio::io_service> sp_io_service
            = FDBfIOServicePool_get_io_service(this->m_p_obj_io_service_pool);
    boost::asio::ip::tcp::resolver rs_host_addr_resolver(*sp_io_service);
    boost::asio::ip::tcp::resolver::query rs_host_query(__FDB_REGISTER_SERVER_ADDRESS__,__FDB_REGISTER_SERVER_PORT__);
    boost::asio::ip::tcp::resolver::iterator ite_endpoint = rs_host_addr_resolver.resolve( rs_host_query );
    boost::asio::ip::tcp::resolver::iterator ite_endpoint_invalid;
    boost::asio::ip::tcp::endpoint chosen_endpoint;
    boost::shared_ptr<boost::asio::ip::tcp::socket> sp_socket(new boost::asio::ip::tcp::socket(*sp_io_service));

    try {
    boost::asio::ip::tcp::resolver::iterator ite_valid_endpoint
            = boost::asio::connect(*sp_socket,ite_endpoint,ite_endpoint_invalid);
    } catch (boost::system::system_error& se) {
        qDebug() << se.what();
        emit this->errorMsgChanged("Connect Host Failed");
        return ;
    }

    int b_auto_unpack_instruction_data = 1;
    bfuncIOHandler_t io_handler = boost::bind(&CRegisterQmlBridge::mfResponseHandler,this,b_auto_unpack_instruction_data,sp_socket,_1);
    std::vector<boost::asio::const_buffer> vec_buffers;
    vec_buffers.push_back( boost::asio::buffer(*pair_send_buffers.first) );
    vec_buffers.push_back( boost::asio::buffer(*pair_send_buffers.second) );
    boost::asio::async_write(*sp_socket,vec_buffers,boost::bind(&FDBfNetworkLogic_send_data_handler_to_recv_next_instruction
                                                                ,pair_send_buffers
                                                                ,sp_socket
                                                                ,b_auto_unpack_instruction_data
                                                                ,io_handler
                                                                ,_1,_2));
}

void CRegisterQmlBridge::mfslCancelClicked()
{
    this->m_p_quick_view->close();
}

#endif //console
