﻿#pragma once

#if !defined(__FDB_CONSOLE_LINK__)

#ifndef Q_MOC_RUN
#include <boost/asio.hpp>
#endif

#include "../Verify/Verify.h"
#include "../../fdbPrimitive/Type/int.h"
#include "../../fdbLogic/NetworkLogic/NetworkLogic.h"
#include "../../fdbPrimitive/IOKit/IOServicePool/IOServicePool.h"

#include <QDialog>
#include <QMouseEvent>
#include <QTimer>

namespace Ui {
class CRegisterDialog;
}

class FDBInterface CRegisterDialog : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(QString m_str_product_name READ productName WRITE setProductName NOTIFY productNameChanged)
    Q_PROPERTY(QString m_str_language READ language WRITE setLanguage NOTIFY languageChanged)
    
public:
    explicit CRegisterDialog(const char *pc_require_product_name
                             , const char *product_language
                             , const char *product_type
                             , const uint32_t u32_require_license_version
                             , const char* license_medium
                             , vector<long> *corrupt_dongles_handle
                             , QWidget *parent = 0);
    ~CRegisterDialog();

signals:
    void sigRegisterPageUrl(const QString url);
    void errorMsgChanged(const QString& ref_str_err_msg);  //错误信号
    void productNameChanged(const QString& ref_str_product_name);
    void languageChanged(const QString& ref_str_language);
	void closeMe();
private:
    Ui::CRegisterDialog *ui;
    int32_t m_i32_require_license_language;
    int32_t m_i32_require_license_version;
    int32_t m_i32_require_license_type;
    QString m_str_language;
    QString m_str_product_name;
    QString m_str_error_msg;
    string m_license_medium;
    vector<long>* m_corrupt_dongles_handle;

    QTimer m_load_timer;
    int m_load_seconds;
    bool m_page_loaded;
protected:
    void mfCalculateProperRegisterPageUrl(const QString& language);
public slots:
    void slLoadRegisterPage(const QString& url);
    void mfslAddJSWindowObject();
    void mfslWebViewCurrentPageLoadFinished(bool b_load_result);

    void mfslRegisterClicked_d(const QVariantMap& ref_infos);
    void mfslRegisterClicked(const QVariantMap& ref_infos);

    void mfslCancelClicked();
    void mfslOpenUrlUseNativeBrowser(const QString& ref_str_url);

    QString productName()
    { return (this->m_str_product_name); }
    void setProductName(const QString& str_product_name)
    { if(this->m_str_product_name != str_product_name)
        {this->m_str_product_name=str_product_name; emit productNameChanged(str_product_name);}
    }
    QString language()
    { return (this->m_str_language); }
    void setLanguage(const QString& str_language)
    { if(str_language != this->m_str_language)
        { this->m_str_language=str_language; emit languageChanged(str_language); }
    }
};

#endif //console
