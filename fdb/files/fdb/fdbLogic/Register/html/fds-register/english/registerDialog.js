﻿var pic_dir = "";
/*!
 * @brief 根据不同的产品设置不同的背景,并且设置注册,取消,购买,qq在线,网址
 */
function fLoadSetting()
{
	//根据语种改变样式表和网站,购买链接地址,以及是否显示qq
	var product_name = NativeRegisterDialog.m_str_product_name;
    product_name = product_name.toLowerCase();
    var product_language = NativeRegisterDialog.m_str_language;
    var website_link_element = document.getElementById("website_link");
    var purchase_button_element = document.getElementById("purchase_button");
    var purchase_url = "";
    switch(product_language) //设置是否显示qq在线,以及设计主图片地址images/language/,以及网站地址,以及输入的提示框
    {
    	case "chinese":
    	{
    		pic_dir = "images/chinese/";
    		// var qq_online_button_element = document.getElementById("qq_online_button");
    		// qq_online_button_element.onclick = function() {
						// 					    			NativeRegisterDialog.mfslOpenUrlUseNativeBrowser("http://wpa.qq.com/msgrd?v=3&uin=1253140355&site=qq&menu=yes");
						// 					    		};
    	}break;
        case "english":
        {
            pic_dir = "images/english/";
            // document.getElementById("qq_online_button").style.display="None";
            var input_name_input_element = document.getElementById("user_name_input_div").getElementsByTagName("input")[0];
            input_name_input_element.defaultValue = "please input your name";
            var input_email_input_element = document.getElementById("user_email_input_div").getElementsByTagName("input")[0];
            input_email_input_element.defaultValue = "please input your email";
            var input_key_input_element = document.getElementById("user_key_input_div").getElementsByTagName("input")[0];
            input_key_input_element.defaultValue = "please input your key";
        }
    	default:
    	{
    		// document.getElementById("qq_online_button").style.display="None";
    	}break;
    }
    /*语种对应的网站地址映射*/
    var lang_to_website_url = {
        chinese: "http://www.sysfix.cn"
        ,english: "http://www.fulldatasoft.com"
    };
    website_link_element.innerHTML = lang_to_website_url[product_language];
    website_link_element.onclick = function() {
        NativeRegisterDialog.mfslOpenUrlUseNativeBrowser(lang_to_website_url[product_language]);
    };
    /*中文产品名称对url的映射*/
    var zh_product_name_to_url_map = {
        elitemts:"http://www.sharebank.net.cn/soft/SoftBuy_46436.htm"
        ,elitewindowsrecovery:"http://www.sharebank.net.cn/soft/SoftBuy_46457.htm",elitedatarecovery:"http://www.sharebank.net.cn/soft/SoftBuy_46457.htm"
        ,elitebitlocker:"http://www.sharebank.net.cn/soft/SoftBuy_46844.htm"
        ,elitemod:"http://www.sharebank.net.cn/soft/SoftBuy_46468.htm"
        ,elitempg:"http://www.sharebank.net.cn/soft/SoftBuy_46470.htm"
        ,eliteavi:"http://www.sharebank.net.cn/soft/SoftBuy_46541.htm"
        ,elitem2ts:"http://www.sharebank.net.cn/soft/SoftBuy_47389.htm"
        ,elitem2t:"http://www.sharebank.net.cn/soft/SoftBuy_47360.htm"
        ,elitemxf:"http://www.sharebank.net.cn/soft/SoftBuy_47394.htm"
        ,elitevmfs:"http://www.sharebank.net.cn/soft/SoftBuy_47399.htm"
        ,elitemov:"http://www.sharebank.net.cn/soft/SoftBuy_47408.htm"
        ,elitesqlite:"http://www.sharebank.net.cn/soft/SoftBuy_47450.htm"
        ,elitedahua:"http://www.sharebank.net.cn/soft/SoftBuy_46467.htm"
        ,elitexen:"http://www.sharebank.net.cn/soft/SoftBuy_46463.htm"
        ,eliteh264:"http://www.sharebank.net.cn/soft/SoftBuy_46584.htm"
	    ,elitezfy:"http://www.sharebank.net.cn/soft/SoftBuy_47559.htm"
	    ,elitexc:"http://www.sharebank.net.cn/soft/SoftBuy_47560.htm"
	    ,elitehaikang:"http://www.sharebank.net.cn/soft/SoftBuy_47585.htm"
	    ,elitejvs:"http://www.sharebank.net.cn/soft/SoftBuy_47589.htm"
	    ,elitedcm:"http://www.sharebank.net.cn/soft/SoftBuy_47590.htm"
	    ,elitedat:"http://www.sharebank.net.cn/soft/SoftBuy_47591.htm"
	    ,elitevvf:"http://www.sharebank.net.cn/soft/SoftBuy_47592.htm"
	    ,elitezwsj:"http://www.sharebank.net.cn/soft/SoftBuy_47595.htm"
	    ,elitealrs:"http://www.sharebank.net.cn/soft/SoftBuy_4.7597.htm"
	    ,elitephonevideo:"http://www.sharebank.net.cn/soft/SoftBuy_47604.htm"
	    ,eliteandroidrecovery:"http://www.sharebank.net.cn/soft/SoftBuy_47605.htm"
    };
    /*英文产品的名称对应的购买地址*/
    var en_product_name_to_url_map = {
        elitemts: "https://www.plimus.com/jsp/buynow.jsp?contractId=2287323"
        ,elitewindowsrecovery:"https://www.plimus.com/jsp/buynow.jsp?contractId=3182936",elitedatarecovery:"https://www.plimus.com/jsp/buynow.jsp?contractId=3182936"
        ,elitemod:"https://www.plimus.com/jsp/buynow.jsp?contractId=3180624"
        ,elitempg:"https://www.plimus.com/jsp/buynow.jsp?contractId=3180630"
        ,eliteavi:"https://www.plimus.com/jsp/buynow.jsp?contractId=3204900"
        ,elitem2ts:"https://www.plimus.com/jsp/buynow.jsp?contractId=3210548"
        ,elitem2t:"https://www.plimus.com/jsp/buynow.jsp?contractId=3210552"
        ,elitemxf:"https://www.plimus.com/jsp/buynow.jsp?contractId=3214300"
        ,elitedahua:"https://www.plimus.com/jsp/buynow.jsp?contractId=3214302"
    };
    /*语种对应要取的产品名称到url的映射*/
    var lang_to_product_url_map = {
        chinese: zh_product_name_to_url_map
        ,english: en_product_name_to_url_map
    };
    purchase_url = lang_to_product_url_map[product_language][product_name];
    purchase_button_element.onclick = function() {
    										NativeRegisterDialog.mfslOpenUrlUseNativeBrowser(purchase_url);
    									};

    //根据不同产品和语种设置产品名称
    var zh_product_name_to_display_product_name = {
        elitemts:"赤兔MTS视频恢复"
        ,elitewindowsrecovery:"赤兔Windows数据恢复"
        ,elitemod:"赤兔MOD视频恢复"
        ,elitempg:"赤兔MPG视频恢复"
        ,eliteavi:"赤兔AVI视频恢复"
        ,elitem2ts:"赤兔M2TS视频恢复"
        ,elitem2t:"赤兔M2T视频恢复"
        ,elitemxf:"赤兔MXF视频恢复"
        ,elitedahua:"赤兔大华监控视频恢复"
        ,elitehaikang:"赤兔海康监控视频恢复"
       ,eliteandroidrecoverypro:"赤兔安卓数据恢复专业版"
    };
    var lang_to_display_product_name = {
        chinese: zh_product_name_to_display_product_name
    };
    var product_name_element = document.getElementById("product_name");
    var product_name_to_display = lang_to_display_product_name[product_language][product_name];
//    console.log(product_name_to_display)
    if(product_name_to_display)
    {
        product_name_element.innerHTML=product_name_to_display;
    } else {
        product_name_element.innerHTML=product_name;
    }

    fNormalButton(document.getElementById("register_button"));
    fNormalButton(document.getElementById("cancel_button"));
    fNormalButton(document.getElementById("purchase_button"));

    //出错信息信号-槽
    console.log('connect error tip');
    NativeRegisterDialog.errorMsgChanged.connect(fShowErrorTip);
}

/*!
 * @brief 当输入框点击时,若其中的文字是缺省消息,此时清空输入框
 */
function fHideInputTip(inputbox_element)
{
	inputbox_element.style.backgroundImage="url(images/line_bg_active.png)";
	var input = inputbox_element.getElementsByTagName("input")[0];	
	input.style.color="black";
	if(input.value===input.defaultValue)
	{
		input.value="";
	}
}
/*!
 * @brief 当输入框失去焦点时,若其中的文字为空,则填入缺省消息,并显暗背景框
 */
function fShowInputTip (inputbox_element) 
{
	inputbox_element.style.backgroundImage="url(images/line_bg_normal.png)";
	var input = inputbox_element.getElementsByTagName("input")[0];
	inputbox_element.style.backgroundImage="url(images/line_bg_normal.png)";
	if(input.value==="")
	{
		input.value=input.defaultValue;
		input.style.color="lightgray"
	}
}

/*!
 @brief 鼠标进入按钮时的行为,替换成hover类的图片
 */
function fHoverButton(button_element)
{
	var buttom_img_element = button_element.getElementsByTagName("img")[0];
	buttom_img_element.src=pic_dir+button_element.name+"_hover.png";
}
/*!
 @brief 鼠标离开按钮时的行为,替换成normal类的图片
 */
 function fNormalButton (button_element) 
 {
	var buttom_img_element = button_element.getElementsByTagName("img")[0];
	buttom_img_element.src=pic_dir+button_element.name+"_normal.png";
 }

/*!
 * @brief 点击注册按钮
 */
function fRegisterClick()
{
	var user_name_input_element = document.getElementById("user_name_input_div").getElementsByTagName("input")[0];
	var user_email_input_element = document.getElementById("user_email_input_div").getElementsByTagName("input")[0];
	var user_key_input_element = document.getElementById("user_key_input_div").getElementsByTagName("input")[0];
	if(user_name_input_element.value === user_name_input_element.defaultValue
		|| user_email_input_element.value === user_name_input_element.defaultValue
		|| user_key_input_element.value === user_key_input_element.defaultValue)
	{
		window.alert("请将信息填写完整");
		return ;
	}
	var infos = {
		"user_name": user_name_input_element.value
		,"user_email": user_email_input_element.value
		,"user_key": user_key_input_element.value
	};
	console.log("ui provide info:",infos);
	document.getElementById("overlay").style.display="block";
	NativeRegisterDialog.mfslRegisterClicked(infos);
}

/*!
 *@brief 错误信息提示
 */
function fShowErrorTip(err_msg)
{
	console.log("show error tip",err_msg);
	var error_tip_button_element = document.getElementById("error_tip_button");
	var error_tip_img_src  = "";
	if( err_msg === "Register key is invalid" )
    { //注册码不可用
        error_tip_img_src = pic_dir+"register_key_is_invalid.png";
    } else if( err_msg === "Connect Host Failed" ) { //连接主机失败
        error_tip_img_src = pic_dir+"connect_host_failed.png";
    } else if( err_msg === "Host TimedOut" ) { //主机响应超时
        error_tip_img_src = pic_dir+"connect_timeout.png";
    } else if( err_msg === "Product unmatched" ) { //注册产品不匹配
        error_tip_img_src = pic_dir+"product_unmatched.png";
    } else if( err_msg === "License Version unmatched" ) { //许可证版本不匹配
        error_tip_img_src = pic_dir+"license_version_unmatched.png";
    } else if( err_msg === "License Type unmatched" ) { //许可证类型不匹配
        error_tip_img_src = pic_dir+"license_type_unmatched.png";
    } else if( err_msg === "Language unmatched" ) { //产品语种不匹配
        error_tip_img_src = pic_dir+"license_language_unmatched.png";
    } else if( err_msg === "Server error" ) { //服务器出错
        error_tip_img_src = pic_dir+"server_error.png";
    }
    error_tip_button_element.style.display = "inline-block";
    error_tip_button_element.getElementsByTagName("img")[0].src=error_tip_img_src;
	error_tip_button_element.focus();

	document.getElementById("overlay").style.display="none";
}

/*!
 *@brief 错误提示显示面板点击或是去焦点
 */
function fHideErrorTipPanel(error_msg_display_element)
{
	error_msg_display_element.style.display="none";
}
