﻿#include "../../fdbPrimitive/IOKit/IOServicePool/IOServicePool.h"
#include "../../fdbLogic/NetworkLogic/NetworkLogic.h"

#include "../Qt/FDBExecutionEngine.h"

class CRegisterQmlBridge : public FDBCExecutionEngine {
    Q_OBJECT
    Q_PROPERTY(QString m_str_product_name READ productName WRITE setProductName NOTIFY productNameChanged)
    Q_PROPERTY(QString m_str_language READ language WRITE setLanguage NOTIFY languageChanged)

public:
    explicit CRegisterQmlBridge(QQuickView* p_quick_view
                                ,const char *pc_require_product_name
                             , const uint32_t u32_require_language
                             , const uint32_t u32_require_license_type
                             , const uint32_t u32_require_license_version);
    ~CRegisterQmlBridge();

public slots:
signals:
    void errorMsgChanged(const QString& ref_str_err_msg);  //错误信号
    void productNameChanged(const QString& ref_str_product_name);
    void languageChanged(const QString& ref_str_language);
    void closeMe();
private:
    int32_t m_i32_require_license_language;
    int32_t m_i32_require_license_version;
    int32_t m_i32_require_license_type;
    QString m_str_language;
    QString m_str_product_name;
    QString m_str_error_msg;
//    boost::asio::io_service m_obj_io_service;
    FDBCIOServicePool* m_p_obj_io_service_pool;
    __FDB_NETWORKLOGIC_INSTRUCTION_IO_HANDLER_SIGNATURE__(mfResponseHandler);

public slots:
    void mfslRegisterClicked(const QVariantMap& ref_infos);
    void mfslCancelClicked();

    QString productName()
    { return (this->m_str_product_name); }
    void setProductName(const QString& str_product_name)
    { if(this->m_str_product_name != str_product_name)
        {this->m_str_product_name=str_product_name; emit productNameChanged(str_product_name);}
    }
    QString language()
    { return (this->m_str_language); }
    void setLanguage(const QString& str_language)
    { if(str_language != this->m_str_language)
        { this->m_str_language=str_language; emit languageChanged(str_language); }
    }
};
