#include "licenseGenerator.h"

#include "../../../fdbPrimitive/Math/Math.h"

#include <stdexcept>

char*
FDBfRegister_license_generate(const FDBSLicenseBasis* p_s_license_basis)
{
    return (NULL);
}

boost::shared_ptr<FDBSLicenseBasis>
FDBfRegister_license_from_le_buffer(void* p_le_license_buffer)
{
    int32_t i32_struct_size = 0;
    if( ENDIANNESS == 'l' ) { //小端,万事大吉
        //由于历史原因,struct_size不在这个结构体的最前面...
        FDBfMath_le_array_to_host_value((char*)p_le_license_buffer+83208,4,&i32_struct_size);
        boost::shared_ptr<FDBSLicenseBasis> sp_license_basis(new FDBSLicenseBasis());
        //接收来的FDBSLicenseBasis比本地的fdb的FDBSLicenseBasis要大
        if(i32_struct_size <= 0 || i32_struct_size > sizeof(FDBSLicenseBasis))
        {
            i32_struct_size = sizeof(FDBSLicenseBasis);
        }
        memcpy(sp_license_basis.get(),p_le_license_buffer,i32_struct_size);
        return ( sp_license_basis );
    } else { //大端,shit
        std::runtime_error("unsupport big endian");
        return (boost::shared_ptr<FDBSLicenseBasis>());
    }
}

FDBInterface boost::shared_ptr<std::vector<char> >
FDBfRegister_license_to_le_buffer(FDBSLicenseBasis* p_s_license_basis)
{
    if(ENDIANNESS == 'l')  //小端
    {
        //由于历史原因,struct_size不在这个结构体的最前面...
        p_s_license_basis->i32_struct_size = sizeof(FDBSLicenseBasis);
        boost::shared_ptr<std::vector<char> > sp_le_buffer(new std::vector<char>(sizeof(FDBSLicenseBasis)));
        memcpy(sp_le_buffer->data(),p_s_license_basis,sizeof(FDBSLicenseBasis));
        return (sp_le_buffer);
    } else { //大端
        std::runtime_error("unsupport big endian");
        return (boost::shared_ptr<std::vector<char> >());
    }
}
