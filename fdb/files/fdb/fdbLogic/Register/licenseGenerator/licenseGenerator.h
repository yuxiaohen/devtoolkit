﻿#ifndef __fdb_license_generator_h_2013_11_05_12_58__
#define __fdb_license_generator_h_2013_11_05_12_58__

#include "licenseGeneratorlib.h"

#ifndef Q_MOC_RUN
    #include <boost/shared_ptr.hpp>
#endif

/*!
 * @brief 将传入的license basis的extend字段填充,然后经过相应的加密算法加密,返回加密后的密文(内存buffer密文)
 */
FDBInterface char*
FDBfRegister_license_generate(const FDBSLicenseBasis* p_s_license_basis);

/*!
 * @brief 接收纯粹的小端licensebasis内存区域,根据大小端将其自动转换为本地的FDBSlicenseBasis,此接口对传入的p_license_buffer
 *         有自主的处理权限(可释放,可修改,可读)
 */
FDBInterface boost::shared_ptr<FDBSLicenseBasis>
FDBfRegister_license_from_le_buffer(void *p_le_license_buffer);

/*!
 * @brief 将一个本机的license basis结构转为小端buffer
 */
FDBInterface boost::shared_ptr<std::vector<char> >
FDBfRegister_license_to_le_buffer(FDBSLicenseBasis *p_s_license_basis);

#endif //header
