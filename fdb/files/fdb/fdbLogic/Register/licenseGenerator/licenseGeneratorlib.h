#ifndef __fdb_license_generator_lib_h_2013_11_05_11_21__
#define __fdb_license_generator_lib_h_2013_11_05_11_21__

#include "../../Verify/Verifylib.h"
#include "../../../fdbPrimitive/Type/Type.h"
#include "../Register.h"

/*!
 * @brief license文件基本的信息
 */
#define __FDB_LICENSE_EXTEND_LEN__ 81920

#define __FDB_LICENSE_SPLITER__ "<----fu11d0t0s0ft---->"
#define __FDB_LICENSE_SPLITER_LEN__ 22

/*!
 * @brief
 * @note 会通过网络传输,处理时注意大小端问题
 */
extern "C" {
#pragma pack(push)
#pragma pack(1)

typedef struct FDBSLicenseBasis {
    char pc_license_key[__FDB_LICENSE_KEY_LEN__]; //key

    char pc_license_product_name[__FDB_LICENSE_PRODUCT_NAME_LEN__]; //产品名称

    int64_t i64_license_use_expire_time; //使用过期时间,精确到秒
    int64_t i64_license_every_time_demo; //每次打开可供运行多长时间,精确到秒

    int32_t i32_license_usb_vendor_id; //注册usb的vid
    int32_t i32_license_usb_product_id; //注册usb的pid
    char pc_license_usb_sn[__FDB_LICENSE_USB_SN_LEN__]; //注册usb的序列号

    char pc_license_hd_sn[__FDB_LICENSE_HD_SN_LEN__]; //注册硬盘序列号
    char pc_license_cpu_sn[__FDB_LICENSE_CPU_SN_LEN__]; //注册cpu的序列号
    char pc_license_bios_sn[__FDB_LICENSE_BIOS_SN_LEN__]; //注册bios的序列号

    int32_t i32_license_type; //许可证类型
    int32_t i32_license_version; //许可证版本
    int32_t i32_license_language; //语种

    char pc_extend[__FDB_LICENSE_EXTEND_LEN__]; //许可证扩展信息,比如DiskBooter:\\.\|LUStartNumber:0|LUEndNumber:100|
    int32_t i32_struct_size;
    #if defined(__cplusplus)
        FDBSLicenseBasis()
        {
            memset(this,0,sizeof(FDBSLicenseBasis));
            this->i32_struct_size = sizeof(FDBSLicenseBasis);
        }
    #endif
} FDBSLicenseBasis;

#pragma pack(pop)
}

#endif //header
