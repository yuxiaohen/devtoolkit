﻿#if !defined(__FDB_CONSOLE_LINK__)

#ifndef Q_MOC_RUN
#include <boost/bind.hpp>
#endif

#include "../../fdbPrimitive/HardwareInfo/HardwareInfo.h"
#include "../../fdbPrimitive/Crypto/Crypto.h"
#include <fdb/fdbPrimitive/Network/Network.h>

#include "../Verify/register.h"
#include "registerDialog.h"
#include "ui_registerDialog.h"

#include <smartdog/SmartUKeyApp.h>
#include <smartdog/SmartUKeyFileSystem.h>
#include "../Verify/dongle.h"

#include <QMessageBox>
#include <QFile>
#include <QString>
#include <QWidget>
#include <QWebFrame>
#include <QDesktopServices>
#include <QPropertyAnimation>
#include <QMovie>
#include <QtConcurrent>
#include <QHostInfo>
#include <QHostAddress>

#if defined(LINK_FDB_FROM_SOURCE)
    #include <QWebInspector>
#endif

#include <QDebug>
//system
#if defined(_WIN32)
    #define NOMINMAX
    #include <Windows.h>
    #include <Windowsx.h>
#endif

#include <jsonite/jsonite.h>

using namespace std;

CRegisterDialog::CRegisterDialog(const char *pc_require_product_name
                                 , const char* product_language
                                 , const char* product_type
                                 , const uint32_t u32_require_license_version
                                 , const char *license_medium
                                 , vector<long> *corrupt_dongles_handle
								 , QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CRegisterDialog)
{
    ui->setupUi(this);
    this->ui->webView->setVisible(false);
    QMovie* loading_movie = new QMovie(":/register_dialog_images/images/loading.gif");
    this->ui->status_label->setMovie(loading_movie);
    loading_movie->start();

    this->m_str_language = QString(product_language);

    this->m_i32_require_license_language = FDBfLangStrToIndex(product_language);
    this->m_str_product_name = QString(pc_require_product_name);
    this->m_i32_require_license_type = FDBfLicenseTypeStrToIndex(product_type);
    this->m_i32_require_license_version = u32_require_license_version;
    this->m_license_medium = string(license_medium);
    this->m_corrupt_dongles_handle = corrupt_dongles_handle;

    QString str_license_type = QString(product_type);
    this->setWindowTitle(pc_require_product_name+QString(" ")
                         +this->m_str_language+QString(" ")
                         +str_license_type+QString(" ")
                         +QString::number(u32_require_license_version));

//    #if defined(LINK_FDB_FROM_SOURCE)
//        this->ui->webView->page()->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled,true);
//        QWebInspector* p_obj_inspector = new QWebInspector();
//        p_obj_inspector->setPage(this->ui->webView->page());
//        p_obj_inspector->setVisible(true);
//    #endif

    QObject::connect( this->ui->webView->page()->mainFrame(),SIGNAL(javaScriptWindowObjectCleared())
                      ,this,SLOT(mfslAddJSWindowObject()) );

    QObject::connect( this->ui->webView->page(),SIGNAL(loadFinished(bool))
                      ,this,SLOT(mfslWebViewCurrentPageLoadFinished(bool)));
    QObject::connect(this->ui->webView->page(),&QWebPage::loadStarted,[=](){
        this->ui->wait_label->setText(QStringLiteral("载入注册界面...."));
    });
    QObject::connect(this->ui->webView->page(),&QWebPage::loadProgress,[=](int progress) {
        QString text = QStringLiteral("载入注册界面....");
        text += QString::number(progress)+QString("%");
        this->ui->wait_label->setText(text);
    });

    QObject::connect( this,SIGNAL(closeMe())
                      ,this,SLOT(mfslCancelClicked()));

    //处理注册页面的显示
    #if !defined(LINK_FDB_FROM_SOURCE)
        this->ui->webView->setContextMenuPolicy(Qt::NoContextMenu);
    #endif
    QObject::connect( this,&CRegisterDialog::sigRegisterPageUrl,this,&CRegisterDialog::slLoadRegisterPage );

    QtConcurrent::run(this,&CRegisterDialog::mfCalculateProperRegisterPageUrl,this->m_str_language);
}

void CRegisterDialog::mfCalculateProperRegisterPageUrl(const QString &language)
{
    auto http_host = FDBfGetAvailabelHttpDomain();
    QString url("http://%1/fds-register/%2/registerDialog.html");
    url = url.arg(http_host.data()).arg(language);
    emit CRegisterDialog::sigRegisterPageUrl(url);
}

void CRegisterDialog::slLoadRegisterPage(const QString &url)
{
    this->ui->wait_label->setText(QStringLiteral("马上就好了..."));
    this->ui->webView->setUrl( QUrl(url) );
//    this->ui->webView->setUrl(QUrl("qrc:/html/html/fds-register/chinese/registerDialog.html"));
    this->m_load_timer.setInterval(1000);
    this->m_load_timer.start();
    this->m_load_seconds = 0;
    QObject::connect(&this->m_load_timer,&QTimer::timeout,[=] () {
        ++ this->m_load_seconds;
        if(this->m_load_seconds >= 5)
        {
            this->m_load_timer.stop();
            //直接使用内置的注册页面
            if(this->m_page_loaded == false)
            {
                this->ui->webView->setUrl(QUrl("qrc:/html/html/fds-register/chinese/registerDialog.html"));
            }
        }
    });
}

CRegisterDialog::~CRegisterDialog()
{
    delete ui;
}

void CRegisterDialog::mfslAddJSWindowObject()
{
    this->ui->webView->page()->mainFrame()->addToJavaScriptWindowObject("NativeRegisterDialog",this);
}

void CRegisterDialog::mfslWebViewCurrentPageLoadFinished(bool b_load_result)
{
    this->ui->status_widget->setVisible(false);
    this->ui->webView->setVisible(true);
    this->m_page_loaded = true;
    if(b_load_result)
    {
        auto frame_content_size = this->ui->webView->page()->currentFrame()->contentsSize();
//        qDebug() << "load webview succeed"
//                 << "current frame contact size" << frame_content_size;
        //动画过渡到期望大小
        QPropertyAnimation* p_obj_animation = new QPropertyAnimation(this,"geometry");
        p_obj_animation->setDuration(470);
        p_obj_animation->setEasingCurve(QEasingCurve::OutQuart);
        p_obj_animation->setStartValue(QRect(this->x(),this->y(),this->width(),this->height()));
        if(this->x()-200 > 0 && this->y()-200 > 0)
        {
            p_obj_animation->setEndValue(QRect(this->x()-200,this->y()-200,frame_content_size.width(),frame_content_size.height()));
        } else {
            p_obj_animation->setEndValue(QRect(this->x(),this->y(),frame_content_size.width(),frame_content_size.height()));
        }
        p_obj_animation->start();
    } else {
        this->ui->webView->setUrl(QUrl("qrc:/html/html/load_register_dialog_failed.html"));
    }
}

void CRegisterDialog::mfslRegisterClicked_d(const QVariantMap &ref_infos)
{
    boost::shared_ptr<CJsonite> sp_obj_register_infos = boost::shared_ptr<CJsonite>(new CJsonite());
    sp_obj_register_infos->addPayload_string("user_name"
                                            ,ref_infos.value("user_name").toString().toUtf8().constData());
    sp_obj_register_infos->addPayload_string("user_email"
                                            ,ref_infos.value("user_email").toString().toUtf8().constData());
    sp_obj_register_infos->addPayload_string("user_key"
                                            ,ref_infos.value("user_key").toString().toUtf8().constData());

    string str_hd_sn = FDBfHardware_get_hd_sn();
    string str_cpu_sn = FDBfHardware_get_cpu_id();
    string str_bios_sn = FDBfHardware_get_bios_sn();
    sp_obj_register_infos->addPayload_string("hd_sn"
                                             ,str_hd_sn.data());
    sp_obj_register_infos->addPayload_string("cpu_sn"
                                             ,str_cpu_sn.data());
    sp_obj_register_infos->addPayload_string("bios_sn"
                                             ,str_bios_sn.data());
    //usb信息
    sp_obj_register_infos->addPayload_int("usb_vid",0);
    sp_obj_register_infos->addPayload_int("usb_pid",0);
    sp_obj_register_infos->addPayload_string("usb_sn","");

    sp_obj_register_infos->addPayload_string("product_name"
                                            ,this->m_str_product_name.toUtf8().constData());
    sp_obj_register_infos->addPayload_string("license_language"
                                            ,FDBfLangIndexToStr(this->m_i32_require_license_language).data());
    sp_obj_register_infos->addPayload_int("license_version"
                                            ,this->m_i32_require_license_version);
    sp_obj_register_infos->addPayload_string("license_type"
                                            ,FDBfLicenseTypeIndexToStr(this->m_i32_require_license_type).data());
    sp_obj_register_infos->addPayload_int("register_version",7);

    if(this->m_corrupt_dongles_handle->size() > 0)
    {
        for(int i=0;i < this->m_corrupt_dongles_handle->size();++ i)
        {
            long dongle_handle = this->m_corrupt_dongles_handle->at(i);
			char p_uid[33] = {0};
			SmartUKeyGetUid(dongle_handle,p_uid);
            sp_obj_register_infos->addPayload_string("smartdog_uid",p_uid);
            auto register_result = fSendRecvRegisterRequest(sp_obj_register_infos.get());
            if(register_result.first == 0)
            {
                string& license_file_buf = register_result.second;
                auto product_name_utf8 = this->m_str_product_name.toUtf8();
                fSaveLicenseBuf(product_name_utf8.constData()
                                ,license_file_buf.data(),license_file_buf.size()
                                ,this->m_license_medium.data()
                                ,dongle_handle);
                emit CRegisterDialog::closeMe();
                break;
            } else {
                emit CRegisterDialog::errorMsgChanged(register_result.second.data());
            }
        }
    } else {
        auto register_result = fSendRecvRegisterRequest(sp_obj_register_infos.get());
        if(register_result.first == 0)
        {
            string& license_file_buf = register_result.second;
            auto product_name_utf8 = this->m_str_product_name.toUtf8();
            fSaveLicenseBuf(product_name_utf8.constData()
                            ,license_file_buf.data(),license_file_buf.size()
                            ,this->m_license_medium.data());
            emit CRegisterDialog::closeMe();
        } else {
            emit CRegisterDialog::errorMsgChanged(register_result.second.data());
        }
    }
}

void CRegisterDialog::mfslRegisterClicked(const QVariantMap &ref_infos)
{
    QtConcurrent::run(this,&CRegisterDialog::mfslRegisterClicked_d,ref_infos);
}

void CRegisterDialog::mfslCancelClicked()
{
    this->close();
}

void CRegisterDialog::mfslOpenUrlUseNativeBrowser(const QString &ref_str_url)
{
    QDesktopServices::openUrl(QUrl(ref_str_url));
}

#endif //console
