﻿#ifndef __fdb_compress_h_2013_11_05_03_19__
#define __fdb_compress_h_2013_11_05_03_19__

#include "Compresslib.h"

#include "../../fdbPrimitive/Type/Type.h"

#include <vector>

#ifndef Q_MOC_RUN
    #include <boost/shared_ptr.hpp>
#endif

using namespace std;
///*!
// * @brief 压缩输入的源内容
// * @param i_compress_level 压缩等级,-1自动,1 -> 9 依次增加压缩比
// * @param p_u32_compressed_buffer_len 返回的压缩缓冲区的长度
// * @param p_u32_consumed_origin_buffer_len 本次调用处理了多少源内容
// * @return 分片的压缩内容
// *         p_u32_consumed_origin_buffer_len(即原内容被处理的长度)是否小于
// *         u32_origin_buffer_len(即原内容长度),此时即原内容并未压缩完,应重新调用此接口以再次压缩
// *          (此时应该偏移p_origin_buffer到p_origin_buffer+p_u32_consumed_origin_buffer_len继续上次的压缩
// *           注意每次调用p_u32_consumed_origin_buffer_len都只能表示本次调用消耗了多少原内容)
// *         null时表示压缩失败,
// */
//FDBInterface std::vector<std::pair<char *, uint32_t> > FDBfCompress_compress_buffer(const void* p_origin_buffer
//                                                , const uint32_t u32_origin_buffer_len
//                                                , const int i_compress_level
//                                                );

/*!
 * @brief 压缩输入的源内容
 * @return 整块的压缩内容,外部释放
 */
FDBInterface boost::shared_ptr<vector<char> >
FDBfCompress_compress_buffer(const void* p_origin_buffer
                             , const uint32_t u32_origin_buffer_len
                             , const int i_compress_level
                             );

///*!
// * @brief 解压缩输入的源内容,返回分片的内容
// * @param p_u32_uncompressed_buffer_len 返回的解压缓冲区的长度
// * @param p_u32_consumed_origin_buffer_len 本次调用处理了多少源内容
// */
//FDBInterface std::vector<std::pair<char *, uint32_t> > FDBfCompress_uncompress_buffer(const void* p_origin_buffer
//                                                                                      , const uint32_t u32_origin_buffer_len
//                                                                                      );

/*!
 * @brief 解压缩输入的源内容
 * @return 外部释放
 */
FDBInterface boost::shared_ptr<vector<char> >
FDBfCompress_uncompress_buffer(const void* p_origin_buffer
                               ,const uint32_t u32_origin_buffer_len);

#endif //header
