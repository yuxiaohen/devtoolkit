#include "Compress.h"

#include <zlib/zlib.h>

#include <stdlib.h>

boost::shared_ptr<vector<char> >
FDBfCompress_compress_buffer(const void *p_origin_buffer
                             , const uint32_t u32_origin_buffer_len
                             , const int i_compress_level)
{
    boost::shared_ptr<vector<char> > sp_total_compress_buffer(new vector<char>());
    z_stream strm;

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;

    int i_deflate_init_result = deflateInit(&strm, i_compress_level);
    if (i_deflate_init_result != Z_OK)
    {
        return (boost::shared_ptr<vector<char> >());
    }

    int b_error_occurred = 0;
    while(1)
    {
        sp_total_compress_buffer->resize( sp_total_compress_buffer->size()+__FDB_COMPRESS_CHUNK_SIZE__ );

        strm.avail_in = u32_origin_buffer_len - strm.total_in;
        strm.next_in = (unsigned char*)p_origin_buffer + strm.total_in;
        strm.avail_out = __FDB_COMPRESS_CHUNK_SIZE__;
        strm.next_out = (unsigned char*)sp_total_compress_buffer->data()+sp_total_compress_buffer->size()-__FDB_COMPRESS_CHUNK_SIZE__;
        int i_deflate_result = deflate(&strm,Z_FINISH);

        if(i_deflate_result == Z_STREAM_END) //压缩结束,截掉未利用的缓冲区
        {
            sp_total_compress_buffer->resize( sp_total_compress_buffer->size() - strm.avail_out );

            break;
        } else if(i_deflate_result == Z_STREAM_ERROR) {
            b_error_occurred = 1;
            break;
        } else if(i_deflate_result == Z_BUF_ERROR) { //输出耗竭
            if(strm.avail_in == 0) //输入消耗完了
            {
                break;
            }
        } else { //源数据未处理完,扩大压缩缓冲区,继续压缩
            continue;
        }
    }

     /* clean up and return */
    (void)deflateEnd(&strm);

    if(b_error_occurred)
    {
        return ( boost::shared_ptr<vector<char> >() );
    }

    return (sp_total_compress_buffer);
}

boost::shared_ptr<vector<char> >
FDBfCompress_uncompress_buffer(const void* p_origin_buffer
                               , const uint32_t u32_origin_buffer_len)
{
    boost::shared_ptr<vector<char> > sp_total_uncompress_buffer(new vector<char>(0,'\x00'));
    z_stream strm;

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;

    int i_inflate_init_result = inflateInit(&strm);
    if (i_inflate_init_result != Z_OK)
    {
        return (boost::shared_ptr<vector<char> >());
    }

    int b_error_occurred = 0;
    while(1)
    {
        sp_total_uncompress_buffer->resize( sp_total_uncompress_buffer->size()+__FDB_COMPRESS_CHUNK_SIZE__ );

        strm.avail_in = u32_origin_buffer_len - strm.total_in;
        strm.next_in = (unsigned char*)p_origin_buffer + strm.total_in;
        strm.avail_out = __FDB_COMPRESS_CHUNK_SIZE__;
        strm.next_out = (unsigned char*)sp_total_uncompress_buffer->data()+sp_total_uncompress_buffer->size()-__FDB_COMPRESS_CHUNK_SIZE__;
        int i_inflate_result = inflate(&strm,Z_FINISH);

        if(i_inflate_result == Z_STREAM_END) //解压缩完成,回收未使用的解压缩缓冲区
        {
            sp_total_uncompress_buffer->resize( sp_total_uncompress_buffer->size() - strm.avail_out );

            break;
        } else if(i_inflate_result == Z_STREAM_ERROR || i_inflate_result == Z_DATA_ERROR) {
            b_error_occurred = 1;
            break;
        } else if(i_inflate_result == Z_BUF_ERROR) { //输出不够了
            if(strm.avail_in == 0) //输入也消耗完了
            {
                break;
            }
        } else { //not end
            continue;
        }
    }

    /* clean up and return */
    (void)deflateEnd(&strm);

    if(b_error_occurred)
    {
        return (boost::shared_ptr<vector<char> >());
    }

    return (sp_total_uncompress_buffer);
}
