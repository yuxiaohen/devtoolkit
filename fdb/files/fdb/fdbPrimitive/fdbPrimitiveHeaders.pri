#Codec
include($$PWD/Codec/CodecHeaders.pri)

#Compress
include($$PWD/Compress/CompressHeaders.pri)

#Crypto
include($$PWD/Crypto/CryptoHeaders.pri)

#Database
#include($$PWD/Database/DatabaseHeaders.pri)

#Exception
include($$PWD/Exception/ExceptionHeaders.pri)

#Hardware Info
include($$PWD/HardwareInfo/HardwareInfoHeaders.pri)

#Hash
include($$PWD/Hash/HashHeaders.pri)

#IOKit
include($$PWD/IOKit/IOKitHeaders.pri)

#Library
include($$PWD/Library/LibraryHeaders.pri)

#Math
include($$PWD/Math/MathHeaders.pri)

#MemoryManagement
include($$PWD/MemoryManagement/MemoryManagementHeaders.pri)

#Netwrok
include($$PWD/Network/NetworkHeaders.pri)

#Text
include($$PWD/Text/TextHeaders.pri)

#Type
include($$PWD/Type/TypeHeaders.pri)
