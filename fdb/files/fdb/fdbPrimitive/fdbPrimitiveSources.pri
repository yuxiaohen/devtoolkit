#Codec
include($$PWD/Codec/CodecSources.pri)

#Compress
include($$PWD/Compress/CompressSources.pri)

#Crypto
include($$PWD/Crypto/CryptoSources.pri)

#Database
#include($$PWD/Database/DatabaseSources.pri)

#Exception
include($$PWD/Exception/ExceptionSources.pri)

#Hardware Info
include($$PWD/HardwareInfo/HardwareInfoSources.pri)

#Hash
include($$PWD/Hash/HashSources.pri)

#IOKit
include($$PWD/IOKit/IOKitSources.pri)

#Library
include($$PWD/Library/LibrarySources.pri)

#Math
include($$PWD/Math/MathSources.pri)

#MemoryManagement
include($$PWD/MemoryManagement/MemoryManagementSources.pri)

#Network
include($$PWD/Network/NetworkSources.pri)

#Text
include($$PWD/Text/TextSources.pri)

#Type
include($$PWD/Type/TypeSources.pri)
