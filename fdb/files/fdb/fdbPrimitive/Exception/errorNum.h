#ifndef __fdb_error_num_h_2013_08_30_01_22__
#define __fdb_error_num_h_2013_08_30_01_22__

#include <errno.h>

#include "Exceptionlib.h"

/*common custom error code*/
#define FDB_ERR_UNDEFINED -9999
#define FDB_ERR_MEM_NOT_ENOUGH -1
#define FDB_ERR_INVALID_DOT_PERCENT_IP_ADDR -2 //invalid dot percent ip address
#define FDB_ERR_CALL_TIMED_OUT -3 //timed out
#define FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR -4 //operate on a invalid FDBCIODevice->device,normally not open

#if defined(_WIN32)
    #include <Windows.h>
    #define FDB_ERR_FILE_NOT_FOUND ERROR_FILE_NOT_FOUND  //0x2,file not found
    #define FDB_ERR_ACCESS_DENIED ERROR_ACCESS_DENIED //0x5,access denied
    #define FDB_ERR_PARAMETER_IS_INCORRECT ERROR_INVALID_PARAMETER //0x57,parameter is incorrect
#elif defined(__APPLE__) || defined(__linux__)
    #include <unistd.h>
    /*for posix standard*/
    #if _POSIX_VERSION >= 200112L //IEEE 1003.1-2001
        #define FDB_ERR_FILE_NOT_FOUND ENOENT //0x02
        #define FDB_ERR_ACCESS_DENIED EACCES  //13
        #define FDB_ERR_PARAMETER_IS_INCORRECT EINVAL //22
    #else
        #error "Unsupport Platform"
    #endif
#else
    #error "Unsupported Platform"
#endif

#define FDB_ERR_MEM_NOT_ENOUGH_STR \
    "memory is not enough"
#define FDB_ERR_INVALID_DOT_PERCENT_IP_ADDR_STR \
    "invliad dot percent ip address"
#define FDB_ERR_CALL_TIMED_OUT_STR \
	"call function timed out"
#define FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR \
	"operate on a invalid FDBCIODevice->device,forgot open?"
#define FDB_ERR_FILE_NOT_FOUND_STR \
    "file or directory not found"
#define FDB_ERR_ACCESS_DENIED_STR \
    "permission denied"
#define FDB_ERR_PARAMETER_IS_INCORRECT_STR \
    "parametr is incorrect"

FDBInterface extern int giFDBErrNum;
FDBInterface extern char gacFDBErrStr[4096];

    #if defined(FDB_RECORD_ERR)
        #define FDBErrStr(format) \
            sprintf(gacFDBErrStr,"file:%s,function:%s,line:%d|" format ,__FILE__,__FUNCTION__,__LINE__);
        #define FDBErrStr_ONE_PARAM(format,param1) \
            sprintf(gacFDBErrStr,"file:%s,function:%s,line:%d|" format ,__FILE__,__FUNCTION__,__LINE__,param1);
        #define FDBErrStr_TWO_PARAM(format,param1,param2) \
            sprintf(gacFDBErrStr,"file:%s,function:%s,line:%d|" format ,__FILE__,__FUNCTION__,__LINE__,param1,param2);
        #define FDBErrStr_THREE_PARAM(format,param1,param2,param3) \
            sprintf(gacFDBErrStr,"file:%s,function:%s,line:%d|" format ,__FILE__,__FUNCTION__,__LINE__,param1,param2,param3);
        #define FDBErrStr_FOUR_PARAM(format,param1,param2,param3,param4) \
            sprintf(gacFDBErrStr,"file:%s,function:%s,line:%d|" format ,__FILE__,__FUNCTION__,__LINE__,param1,param2,param3,param4);
        #define FDBErrStr_FIVE_PARAM(format,param1,param2,param3,param4,param5) \
            sprintf(gacFDBErrStr,"file:%s,function:%s,line:%d|" format ,__FILE__,__FUNCTION__,__LINE__,param1,param2,param3,param4,param5);
    #else
        #define FDBErrStr(format)
        #define FDBErrStr_ONE_PARAM(format,param1)
        #define FDBErrStr_TWO_PARAM(format,param1,param2)
        #define FDBErrStr_THREE_PARAM(format,param1,param2,param3)
        #define FDBErrStr_FOUR_PARAM(format,param1,param2,param3,param4)
        #define FDBErrStr_FIVE_PARAM(format,param1,param2,param3,param4,param5)
    #endif

#endif //header
