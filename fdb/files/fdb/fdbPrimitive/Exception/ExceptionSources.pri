win32 {
    SOURCES += \
        $$PWD/dump.cpp
}

CONFIG(qt,qt) {
    SOURCES += \
    #gui
    $$PWD/gui/ccrashfeedbackdialog.cpp
}

SOURCES += \
    $$PWD/errorNum.cpp

RESOURCES += \
    $$PWD/gui/exception_images.qrc
