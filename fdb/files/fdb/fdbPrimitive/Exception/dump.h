﻿#ifndef __fdb_dump_h_2013_08_04_01_34__
#define __fdb_dump_h_2013_08_04_01_34__

#include "Exceptionlib.h"

#if defined(_WIN32)

#include <Windows.h>

#define DUMP_TRIGGER(FunName) long(__stdcall FunName)(struct _EXCEPTION_POINTERS*)

FDBInterface LONG WINAPI FDBfException_dump(struct _EXCEPTION_POINTERS* p_exception_info);
void _fDisableCRTUnhandleExceptionFilter();

//void fSetDumpTrigger(long(__stdcall *pfTrigger)(struct _EXCEPTION_POINTERS *));
FDBInterface void FDBfException_set_dump_trigger(DUMP_TRIGGER(pfTrigger));

#endif //_WIN32

#endif //header
