﻿#ifndef CCRASHFEEDBACKDIALOG_H
#define CCRASHFEEDBACKDIALOG_H

#if !defined(__FDB_CONSOLE_LINK__) && defined(QT_GUI_LIB)

#include <QDialog>

namespace Ui {
class CCrashFeedbackDialog;
}

class CCrashFeedbackDialog : public QDialog
{
    Q_OBJECT
private:
    QString m_str_product_name;
public:
    struct _EXCEPTION_POINTERS * m_p_exception_info;

public:
    explicit CCrashFeedbackDialog(QString str_product_name
                                  ,QWidget *parent = 0);
    ~CCrashFeedbackDialog();

private:
    Ui::CCrashFeedbackDialog *ui;
public slots:
    void mfslDump();
    void mfslVisitWebsite();
    void mfslTalkQQ();
};

#endif
#endif // CCRASHFEEDBACKDIALOG_H
