/********************************************************************************
** Form generated from reading UI file 'ccrashfeedbackdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CCRASHFEEDBACKDIALOG_H
#define UI_CCRASHFEEDBACKDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CCrashFeedbackDialog
{
public:
    QVBoxLayout *verticalLayout;
    QTextBrowser *textBrowser;
    QLabel *label;
    QTextEdit *desc_text_edit;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *commit_push_button;
    QPushButton *cancel_push_button;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *company_logo_push_button;
    QPushButton *company_name_push_button;
    QPushButton *qq_push_button;
    QPushButton *telephone_push_button;
    QPushButton *website_push_button;

    void setupUi(QDialog *CCrashFeedbackDialog)
    {
        if (CCrashFeedbackDialog->objectName().isEmpty())
            CCrashFeedbackDialog->setObjectName(QStringLiteral("CCrashFeedbackDialog"));
        CCrashFeedbackDialog->resize(827, 565);
        CCrashFeedbackDialog->setStyleSheet(QStringLiteral(""));
        CCrashFeedbackDialog->setModal(true);
        verticalLayout = new QVBoxLayout(CCrashFeedbackDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        textBrowser = new QTextBrowser(CCrashFeedbackDialog);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(textBrowser->sizePolicy().hasHeightForWidth());
        textBrowser->setSizePolicy(sizePolicy);
        textBrowser->setMaximumSize(QSize(16777215, 120));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font.setPointSize(10);
        textBrowser->setFont(font);
        textBrowser->setStyleSheet(QLatin1String("QTextBrowser {\n"
"	border:0;\n"
"	background:transparent;\n"
"}"));

        verticalLayout->addWidget(textBrowser);

        label = new QLabel(CCrashFeedbackDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font);

        verticalLayout->addWidget(label);

        desc_text_edit = new QTextEdit(CCrashFeedbackDialog);
        desc_text_edit->setObjectName(QStringLiteral("desc_text_edit"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        desc_text_edit->setFont(font1);

        verticalLayout->addWidget(desc_text_edit);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        commit_push_button = new QPushButton(CCrashFeedbackDialog);
        commit_push_button->setObjectName(QStringLiteral("commit_push_button"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font2.setPointSize(14);
        commit_push_button->setFont(font2);

        horizontalLayout->addWidget(commit_push_button);

        cancel_push_button = new QPushButton(CCrashFeedbackDialog);
        cancel_push_button->setObjectName(QStringLiteral("cancel_push_button"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font3.setPointSize(9);
        cancel_push_button->setFont(font3);

        horizontalLayout->addWidget(cancel_push_button);


        horizontalLayout_2->addLayout(horizontalLayout);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        company_logo_push_button = new QPushButton(CCrashFeedbackDialog);
        company_logo_push_button->setObjectName(QStringLiteral("company_logo_push_button"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(company_logo_push_button->sizePolicy().hasHeightForWidth());
        company_logo_push_button->setSizePolicy(sizePolicy1);
        company_logo_push_button->setMinimumSize(QSize(65, 44));
        company_logo_push_button->setMaximumSize(QSize(65, 44));
        company_logo_push_button->setStyleSheet(QLatin1String("QPushButton {\n"
"	border:0;\n"
"	background-image: url(:/exception_images/company_logo_normal.png);\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	border:0;\n"
"	\n"
"	background-image: url(:/exception_images/company_logo_active.png);\n"
"}"));
        company_logo_push_button->setFlat(true);

        horizontalLayout_3->addWidget(company_logo_push_button);

        company_name_push_button = new QPushButton(CCrashFeedbackDialog);
        company_name_push_button->setObjectName(QStringLiteral("company_name_push_button"));
        sizePolicy1.setHeightForWidth(company_name_push_button->sizePolicy().hasHeightForWidth());
        company_name_push_button->setSizePolicy(sizePolicy1);
        company_name_push_button->setMinimumSize(QSize(184, 24));
        company_name_push_button->setMaximumSize(QSize(184, 24));
        company_name_push_button->setStyleSheet(QLatin1String("QPushButton {\n"
"	border: 0;\n"
"	background-image: url(:/exception_images/company_name_normal.png);\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	border: 0;\n"
"	background-image: url(:/exception_images/company_name_active.png);\n"
"}"));
        company_name_push_button->setFlat(true);

        horizontalLayout_3->addWidget(company_name_push_button);

        qq_push_button = new QPushButton(CCrashFeedbackDialog);
        qq_push_button->setObjectName(QStringLiteral("qq_push_button"));
        sizePolicy1.setHeightForWidth(qq_push_button->sizePolicy().hasHeightForWidth());
        qq_push_button->setSizePolicy(sizePolicy1);
        qq_push_button->setMinimumSize(QSize(128, 24));
        qq_push_button->setMaximumSize(QSize(128, 24));
        qq_push_button->setStyleSheet(QLatin1String("QPushButton {\n"
"	border:0;\n"
"	background-image: url(:/exception_images/qq_normal.png);\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	border:0;\n"
"	background-image: url(:/exception_images/qq_active.png);\n"
"}"));
        qq_push_button->setFlat(true);

        horizontalLayout_3->addWidget(qq_push_button);

        telephone_push_button = new QPushButton(CCrashFeedbackDialog);
        telephone_push_button->setObjectName(QStringLiteral("telephone_push_button"));
        sizePolicy1.setHeightForWidth(telephone_push_button->sizePolicy().hasHeightForWidth());
        telephone_push_button->setSizePolicy(sizePolicy1);
        telephone_push_button->setMinimumSize(QSize(154, 24));
        telephone_push_button->setMaximumSize(QSize(154, 24));
        telephone_push_button->setStyleSheet(QLatin1String("QPushButton {\n"
"	border:0;\n"
"	background-image: url(:/exception_images/telephone_normal.png);\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	border:0;\n"
"	background-image: url(:/exception_images/telephone_active.png);\n"
"}"));
        telephone_push_button->setFlat(true);

        horizontalLayout_3->addWidget(telephone_push_button);

        website_push_button = new QPushButton(CCrashFeedbackDialog);
        website_push_button->setObjectName(QStringLiteral("website_push_button"));
        sizePolicy1.setHeightForWidth(website_push_button->sizePolicy().hasHeightForWidth());
        website_push_button->setSizePolicy(sizePolicy1);
        website_push_button->setMinimumSize(QSize(252, 24));
        website_push_button->setMaximumSize(QSize(252, 24));
        website_push_button->setStyleSheet(QLatin1String("QPushButton {\n"
"	border:0;\n"
"	background-image: url(:/exception_images/web_normal.png);\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	border:0;\n"
"	background-image: url(:/exception_images/web_active.png);\n"
"}"));
        website_push_button->setFlat(true);

        horizontalLayout_3->addWidget(website_push_button);


        verticalLayout->addLayout(horizontalLayout_3);


        retranslateUi(CCrashFeedbackDialog);

        QMetaObject::connectSlotsByName(CCrashFeedbackDialog);
    } // setupUi

    void retranslateUi(QDialog *CCrashFeedbackDialog)
    {
        CCrashFeedbackDialog->setWindowTitle(QApplication::translate("CCrashFeedbackDialog", "CrashReporter", 0));
        textBrowser->setHtml(QApplication::translate("CCrashFeedbackDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'\345\256\213\344\275\223'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/exception_images/bug.png\" /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'SimSun';\">\351\235\236\345\270\270\346\212\261\346\255\211...\350\275\257\344\273\266\344\270\215\345\260\217\345\277\203\345\264\251\346\272\203\344\272\206</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'SimSun';\">\346\217\220\344\272"
                        "\244\344\270\200\344\270\252\346\212\245\345\221\212\345\260\206\346\234\211\345\212\251\344\272\216\346\210\221\344\273\254\347\232\204\345\267\245\347\250\213\345\270\210\350\247\243\345\206\263\350\277\231\344\270\252\351\227\256\351\242\230,\346\202\250\344\271\237\345\260\206\346\234\211\346\234\272\344\274\232\350\216\267\345\276\227\344\270\200\344\273\275\347\245\236\347\247\230\347\244\274\345\223\201</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'SimSun'; color:#f50408;\">\346\263\250\346\204\217:\346\202\250\351\234\200\350\246\201\346\217\220\344\276\233\344\270\200\344\270\252\344\270\264\346\227\266\347\232\204\345\234\260\346\226\271\350\256\251\346\212\245\345\221\212\350\277\233\350\241\214\344\277\235\345\255\230,\350\257\267\344\270\215\350\246\201\351\200\211\346\213\251\346\202\250\351\234\200\350\246\201\346\201\242\345\244\215\346\225\260\346\215\256\347\232\204\347\241\254"
                        "\347\233\230</span></p></body></html>", 0));
        label->setText(QApplication::translate("CCrashFeedbackDialog", "\345\264\251\346\272\203\346\203\205\345\206\265\346\217\217\350\277\260:", 0));
        commit_push_button->setText(QApplication::translate("CCrashFeedbackDialog", "\345\215\216\344\270\275\344\270\275\345\234\260\346\217\220\344\272\244", 0));
        cancel_push_button->setText(QApplication::translate("CCrashFeedbackDialog", "\346\256\213\345\277\215\345\234\260\345\217\226\346\266\210", 0));
        company_logo_push_button->setText(QString());
        company_name_push_button->setText(QString());
        qq_push_button->setText(QString());
        telephone_push_button->setText(QString());
        website_push_button->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class CCrashFeedbackDialog: public Ui_CCrashFeedbackDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CCRASHFEEDBACKDIALOG_H
