﻿#if defined(QT_GUI_LIB) && defined(_WIN32)

#include <Windows.h>

#include <DbgHelp.h>

#include "ccrashfeedbackdialog.h"
#include "ui_ccrashfeedbackdialog.h"

#include <QFileDialog>
#include <QDateTime>
#include <QDesktopServices>

#include <QDebug>

CCrashFeedbackDialog::CCrashFeedbackDialog(QString str_product_name, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CCrashFeedbackDialog)
{
    ui->setupUi(this);
    this->m_str_product_name = str_product_name;

    QObject::connect( this->ui->commit_push_button,&QPushButton::clicked
                      ,this,&CCrashFeedbackDialog::mfslDump);
    QObject::connect( this->ui->company_logo_push_button,&QPushButton::clicked
                      ,this,&CCrashFeedbackDialog::mfslVisitWebsite);
    QObject::connect( this->ui->company_name_push_button,&QPushButton::clicked
                      ,this,&CCrashFeedbackDialog::mfslVisitWebsite);
    QObject::connect( this->ui->website_push_button,&QPushButton::clicked
                      ,this,&CCrashFeedbackDialog::mfslVisitWebsite);
    QObject::connect( this->ui->qq_push_button,&QPushButton::clicked
                      ,this,&CCrashFeedbackDialog::mfslTalkQQ);
}

CCrashFeedbackDialog::~CCrashFeedbackDialog()
{
    delete ui;
}

void
CCrashFeedbackDialog::mfslDump()
{
    QString str_crash_report_save_dir = QFileDialog::getExistingDirectory(NULL,QStringLiteral("请选择报告保存的地方")
                                                                          ,QDir::home().dirName());
    QString str_crash_report_save_path = QString("%1/%2-%3.dmp").arg(str_crash_report_save_dir)
            .arg(this->m_str_product_name).arg(QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm-ss"));
    QByteArray ba_crash_report_save_path = str_crash_report_save_path.toLocal8Bit();
//    qDebug() << "report path:" << ba_crash_report_save_path;
//    this->ui->desc_text_edit->setText( str_crash_report_save_path );

    this->ui->commit_push_button->setEnabled(false);
    this->ui->cancel_push_button->setEnabled(false);
    this->ui->cancel_push_button->setText( QStringLiteral("马上就好了...") );

    HANDLE hFile = CreateFileA( ba_crash_report_save_path.constData()
                                , GENERIC_WRITE, 0, NULL, CREATE_ALWAYS
                                , FILE_ATTRIBUTE_NORMAL, NULL);
    if( hFile != INVALID_HANDLE_VALUE)
    {
        MINIDUMP_EXCEPTION_INFORMATION einfo;
        einfo.ThreadId = GetCurrentThreadId();
        einfo.ExceptionPointers = this->m_p_exception_info;
        einfo.ClientPointers = FALSE;

        MiniDumpWriteDump(GetCurrentProcess()
                          , GetCurrentProcessId()
                          , hFile
                          ,MiniDumpWithFullMemory
                          , &einfo, NULL, NULL);
        CloseHandle(hFile);
    }
    this->close();
}

void
CCrashFeedbackDialog::mfslVisitWebsite()
{
    QDesktopServices::openUrl( QUrl("http://www.sysfix.cn") );
}

void
CCrashFeedbackDialog::mfslTalkQQ()
{
    QDesktopServices::openUrl( QUrl("http://wpa.qq.com/msgrd?v=3&uin=1253140355&site=qq&menu=yes") );
}

#endif
