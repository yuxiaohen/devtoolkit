win32 {
    LIBS += -lDbghelp
}

HEADERS += \
    $$PWD/Exception.h \
    $$PWD/Exceptionlib.h \
    $$PWD/dump.h \
    $$PWD/errorNum.h \
    #gui
    $$PWD/gui/ccrashfeedbackdialog.h

FORMS += \
    $$PWD/gui/ccrashfeedbackdialog.ui

