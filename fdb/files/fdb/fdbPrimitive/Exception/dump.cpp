﻿#if defined(_WIN32)

#include "dump.h"

#include <stdio.h>

#include "gui/ccrashfeedbackdialog.h"

extern CCrashFeedbackDialog* g_p_obj_crash_dialog;

LONG WINAPI FDBfException_dump(struct _EXCEPTION_POINTERS* p_exception_info)
{
    g_p_obj_crash_dialog->m_p_exception_info = p_exception_info;
    g_p_obj_crash_dialog->setModal(true);
    g_p_obj_crash_dialog->exec();

    return (EXCEPTION_EXECUTE_HANDLER);
}
void _fDisableCRTUnhandleExceptionFilter()
{
    void *addr = (void*)GetProcAddress(LoadLibraryA("kernel32.dll"), "SetUnhandledExceptionFilter");

    if (addr)
    {
        DWORD dwOldFlag, dwTempFlag;

        unsigned char code[16];
        int size = 0;
        code[size++] = 0x33;
        code[size++] = 0xC0;
        code[size++] = 0xC2;
        code[size++] = 0x04;
        code[size++] = 0x00;

        VirtualProtect(addr, size, PAGE_READWRITE, &dwOldFlag);
        WriteProcessMemory(GetCurrentProcess(), addr, code, size, NULL);
        VirtualProtect(addr, size, dwOldFlag, &dwTempFlag);
    }
}

//void fSetDumpTrigger( long(__stdcall *pfTrigger)(struct _EXCEPTION_POINTERS*) )
void FDBfException_set_dump_trigger(DUMP_TRIGGER(pfTrigger))
{
    SetUnhandledExceptionFilter(pfTrigger);
    //_fDisableCRTUnhandleExceptionFilter();
}

#endif
