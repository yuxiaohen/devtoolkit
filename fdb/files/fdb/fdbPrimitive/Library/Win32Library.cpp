#if defined(_WIN32)

#include "Win32Library.h"
#include "Library.h"

#include "../../fdbPrimitive/Exception/Exception.h"
#include "../../fdbPrimitive/MemoryManagement/MemoryManagement.h"

#include <stdlib.h>

#if defined(QT_GUI_LIB)
    #include <QMessageBox>
#endif

FDBCLibraryHandler* FDBfLibrary_load(const char *p_library_path)
{
    FDBCLibraryHandler* p_obj_library_handler
            = (FDBCLibraryHandler*)calloc(1,sizeof(FDBCLibraryHandler));
    if(p_obj_library_handler == NULL)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_TWO_PARAM("%s when allocate library handler,path: %s"
                            ,FDB_ERR_MEM_NOT_ENOUGH_STR,p_library_path);
        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"%s when allocate library handler,path: %s"
                            ,FDB_ERR_MEM_NOT_ENOUGH_STR,p_library_path);

        return (NULL);
    }
    p_obj_library_handler->h_library = LoadLibraryA(p_library_path);
    if(p_obj_library_handler->h_library == NULL)
    {
        #if defined(QT_GUI_LIB)
            QString str_err_content = QString("error when loading %1").arg(p_library_path);
            QMessageBox::critical(NULL,"Error",str_err_content);
        #endif
        FDBfLibrary_unload(&p_obj_library_handler);
        return (NULL);
    }

    return (p_obj_library_handler);
}

FDBLibraryAddr FDBfLibrary_get_proc_address(FDBCLibraryHandler *p_obj_library_handler
                                            ,const char* p_proc_name)
{
    FDBLibraryAddr ret = NULL;
    ret = GetProcAddress((HMODULE)p_obj_library_handler->h_library,p_proc_name);

    return (ret);
}

int FDBfLibrary_unload(FDBCLibraryHandler **pp_obj_library_handler)
{
    int i_ret = 0;
    if(*pp_obj_library_handler != NULL)
    {
        if( (*pp_obj_library_handler)->h_library != NULL )
        {
            i_ret = FreeLibrary( (HMODULE)(*pp_obj_library_handler)->h_library );
        }
    }
    FDBfFree( (void**)pp_obj_library_handler );
    return (i_ret);
}

#endif //platform
