#if defined(_WIN32)

#ifndef __fdb_win32_library_h_2013_11_04_03_59__
#define __fdb_win32_library_h_2013_11_04_03_59__

#include <Windows.h>

typedef struct FDBCLibraryHandler {
    HANDLE h_library;
} FDBCLibraryHandler;

#define FDBLibraryAddr FARPROC

#endif //header

#endif //platform
