#ifndef __fdb_library_2013_11_04_03_35__
#define __fdb_library_2013_11_04_03_35__

#include "Librarylib.h"

#if defined(_WIN32)
    #include "Win32Library.h"
#else
    #error "Unsupport platform"
#endif

/*!
 * @brief 装载一个dll/so的动态库
 */
FDBInterface FDBCLibraryHandler *FDBfLibrary_load(const char* p_library_path);

/*!
 * @brief 从装载后的一个动态库中取得某个函数的指针地址
 */
FDBInterface FDBLibraryAddr FDBfLibrary_get_proc_address(FDBCLibraryHandler* p_obj_library_handler
                                                         ,const char* p_proc_name);

/*!
 * @brief 卸载一个已装载的动态库
 * @return 成功1,失败0
 */
FDBInterface int FDBfLibrary_unload(FDBCLibraryHandler **pp_obj_library_handler);

#endif
