#pragma once

#include <time.h>

time_t getCompileDateTime()
{
	char month_name[4] = {0};
	int month,day,year,hour,minute,second = 0;
	struct tm t = {0};
	static const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";
    sscanf(__DATE__"-"__TIME__,"%s %d %d-%d:%d:%d",month_name,&day,&year,&hour,&minute,&second);
	month = ( (strstr(month_names,month_name)-month_names) ) / 3;
	t.tm_mon = month;
	t.tm_mday = day;
	t.tm_year = year - 1900;
	t.tm_hour = hour;
	t.tm_min = minute;
	t.tm_sec = second;
    return (mktime(&t));
}
