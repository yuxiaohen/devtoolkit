/*******************************************************************
 *  FDB - Full Data Box -- FullDataSoft Company Foudation Library
 *
 *  author:TuKai
 *  changer:shawhen
 *
 *  Requirement: openssl
 *
 *  aes.h -- function unit:
 *      \1.aes 128bit cbc encryption and decryption
 *
 *      \2.获取公有加密和解密key
 *
 * stable     version:0.2.0     2013-3-15
 *            version:0.3.0     2013-07-05 reject qt
 *******************************************************************/
#ifndef __fdb_aes_h_2013_08_30_01_44__
#define __fdb_aes_h_2013_08_30_01_44__

//openssl
#include <openssl/aes.h>

//fdb
#include "Cryptolib.h"

#include "../Type/Type.h"

FDBInterface unsigned long FDBfCrypto_calculate_aes_128_cbc_align_len(const uint32_t u32Len);
/*!
 * @brief 128cbc,this should add bits to padding 16 bytes(128 bits) alignment.
 * @param p_u32_encrypted_buffer_len 对齐后的密文长度
 */
FDBInterface char* FDBfCrypto_aes_128cbc_encrypt(const void* p_origin_buffer
                                                 , const uint32_t u32_origin_buffer_len
                                                 , const void *p_encrypt_key
                                                 , const int i_encrypt_key_len
                                                 , uint32_t* p_u32_encrypted_buffer_len);

FDBInterface char* FDBfCrypto_aes_128cbc_decrypt(const void* p_cipher_buffer
                                                 , const uint32_t u32_cipher_buffer_len
                                                 , const void* p_decrypt_key
                                                 , const int i_decrypt_key_len
                                                 , uint32_t* p_u32_decrypted_buffer_len);

/*!
 * @brief fake key
 * @return don't free for yourself
 */
FDBInterface const char * FDBfCrypto_get_fake_aes_128_key();

/*!
 * @biref calculate fake key to real key
 * @return public key,please free it yourself
 */
FDBInterface char* FDBfCrypto_get_public_aes_128_key(const char* p_fake_key);

#endif //header
