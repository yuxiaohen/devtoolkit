﻿#ifndef __fdb_rsa_h_2013_11_03_04_58__
#define __fdb_rsa_h_2013_11_03_04_58__

#include "../Type/Type.h"

#ifndef Q_MOC_RUN
    #include <boost/shared_ptr.hpp>
#endif

#include <vector>

/*!
 * @brief rsa公钥加密,内部分片
 * @param encrypted_buf 密文的内存块,在函数内部申请,请在外部使用完释放
 * @return 密文长度(补齐的长度),-1时错误
 */
FDBInterface int
FDBfCrypto_rsa_public_encrypt(const void * plain_text
                              , const uint32_t plain_text_len
                              ,char** encrypted_buf
                              , char* p_public_key_path);

/*!
 * @brief rsa私钥解密,内部分片
 * @return 明文(补齐),外部释放
 */
FDBInterface int
FDBfCrypto_rsa_private_decrypt(const char* p_cipher_text
                               ,const uint32_t u32_str_len
                               ,char** decrypted_buf
                               ,char* p_private_key_path);

#endif
