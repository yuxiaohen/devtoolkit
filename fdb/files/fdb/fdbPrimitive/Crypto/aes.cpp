#include "aes.h"

#include "../MemoryManagement/MemoryManagement.h"
#include "../Math/Math.h"

#include <string.h>
#include <stdlib.h>

unsigned long FDBfCrypto_calculate_aes_128_cbc_align_len(const uint32_t u32Len)
{
    unsigned long u32AlignedLen = 0;
    u32AlignedLen = u32Len%AES_BLOCK_SIZE == 0
            ? u32Len
            : (u32Len/AES_BLOCK_SIZE+1)*AES_BLOCK_SIZE;
    return (u32AlignedLen);
}

char* FDBfCrypto_aes_128cbc_encrypt(const void *p_origin_buffer
                                    , const uint32_t u32_origin_buffer_len
                                    , const void *p_encrypt_key
                                    , const int i_encrypt_key_len
                                    , uint32_t* p_u32_encrypted_buffer_len)
{
    uint32_t u32_aligned_origin_buffer_len = 0;
    char* p_aligned_plain_buffer = NULL;
    char* p_encrypted_buffer = NULL;

    char p_aligned_key[AES_BLOCK_SIZE] = {0};
    AES_KEY aesKey = {0};

    unsigned char pInitVector[16] = {0};    //init vector

    if( p_origin_buffer == NULL || u32_origin_buffer_len == 0 || p_encrypt_key == NULL || p_u32_encrypted_buffer_len == NULL )
    {
        return (NULL);
    }

    //plaintext alignment for 16*n bytes
    u32_aligned_origin_buffer_len = FDBfCrypto_calculate_aes_128_cbc_align_len(u32_origin_buffer_len);
    p_aligned_plain_buffer = (char*)calloc(1,u32_aligned_origin_buffer_len);
    memcpy(p_aligned_plain_buffer,p_origin_buffer,u32_origin_buffer_len);

    //encrypt key alignment for 16 bytes
    memcpy(p_aligned_key,p_encrypt_key,FDB_MIN(AES_BLOCK_SIZE,i_encrypt_key_len));
    if(AES_set_encrypt_key( (unsigned char*)p_aligned_key, AES_BLOCK_SIZE*8, &aesKey) < 0)
    {
        FDBfFree( (void**)&p_aligned_plain_buffer);
        return (NULL);
    }

    p_encrypted_buffer = (char*)calloc(1,u32_aligned_origin_buffer_len);
    AES_cbc_encrypt( (unsigned char*)p_aligned_plain_buffer
                    ,(unsigned char*)p_encrypted_buffer
                    ,u32_aligned_origin_buffer_len
                    ,&aesKey
                    ,pInitVector
                    ,AES_ENCRYPT);

    FDBfFree( (void**)&p_aligned_plain_buffer );
    *p_u32_encrypted_buffer_len = u32_aligned_origin_buffer_len;

    return (p_encrypted_buffer);
}

char* FDBfCrypto_aes_128cbc_decrypt(const void *p_cipher_buffer
                                    , const uint32_t u32_cipher_buffer_len
                                    , const void *p_decrypt_key
                                    , const int i_decrypt_key_len
                                    , uint32_t* p_u32_decrypted_buffer_len)
{
    uint32_t u32_aligned_cipher_buffer_len = 0;
    char* p_aligned_cipher_buffer = NULL;

    char p_aligned_key[AES_BLOCK_SIZE] = {0};
    AES_KEY s_aes_key = {0};

    char* p_decrypted_buffer = NULL;

    unsigned char pInitVector[16] = {0};    //init vector

    if( p_cipher_buffer==NULL || u32_cipher_buffer_len==0 || p_decrypt_key==NULL || p_u32_decrypted_buffer_len==NULL )
    {
        return (NULL);
    }

    //cipher alignment for 16*n bytes
    u32_aligned_cipher_buffer_len = FDBfCrypto_calculate_aes_128_cbc_align_len( u32_cipher_buffer_len);
    p_aligned_cipher_buffer = (char*)calloc(1,u32_aligned_cipher_buffer_len);
    memcpy(p_aligned_cipher_buffer,p_cipher_buffer,u32_cipher_buffer_len);

    //encrypt key alignment for 16 bytes
     memcpy(p_aligned_key,p_decrypt_key,FDB_MIN(AES_BLOCK_SIZE,i_decrypt_key_len));
    if(AES_set_decrypt_key((unsigned char*)p_aligned_key, AES_BLOCK_SIZE*8, &s_aes_key) < 0)
    {
        return (NULL);
    }

	//to be return
    p_decrypted_buffer = (char*)calloc(1,u32_aligned_cipher_buffer_len);
    AES_cbc_encrypt((unsigned char*)p_cipher_buffer
                    ,(unsigned char*)p_decrypted_buffer
                    ,u32_aligned_cipher_buffer_len
                    ,&s_aes_key
                    ,pInitVector
                    ,AES_DECRYPT);

    FDBfFree( (void**)&p_aligned_cipher_buffer );
    *p_u32_decrypted_buffer_len = u32_aligned_cipher_buffer_len;

    return (p_decrypted_buffer);
}

const char *FDBfCrypto_get_fake_aes_128_key()
{
    static const char* p = "ixoogdwdvriw";
    return (p);
}

char* FDBfCrypto_get_public_aes_128_key(const char* p_fake_key/*= FDBfCrypto_get_fake_aes_128_key()*/)
{
    char* pcRealKey = (char*)calloc(1,16);
    int i = 0;
    for(i = 0;i < FDB_MIN(strlen(p_fake_key),16);++ i)
    {
        pcRealKey[i] = p_fake_key[i] - 3;
    }
    return (pcRealKey);
}
