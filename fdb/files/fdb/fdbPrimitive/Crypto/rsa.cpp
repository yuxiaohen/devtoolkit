#include "rsa.h"

#include "../Exception/Exception.h"
#include "../Math/Math.h"

#include <stdio.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/err.h>

#include <vector>

int FDBfCrypto_rsa_public_encrypt(const void* plain_text
                              , const uint32_t plain_text_len
                              , char **encrypted_buf
                                  , char* p_public_key_path)
{
    RSA * p_public_key_rsa = NULL;
    FILE* pf_public_key_file = NULL;
    int rsa_length,i_encrypted_text_len = 0;
    int i_encrypted_ret = 0;
    int i_encrypt_time_cursor,i_encrypt_time_total = 0; //加密次数,用于分片加密
    int i_everytime_encrypt_text_len = 0; //每次分片加密产生的密文长度
    int i_everytime_encrypt_plain_text_len = 0; //每次分片加密使用的明文长度

    pf_public_key_file = fopen(p_public_key_path,"rb");
    if(pf_public_key_file == NULL)
    {
        giFDBErrNum = FDB_ERR_FILE_NOT_FOUND;
        FDBErrStr_TWO_PARAM("%s:%s",FDB_ERR_FILE_NOT_FOUND_STR,p_public_key_path);
        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"%s:%s",FDB_ERR_FILE_NOT_FOUND_STR,p_public_key_path);

        return (-1);
    }
    p_public_key_rsa = PEM_read_RSA_PUBKEY(pf_public_key_file,NULL,NULL,NULL);
    if(p_public_key_rsa == NULL)
    {
        giFDBErrNum = FDB_ERR_UNDEFINED;
        FDBErrStr_ONE_PARAM("PEM_read_RSA_PUBKEY failed,key path:%s",p_public_key_path);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"PEM_read_RSA_PUBKEY failed,key path:%s",p_public_key_path);

        RSA_free(p_public_key_rsa);
        fclose(pf_public_key_file);
        return (-1);
    }

    rsa_length = RSA_size(p_public_key_rsa); //密钥长度
    i_everytime_encrypt_text_len = rsa_length;
    i_everytime_encrypt_plain_text_len = rsa_length - 11;
    i_encrypt_time_total = FDBfMath_cal_ceil_div(plain_text_len,i_everytime_encrypt_plain_text_len); //分片数
    i_encrypted_text_len = i_encrypt_time_total*i_everytime_encrypt_text_len; //密文总长度
    *encrypted_buf = (char*)calloc(1,i_encrypted_text_len);

    for( i_encrypt_time_cursor=0
         ;i_encrypt_time_cursor < i_encrypt_time_total
         ;++ i_encrypt_time_cursor )
    {
        i_encrypted_ret
                = RSA_public_encrypt(rsa_length
                                     ,(unsigned char*)plain_text+i_encrypt_time_cursor*i_everytime_encrypt_plain_text_len
                                     ,(unsigned char*)*encrypted_buf+i_encrypt_time_cursor*i_everytime_encrypt_text_len
                                     ,p_public_key_rsa,RSA_NO_PADDING);
        if(i_encrypted_ret < 0)
        {
            unsigned long ul_error_code = ERR_get_error();
            char* p_error = ERR_error_string(ul_error_code,NULL);
            giFDBErrNum = FDB_ERR_UNDEFINED;
            FDBErrStr_ONE_PARAM("RSA_public_encrypt failed,key path:%s",p_public_key_path);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"RSA_public_encrypt failed,key path:%s",p_public_key_path);

            RSA_free(p_public_key_rsa);
            fclose(pf_public_key_file);
            free(*encrypted_buf);
            return (-1);
        }
    }

    RSA_free(p_public_key_rsa);
    fclose(pf_public_key_file);

    return (i_encrypted_text_len);
}

int FDBfCrypto_rsa_private_decrypt(const char* p_cipher_text
                               , const uint32_t u32_str_len
                               , char **decrypted_buf
                               , char* p_private_key_path)
{
    RSA * p_private_key_rsa = NULL;
    FILE* pf_private_key_file = NULL;
    int rsa_length,i_decrypted_text_len = 0;
    int i_decrypted_ret = 0;
    int i_decrypt_time_cursor,i_decrypt_time_total = 0; //解密次数,用于分片解密
    int i_everytime_decrypt_text_len = 0; //每次分片解密的长度
    int i_everytime_decrypt_plain_text_len = 0; //每次分片解密出的明文的长度

    pf_private_key_file = fopen(p_private_key_path,"r");
    if(pf_private_key_file == NULL)
    {
        giFDBErrNum = FDB_ERR_FILE_NOT_FOUND;
        FDBErrStr_THREE_PARAM("%s:%s(ec:%d)",FDB_ERR_FILE_NOT_FOUND_STR,p_private_key_path,giFDBErrNum);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s:%s",FDB_ERR_FILE_NOT_FOUND_STR,p_private_key_path,giFDBErrNum);

        return (-1);
    }
    p_private_key_rsa = PEM_read_RSAPrivateKey(pf_private_key_file,NULL,NULL,NULL);
    if(p_private_key_rsa == NULL)
    {
        giFDBErrNum = FDB_ERR_UNDEFINED;
        FDBErrStr_ONE_PARAM("PEM_read_RSA_PUBKEY failed,key path:%s",p_private_key_path);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"PEM_read_RSA_PUBKEY failed,key path:%s",p_private_key_path);

        RSA_free(p_private_key_rsa);
        fclose(pf_private_key_file);
        return (-1);
    }

    rsa_length = RSA_size(p_private_key_rsa); //密钥长度
    i_everytime_decrypt_text_len = rsa_length;
    i_everytime_decrypt_plain_text_len = rsa_length;
    i_decrypt_time_total = FDBfMath_cal_ceil_div(u32_str_len,i_everytime_decrypt_text_len); //明文片数
    i_decrypted_text_len = i_decrypt_time_total*i_everytime_decrypt_plain_text_len; //明文总长度
    *decrypted_buf = (char*)calloc(1,i_decrypted_text_len);

    for( i_decrypt_time_cursor=0
         ;i_decrypt_time_cursor < i_decrypt_time_total
         ;++ i_decrypt_time_cursor )
    {
        i_decrypted_ret
                = RSA_private_decrypt(rsa_length
                                      ,(unsigned char*)p_cipher_text+i_decrypt_time_cursor*i_everytime_decrypt_text_len
                                      ,(unsigned char*)*decrypted_buf+i_decrypt_time_cursor*i_everytime_decrypt_plain_text_len
                                      ,p_private_key_rsa,RSA_NO_PADDING);
        if(i_decrypted_ret < 0)
        {
            giFDBErrNum = FDB_ERR_UNDEFINED;
            FDBErrStr_ONE_PARAM("RSA_public_encrypt failed,key path:%s",p_private_key_path);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"RSA_public_encrypt failed,key path:%s",p_private_key_path);

            RSA_free(p_private_key_rsa);
            fclose(pf_private_key_file);
            free(*decrypted_buf);
            return ( -1 );
        }
    }

    RSA_free(p_private_key_rsa);
    fclose(pf_private_key_file);

    return (i_decrypted_text_len);
}
