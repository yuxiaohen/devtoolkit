﻿#ifndef _FUPDATE_H_
#define _FUPDATE_H_
#include <Windows.h>
#include <stdio.h>
#include <errno.h>
#include "..\..\Network\Network.h"
#include "..\..\Hash\Hash.h"
struct SUpDateFileInformation
{
	char cStatus;//‘+’表示添加或更新,’-’表示删除。
	char* pcFilePath;//文件在本地的相对路径，用于寻找file。
	unsigned int uiFileSize;//服务器端文件大小，用于对比本地文件大小，粗略判断是否相同。
	char* pcURL;//服务器端文件所在路径。
	char acSHA1[40];//服务器端文件的SHA1值，用于详细比较文件是否相同。
	int iError;//错误信息
	struct SUpDateFileInformation* pNext;//下一个指针的节点
};
char* FDBfGetPath(const char* pBuf,char* Path);
char* fStrToUI32(char* pBuf,unsigned int* Number);
int fGetSHA1Str(char* pBuf,char* SHA1);
struct SUpDateFileInformation* SUpDateFileInformation_new();
int fGetUpDateInformation(char* op,struct SUpDateFileInformation *p);
void SUpDateFileInformation_delete(struct SUpDateFileInformation** head);
void fDeleteFile(char* fPath);
int fFindFile(char* fPath);
void fGetFileSHA1(char* fPath,unsigned int fileSize,char* SHA1);
int equalSHA1(char* SHA1A,char* SHA2B);
int FDBfUpdate(char* op,char* pDirctory);
/*!
*   @op 数据行
*   @pDirctory 缓存文件路径
*   @return 0表示需要删除文件但是文件不存在或文件已更新到最新 1表示成功执行-1表示内存分配没有成功。-2表示Str匹配不成功。-3表示存在没有权限删除的文件-4表示调用FDBfHTTPDoenLoad失败
*/
int FDBfUpDate(char* op,char* pDirctory);
#endif
