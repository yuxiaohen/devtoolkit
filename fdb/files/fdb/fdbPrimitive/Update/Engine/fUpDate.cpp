﻿#include "fUpdate.h"

char* fGetPath(const char *pBuf, char* Path)
{
    char pc[500];
    int len =0;
    while(*pBuf != ' '&&*pBuf != '\0')
    {
        if(*pBuf == '\\')
        {
            ++pBuf;
        }
        pc[len] = *pBuf;
        ++pBuf;
        ++len;
    }
    pc[len] = '\0';

    memcpy(Path,pc,len+1);
    return (char*)pBuf;
}

char* fStrToUI32(char* pBuf,unsigned int* Number)
{
	*Number =0;
    while(*pBuf != ' '&&*pBuf != '\0')
    {
        *Number = (*Number) * 10 + (((unsigned char)(*pBuf))&0x0f) ;
        pBuf++;
    }

	return pBuf;
}

int fGetSHA1Str(char* pBuf,char* SHA1)
{
    int len = 0;
    for(len = 0 ; len<40 && *pBuf != '\0' && *pBuf != ' ';++len,++pBuf)
    {
        SHA1[len] = *pBuf;
    }
    return len == 40 ? 1:-1;//1表示正确，-1表示匹配失败
}

struct SUpDateFileInformation* SUpDateFileInformation_new()
{
    struct SUpDateFileInformation* p = (struct SUpDateFileInformation*)malloc(sizeof(struct SUpDateFileInformation));
	if(p == NULL)
	{
        return NULL;//表示无法分配到内存
	}
	//
	p->pNext = NULL;
	p->pcURL = (char *)malloc(200);
	p->pcFilePath = (char *)malloc(200);
	p->iError = 0;
	p->uiFileSize = 0;
    return p;
}

int fGetUpDateInformation(char* op,struct SUpDateFileInformation *p)
{
	//提取status
    p->cStatus = *op;
	//提取FilePath
    op = fGetPath(op+2,p->pcFilePath);
	if(p->cStatus == '-')
	{
        return 0;
	}
	//提取FileSize
    op = fStrToUI32(op+1,&(p->uiFileSize));
	//提取URL
    op = fGetPath(op+1,p->pcURL);
	//提取FileSHA1
    return fGetSHA1Str(op+1,p->acSHA1) == 1? 1 : -1;//-1字符串匹配不成功

}

void SUpDateFileInformation_delete(struct SUpDateFileInformation** head)
{
    if((*head) == NULL)
	{
		return ;
	}
    SUpDateFileInformation_delete(&((*head)->pNext));
    if((*head)->pcFilePath!=NULL)
	{
        free((*head)->pcFilePath);
	}
    if((*head)->pcURL!=NULL)
	{
        free((*head)->pcURL);
	}
    free((*head));
    *head = NULL;
}

void fDeleteFile(char* fPath)
{
	DeleteFileA(fPath);
}

int fFindFile(char* fPath)
{
	return access(fPath,0) == -1? -1 : 1;
}

int equalSHA1(char* SHA1A,char* SHA1B)
{
	int i=0;
	for(i=0;i<40;++i)
	{
		if((SHA1A[i]^SHA1B[i])&0x1f)
		{
			return -1;
		}
	}
	return 1;
}

int FDBfUpdate(char* op,char* pDirctory)
{
    struct SUpDateFileInformation* sUpDateInformation = SUpDateFileInformation_new();
	unsigned int uiFileSize;
	char acSHA1[20];
    if(sUpDateInformation == NULL)
    {
        return -1;//表示内存分配没有成功
    }
    if(-1 == fGetUpDateInformation(op,sUpDateInformation))
    {
        SUpDateFileInformation_delete(&sUpDateInformation);
        return -2;//-2表示Str匹配不成功
    }
	if(sUpDateInformation->cStatus == '-')
    {
		if( access(sUpDateInformation->pcFilePath,0) != -1 )
		{
			fDeleteFile(sUpDateInformation->pcFilePath);
            SUpDateFileInformation_delete(&sUpDateInformation);
            return 1;//1表示成功执行
		}else if( errno == EACCES )
		{
            SUpDateFileInformation_delete(&sUpDateInformation);
            return -3;//-3表示存在没有权限删除的文件
		}else
		{
            SUpDateFileInformation_delete(&sUpDateInformation);
            return 0;//0表示需要删除文件但是文件不存在或文件已更新到最新
        }
	}else if(sUpDateInformation->cStatus == '+')
	{
		if( fFindFile(sUpDateInformation->pcFilePath) == -1 )
		{
            if(0 > FDBfHTTPDownload(sUpDateInformation->pcURL,pDirctory))
            {
                SUpDateFileInformation_delete(&sUpDateInformation);
                return -4;
            }
		}else
		{
			uiFileSize = GetFileAttributesA(sUpDateInformation->pcFilePath);
			if( uiFileSize != sUpDateInformation->uiFileSize)
			{
                if(0 > FDBfHTTPDownload(sUpDateInformation->pcURL,pDirctory))
                {
                    SUpDateFileInformation_delete(&sUpDateInformation);
                    return -4;
                }
			}else
			{
				FDBfGetFileSHA1(sUpDateInformation->pcFilePath,acSHA1);
				if(equalSHA1(sUpDateInformation->acSHA1,acSHA1)==-1)
				{
                    if(0 > FDBfHTTPDownload(sUpDateInformation->pcURL,pDirctory))
                    {
                        SUpDateFileInformation_delete(&sUpDateInformation);
                        return -4;
                    }
				}
			}
		}
	}
    SUpDateFileInformation_delete(&sUpDateInformation);
    return 1;
}
