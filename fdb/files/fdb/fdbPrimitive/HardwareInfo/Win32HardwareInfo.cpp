#if defined(_WIN32)

#include "Win32HardwareInfo.h"
#include "HardwareInfo.h"
#include "MasterHDInfo.h"

#include "../../fdbPrimitive/Library/Library.h"

typedef char * (__stdcall *GetCPUInfomation)(int);
typedef char * (__stdcall *GetBIOSSN)();
typedef char * (__stdcall *GetIdeSN)();
typedef char * (__stdcall *GetMAC)(int);

std::string FDBfHardware_get_cpu_id()
{
    std::string str_cpu_sn;
    GetCPUInfomation pfunc_getCPUInfomation;
    FDBCLibraryHandler* p_obj_library_handler = FDBfLibrary_load("HWInfo.dll");
    if(p_obj_library_handler != NULL)
    {
        pfunc_getCPUInfomation = (GetCPUInfomation)FDBfLibrary_get_proc_address(p_obj_library_handler
                                                                 ,"getCPUInfomation");
        char* p_cpu_sn = pfunc_getCPUInfomation(1);
        str_cpu_sn = std::string(p_cpu_sn);
        FDBfLibrary_unload(&p_obj_library_handler);
    }
    return (str_cpu_sn);
}

std::string FDBfHardware_get_hd_sn()
{
    FDB::IOKit::CMasterHardDiskSerial o;
    std::string str_hd_sn = o.GetSerialNo();
	if(str_hd_sn.empty())
	{
		GetIdeSN pfunc_getIDEInfomation;
		FDBCLibraryHandler* p_obj_library_handler = FDBfLibrary_load("HWInfo.dll");
		if(p_obj_library_handler != NULL)
		{
			pfunc_getIDEInfomation = (GetIdeSN)FDBfLibrary_get_proc_address(p_obj_library_handler
				,"getIdeSN");
			char* p_ide_sn = pfunc_getIDEInfomation();
			str_hd_sn = std::string(p_ide_sn);
			FDBfLibrary_unload(&p_obj_library_handler);
		}
	}

    return (str_hd_sn);
}

std::string FDBfHardware_get_bios_sn()
{
    std::string str_bios_sn;
    GetBIOSSN pfunc_getBIOSSN;
    FDBCLibraryHandler* p_obj_library_handler = FDBfLibrary_load("HWInfo.dll");
    if(p_obj_library_handler != NULL)
    {
        pfunc_getBIOSSN = (GetBIOSSN)FDBfLibrary_get_proc_address(p_obj_library_handler
                                                                 ,"getBiosSN");
        char* p_bios_sn = pfunc_getBIOSSN();
        str_bios_sn = std::string(p_bios_sn);
        FDBfLibrary_unload(&p_obj_library_handler);
    }

    return (str_bios_sn);
}

#endif //platform
