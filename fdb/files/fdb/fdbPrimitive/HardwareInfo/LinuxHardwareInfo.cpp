#if defined(__linux__)

#include "LinuxHardwareInfo.h"
#include "HardwareInfo.h"

#include <string.h>

#include <fcntl.h>

std::string FDBfHardware_get_cpu_id()
{
    char p_dmidecode_result[4096] = {0};
    FILE* pf_dmidecode = popen("dmidecode -t processor","r");
    int i_read_result_bytes = fread(p_dmidecode_result,1,4096,pf_dmidecode);
    if( i_read_result_bytes > 0 )
    {
        char* pc_cpu_id_begin_pos = strstr(p_dmidecode_result,"ID:");
        if(pc_cpu_id_begin_pos) { pc_cpu_id_begin_pos += 3; }
        while (pc_cpu_id_begin_pos && (*pc_cpu_id_begin_pos == ' ') )
        {
            ++ pc_cpu_id_begin_pos;
        }
        if(pc_cpu_id_begin_pos != NULL)
        {
            //找到cpuid结束的位置
            char* pc_cpu_id_end_pos = pc_cpu_id_begin_pos;
            while( *pc_cpu_id_end_pos != '\n' )
            {
                ++ pc_cpu_id_end_pos;
            }
            pclose(pf_dmidecode);
            return ( std::string(pc_cpu_id_begin_pos,pc_cpu_id_end_pos-pc_cpu_id_begin_pos) );
        }
    }
    return (std::string(""));
}

std::string FDBfHardware_get_hd_sn()
{
    char p_hdparm_result[4096] = {0};
    FILE* pf_dmidecode = popen("hdparm -I /dev/sda","r");
    int i_read_result_bytes = fread(p_hdparm_result,1,4096,pf_dmidecode);
    if( i_read_result_bytes > 0 )
    {
        char* pc_hd_sn_begin_pos = strstr(p_hdparm_result,"Serial Number:");
        if(pc_hd_sn_begin_pos) { pc_hd_sn_begin_pos += 13; }
        while( pc_hd_sn_begin_pos && (*pc_hd_sn_begin_pos == ' ') )
        {
            ++pc_hd_sn_begin_pos;
        }
        if(pc_hd_sn_begin_pos != NULL)
        {
            //找到Serial Number结束的位置
            char* pc_hd_sn_end_pos = pc_hd_sn_begin_pos;
            while( *pc_hd_sn_end_pos != '\n' && *pc_hd_sn_end_pos != ' ' )
            {
                ++ pc_hd_sn_end_pos;
            }
            pclose(pf_dmidecode);
            return ( std::string(pc_hd_sn_begin_pos,pc_hd_sn_end_pos-pc_hd_sn_begin_pos) );
        }
    }
    return (std::string(""));
}

std::string FDBfHardware_get_bios_sn()
{
    char p_dmidecode_result[4096] = {0};
    FILE* pf_dmidecode = popen("dmidecode -t BaseBoard","r");
    int i_read_result_bytes = fread(p_dmidecode_result,1,4096,pf_dmidecode);
    if( i_read_result_bytes > 0 )
    {
        char* pc_bios_sn_begin_pos = strstr(p_dmidecode_result,"Serial Number:");
        if(pc_bios_sn_begin_pos) { pc_bios_sn_begin_pos += 13; }
        while(pc_bios_sn_begin_pos && (*pc_bios_sn_begin_pos == ' ') )
        {
            ++pc_bios_sn_begin_pos;
        }
        if(pc_bios_sn_begin_pos != NULL)
        {
            //找到Serial Number结束的位置
            char* pc_bios_sn_end_pos = pc_bios_sn_begin_pos;
            while( *pc_bios_sn_end_pos != '\n' && *pc_bios_sn_end_pos != ' ' )
            {
                ++ pc_bios_sn_end_pos;
            }
            pclose(pf_dmidecode);
            return ( std::string(pc_bios_sn_begin_pos,pc_bios_sn_end_pos-pc_bios_sn_begin_pos) );
        }
    }
    return (std::string(""));
}

#endif //platform
