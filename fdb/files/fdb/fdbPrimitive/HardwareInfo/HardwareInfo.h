﻿#ifndef __fdb_hardware_info_h_2013_11_04_02_52__
#define __fdb_hardware_info_h_2013_11_04_02_52__

#include "HardwareInfolib.h"

#if defined(_WIN32)
    #include "Win32HardwareInfo.h"
#elif defined(__linux__)
    #include "LinuxHardwareInfo.h"
#elif defined(__APPLE__)
    //please not report error now
#else
    #error "Unsupport platform"
#endif

#include <string>

/*!
 * @brief 获取cpu序列号
 * @return cpu序列号,外部释放
 */
FDBInterface std::string FDBfHardware_get_cpu_id();

/*!
 * @brief 获取硬盘的序列号
 * @return 硬盘的序列号,外部释放
 */
FDBInterface std::string FDBfHardware_get_hd_sn();

/*!
 * @brief 获取bios的序列号
 * @return bios的序列号,外部释放
 */
FDBInterface std::string FDBfHardware_get_bios_sn();

#endif //header
