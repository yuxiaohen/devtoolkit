#include "FileSystem.h"
#include <locale.h>

void help()
{
	printf("ViewFile.exe -v <DiskPath> <Offset>\n");
}

typedef unsigned long long DWORDLONG;

void ListAllFile(const char* Path, DWORDLONG Offset)
{
	CFileSystem* pFileSystem = newCFileSystemObject(Path,Offset);
	if(pFileSystem==NULL)
	{
		fprintf(stderr,"Open Failed\n");
		return;
	}
	if(pFileSystem->GetType()==CFileSystem::NTFS)
	{
		CBitMap Bitmap;
		if(((CNtfs*)pFileSystem)->GetBitMap(&Bitmap))
		{
			CFragmentList list = Bitmap.GetFragmentList(false);
			int i;
			for(i=0;i<list.GetTotalFrag();++i)
			{
				printf("%d:%I64X:%d\n",i,list.GetOffset(i),list.GetClusterNum(i));
			}
		}
	}
	CFileScanner* pFileScanner = pFileSystem->newCFileScannerObject();
	SFatFileInfoSet FatSet;
	SNtfsFileInfoSet NtfsSet;
	if(pFileSystem->GetType()!=CFileSystem::RAW)
	{
		if(pFileSystem->GetType()==CFileSystem::FAT32||pFileSystem->GetType()==CFileSystem::FAT16||pFileSystem->GetType()==CFileSystem::EXFAT)
			pFileScanner->AddInfoSet(&FatSet);
		if(pFileSystem->GetType()==CFileSystem::NTFS)
			pFileScanner->AddInfoSet(&NtfsSet);
		fprintf(stderr,"Start Scaning....\n");
		pFileScanner->ScanFile(0);
		fprintf(stderr,"Scan Complete\n");

		if(NtfsSet.size()>0)
		{
			printf("NTFS: %d Files\n",NtfsSet.size());
			int i;
			for(i=0;i<NtfsSet.size();++i)
			{
				SFileInfo* pFileInfo = (SFileInfo*)NtfsSet.at(i);
				wprintf(L"%I64X: %s : %I64d Bytes\n",pFileInfo->GetOffset(),
						pFileInfo->GetFileName().c_str(),pFileInfo->GetFileSize());
				fflush(stdout);
			}
		}
		if(FatSet.size()>0)
		{
			printf("FAT: %d Files\n",FatSet.size());
			int i;
			for(i=0;i<FatSet.size();++i)
			{
				SFileInfo* pFileInfo = (SFileInfo*)FatSet.at(i);
				wprintf(L"%I64X: %s : %I64d Bytes\n",pFileInfo->GetOffset(),
						pFileInfo->GetFileName().c_str(),pFileInfo->GetFileSize());
				fflush(stdout);
			}
		}
	}
	else
	{
		fprintf(stderr,"Can not find FileSystem\n");
	}
	delete pFileScanner;
	delete pFileSystem;
}

void ListAllExtFile(const char* Path, DWORDLONG Offset)
{
	CExt4 ext;
	if(ext.Open(Path)>0 && (ext.SetBaseOffset(Offset),1) && ext.Config()>0)
	{
		printf("Open Succes\n");
		CFileScanner* pFileScanner = ext.newCFileScannerObject();
		SExtFileInfoSet ExtSet;
		pFileScanner->AddInfoSet(&ExtSet);
		fprintf(stderr,"Start Scaning....\n");
		pFileScanner->ScanFile(0);
		fprintf(stderr,"Scan Complete\n");
		if(ExtSet.size()>0)
		{
			printf("Ext: %d Files\n",ExtSet.size());
			int i;
			for(i=0;i<ExtSet.size();++i)
			{
				SFileInfo* pFileInfo = (SFileInfo*)ExtSet.at(i);
				wprintf(L"%I64X: %d : %s : %I64d Bytes\n",pFileInfo->GetOffset(),pFileInfo->GetParentIndex(),
						pFileInfo->GetFileName().c_str(),pFileInfo->GetFileSize());
				fflush(stdout);
			}
		}

		delete pFileScanner;
	}
	else
	{
		printf("Open Failed\n");
	}
}

int main(int argc,char **argv)
{
	const char* Path = NULL;
	DWORDLONG Offset = 0;
	setlocale(LC_ALL,"");
	if(argc>1)
	{
		if(argv[1][0]=='-')
		{
			if(argv[1][1]=='v' && argc >= 4)
			{
				Path = argv[2];
				sscanf(argv[3],"%I64X",&Offset);
				ListAllExtFile(Path,Offset);
				return 1;
			}
		}
	}
	help();
	return 0;
}
