<<<<<<< HEAD
//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/locks.hpp>

// template <class Mutex> class shared_lock;

// template <class Rep, class Period>
//   bool try_lock_for(const chrono::duration<Rep, Period>& rel_time);

#include <boost/thread/lock_types.hpp>
//#include <boost/thread/shared_mutex.hpp>
#include <boost/detail/lightweight_test.hpp>

bool try_lock_for_called = false;

typedef boost::chrono::milliseconds ms;

struct shared_mutex
{
  template <class Rep, class Period>
  bool try_lock_shared_for(const boost::chrono::duration<Rep, Period>& rel_time)
  {
    BOOST_TEST(rel_time == ms(5));
    try_lock_for_called = !try_lock_for_called;
    return try_lock_for_called;
  }
  void unlock_shared()
  {
  }
};

shared_mutex m;

int main()
{
  boost::shared_lock<shared_mutex> lk(m, boost::defer_lock);
  BOOST_TEST(lk.try_lock_for(ms(5)) == true);
  BOOST_TEST(try_lock_for_called == true);
  BOOST_TEST(lk.owns_lock() == true);
  try
  {
    lk.try_lock_for(ms(5));
    BOOST_TEST(false);
  }
  catch (boost::system::system_error& e)
  {
    BOOST_TEST(e.code().value() == boost::system::errc::resource_deadlock_would_occur);
  }
  lk.unlock();
  BOOST_TEST(lk.try_lock_for(ms(5)) == fal
=======
#include "FileSystem.h"

#include <map>
using std::map;

int main()
{
//*
    CHfsPlus a;
    CDisk c;
    c.Open("F:\\hfsfilesystem.img");
    //c.Open("\\\\.\\PHYSICALDRIVE1");
    //c.SetBaseOffset(0X100000);
    a.Set(&c);
    int ret = a.Config();
    printf("ret:%d\n",ret);
    vector<SHfsFileInfo> list;
    a.GetJournalNode(list);
    printf("list size:%d\n",list.size());
    int i;
    for(i=0;i<list.size();++i)
    {
	    wprintf(L"%s %d\n",list.at(i).GetFileName().c_str(),list.at(i).BlockList.GetTotalFrag());
    }
/*
    CFileScanner* sa =  a.newCFileScannerObject();
    SHfsFileInfoSet HfsSet;
    sa->AddInfoSet(&HfsSet);
    sa->ScanFile(0);

    int i;
    for(i=0;i<HfsSet.size();++i)
    {
	    wprintf(L"%d ",((SHfsFileInfo*)HfsSet.at(i))->data.Id);
	    wprintf(L"%d ",((SHfsFileInfo*)HfsSet.at(i))->GetParentIndex());
	    wprintf(L"%d ",((SHfsFileInfo*)HfsSet.at(i))->data.RecordType);
	    wprintf(L"%s\n",((SHfsFileInfo*)HfsSet.at(i))->GetFileName().c_str());
    }

    printf("%d\n",HfsSet.size());

    
    vector<SHfsFileInfo> list;
    a.GetAllNode(list);
    map<int,SHfsFileInfo*> listset;
    {
	    int i;
	    for(i=0;i<list.size();++i)
	    {
		    listset.insert(std::pair<int,SHfsFileInfo*>(list.at(i).data.Id,&list.at(i)));
	    }
    }
    {
	    map<int,SHfsFileInfo*>::iterator i;
	    for(i=listset.begin();i!=listset.end();++i)
	    {
		wprintf(L"%u %u %d %d %lld %s\n",
		i->second->data.Id,
		i->second->data.ParentId,
		i->second->data.Flags,
		i->second->data.RecordType,
		i->second->data.Size,
		i->second->GetFileName().c_str());
	    }
    }
    printf("list:%d\n",list.size());
//*/
    return 0;
}
>>>>>>> a9d47686722050a38fed7e884bf851a3947e531f
