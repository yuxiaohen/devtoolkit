#pragma once

#include "CDisk.h"
#include "CBitlockerDisk.h"
#include "FAT/FileSystem_Fat.h"
#include "NTFS/FileSystem_Ntfs.h"
#include "Partition/CPartition.h"
#include "EXT/FileSystem_EXT.h"
#include "HFSPLUS/FileSystem_HFSPLUS.h"
#include "SInfoSet.h"

LIB_INTERFACE CFileSystem * newCFileSystemObject(CIO* pIO,uint64_t Offset=0,uint64_t Size=0);
