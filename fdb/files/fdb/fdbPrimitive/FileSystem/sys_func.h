﻿#pragma once

#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <stdint.h>
#pragma pack(1)

#define LIB_INTERFACE
//Type Define
enum POS{START_POS=0,CURRENT_POS,END_POS};

//In Windows System
//#define SYS_WINDOWS

//Use Fdb
#define FDB_FUNC

//Windows
#ifdef SYS_WINDOWS
//#define NOMINMAX
#include <windows.h>
typedef HANDLE FILE_HANDLE;
#define INVALID_FILEHANDLE_VALUE INVALID_HANDLE_VALUE
#define INVALID_FILEHANDLE(x) ((x)==INVALID_FILEHANDLE_VALUE)
#endif

//FDB
#ifdef FDB_FUNC
#undef LIB_INTERFACE
#define LIB_INTERFACE FDBInterface
#include <fdb/fdbPrimitive/IOKit/IOKit.h>
typedef FDBCIOSystemDevice* FILE_HANDLE;
#define INVALID_FILEHANDLE_VALUE NULL
#define INVALID_FILEHANDLE(x) ((x)==INVALID_FILEHANDLE_VALUE)
#endif

//Function
FILE_HANDLE LIB_INTERFACE f_SYS_OpenFile(const char* path, uint32_t ReadOnly, uint32_t OpenAlways);

uint32_t LIB_INTERFACE f_SYS_ReadFile(FILE_HANDLE fh,void* buf, uint32_t len);

uint32_t LIB_INTERFACE f_SYS_WriteFile(FILE_HANDLE fh,const void* buf, uint32_t len);

uint64_t LIB_INTERFACE f_SYS_SetPoint(FILE_HANDLE fh,uint64_t offset,int mode);

uint64_t LIB_INTERFACE f_SYS_GetFileSize(FILE_HANDLE fh);

void LIB_INTERFACE f_SYS_CloseFile(FILE_HANDLE fh);



