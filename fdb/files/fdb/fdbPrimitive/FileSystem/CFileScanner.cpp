#include "CFileScanner.h"
CFileScanner::CFileScanner(CFileSystem* pFs):CFileSystem(pFs)
{
    this->mull_Scanned = 0;
    this->mull_Total = 0;
    this->mul_LastScannedSet = -1;
    this->SetBufferSector(256);
}

bool CFileScanner::AddInfoSet(SInfoSet* pInfoSet)
{
    if(pInfoSet!=NULL)
    {
        this->m_InfoSetList.push_back(pInfoSet);
        return true;
    }
    return false;
}

uint32_t CFileScanner::AddInfo(SInfo* pInfo)
{
    uint32_t Total = 0;
    if(pInfo!=NULL)
    {
        uint32_t i;
        for(i=0;i<this->size();++i)
        {
            if(this->at(i)->AddInfo(pInfo))
            {
                ++Total;
            }
        }
    }
    return Total;
}

void CFileScanner::clear()
{
    this->m_InfoSetList.clear();
}

SInfoSet* CFileScanner::at(uint32_t n)
{
    if(n>=this->m_InfoSetList.size())
    {
        return NULL;
    }
    return this->m_InfoSetList.at(n);
}

uint32_t CFileScanner::size()
{
    return this->m_InfoSetList.size();
}

bool CFileScanner::isStart()
{
    return this->GetStatus()==CFileScanner::NORMAL_START;
}

void CFileScanner::Start()
{
    this->SetStatus(CFileScanner::NORMAL_START);
}

void CFileScanner::Stop()
{
    if(this->GetStatus()==CFileScanner::NORMAL_START)
        this->SetStatus(CFileScanner::NORMAL_STOP);
}

uint32_t CFileScanner::ScanFile(uint32_t StartSector)
{
    if(this->GetSectorSize()==0 && this->size()==0)return 0;
    return this->ScanArea(0,StartSector,this->GetPartitionSize()/this->GetSectorSize());
}

uint32_t CFileScanner::in_ScanArea(uint64_t Offset, uint32_t StartSector, uint32_t EndSector)
{
    if( !this->Usable() )return -1;//扫描无法开始
    if(StartSector>=EndSector || this->GetSectorSize()==0)return 0;//参数错误
    if(this->size()==0)return 0;
    uint32_t BufferSector = this->GetBufferSector();
    uint32_t ScanSize =(EndSector-StartSector);
    this->mul_LastScannedSet = -1;
    if(BufferSector==0 || ScanSize==0)return 0;//设置参数错误

    //分配空间
    uint8_t* buf = (uint8_t*)malloc(sizeof(uint8_t)*BufferSector*this->GetSectorSize());
    if(buf==NULL)exit(1);

    uint64_t CurrentOffset = Offset + StartSector * this->GetSectorSize();
    uint32_t CurrentSector = StartSector;

    while(this->isStart()&& CurrentSector < EndSector)
    {
        this->SetPoint(CurrentOffset,CIO::START_POS);

        //当前处理块大小
        uint32_t nBlock =
        BufferSector > EndSector - CurrentSector ? EndSector - CurrentSector : BufferSector;
        uint32_t nRead = nBlock * this->GetSectorSize();
        nRead = this->Read(buf,nRead);

        if(nRead==-1)
        {
            //磁盘断开
            this->SetStatus(ABNORMAL_STOP);
            break;
        }

        if(nRead>0)
        {
            mull_Scanned+=nRead;
            uint32_t i;
            for(i=0;i<this->size();++i)
            {
                SInfoSet * pInfoSet = this->at(i);
                uint32_t LastSize = pInfoSet->size();
                pInfoSet->ReadBlock(CurrentOffset,buf,nRead);
                if(pInfoSet->size()>LastSize)
                {
                    this->mul_LastScannedSet = i;
                }
            }

            CurrentOffset += nRead;
            CurrentSector += nBlock;
        }
        else
        {
            break;
        }
    }
    free(buf);
    this->mul_LastScannedSet = -1;
    return CurrentSector-StartSector;
}

uint32_t CFileScanner::ScanArea(uint64_t Offset, uint32_t StartSector, uint32_t EndSector)
{
    mull_Total = (EndSector - StartSector)*(uint64_t)this->GetSectorSize();
    mull_Scanned = 0;
    this->Start();
    uint32_t Ret = in_ScanArea(Offset,StartSector,EndSector);
    this->Stop();
    return Ret;
}

uint32_t CFileScanner::ScanFragment(uint64_t Offset, CFragmentList *pList, uint32_t StartSector, uint32_t EndSector)
{
    if( pList==NULL || StartSector>=EndSector||pList==NULL)
        return -1;

    mull_Total = pList->GetTotalCluster()*this->GetClusterSize();
    mull_Scanned = 0;

    //分配空间
    uint32_t BufferSector = this->GetBufferSector();
    uint8_t* buf = (uint8_t*)malloc(sizeof(uint8_t)*BufferSector*this->GetSectorSize());
    if(buf==NULL)exit(1);

    this->Start();

    uint32_t TotalSector = 0;//遍历过的扇区
    uint32_t i;
    for(i=0;this->isStart()&&i<pList->GetTotalFrag();++i)
    {
        uint64_t FragOffset = Offset + pList->GetOffset(i)*this->GetClusterSize();//偏移量
        uint64_t FragSector =
        pList->GetDecompressNum(i)*this->GetClusterSize()/this->GetSectorSize();//扇区个数

        uint32_t FragStartSector = 0;
        uint32_t FragEndSector = FragSector;

        //还没到达开始读取扇区
        if(StartSector>=TotalSector + FragSector)
        {
            TotalSector += FragSector;
            continue;
        }
        else if(StartSector > TotalSector)//已经到达起始读取扇区
        {
            FragStartSector = StartSector - TotalSector;
        }

        //已经到达最后读取扇区
        if(EndSector<TotalSector + FragSector)
        {
            FragEndSector = EndSector - TotalSector;
        }

        uint32_t Ret = this->in_ScanArea(FragOffset,FragStartSector,FragEndSector);

        TotalSector += Ret;

        if(EndSector<=TotalSector)
        {
            //读取完毕
            break;
        }
    }
    this->Stop();
    return TotalSector-StartSector;
}
