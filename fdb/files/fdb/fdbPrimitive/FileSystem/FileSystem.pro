message("       --------------------->FDB FileSystem")

TEMPLATE = lib
CONFIG += dll

CONFIG(debug,debug|release) {
    TARGET = libFDBFileSystemd
} else {
    TARGET = libFDBFileSystem
}

DEFINES += COMPILE_FDB_AS_LIBRARY
include(x:/fdb/fdb.pri)
include(x:/boost/boost.pri)

SOURCES += \
    sys_func.cpp \
    string2wstring.cpp \
    FileSystem.cpp \
    fdb_func.cpp \
    CFragmentReader.cpp \
    CFragmentList.cpp \
    CFileSystem.cpp \
    CFileScanner.cpp \
    CFileOperate.cpp \
    CBitMap.cpp \
    Partition/SPartitionInfoSet.cpp \
    Partition/CPartition.cpp \
    EXT/SExtFileInfoSet.cpp \
    EXT/ext_diritem.cpp \
    EXT/CExtFileScanner.cpp \
    EXT/CExtFileReader.cpp \
    EXT/CExt4.cpp \
    EXT/CExt2_3.cpp \
    EXT/CExt.cpp \
    FAT/SFatFileInfoSet.cpp \
    FAT/fat_struct.cpp \
    FAT/exfat_struct.cpp \
    FAT/CFatFileScanner.cpp \
    FAT/CFatFileReader.cpp \
    FAT/CFatDir.cpp \
    FAT/CFat32.cpp \
    FAT/CFat16.cpp \
    FAT/CFat.cpp \
    FAT/CExfat.cpp \
    FAT/CDirItem.cpp \
    NTFS/SNtfsFileInfoSet.cpp \
    NTFS/ntfs_struct.cpp \
    NTFS/ntfs_decompress.cpp \
    NTFS/ntfs_datarun.cpp \
    NTFS/CNtfsFileScanner.cpp \
    NTFS/CNtfsFileReader.cpp \
    NTFS/CNtfs.cpp \
    NTFS/CMftDir.cpp \
    NTFS/CMft.cpp \
    HFSPLUS/CHfsPlus.cpp \
    HFSPLUS/CHfsPlusFileScanner.cpp \
    HFSPLUS/SHfsFileInfoSet.cpp \
    HFSPLUS/CHfsPlusFileReader.cpp \
    CBitlockerDisk.cpp


message("       <---------------------FDB FileSystem")

OTHER_FILES +=

HEADERS += \
    sys_func.h \
    string2wstring.h \
    SInfoSet.h \
    SInfo.h \
    FileSystem.h \
    drive_struct.h \
    dosdatetime.h \
    CFragmentReader.h \
    CFragmentList.h \
    CFileSystem.h \
    CFileScanner.h \
    CFileReader.h \
    CFileOperate.h \
    CDisk.h \
    CBitMap.h \
    Partition/SPartitionInfoSet.h \
    Partition/SPartitionInfo.h \
    Partition/CPartition.h \
    EXT/SExtFileInfoSet.h \
    EXT/SExtFileInfo.h \
    EXT/FileSystem_EXT.h \
    EXT/ext4_inode.h \
    EXT/ext_type.h \
    EXT/ext_superblock.h \
    EXT/ext_inode.h \
    EXT/ext_diritem.h \
    EXT/ext_blockgroup.h \
    EXT/CExtFileScanner.h \
    EXT/CExtFileReader.h \
    EXT/CExt4.h \
    EXT/CExt2_3.h \
    EXT/CExt.h \
    FAT/SFatFileInfoSet.h \
    FAT/SFatFileInfo.h \
    FAT/FileSystem_Fat.h \
    FAT/fat_struct.h \
    FAT/exfat_struct.h \
    FAT/CFatFileScanner.h \
    FAT/CFatFileReader.h \
    FAT/CFatDir.h \
    FAT/CFat32.h \
    FAT/CFat16.h \
    FAT/CFat.h \
    FAT/CExfat.h \
    FAT/CDirItem.h \
    NTFS/SNtfsFileInfoSet.h \
    NTFS/SNtfsFileInfo.h \
    NTFS/ntfs_struct.h \
    NTFS/ntfs_decompress.h \
    NTFS/ntfs_datarun.h \
    NTFS/FileSystem_Ntfs.h \
    NTFS/CNtfsFileScanner.h \
    NTFS/CNtfsFileReader.h \
    NTFS/CNtfs.h \
    NTFS/CMftDir.h \
    NTFS/CMft.h \
    CIO.h \
    HFSPLUS/hfs_volume_header.h \
    HFSPLUS/CHfsPlus.h \
    HFSPLUS/SHfsFileInfo.h \
    HFSPLUS/CHfsPlusFileScanner.h \
    HFSPLUS/SHfsFileInfoSet.h \
    HFSPLUS/CHfsPlusFileReader.h \
    EXT/ext_journal.h \
    CBitlockerDisk.h
