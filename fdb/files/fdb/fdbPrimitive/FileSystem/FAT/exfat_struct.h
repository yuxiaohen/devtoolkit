﻿#pragma once

#include "../drive_struct.h"
#include "../sys_func.h"
#include "../dosdatetime.h"

//计算单个字符的效验和,a为老效验和,b为字符
#define CHECKSUM_CALC(a,b) ( ( (a) <<15 | (a)>> 1) + b)

typedef struct EXFAT_BPB
{
    uint64_t HideSector;//隐藏扇区数, 从MBR(EBR) 到 分区头部的扇区数
    uint64_t TotalSectorCount;//分区总扇区数
    uint32_t     FATStartSector;//FAT起始扇区号
    uint32_t     FATTableSector;//FAT表扇区数
    uint32_t     FirstClusterSector;//首簇起始扇区号
    uint32_t     PartitionCluster;//分区内总簇数
    uint32_t     RootCluster;//根目录首簇号
    uint8_t      VolumeSerialNum[4];//卷序列号
    uint16_t      VolumeVersion;//卷版本描述
    uint16_t      VolumeSign;//卷标志
    uint8_t      uint8_tsPerSector;//每扇区字节数(2^n);
    uint8_t      SectorPerCluster;//每簇扇区数(2^n);
    uint8_t      FATCount;//Fat表个数
    uint8_t      DriverMark;//驱动标记
    uint8_t      Percentage;//硬盘使用百分数
    uint8_t      Reserved[7];//保留
}EXFAT_BPB;

typedef struct EXFAT_DBR
{
    uint8_t JMP[3];
    uint8_t OEM[8];
    uint8_t NoUse[53];

//0X40
    EXFAT_BPB bpb;//BPB

    uint8_t Code[390];//引导代码

    uint16_t MagicNum;//0X55AA

}EXFAT_DBR;

typedef struct EXFAT_Attr1
{
    uint8_t  Type;//目录项类型,属性1特征值为85H
    uint8_t  AttachDir;//附属目录项数
    uint16_t  CheckSum;//效验和
    uint32_t FileAttribute;//文件属性

    uint32_t CreateTime;//文件创建时间
    uint32_t LastChange;//文件最后修改时间
    uint32_t LastAccess;//文件最后访问时间
    uint8_t  CreateTime10ms;//文件创建时间精确到10ms
    uint8_t  Reserve1[3];//保留
    uint8_t  Reserve2[8];//保留
}EXFAT_Attr1;

typedef struct EXFAT_Attr2
{
    uint8_t Type;//目录项类型 ,特征值为C0H
    uint8_t FileFragment;//文件碎片标志
    uint8_t Reserve1;//保留
    uint8_t FileNameLength;//文件名字符数
    uint16_t FileNameHash;//文件名Hash值
    uint16_t Reserve2;//保留
    uint64_t FileSize1;//文件大小1;
    uint32_t Reserve3;//保留
    uint32_t StartCluster;//起始簇号
    uint64_t FileSize2;//文件大小2;
}EXFAT_Attr2;

typedef struct EXFAT_Attr3
{
    uint8_t Type;//目录项类型 特征值为C1H
    uint8_t Reserve;//保留
    uint8_t FileName[30];// 文件名
}EXFAT_Attr3;

typedef struct EXFAT_Volume//卷标结构
{
    uint8_t Type;//默认为83H 如果无卷标为03H
    uint8_t VolumeLength;//卷标字符数
    uint8_t Volume[30];//卷标,Unicode
}EXFAT_Volume;

typedef struct EXFAT_Bitmap//簇位图结构
{
    uint8_t Type;//默认为81H
    uint8_t Reserve[1+18];//保留
    uint32_t StartCluster;//起始簇号
    uint64_t FileSize;//文件大小
}EXFAT_Bitmap;

typedef struct EXFAT_Upcase//大写字符文件
{
    uint8_t Type;//默认为82H
    uint8_t Reserve[3+16];//保留(书上是3+14,有误)
    uint32_t StartCluster;//起始簇号
    uint64_t FileSize;//文件大小
}EXFAT_Upcase;

int f_EXFAT_Requires(const void* dbr);

