#pragma once

#include "CFat.h"
#include "../CFileReader.h"
#include "../CFragmentReader.h"
#include "SFatFileInfoSet.h"

class CFat;

class LIB_INTERFACE CFatFileReader:public CFileReader
{
public:
    CFatFileReader(CFileSystem* pFileSystem = NULL);
    virtual ~CFatFileReader(){}
    bool SetFileSystem(CFileSystem* pFileSystem);
    bool Config(SInfoSet *pInfoSet);
    void Config(uint64_t DataOffset,uint32_t ClusterSize);
    bool FromFile(SFileInfo* pInfo);
    uint32_t ReadBlock(uint32_t Index,uint8_t* OutBuf,uint32_t Size);
    uint32_t ReadNextBlock(uint8_t* OutBuf,uint32_t Size);
private:
    CFileSystem* mp_Fat;
    SFatFileInfo* mp_Info;
    CFragmentReader m_Reader;
    uint64_t mull_DataOffset;
    uint32_t mul_ClusterSize;
};
