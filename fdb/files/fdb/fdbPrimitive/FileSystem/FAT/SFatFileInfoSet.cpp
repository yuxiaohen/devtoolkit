#include "SFatFileInfoSet.h"
//*******************************************Fat***********************************//
SFatFileInfoSet::SFatFileInfoSet()
{
    this->mul_SectorSize = SECTOR_SIZE;
}

bool SFatFileInfoSet::AddInfo(SInfo* pInfo)
{
    if(pInfo!=NULL && ( pInfo->GetType()==CFileSystem::FAT32 || pInfo->GetType()==CFileSystem::FAT16) )
    {
        {
            //进行簇筛选
            {
                uint32_t FatTable= 0X10000000;

                if(((SFatFileInfo *)pInfo)->Item.FirstCluster()!=0 && FatTable>0 && ((SFatFileInfo *)pInfo)->Item.FirstCluster() >= FatTable)
                {
                    return false;
                }
            }

            if(((SFatFileInfo *)pInfo)->Item.isDel())
            {
                uint32_t i = this->m_InfoList.size();
                while(i>0)
                {
                    --i;
                    if(this->m_InfoList.at(i).Item.Size()>0)
                    {
                        ((SFatFileInfo *)pInfo)->Item.m_FatItem.FAT32HighWord = this->m_InfoList.at(i).Item.m_FatItem.FAT32HighWord;
                        break;
                    }
                }
            }
            this->m_InfoList.push_back(*(SFatFileInfo*)pInfo);
        }
        return true;
    }
    return false;
}
uint32_t SFatFileInfoSet::ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize)
{
    uint32_t Total = 0;
    while(Total+FATITEM_SIZE<=BlockSize)
    {
        uint32_t Remainuint8_t = (BlockSize-Total)/FATITEM_SIZE;
        uint32_t Ret = this->m_FatInfo.Item.ReadItem(pBlock+Total,Remainuint8_t );
        if(this->m_FatInfo.Item.Usable())            //已读取完
        {
            this->m_FatInfo.data.Offset = Offset + Total;
            this->m_FatInfo.data.ParentIndex = -1;
            this->AddInfo(&this->m_FatInfo);
            this->m_FatInfo.Clear();
        }
        else if( Ret < Remainuint8_t ) //在当前块内搜索出错,跳转到下一个扇区
        {
            //this->m_FatInfo.Clear();
            Total /= this->mul_SectorSize;
            Total *= this->mul_SectorSize;
            Total += this->mul_SectorSize;
            continue;
        }
        Total += (Ret+1)*FATITEM_SIZE;
    }
    return Total;
}

uint32_t SFatFileInfoSet::GetType()
{
    return CFileSystem::FAT32;
}

uint32_t SFatFileInfoSet::size()
{
    return this->m_InfoList.size();
}
SInfo* SFatFileInfoSet::at(uint32_t index)
{
    if(index>=this->m_InfoList.size())
    {
        return NULL;
    }
    return &this->m_InfoList.at(index);
}
void SFatFileInfoSet::clear()
{
    this->m_InfoList.clear();
}

//*********************************Exfat*******************************************//
SExFatFileInfoSet::SExFatFileInfoSet()
{
    this->mul_SectorSize = SECTOR_SIZE;
}

bool SExFatFileInfoSet::AddInfo(SInfo* pInfo)
{
    if(pInfo!=NULL && pInfo->GetType()==CFileSystem::EXFAT )
    {
        {
            //效验和
            {
                if(!((SExFatFileInfo*)pInfo)->Item.CheckSumCorrect)
                {
                    return false;
                }
            }
            //进行簇筛选
            {
                uint32_t ExFatTable = 0X10000000;

                if(((SExFatFileInfo*)pInfo)->Item.FirstCluster()!=0 && ExFatTable>0 && ((SExFatFileInfo*)pInfo)->Item.FirstCluster() >= ExFatTable)
                {
                    return false;
                }
            }
            this->m_InfoList.push_back(*(SExFatFileInfo*)pInfo);
        }
        return true;
    }
    return false;
}
uint32_t SExFatFileInfoSet::ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize)
{
    uint32_t Total = 0;
    while(Total+FATITEM_SIZE<=BlockSize)
    {
        uint32_t Remainuint8_t = (BlockSize-Total)/FATITEM_SIZE;
        uint32_t Ret = this->m_ExFatInfo.Item.ReadItem(pBlock+Total,Remainuint8_t );
        if(this->m_ExFatInfo.Item.Usable())            //已读取完
        {
            this->m_ExFatInfo.data.Offset = Offset + Total;
            this->m_ExFatInfo.data.ParentIndex = -1;
            this->AddInfo(&this->m_ExFatInfo);
            this->m_ExFatInfo.Clear();
        }
        else if( Ret < Remainuint8_t ) //在当前块内搜索出错,跳转到下一个扇区
        {
            Total /= this->mul_SectorSize;
            Total *= this->mul_SectorSize;
            Total += this->mul_SectorSize;
            continue;
        }
        Total += (Ret+1)*FATITEM_SIZE;
    }
    return Total;
}

uint32_t SExFatFileInfoSet::GetType()
{
    return CFileSystem::EXFAT;
}

uint32_t SExFatFileInfoSet::size()
{
    return this->m_InfoList.size();
}
SInfo* SExFatFileInfoSet::at(uint32_t index)
{
    if(index>=this->m_InfoList.size())
    {
        return NULL;
    }
    return &this->m_InfoList.at(index);
}
void SExFatFileInfoSet::clear()
{
    this->m_InfoList.clear();
}
