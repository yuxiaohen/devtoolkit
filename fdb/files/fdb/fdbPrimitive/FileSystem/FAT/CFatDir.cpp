#include "CFatDir.h"
CFatDir::CFatDir(CFat* pCFat)
{
    mp_SectorBuf = NULL;
    mp_Fat = NULL;
    this->Set(pCFat);
}

CFatDir::~CFatDir()
{
    if(mp_SectorBuf!=NULL)
    free(mp_SectorBuf);
}

bool CFatDir::Set(CFat* pCFat)
{
    if(pCFat!=NULL)
    {
        mp_Fat = pCFat;
        mul_ClusterSize = pCFat->GetClusterSize();

        if(mul_ClusterSize==0)return false;

        if(mp_SectorBuf!=NULL)
        {
            free(mp_SectorBuf);
            mp_SectorBuf = NULL;
        }

        mp_SectorBuf = (uint8_t*)malloc(sizeof(uint8_t)*this->mul_ClusterSize);
        if(mp_SectorBuf==NULL)exit(1);

        return true;
    }
    return false;
}

bool CFatDir::GetFirstDirItem(uint64_t Offset,CDirItem* pDirItem)
{
    if(pDirItem==NULL || mp_Fat==NULL || !mp_Fat->Usable() || this->mul_ClusterSize==0)return false;
    mul_nFileItemRead = 0;
    mull_Offset = Offset;

    return GetNextDirItem(pDirItem);
}

bool CFatDir::GetNextDirItem(CDirItem* pDirItem)
{
    if(pDirItem==NULL || mp_Fat==NULL || !mp_Fat->Usable() || this->mul_ClusterSize==0)return false;

    pDirItem->Clear();
    bool find = false;
    while(!find)
    {
        if(mul_nFileItemRead==0)
        {
            mp_Fat->SetPoint(mull_Offset,CDisk::START_POS);
            if(mp_Fat->Read(mp_SectorBuf,this->mul_ClusterSize)<=0)
            {
                break;
            }
        }
        uint32_t nItemNeedToRead = this->mul_ClusterSize/FATITEM_SIZE-mul_nFileItemRead;
        uint32_t nRead = pDirItem->ReadItem(mp_SectorBuf+mul_nFileItemRead*FATITEM_SIZE,nItemNeedToRead);

        mul_nFileItemRead += nRead+1;

        if(!pDirItem->Usable())
        {
            if(nRead<nItemNeedToRead)//遇到非法目录项,读取结束
            {
                break;
            }
        }
        else    //成功读取目录项
        {
            find = true;
        }

        uint32_t DataOffset = mp_Fat->GetDataOffset();
        if( mul_nFileItemRead>=mul_ClusterSize/FATITEM_SIZE )
        {
            if(
                   mull_Offset<DataOffset
              ) //读取FDT目录
            {
                mull_Offset+=this->mul_ClusterSize;
            }
            else
            {
                uint32_t NextOffset = mp_Fat->GetNextCluster(mull_Offset);
                if(NextOffset==0)
                {
                    break;
                }
                mull_Offset = NextOffset;
            }
            mul_nFileItemRead = 0;
        }
    }
    return find;
}
