#include "fat_struct.h"
#include <stdlib.h>
#include "locale.h"

int f_FAT16_Requires(const void* dbr)
{
    return !memcmp(&((const char*)dbr)[0X36],"FAT16",5) &&
           HEAD_SECTOR_USABLE(dbr);
}

int f_FAT32_Requires(const void* dbr)
{
    return !memcmp(&((const char*)dbr)[0X52],"FAT32",5) &&
           HEAD_SECTOR_USABLE(dbr);
}

void f_FAT_LongFileNameToWstring(FAT_LONGFILE *pLongFile,wchar_t*LongFileName)//将长文件名目录项中的文件名放入wchar_t[13]中
{
    if(pLongFile!=NULL && LongFileName!=NULL)
    {
        uint32_t s = 12;
        LongFileName[s--] = pLongFile->FileName12to13[1];
        LongFileName[s--] = pLongFile->FileName12to13[0];
        LongFileName[s--] = pLongFile->FileName6to11[5];
        LongFileName[s--] = pLongFile->FileName6to11[4];
        LongFileName[s--] = pLongFile->FileName6to11[3];
        LongFileName[s--] = pLongFile->FileName6to11[2];
        LongFileName[s--] = pLongFile->FileName6to11[1];
        LongFileName[s--] = pLongFile->FileName6to11[0];
        LongFileName[s--] = pLongFile->FileName1to5[4];
        LongFileName[s--] = pLongFile->FileName1to5[3];
        LongFileName[s--] = pLongFile->FileName1to5[2];
        LongFileName[s--] = pLongFile->FileName1to5[1];
        LongFileName[s--] = pLongFile->FileName1to5[0];
    }
}
uint32_t f_FAT_GetLongFileName(uint8_t* buffer,wchar_t**pLongFileName)  //获取目录项,将长目录文件名字串放入堆内存中(注意释放堆内存)
{
    uint32_t i = 0;
    uint32_t n = 0;
    if(buffer!=NULL && pLongFileName!=NULL)
    {
        FAT_LONGFILE *pLongFile = (FAT_LONGFILE *)buffer;
        wchar_t *LongFileName = NULL;
        uint8_t c = pLongFile[i].SerialNumber;
        n = c&0X3F;//长文件名目录项数

        LongFileName = (wchar_t *)malloc( (n*13+1)*sizeof(wchar_t) );

        {
            uint32_t s = n*13;

            *pLongFileName = LongFileName;
            LongFileName[s--] = 0;
            do
            {
                if(pLongFile[i].FileAttribute == 0X0F)
                {
                    s-=12;
                    f_FAT_LongFileNameToWstring(&pLongFile[i],&LongFileName[s--]);
                }
                else
                {
                    break;
                }
                ++i;
            }
            while(s>=0 && i<n);
        }
    }
    return n;
}
