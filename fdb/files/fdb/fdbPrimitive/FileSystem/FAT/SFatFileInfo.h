#pragma once

#include "../SInfo.h"
#include "CDirItem.h"

//因为Fat系列文件系统的目录项结构都比较小 (Fat和Fat32 一个文件的目录项只有32B, Exfat一个文件的目录项是64B)
//于是将Fat文件系统的目录项放进Info结构

struct SFatFileInfo:public SFileInfo//Fat和Fat32文件系统同样的目录项
{
    uint64_t GetOffset()
    {
        return data.Offset;
    }

    uint32_t GetParentIndex()
    {
        return data.ParentIndex;
    }

    bool isDir()
    {
        return Item.isDir();
    }

    bool isDel()
    {
        return  Item.isDel();
    }

    wstring& GetFileName()
    {
        return Item.mwstr_FileName;
    }

    uint32_t GetType()
    {
        return CFileSystem::FAT32;
    }

    uint64_t GetFileSize()
    {
        return Item.Size();
    }

    uint32_t FirstCluster()
    {
        return Item.FirstCluster();
    }

    void Clear()
    {
        data.Offset = 0;
        data.ParentIndex = -1;
        Item.Clear();
    }

    struct
    {
        uint64_t Offset;//偏移量
        uint32_t ParentIndex;//父目录项编号
    }data;

    CFatDirItem Item;//目录项结构类
};

struct SExFatFileInfo:public SFileInfo//ExFat文件系统的目录项
{
    uint64_t GetOffset()
    {
        return data.Offset;
    }

    uint32_t GetParentIndex()
    {
        return data.ParentIndex;
    }

    bool isDir()
    {
        return Item.isDir();
    }

    bool isDel()
    {
        return  Item.isDel();
    }

    wstring& GetFileName()
    {
        return Item.mwstr_FileName;
    }

    uint64_t GetFileSize()
    {
        return Item.Size();
    }

    uint32_t GetType()
    {
        return CFileSystem::EXFAT;
    }

    uint32_t FirstCluster()
    {
        return Item.FirstCluster();
    }

    void Clear()
    {
        data.Offset = 0;
        data.ParentIndex = -1;
        Item.Clear();
    }

    struct
    {
        uint64_t Offset;//偏移量
        uint32_t ParentIndex;//父目录项编号
    }data;

    CExFatDirItem Item;//目录项结构类
};
