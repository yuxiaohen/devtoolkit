#include "CExFat.h"

CExFat::CExFat(CFileSystem* pFs):CFat(pFs)
{
    mull_RootDir = 0;
}

CExFat::~CExFat()
{

}

bool CExFat::DbrSet(void* DbrBuf)
{
    if(DbrBuf==NULL||!f_EXFAT_Requires(DbrBuf)) return false;

    EXFAT_DBR * Dbr = (EXFAT_DBR *)DbrBuf;

    this->SetClusterSize( 1<< (Dbr->bpb.SectorPerCluster + Dbr->bpb.uint8_tsPerSector) );
    this->SetSectorSize(Dbr->bpb.uint8_tsPerSector);
    this->SetPartitionSize(Dbr->bpb.TotalSectorCount*Dbr->bpb.uint8_tsPerSector);

    mul_FatSize = Dbr->bpb.FATTableSector*this->GetSectorSize();
    mul_nFat = Dbr->bpb.FATCount;

    this->mul_FirstClusterNum = Dbr->bpb.RootCluster;

    uint64_t offset = Dbr->bpb.FATStartSector*this->GetSectorSize();
    mull_FirstFatOffset = offset;

    offset = Dbr->bpb.FirstClusterSector*(uint64_t)this->GetSectorSize();
    offset += (this->mul_FirstClusterNum-2)*(uint64_t)this->GetClusterSize();
    mull_DataOffset = offset;

    mull_RootDir = offset;

    return true;
}


uint32_t CExFat::GetNextClusterIndex(uint32_t cluster)
{
    if(mp_FAT==NULL)return cluster+1;
    if(cluster>=mul_FatSize)return 0;

    uint32_t next_cluster = ((uint32_t*)mp_FAT)[cluster];

    if(next_cluster==0XFFFFFFFF)//判断下一个簇是否被使用 (值为 8个F, )
    {
        return 0;
    }

    if(next_cluster==0X0) //若为0 则下一个簇被占用
    {
        next_cluster = cluster + 1;
    }

    return next_cluster;
}

bool CExFat::GetBitMap(CBitMap *bitmap)
{
    if(bitmap!=NULL)
    {
        bitmap->CleanBitMap();
        uint8_t buf[DBR_SIZE];

        //遍历根目录,读取出BitMap目录项的地址
        EXFAT_Bitmap* pBm = NULL;
        uint64_t ReadOffset = mull_RootDir;

        while( pBm==NULL && ReadOffset!=0)
        {
            this->SetPoint(ReadOffset,START_POS);
            uint32_t nRead = 0;
            while( pBm==NULL && this->Read(buf,DBR_SIZE)>0 )
            {
                EXFAT_Bitmap *pi = (EXFAT_Bitmap *)buf;
                uint32_t i;
                //扫描这个扇区内有没有簇位图目录项
                for(i=0; i<DBR_SIZE/sizeof(EXFAT_Bitmap); ++i)
                {
                    if(pi[i].Type==0X81 || pi[i].Type==0X01)//0X81H是簇位图目录项标志
                    {
                        pBm = &pi[i];
                    }
                }
                nRead += DBR_SIZE;
                if(nRead>=this->GetClusterSize())//当读取完一个簇后,准备在下一个簇读取
                {
                    break;
                }
            }
            ReadOffset = GetNextClusterIndex(ReadOffset);
        }

        //如果找到了BitMap目录项
        if(pBm!=NULL)
        {
            uint32_t     StartCluster = pBm->StartCluster;
            uint64_t FileSize = pBm->FileSize;
            ReadOffset = GetOffsetByCluster(StartCluster);

            if(ReadOffset==0)
            {
                return false;
            }
            uint8_t *pBuf = (uint8_t*)malloc(sizeof(uint8_t)*this->GetClusterSize());
            if(pBuf==NULL)
            {
                exit(1);
            }
            while(ReadOffset!=0
                    && (this->SetPoint(ReadOffset,START_POS),1)
                    && this->Read(pBuf,this->GetClusterSize())>0)
            {
                if(FileSize<=this->GetClusterSize())
                {
                    bitmap->ReadBitMap(pBuf,FileSize);
                    free(pBuf);
                    return true;
                }
                else
                {
                    bitmap->ReadBitMap(pBuf,this->GetClusterSize());
                    FileSize -= this->GetClusterSize();
                }
                ReadOffset = GetNextClusterIndex(ReadOffset);//通过FAT分配表获取下一个簇地址
            }
            free(pBuf);
        }
    }
    return false;
}

wstring CExFat::GetVolumeName()
{
    wstring Volume;
    if(this->Usable())
    {
        uint8_t buf[DBR_SIZE];

        //遍历根目录,读取出卷标目录项的地址
        EXFAT_Volume* pVol = NULL;
        uint64_t ReadOffset = GetRootDirOffset();

        while( pVol==NULL && ReadOffset!=0)
        {
            this->SetPoint(ReadOffset,START_POS);
            uint32_t nRead = 0;
            while( pVol==NULL && this->Read(buf,DBR_SIZE)>0 )
            {
                EXFAT_Volume *pi = (EXFAT_Volume *)buf;
                uint32_t i;
                //扫描这个扇区内有没有簇位图目录项
                for(i=0; i<DBR_SIZE/sizeof(EXFAT_Volume); ++i)
                {
                    if(pi[i].Type==0X83)//0X83H是卷标目录项标志
                    {
                        pVol = &pi[i];
                    }
                }
                nRead += DBR_SIZE;
                if(nRead>=this->GetClusterSize())//当读取完一个簇后,准备在下一个簇读取
                {
                    break;
                }
            }
            ReadOffset = GetNextCluster(ReadOffset);
        }

        if(pVol!=NULL)
        {
            Volume.assign((wchar_t*)pVol->Volume,pVol->VolumeLength);
        }
    }
    return Volume;
}

