﻿#pragma once

#include "fat_struct.h"
#include "exfat_struct.h"
#include "../CFileSystem.h"
#include "../CBitMap.h"

#include <string>
using std::wstring;

class LIB_INTERFACE CFat:public CFileSystem
{
public:
    CFat(CFileSystem* pFs = NULL);
virtual ~CFat();

private:
    CFat(CFat& );

public:
    //从DBR内存区域或者DBR偏移量内读取DBR结构
    int     Config(uint64_t DbrOffset=0,const void *dbr_buf=NULL);//返回值大于0 正常 ,等于0 读取出错, 等于-1 读取dbr出错,等于-2读取FAT表出错
    void    Close();    //关闭

    virtual CFileScanner* newCFileScannerObject();
    
    virtual CFileReader* newCFileReaderObject(SInfoSet* pInfoSet = NULL);

    uint64_t   GetDataOffset()
    {
        return mull_DataOffset;
    }

    void        SetDataOffset(uint64_t n)
    {
        mull_DataOffset = n;
    }

    uint32_t       GetFatTableSize()
    {
        return mul_FatSize;
    }

    uint64_t   GetOffsetByCluster(uint32_t ClusterNum);

    uint64_t   GetNextCluster(uint64_t CurrentOffset);

    CFragmentList GetFragmentList(uint32_t StartCluster,uint32_t nCluster);//通过文件起始簇获取碎片列表,指定最多读取簇个数(nCluster)

    virtual uint64_t GetRootDirOffset()=0;//获取根目录偏移量 在Fat32中是Data域的开始
    virtual uint32_t     GetNextClusterIndex(uint32_t cluster)=0;
    virtual bool DbrSet(void* DbrBuf)=0;
    virtual bool GetBitMap(CBitMap* bitmap)=0; //获取磁盘当前簇位图
    virtual wstring GetVolumeName();//获取卷标

private:
    void Init();//初始化

protected:
    uint8_t* mp_FAT;//将FAT表保存进内存
    uint32_t mul_FirstClusterNum;//首簇编号(通常为2)
    uint32_t mul_FatSize;//Fat表大小
    uint32_t mul_nFat;//Fat表个数
    uint64_t mull_FirstFatOffset;//第一个Fat表偏移量
    uint64_t mull_DataOffset;//数据域偏移量
};
