#pragma once

#include "CFat.h"
#include "../CFileScanner.h"
#include "SFatFileInfoSet.h"

#include <queue>
using std::queue;

class LIB_INTERFACE CFatFileScanner:public CFileScanner
{
public:
    CFatFileScanner(CFat* pFat = NULL);
    int Set(CFat* pFat);
    uint32_t ScanFile(uint32_t StartSector);
    uint32_t ScanFreeCluster(uint32_t StartSector);
private:
    CFat* mp_Fat;
};
