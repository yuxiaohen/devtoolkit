#include "exfat_struct.h"

int f_EXFAT_Requires(const void* dbr)
{
    return !strncmp(&((const char*)dbr)[0X03],"EXFAT",5) &&
           HEAD_SECTOR_USABLE(dbr);
}
