#include "CFat16.h"

CFat16::CFat16(CFileSystem* pFs):CFat(pFs)
{
    mull_FdtOffset = 0;
}

CFat16::~CFat16()
{

}

bool CFat16::DbrSet(void* DbrBuf)
{
    if(DbrBuf==NULL||!f_FAT16_Requires(DbrBuf)) return false;

    FAT16_DBR * Dbr = (FAT16_DBR *)DbrBuf;

    this->SetPartitionSize( (Dbr->bpb.TotalSectorCount+Dbr->bpb.SectorCount)*Dbr->bpb.BytePerSector);
    this->SetSectorSize(Dbr->bpb.BytePerSector);
    this->SetClusterSize( Dbr->bpb.SectorPerCluster*Dbr->bpb.BytePerSector );
    mul_FatSize = Dbr->bpb.SectorPerFAT*Dbr->bpb.BytePerSector;
    mul_nFat = Dbr->bpb.FATCount;
    this->mul_FirstClusterNum = 2;
    uint64_t offset = Dbr->bpb.ReserveSector*Dbr->bpb.BytePerSector;
    mull_FirstFatOffset = offset;

    offset += mul_nFat * mul_FatSize;
    mull_FdtOffset = offset;

    offset += Dbr->bpb.RootItemCount*sizeof(FAT_SHORTFILE);
    mull_DataOffset = offset;

    return true;
}



uint32_t CFat16::GetNextClusterIndex(uint32_t cluster)
{
    if(mp_FAT==NULL)return cluster+1;
    if(cluster>=mul_FatSize)return 0;

    uint32_t next_cluster = ((uint16_t*)mp_FAT)[cluster];

    if(next_cluster==0XFFFF)//判断下一个簇是否被使用 (值为 4个F)
    {
        return 0;
    }

    return next_cluster;
}

bool CFat16::GetBitMap(CBitMap* bitmap)
{
    if(bitmap!=NULL&&mp_FAT!=NULL)
    {
        uint16_t * pFat = ((uint16_t*)mp_FAT);
        uint32_t i;

        bitmap->CleanBitMap();

        for(i=2;i<mul_FatSize/sizeof(uint16_t);++i)
        {
            bitmap->AddOneCluster(pFat[i]!=0);
        }
        return true;
    }
    return false;
}
