#pragma once

#include "../SInfoSet.h"
#include "SFatFileInfo.h"

#include <vector>
using std::vector;

struct LIB_INTERFACE SFatFileInfoSet:public SInfoSet
{
public:
    SFatFileInfoSet();
    bool AddInfo(SInfo* pInfo);
    uint32_t ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize);

    uint32_t size();
    SInfo* at(uint32_t index);
    void clear();
    uint32_t GetType();

    vector <SFatFileInfo> m_InfoList;
    SFatFileInfo m_FatInfo;
    uint32_t mul_SectorSize;
};

struct LIB_INTERFACE SExFatFileInfoSet:public SInfoSet
{
public:
    SExFatFileInfoSet();
    bool AddInfo(SInfo* pInfo);
    uint32_t ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize);

    uint32_t size();
    SInfo* at(uint32_t index);
    void clear();
    uint32_t GetType();

    vector <SExFatFileInfo> m_InfoList;
    SExFatFileInfo m_ExFatInfo;
    uint32_t mul_SectorSize;
};
