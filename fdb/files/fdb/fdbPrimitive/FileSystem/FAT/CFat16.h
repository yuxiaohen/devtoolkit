#pragma once

#include "CFat.h"

class LIB_INTERFACE CFat16:public CFat
{
public:
    CFat16(CFileSystem* pFs = NULL);
virtual ~CFat16();

private:
    CFat16(CFat16& );

public:
    uint32_t GetType()
    {
        return CFileSystem::FAT16;
    }

    uint64_t GetRootDirOffset()//获取根目录偏移量,在Fat16里面是FDT域
    {
        return mull_FdtOffset;
    }

    static bool DbrRequires(void* dbr)
    {
        return f_FAT16_Requires(dbr);
    }

    bool DbrSet(void* DbrBuf);

    uint32_t GetNextClusterIndex(uint32_t cluster);

    bool GetBitMap(CBitMap* bitmap);

private:
    uint64_t mull_FdtOffset;//FDT表偏移量
};
