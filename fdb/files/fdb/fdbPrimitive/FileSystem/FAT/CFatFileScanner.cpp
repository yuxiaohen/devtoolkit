#include "CFatFileScanner.h"
#include "CFatDir.h"

struct SDirItemInfo //Fat系列文件系统遍历目录树
{
    uint64_t mull_Offset;//目录项偏移量(自己)
    uint64_t mull_DirOffset;//文件夹偏移量
    uint32_t mul_ItemIndex;//目录项序号
};

CFatFileScanner::CFatFileScanner(CFat* pFat):CFileScanner(pFat)
{
    this->Set(pFat);
}

int CFatFileScanner::Set(CFat* pFat)
{
    if(pFat!=NULL)
    {
        this->mp_Fat = pFat;
        return 1;
    }
    return 0;
}

uint32_t CFatFileScanner::ScanFile(uint32_t StartSector)
{
    if(this->mp_Fat==NULL || !this->mp_Fat->Usable() ) return 0;
    this->Start();
    uint32_t TotalFatItem = 0;

    //首先:层遍历目录树
    //记录文件夹的信息
    //队列,用于对目录树进行层遍历
    queue <SDirItemInfo> ItemQueue;

    SDirItemInfo info;
    info.mull_Offset = 0;//目录项所在偏移量
    info.mull_DirOffset = mp_Fat->GetRootDirOffset();//文件夹偏移量
    info.mul_ItemIndex = TotalFatItem;//目录项序号
    ItemQueue.push(info);//加入遍历队列

    SFatFileInfo sFat;
    SExFatFileInfo sExFat;
    CDirItem* pItem = &sFat.Item;

    //设置目录项指针,并且生成一个根目录项
    {
        if(this->mp_Fat->GetType()==CFileSystem::EXFAT)
        {
            pItem = &sExFat.Item;

            sExFat.data.Offset = info.mull_DirOffset;
            sExFat.GetFileName() = L".";
            sExFat.data.ParentIndex = TotalFatItem;
            sExFat.Item.m_Attr2.StartCluster= 0;
            sExFat.Item.CheckSumCorrect = true;
            sExFat.Item.m_Attr1.FileAttribute |= 16;//为文件夹

            this->AddInfo(&sExFat);
        }
        else
        {
            sFat.data.Offset = info.mull_DirOffset;
            sFat.GetFileName() = L".";
            sFat.data.ParentIndex = TotalFatItem;
            sFat.Item.m_FatItem.FAT32HighWord = 0;
            sFat.Item.m_FatItem.StartCluster = 0;
            sFat.Item.m_FatItem.FileAttribute |= 16;//为文件夹

            this->AddInfo(&sFat);
        }
        ++TotalFatItem;
    }

    CFatDir fd(mp_Fat);
    while(!ItemQueue.empty() && this->isStart())
    {
        bool Ret = fd.GetFirstDirItem(ItemQueue.front().mull_DirOffset,pItem);
        while(Ret && this->isStart())
        {
            if(pItem->isDir())//如果是文件夹,则添加入遍历队列
            {
                info.mull_Offset = ItemQueue.front().mull_DirOffset;
                info.mull_DirOffset = mp_Fat->GetOffsetByCluster(pItem->FirstCluster());
                info.mul_ItemIndex = TotalFatItem;

                if(info.mull_DirOffset != ItemQueue.front().mull_DirOffset //判断是否指向自身
                        && info.mull_DirOffset != ItemQueue.front().mull_Offset //判断是否指向父目录
                        && info.mull_DirOffset != 0
                        )
                {
                    ItemQueue.push(info);
                }
                else
                {
                    Ret = fd.GetNextDirItem(pItem);
                    continue;
                }
            }

            //"发出"该文件信息
            if(ItemQueue.front().mull_DirOffset>=mp_Fat->GetRootDirOffset())
            {
                if(this->mp_Fat->GetType()==CFileSystem::EXFAT)
                {
                    sExFat.data.Offset = ItemQueue.front().mull_DirOffset;
                    sExFat.data.ParentIndex = ItemQueue.front().mul_ItemIndex;

                    this->AddInfo(&sExFat);
                }
                else
                {
                    sFat.data.Offset = ItemQueue.front().mull_DirOffset;
                    sFat.data.ParentIndex = ItemQueue.front().mul_ItemIndex;

                    this->AddInfo(&sFat);
                }
                ++TotalFatItem;
            }
            Ret = fd.GetNextDirItem(pItem);
        }
        //目录遍历完毕,将文件夹信息移出队列
        ItemQueue.pop();
    }
    //目录树遍历完毕..
    this->Stop();
    return TotalFatItem;
}
uint32_t CFatFileScanner::ScanFreeCluster(uint32_t StartSector)
{
    if(this->mp_Fat!=NULL && this->mp_Fat->Usable())
    {
        CBitMap b;
        if(this->mp_Fat->GetBitMap(&b))
        {
            CFragmentList list = b.GetFragmentList(false);
            return this->ScanFragment(0,
            &list,
            StartSector,0XFFFFFFFF);
        }
    }
    return 0;
}
