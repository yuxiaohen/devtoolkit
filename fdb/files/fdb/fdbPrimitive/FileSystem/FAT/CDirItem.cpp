#include "CDirItem.h"

uint32_t CFatDirItem::ReadItem(const void* item_buf,uint32_t nItem)
{
    if(item_buf==NULL||nItem<=0)return -1;

    FAT_SHORTFILE * pItem = (FAT_SHORTFILE *)item_buf;
    uint32_t i;
    for(i=0; i<nItem; ++i)
    {
        if(pItem[i].FileAttribute==0X0F)//读取长文件名
        {
            wchar_t filename13[14]= {0};
            f_FAT_LongFileNameToWstring((FAT_LONGFILE*)&pItem[i],filename13);

            wstring filenamepart = filename13;
            mwstr_FileName = filenamepart + mwstr_FileName;
        }
        else//读取短文件名目录项
        {
            //目录项正确性验证
            {
                //根据目录项属性进行目录项有效性验证
                {
                    //具有卷标标记
                    if( (pItem[i].FileAttribute&8) )
                    {
                        continue;
                    }
                    //目录项属性高两位必须为0
                    if( pItem[i].FileAttribute>63 )
                    {
                        break;
                    }
                }

                //当目录项标识为文件夹时,大小为空
                {
                    if( (pItem[i].FileAttribute&16) )
                    {
                        if(pItem[i].FileSize!=0)
                        {
                            //break;
                        }
                    }
                    else //当目录项标识不是文件夹时,大小若不为0,必须有可用簇编号;大小若为0,则可用簇编号必须为0
                    {
                        if( (pItem[i].FileSize==0) != (pItem[i].FAT32HighWord==0 && pItem[i].StartCluster==0) )
                        {
                            break;
                        }
                    }
                }

                //对文件名判定:文件名必须为可见ASCII编码
                {
                    //对第一个字节进行判定(因为当被删除的时候第一个字节置为0XE5)
                    if(pItem[i].FileName[0]!=0XE5 &&
                              (pItem[i].FileName[0]=='\0'
                            || pItem[i].FileName[0]==' '
                            || !CHAR_VISIBLE( pItem[i].FileName[0] )
                            || ILLEGAL_FILENAME(pItem[i].FileName[0])
                               )
                            )
                    {
                        break;
                    }

                    bool SpaceSign = false;
                    uint32_t ii;
                    for(ii=1;ii<8;++ii)
                    {
                        if(pItem[i].FileName[ii]==' ')
                        {
                            SpaceSign = true;
                        }
                        else if(SpaceSign==true)//当空格后有非空格字符,判定非法
                        {
                            break;
                        }
                        else if( !CHAR_VISIBLE( pItem[i].FileName[ii] )
                                 || ILLEGAL_FILENAME(pItem[i].FileName[ii]))
                        {
                            break;
                        }
                    }
                    if(ii<8)
                    {
                        break;
                    }
                    //扩展名判定
                    SpaceSign = false;
                    for(ii=0;ii<3;++ii)
                    {
                        if(pItem[i].FileExtName[ii]==' ')
                        {
                            SpaceSign = true;
                        }
                        else if(SpaceSign==true)//当空格后有非空格字符,判定非法
                        {
                            break;
                        }
                        else if( !CHAR_VISIBLE( pItem[i].FileExtName[ii] )
                                 || ILLEGAL_FILENAME(pItem[i].FileExtName[ii]) )
                        {
                            break;
                        }
                    }
                    if(ii<3)
                    {
                        break;
                    }
                }
            }

            m_FatItem = pItem[i];//拷贝短文件名目录项

            if(mwstr_FileName.size()==0)//拷贝短文件名
            {
                int i;
                for(i=0; i<8; ++i)
                {
                    uint8_t c = m_FatItem.FileName[i];
                    if(c==' ')break;
                    if(c==0XE5)c='#';
                    mwstr_FileName += c;
                }
                if(m_FatItem.FileExtName[0]!=' ')
                {
                    mwstr_FileName += '.';
                    for(i=0; i<3; ++i)
                    {
                        uint8_t c = m_FatItem.FileExtName[i];
                        if(c==' ')break;
                        mwstr_FileName += c;
                    }
                }
            }
            this->mb_Usable = true;
            break;
        }
    }
    //如果由于短文件项解析失败而跳出
    if(i<nItem && !this->mb_Usable)
    {
        this->Clear();
    }
    return i;
}

uint32_t CExFatDirItem::ReadItem(const void* item_buf,uint32_t nItem)
{
    if(item_buf==NULL||nItem<=0)return -1;

    EXFAT_Attr1 * pItem = (EXFAT_Attr1 *)item_buf;
    uint32_t NameLength = 0;
    uint32_t i;
    for(i=0; i<nItem; ++i)
    {
        if     (pItem[i].Type==0X85 || pItem[i].Type==0X05)//用户文件目录项
        {
            if(mul_Sign==0)//未读取目录项
            {
                m_Attr1 = pItem[i];
                //效验和计算
                {
                    uint8_t c = ((uint8_t*)&pItem[i])[0];
                    if(c<0X80)c+=0X80;
                    this->mus_CheckSum = CHECKSUM_CALC(this->mus_CheckSum,c );
                    uint32_t ii;
                    for(ii=1;ii<32;++ii)
                    {
                        if(ii==2 || ii==3)continue;
                        this->mus_CheckSum = CHECKSUM_CALC(this->mus_CheckSum,((uint8_t*)&pItem[i])[ii] );
                    }
                }
                mul_Sign = 1;
            }
            else
            {
                this->Clear();
            }
        }
        else if(pItem[i].Type==0XC0 || pItem[i].Type==0X40)//用户文件目录项2
        {
            if(mul_Sign==1)//已经读取了一个目录项
            {
                m_Attr2 = *(EXFAT_Attr2*)&pItem[i];
                NameLength = m_Attr2.FileNameLength;
                mwstr_FileName = L"";
                //若文件名长度大于0,则表示读取成功了两个目录项,否则表示读取失败
                if(NameLength>0)
                {
                    //效验和计算
                    {
                        uint8_t c = ((uint8_t*)&pItem[i])[0];
                        if(c<0X80)c+=0X80;
                        this->mus_CheckSum = CHECKSUM_CALC(this->mus_CheckSum,c );
                        uint32_t ii;
                        for(ii=1;ii<32;++ii)
                        {
                            this->mus_CheckSum = CHECKSUM_CALC(this->mus_CheckSum,((uint8_t*)&pItem[i])[ii] );
                        }
                    }
                    mul_Sign =  2;
                }
                else
                {
                    this->Clear();
                }
            }
            else
            {
                this->Clear();
            }
        }
        else if(pItem[i].Type==0XC1 || pItem[i].Type==0X41)//文件名
        {
            if(mul_Sign==2)//已经读取了两个目录项
            {
                mwstr_FileName += wstring().assign((wchar_t*)&pItem[i]+1,15);
                //效验和计算
                {
                    uint8_t c = ((uint8_t*)&pItem[i])[0];
                    if(c<0X80)c+=0X80;
                    this->mus_CheckSum = CHECKSUM_CALC(this->mus_CheckSum,c );
                    uint32_t ii;
                    for(ii=1;ii<32;++ii)
                    {
                        this->mus_CheckSum = CHECKSUM_CALC(this->mus_CheckSum,((uint8_t*)&pItem[i])[ii] );
                    }
                }
                if(NameLength<=mwstr_FileName.length())//目录项读取完毕
                {
                    this->mb_Usable = true;
                    this->CheckSumCorrect = (this->m_Attr1.CheckSum == this->mus_CheckSum);
                    break;
                }
            }
            else
            {
                this->Clear();
            }
        }
        else if(pItem[i].Type==0X81 || pItem[i].Type==0X01)//簇位图
        {
            this->Clear();
        }
        else if(pItem[i].Type==0X82 || pItem[i].Type==0X02)//大写字符
        {
            this->Clear();
        }
        else if(pItem[i].Type==0X83 || pItem[i].Type==0X03)//卷标
        {
            this->Clear();
        }
        else
        {
            this->Clear();
            break;
        }
    }
    return i;
}
