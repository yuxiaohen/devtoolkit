#pragma once

#include "CDirItem.h"
#include "CFat.h"

class LIB_INTERFACE CFatDir
{
public:
    CFatDir(CFat* pCFat = NULL);
virtual ~CFatDir();

public:

    bool    Set(CFat* pCFat);

    bool    GetFirstDirItem(uint64_t Offset,CDirItem* pDirItem);//通过偏移量读取目录项
    bool    GetNextDirItem(CDirItem* pDirItem);                  //读取下一个目录项

private:
    uint8_t* mp_SectorBuf;//储存扇区 512字节在堆中
    uint64_t mull_Offset;//当前读取目录的偏移量
    uint32_t mul_ClusterSize;//扇区大小
    uint32_t mul_nFileItemRead;//当前缓存区内已经读取过的目录项个数
    CFat* mp_Fat;
};
