﻿#pragma once

#include "../drive_struct.h"
#include "../sys_func.h"
#include "../dosdatetime.h"

typedef struct FAT16_BPB
{
    uint16_t BytePerSector;//每扇区字节数
    uint8_t SectorPerCluster;//每簇扇区数
    uint16_t ReserveSector;//DBR保留扇区数
    uint8_t FATCount;//FAT个数
    uint16_t RootItemCount;//根目录最大能容纳的目录项数
    uint16_t SectorCount;//扇区总数,小于32MB的分区记录在这里
    uint8_t MediaDescribe;//媒介描述
    uint16_t SectorPerFAT;//每FAT表包含扇区数
    uint16_t SectorPerTrack;//每磁道扇区数
    uint16_t HeadCount;//磁头数
    uint32_t HideSectorCount;//隐藏扇区数
    uint32_t TotalSectorCount;//扇区总数,大于32MB的分区记录在这里
    uint8_t  BIOSDriverNumber;//BIOS驱动器号
    uint8_t  NoUse;//未使用
    uint8_t  ExternBootMark;//扩展引导标记
    uint32_t VolumeNum;//卷序列号,十六进制
    uint8_t VolumeName[11];//卷标
    uint8_t FileSystemType[8];//文件系统类型
}FAT16_BPB;

typedef struct FAT16_DBR
{
    uint8_t JMG[3];
    uint8_t OEM[8];
    FAT16_BPB  bpb;
    uint8_t Code[512-64];
    uint16_t MagicNum; //永远是 大端 0X55AA
}FAT16_DBR;

typedef struct FAT32_BPB
{
    uint16_t BytePerSector;//每扇区字节数
    uint8_t SectorPerCluster;//每簇扇区数
    uint16_t ReserveSector;//DBR保留扇区数
    uint8_t FATCount;//FAT个数
    uint16_t NoUse1;//未使用
    uint16_t NoUse2;//扇区总数,小于32MB的分区记录在这里 (未使用)
    uint8_t MediaDescribe;//媒介描述
    uint16_t NoUse3;//每FAT表包含扇区数(未使用)
    uint16_t SectorPerTrack;//每磁道扇区数
    uint16_t HeadCount;//磁头数
    uint32_t HideSectorCount;//隐藏扇区数
    uint32_t TotalSectorCount;//扇区总数,大于32MB的分区记录在这里

//FAT32 扇区
    uint32_t SectorPerFAT;//每FAT扇区数
    uint16_t  Sign;//标记
    uint16_t  Version;//版本
    uint32_t RootDirFirstCluster;//根目录首簇号
    uint16_t  FSInfoCluster;//文件系统信息扇区号
    uint16_t  DBRBackupCluster;//DBR备份扇区号
    uint8_t  Reverse[12];//保留
//

    uint8_t  BIOSDriverNumber;//BIOS驱动器号
    uint8_t  NoUse;//未使用
    uint8_t  ExternBootMark;//扩展引导标记
    uint32_t VolumeNum;//卷序列号,十六进制
    uint8_t VolumeName[11];//卷标
    uint8_t FileSysteType[8];//文件系统类型
}FAT32_BPB;

typedef struct FAT32_DBR
{
    uint8_t JMG[3];
    uint8_t OEM[8];
    FAT32_BPB  bpb;
    uint8_t Code[512-92];
    uint16_t MagicNum; //永远是 大端 0X55AA
}FAT32_DBR;

typedef struct FAT_SHORTFILE
{
    uint8_t FileName[8];//文件名
    uint8_t FileExtName[3];//扩展名
    uint8_t FileAttribute;//文件属性
    uint8_t NoUse;//未用
    uint8_t CreateTime10Ms;//文件创建时间精确到10ms的值
    uint16_t CreateTime;//文件创建时间精确到时分秒
    uint16_t CreateDate;//文件创建时间精确到年月日
    uint16_t AccessDate;//文件最近访问日期精确到年月日
    uint16_t FAT32HighWord;//在Fat32文件系统中作为起始簇号偏移高位
    uint16_t ChangeTime;//文件最近修改日期精确到时分秒
    uint16_t ChangeDate;//文件最近修改日期精确到年月日
    uint16_t StartCluster;//文件起始簇号
    uint32_t FileSize;//文件大小(以字节为单位)
}FAT_SHORTFILE;

typedef struct FAT_LONGFILE
{
    uint8_t SerialNumber;//序列号, 当第5 bit位 为1 时
    uint16_t FileName1to5[5];//Unicode文件名的1-5个字符
    uint8_t FileAttribute;//长文件名目录项的属性标志,固定为0F,通过此标识来区分目录项是否是长文件名目录项
    uint8_t Reserved;//保留未用
    uint8_t CheckSum;//短文件名效验和
    uint16_t FileName6to11[6];//文件名第6-11个Unicode码字符
    uint16_t Zero;//始终为0;
    uint16_t FileName12to13[2];//文件名第12-13个Unicode码字符
}FAT_LONGFILE;

int f_FAT16_Requires(const void* dbr);//FAT16文件系统的DBR有效判定

int f_FAT32_Requires(const void* dbr);//FAT32文件系统的DBR有效判定

void f_FAT_LongFileNameToWstring(FAT_LONGFILE *pLongFile,wchar_t*LongFileName);

uint32_t f_FAT_GetLongFileName(uint8_t* buffer,wchar_t**pLongFileName);
