#include "CFatFileReader.h"
CFatFileReader::CFatFileReader(CFileSystem* pFileSystem)
{
    this->mull_DataOffset = 0;
    this->mul_ClusterSize = 0;
    this->SetFileSystem(pFileSystem);
}

bool CFatFileReader::SetFileSystem(CFileSystem* pFileSystem)
{
    if(pFileSystem!=NULL
    && pFileSystem->Usable())
    {
        this->m_Reader.Close();
        this->mp_Info = NULL;
        this->mp_Fat = pFileSystem;
        if(pFileSystem->GetType()==CFileSystem::FAT16
        ||pFileSystem->GetType()==CFileSystem::FAT32
        ||pFileSystem->GetType()==CFileSystem::EXFAT
        )
        {
            this->Config( ((CFat*)pFileSystem)->GetDataOffset(),pFileSystem->GetClusterSize() );
        }
        return true;
    }
    return false;
}

void CFatFileReader::Config(uint64_t DataOffset,uint32_t ClusterSize)
{
    this->mull_DataOffset = DataOffset;
    this->mul_ClusterSize = ClusterSize;
}

bool CFatFileReader::Config(SInfoSet *pInfoSet)
{
    if(this->mull_DataOffset!=0 && this->mul_ClusterSize!=0)return true;
    if(
        pInfoSet!=NULL
        &&
        (pInfoSet->GetType()==CFileSystem::FAT16
        ||pInfoSet->GetType()==CFileSystem::FAT32
        ||pInfoSet->GetType()==CFileSystem::EXFAT
        )
    )
    {
        uint32_t SectorSize = this->mp_Fat->GetSectorSize();
        uint32_t Size = pInfoSet->size();
        uint32_t i1,i2;
        for(i1=0;i1<Size;++i1)
        {
            SFatFileInfo* pInfo1 = (SFatFileInfo*)pInfoSet->at(i1);
            for(i2=i1+1;i2<Size;++i2)
            {
                SFatFileInfo* pInfo2 = (SFatFileInfo*)pInfoSet->at(i2);
                if(pInfo1->GetOffset() + SectorSize > pInfo2->GetOffset() )
                {
                    continue;
                }

                {
                    uint64_t O1,O2;//Offset
                    O1 = pInfo1->GetOffset()/SectorSize*SectorSize;
                    O2 = pInfo2->GetOffset()/SectorSize*SectorSize;
                    uint32_t C1,C2;//Cluster
                    C1 = pInfo1->Item.FirstCluster();
                    C2 = pInfo2->Item.FirstCluster();

                    if(C1==C2 || O1==O2)continue;
                    uint32_t ClusterSize = (O2-O1)/(C2-C1);
                    uint64_t DataOffset = O1 - ClusterSize * C1 ;
                    DataOffset += 3*ClusterSize;

                    if(SectorSize!=0 && ClusterSize%SectorSize==0 && ClusterSize/SectorSize>0)
                    {
                        Config(DataOffset,ClusterSize);
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool CFatFileReader::FromFile(SFileInfo* pInfo)
{
    if(pInfo!=NULL
    && (
        pInfo->GetType()==CFileSystem::FAT16
        || pInfo->GetType()==CFileSystem::FAT32
        || pInfo->GetType()==CFileSystem::EXFAT
        )
    && this->mp_Fat->Usable())
    {
        this->m_Reader.Close();
        this->mp_Info = (SFatFileInfo*)pInfo;

        uint32_t ClusterSize = this->mp_Fat->GetClusterSize();
        uint32_t SectorSize = this->mp_Fat->GetSectorSize();
        uint32_t ClusterNum = this->mp_Info->Item.Size()/ClusterSize + 1;

        CFragmentList fList;
         fList.AddFragment(this->mp_Info->Item.FirstCluster(),ClusterNum,ClusterNum);

        this->m_Reader.Set(this->mp_Fat->GetDisk(),&fList,ClusterSize,this->mull_DataOffset-2*this->mul_ClusterSize,SectorSize);
        return true;
    }
    return false;
}

uint32_t CFatFileReader::ReadBlock(uint32_t Index,uint8_t* OutBuf,uint32_t Size)
{
    if(this->mp_Info==NULL)return 0;

    return this->m_Reader.ReadSector(Index,OutBuf,Size);
}
uint32_t CFatFileReader::ReadNextBlock(uint8_t* OutBuf,uint32_t Size)
{
    if(this->mp_Info==NULL)return 0;

    return this->m_Reader.ReadNextSector(OutBuf,Size);
}
