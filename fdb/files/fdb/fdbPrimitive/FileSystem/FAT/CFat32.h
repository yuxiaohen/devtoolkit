#pragma once

#include "CFat.h"

class LIB_INTERFACE CFat32:public CFat
{
public:
    CFat32(CFileSystem* pFs = NULL);
virtual ~CFat32();

private:
    CFat32(CFat32& );

public:
    uint32_t GetType()
    {
        return CFileSystem::FAT32;
    }

    uint64_t GetRootDirOffset()//获取根目录偏移量 在Fat32中是Data域的开始
    {
        return mull_DataOffset;
    }

    static bool DbrRequires(void* dbr)
    {
        return f_FAT32_Requires(dbr);
    }

    bool DbrSet(void* DbrBuf);

    uint32_t GetNextClusterIndex(uint32_t cluster);

    bool GetBitMap(CBitMap* bitmap);
};
