#include "CFat.h"
#include "CFatFileReader.h"
#include "CFatFileScanner.h"

CFat::CFat(CFileSystem* pFs):CFileSystem(pFs)
{
    Init();
    mp_FAT = NULL;
}

CFat::~CFat()
{
    Close();
}

void CFat::Init()
{
    mul_FatSize = 0;
    mul_nFat = 0;
    mull_FirstFatOffset = 0;
    mull_DataOffset = 0;
}

void CFat::Close()
{
    //删除FAT表
    if(mp_FAT!=NULL)
    {
        free(mp_FAT);
        mp_FAT = NULL;
    }
}

CFileScanner* CFat::newCFileScannerObject()
{
    return new CFatFileScanner(this);
}
CFileReader* CFat::newCFileReaderObject(SInfoSet* pInfoSet)
{
    CFileReader * pFileReader = new CFatFileReader(this);
	if(pFileReader!=NULL&&pInfoSet!=NULL)
        {
            if(!pFileReader->Config(pInfoSet))
            {
                delete pFileReader;
                pFileReader = NULL;
            }
        }
        return pFileReader;
}

int CFat::Config(uint64_t DbrOffset,const void *dbr_buf)//用DBR和FAT表进行配置,FatOffset指定Fat表偏移量
{
    if(!this->Usable())return 0;

    Init();

    uint8_t DbrBuf[DBR_SIZE];
    uint8_t *pDbr = (uint8_t*)dbr_buf;
    if(pDbr == NULL)
    {
        //未传入dbr结构 则分配空间
        pDbr = DbrBuf;
    }
    this->SetPoint(DbrOffset,CDisk::START_POS);
    if(
        (  dbr_buf!=NULL  //从磁盘中读取DBR,扇区大小为512字节
           || this->Read(pDbr,DBR_SIZE)>0
        )
    )
    {
        if(!DbrSet(pDbr))//设置dbr,并判断dbr结构是否符合格式
        {
            return -1;
        }

        //先删掉FAT表
        if(mp_FAT!=NULL)
        {
            free(mp_FAT);
            mp_FAT = NULL;
        }

        //读取FAT表
        {
            mp_FAT = (uint8_t*)malloc(mul_FatSize*sizeof(uint8_t));
            //检查所有可用的FAT表
            int i;
            for(i=0;i<this->mul_nFat;++i)
            {
                this->SetPoint(mull_FirstFatOffset+i*mul_FatSize,CDisk::START_POS);
                if( this->Read(mp_FAT,mul_FatSize*sizeof(uint8_t))>0
                && mp_FAT[1]==0XFF && mp_FAT[2]==0XFF )
                {
                    return 1;
                }
            }
            free(mp_FAT);
            mp_FAT = NULL;
        }
        return -2;
    }
    return 0;
}

uint64_t CFat::GetOffsetByCluster(uint32_t ClusterNum)
{
    if(!this->Usable())return 0;
    if(ClusterNum<this->mul_FirstClusterNum) return 0;
    return mull_DataOffset + (ClusterNum - this->mul_FirstClusterNum)*this->GetClusterSize();
}

uint64_t CFat::GetNextCluster(uint64_t CurrentOffset)
{
    uint32_t cluster = (CurrentOffset - mull_DataOffset)/this->GetClusterSize() + this->mul_FirstClusterNum; //获取簇编号
    uint32_t next_cluster = GetNextClusterIndex(cluster);
    return GetOffsetByCluster(next_cluster);
}

wstring CFat::GetVolumeName()
{
    wstring Volume;
    if(this->Usable())
    {
        uint8_t *buf = (uint8_t*)malloc(this->GetSectorSize()*sizeof(uint8_t));
        FAT_SHORTFILE *pi = (FAT_SHORTFILE *)buf;
        FAT_SHORTFILE* pVol = NULL;

        //遍历根目录,读取出卷标目录项的地址
        uint64_t ReadOffset = GetRootDirOffset();

        while( pVol==NULL && ReadOffset!=0)
        {
            this->SetPoint(ReadOffset,START_POS);
            uint32_t nRead = 0;
            while( pVol==NULL && this->Read(buf,this->GetSectorSize())>0 )
            {
                uint32_t i;
                //扫描这个扇区内有没有簇位图目录项
                for(i=0; i<this->GetSectorSize()/sizeof(FAT_SHORTFILE); ++i)
                {
                    if( pi[i].FileAttribute != 0XF && (pi[i].FileAttribute & 0X08) )//第3位 置1表示目录项为卷标目录项
                    {
                        pVol = &pi[i];
                        break;
                    }
                }
                nRead += this->GetSectorSize();
                if(nRead>=this->GetClusterSize())//当读取完一个簇后,准备在下一个簇读取
                {
                    break;
                }
            }
            ReadOffset = GetNextCluster(ReadOffset);
        }

        if(pVol!=NULL)
        {
            uint32_t i;
            uint32_t n = 11;
            while(' '==((char*)pVol)[n-1] && n-1>=0)--n;//排除卷标尾部的空格
            for(i=0;i<n;++i)
            {
                Volume += ((char*)pVol)[i];
            }
        }
        free(buf);
    }
    return Volume;
}

CFragmentList CFat::GetFragmentList(uint32_t StartCluster,uint32_t nCluster)
{
    CFragmentList fragmentlist;
    uint32_t ClusterHead = StartCluster;//碎片头部
    uint32_t CurrentCluster = ClusterHead;//当前簇
    uint32_t ClusterCount = 0;
    while(CurrentCluster!=0)
    {
        uint32_t NextCluster = this->GetNextClusterIndex(CurrentCluster);//获取下一个簇
        ++ClusterCount;
        if(NextCluster!=CurrentCluster+1 || ClusterCount>=nCluster )//当前碎片读取完毕,或者已经读取完指定的簇个数,存储
        {
            uint32_t Length = CurrentCluster - ClusterHead + 1;//计算碎片长度
            fragmentlist.AddFragment(ClusterHead,Length,Length);
            ClusterHead = NextCluster;  //设置新碎片起始簇
        }
        CurrentCluster = NextCluster;
    }
    return fragmentlist;
}
