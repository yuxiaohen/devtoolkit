#pragma once

#include "CFat.h"

class LIB_INTERFACE CExFat:public CFat
{
public:
    CExFat(CFileSystem* pFs = NULL);
virtual ~CExFat();

private:
    CExFat(CExFat& );

public:
    uint32_t GetType()
    {
        return CFileSystem::EXFAT;
    }

    uint64_t GetRootDirOffset()//获取根目录偏移量 在Fat32中是Data域的开始
    {
        return mull_RootDir;
    }

    static bool DbrRequires(void* dbr)
    {
        return f_EXFAT_Requires(dbr);
    }

    bool DbrSet(void* DbrBuf);

    uint32_t GetNextClusterIndex(uint32_t cluster);

    bool GetBitMap(CBitMap* bitmap);

    wstring GetVolumeName();

private:
    uint64_t mull_RootDir;//簇位图文件偏移量
};
