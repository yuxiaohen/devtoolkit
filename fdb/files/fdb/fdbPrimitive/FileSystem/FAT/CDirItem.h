#pragma once

#include "fat_struct.h"
#include "exfat_struct.h"
#include "../CDisk.h"

#include <string>
using std::wstring;

#define FATITEM_SIZE 32

//可见字符
#define CHAR_VISIBLE(x) (' '<=((uint8_t)x) )
//非法文件名
#define ILLEGAL_FILENAME(x) (((uint8_t)x)=='\\'||((uint8_t)x)=='/'||\
    ((uint8_t)x)==':'||((uint8_t)x)=='?'||((uint8_t)x)=='/'||((uint8_t)x)=='*'||\
    ((uint8_t)x)=='"'||((uint8_t)x)=='|'||((uint8_t)x)=='<'||((uint8_t)x)=='>')

//目录项类的基类
class LIB_INTERFACE CDirItem
{
public:
    CDirItem(){}
virtual ~CDirItem(){}

    virtual void Clear()=0;//清空目录项对象

    //从缓存区中读入item,nItem指定item个数,
    //返回值为读取的item个数,通过Usable()函数可知目录项是否读取成功
    virtual uint32_t ReadItem(const void* item_buf,uint32_t nItem)=0;
    virtual uint64_t Size()=0;//大小
    virtual uint32_t FirstCluster()=0;//获取首簇编号
    virtual bool isDir()=0;//判断是否目录
    virtual bool isDel()=0;//判断是否被删除
    virtual wstring& GetFileName()=0;

    bool Usable(){return mb_Usable;}
    void SetUsable(bool b){mb_Usable = b;}
protected:
    bool mb_Usable;//目录项可用
};

class LIB_INTERFACE CFatDirItem:public CDirItem
{
public:

    wstring mwstr_FileName;//文件名
    FAT_SHORTFILE m_FatItem;//Fat目录项
    CFatDirItem()
    {
        m_FatItem.FAT32HighWord = 0;
        m_FatItem.StartCluster = 0;
        Clear();
    }
    void Clear()
    {
        mb_Usable = false;
        mwstr_FileName=L"";
    }
    wstring& GetFileName()
    {
        return mwstr_FileName;
    }
    bool isDir()//判断是否目录
    {
        return (m_FatItem.FileAttribute&16)!=0;
    }
    bool isDel()//判断是否被删除
    {
        return m_FatItem.FileName[0]==0XE5;
    }
    uint64_t Size()//大小
    {
        return m_FatItem.FileSize;
    }
    uint32_t FirstCluster()//获取首簇编号
    {
        return (uint32_t)m_FatItem.FAT32HighWord<<16 | (uint32_t) m_FatItem.StartCluster;
    }
    uint32_t ReadItem(const void* item_buf,uint32_t nItem);
};

class LIB_INTERFACE CExFatDirItem:public CDirItem
{
public:
    CExFatDirItem()
    {
        m_Attr2.StartCluster = 0;
        Clear();
    }
virtual ~CExFatDirItem(){}

    wstring mwstr_FileName;//文件名
    EXFAT_Attr1 m_Attr1;
    EXFAT_Attr2 m_Attr2;
    bool CheckSumCorrect;//如果效验和正确,为真

    void Clear()
    {
        mul_Sign = 0;
        mus_CheckSum = 0;
        mb_Usable = false;
        CheckSumCorrect = false;
        mwstr_FileName=L"";
    }
    wstring& GetFileName()
    {
        return mwstr_FileName;
    }
    bool isDir()//判断是否目录
    {
        return (m_Attr1.FileAttribute&16)!=0;
    }
    bool isDel()//判断是否被删除
    {
        return m_Attr1.Type<0X80;
    }
    uint64_t Size()
    {
        return m_Attr2.FileSize1;
    }
    uint32_t FirstCluster()//获取首簇编号
    {
        return m_Attr2.StartCluster;
    }
    uint32_t ReadItem(const void* item_buf,uint32_t nItem);

private:
    uint32_t mul_Sign;//属性读取标志
    uint16_t mus_CheckSum;//效验和
};
