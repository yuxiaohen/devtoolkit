#include "CFat32.h"

CFat32::CFat32(CFileSystem* pFs):CFat(pFs)
{
    ;
}

CFat32::~CFat32()
{

}

bool CFat32::DbrSet(void* DbrBuf)
{
    if(DbrBuf==NULL||!f_FAT32_Requires(DbrBuf)) return false;

    FAT32_DBR * Dbr = (FAT32_DBR *)DbrBuf;
    this->SetPartitionSize( (Dbr->bpb.TotalSectorCount)*Dbr->bpb.BytePerSector);
    this->SetSectorSize(Dbr->bpb.BytePerSector);
    this->SetClusterSize( Dbr->bpb.SectorPerCluster*Dbr->bpb.BytePerSector );
    mul_FatSize = Dbr->bpb.SectorPerFAT*Dbr->bpb.BytePerSector;
    mul_nFat = Dbr->bpb.FATCount;

    this->mul_FirstClusterNum = Dbr->bpb.RootDirFirstCluster;

    uint64_t offset = Dbr->bpb.ReserveSector*Dbr->bpb.BytePerSector;
    offset += (this->mul_FirstClusterNum-2)*(uint64_t)this->GetClusterSize();
    mull_FirstFatOffset = offset;

    offset += mul_nFat * mul_FatSize;
    mull_DataOffset = offset;

    return true;
}






uint32_t CFat32::GetNextClusterIndex(uint32_t cluster)
{
    if(mp_FAT==NULL)return cluster+1;
    if(cluster>=mul_FatSize)return 0;

    uint32_t next_cluster = ((uint32_t*)mp_FAT)[cluster];

    if(next_cluster==0XFFFFFFF)//判断下一个簇是否被使用 (值为 7个F, (十进制)268435455)
    {
        return 0;
    }

    return next_cluster;
}

bool CFat32::GetBitMap(CBitMap* bitmap)
{
    if(bitmap!=NULL&&mp_FAT!=NULL)
    {
        uint32_t * pFat = ((uint32_t*)mp_FAT);
        uint32_t i;

        bitmap->CleanBitMap();

        for(i=2;i<mul_FatSize/sizeof(uint32_t);++i)
        {
            bitmap->AddOneCluster(pFat[i]!=0);
        }
        return true;
    }
    return false;
}
