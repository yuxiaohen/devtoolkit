﻿#pragma once

#include "NTFS/ntfs_datarun.h"
#include "NTFS/ntfs_decompress.h"
#include "drive_struct.h"
#include "sys_func.h"

#include <vector>
using  std::vector;

class LIB_INTERFACE CFragmentList
{
public:
    CFragmentList(){mull_TotalCluster = 0;}
public:
    void ReadDataRunList(uint8_t* RunList);//读入RunList,返回值为总簇数
    void AddFragment(uint64_t ClusterIndex,uint64_t ClusterLength,uint64_t DecompressLength=0);//添加一个块: 指定大小和簇块内簇个数
    void RemoveFragment(uint32_t n);//移除一个块;
    void ClearList();//清除列表
    uint32_t GetFirstClusterOffset(uint64_t &offset);//获取第一个簇偏移,调用此函数会影响 GetNextClusterOffset,返回值为碎片编号,失败返回-1
    uint32_t GetNextClusterOffset(uint64_t &offset);//获取下一个簇偏移,返回值为碎片编号,失败返回-1
    bool GetClusterOffsetByIndex(uint64_t index,uint64_t &offset)const;//根据簇序号寻找簇偏移量,不会影响GetNextClusterOffset函数
    uint64_t GetTotalCluster()const //获取(解压缩后的)簇个数
    {
        return mull_TotalCluster;
    }
    uint32_t GetTotalFrag()const
    {
        return m_OffsetList.size();
    }
    uint64_t GetOffset(uint32_t n)const
    {
        return m_OffsetList.at(n);
    }
    uint64_t GetClusterNum(uint32_t n)const
    {
        return m_ClusterList.at(n);
    }
    uint64_t GetDecompressNum(uint32_t n)const
    {
        return m_DecompressList.at(n);
    }
public:
//对被压缩NTFS文件碎片进行解压缩,在CFragmentList中有写解压缩后体积的大小,返回值如果小于等于0表示解压缩失败
    static int Decompress(uint8_t *dest, uint32_t dest_size,uint8_t* const cb_start, uint32_t cb_size);
private:
    uint64_t mull_CurrentOffset;
    uint64_t mul_CurrentFrag;
    uint64_t mull_TotalCluster;//总共簇个数

    vector <uint64_t> m_OffsetList;
    vector <uint64_t> m_ClusterList;
    vector <uint64_t> m_DecompressList;
};
