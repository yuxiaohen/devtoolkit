#pragma once

#include "CFileSystem.h"
#include "SInfoSet.h"
#include "CFragmentReader.h"

#include <vector>
using std::vector;

class CFileSystem;

class LIB_INTERFACE CFileScanner:public CFileSystem
{
public:
    enum STATUS{NORMAL_STOP=0,NORMAL_START,ABNORMAL_STOP};
    CFileScanner(CFileSystem* pFs = NULL);
    uint32_t ScanArea(uint64_t Offset, uint32_t StartSector, uint32_t EndSector);
    uint32_t ScanFragment(uint64_t Offset, CFragmentList *pList, uint32_t StartSector, uint32_t EndSector);

    virtual uint32_t ScanFile(uint32_t StartSector);
    virtual uint32_t ScanFreeCluster(uint32_t StartSector){return 0;}

    void SetBufferSector(uint32_t n){this->mul_BufferSector = n;}
    uint32_t GetBufferSector(){return this->mul_BufferSector;}

    uint32_t GetStatus()
    {
        return this->e_Status;
    }
    void SetStatus(uint32_t n)
    {
        this->e_Status = n;
    }
    void Start();
    void Stop();
    bool isStart();

    bool AddInfoSet(SInfoSet* pInfoSet);
    uint32_t AddInfo(SInfo* pInfo);
    void clear();
    SInfoSet* at(uint32_t n);
    uint32_t size();
    uint64_t GetScannedByte()
    {
        return this->mull_Scanned;
    }
    uint64_t GetTotalByte()
    {
        return this->mull_Total;
    }
    SInfoSet* GetLastScannedInfoSet()
    {
        if(this->mul_LastScannedSet==-1)return NULL;
        return this->at(this->mul_LastScannedSet);
    }
private:
    uint32_t in_ScanArea(uint64_t Offset, uint32_t StartSector, uint32_t EndSector);
private:
    vector <SInfoSet*> m_InfoSetList;
    uint32_t mul_LastScannedSet;
    uint32_t mul_BufferSector;
    uint64_t mull_Scanned;
    uint64_t mull_Total;
    uint32_t e_Status;
};
