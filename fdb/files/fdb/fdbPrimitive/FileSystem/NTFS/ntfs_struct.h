﻿#pragma once

//#define DEBUG

#include "../drive_struct.h"
#include "../sys_func.h"
#include "ntfs_decompress.h"
#include "ntfs_datarun.h"


//Define

#define BYTE_PER_CLUSTER(d) ((d).bpb.bytes_per_sector * (d).bpb.sectors_per_cluster)
#define MFT_OFFSET(d) ( (d).mft_lcn * BYTE_PER_CLUSTER(d) )
#define MFTMIRR_OFFSET(d) ( (d).mftmirr_lcn * BYTE_PER_CLUSTER(d) )
#define DATARUN(ma)  ( (uint8_t*)(ma) + (ma)->Common.NonResident.RunListOffset )
#define NONRESIDENTSIZE(ma)  ((ma)->Common.NonResident.StreamRealSize)
#define RESIDENTSIZE(ma)  ((ma)->Common.resident.StreamLength)
#define RESIDENTOFFSET(ma) ((ma)->Common.resident.StreamOffset)
#define GET_ATTRIBUTE(ma)  ( (uint8_t*)(ma)+0X18 )
#define FILENAMEATTRIBUTE(ma)   ( (FileNameAttribute*) GET_ATTRIBUTE(ma) )
#define FILE_NAME(na) ((wchar_t*)  ( ((char*)(na)) + 0X18 + sizeof(FileNameAttribute) ) )
#define ATTRIBUTE_LIST(ma) ( (AttributeList*) GET_ATTRIBUTE(ma) )
#define STANDARD_FILE_INFO(ma) ( (StandardInfomation*) GET_ATTRIBUTE(ma) )
#define INDEX_STRUCT(ma)  ( (IndexStruct*) ( ((uint8_t*)(ma)) + 0X20 ) )
#define NEXT_INDEX_ITEM(ii)  ((IndexItem*)( ((uint8_t*)(ii)) + (ii)->SizeofIndex ) )



//DBR Struct

typedef struct
{
        uint16_t    bytes_per_sector;       /* Size of a sector in uint8_ts. */
        uint8_t    sectors_per_cluster;    /* Size of a cluster in sectors. */
        uint16_t    reserved_sectors;       /* zero */
        uint8_t    fats;               /* zero */
        uint16_t    root_entries;       /* zero */
        uint16_t    sectors;            /* zero */
        uint8_t    media_type;         /* 0xf8 = hard disk */
        uint16_t    sectors_per_fat;        /* zero */
/*0x0d*/uint16_t    sectors_per_track;      /* Required to boot Windows. */
/*0x0f*/uint16_t    heads;          /* Required to boot Windows. */
/*0x11*/uint32_t   hidden_sectors;     /* Offset to the start of the partition
                       relative to the disk in sectors.
                       Required to boot Windows. */
/*0x15*/uint32_t   large_sectors;      /* zero */
/* sizeof() = 25 (0x19) uint8_ts */
}NTFS_BPB;

typedef struct
{
        uint8_t        jump[3];            /* Irrelevant (jump to boot up code).*/
        uint8_t        oem_id[8];         /* Magic "NTFS    ". */
/*0x0b*/NTFS_BPB bpb;   /* See NTFS_BPB. */
        uint8_t        physical_drive;     /* 0x00 floppy, 0x80 hard disk */
        uint8_t        current_head;       /* zero */
        uint8_t        extended_boot_signature;    /* 0x80 */
        uint8_t        reserved2;          /* zero */
/*0x28*/uint64_t   number_of_sectors;  /* Number of sectors in volume. Gives
                                           maximum volume size of 2^63 sectors.
                                           Assuming standard sector size of 512
                                           uint8_ts, the maximum uint8_t size is
                                           approx. 4.7x10^21 uint8_ts. (-; */
        uint64_t   mft_lcn;            /* Cluster location of mft data. */
        uint64_t   mftmirr_lcn;        /* Cluster location of copy of mft. */
        CHAR        clusters_per_mft_record;    /* Mft record size in clusters. */
        uint8_t        reserved0[3];       /* zero */
        CHAR        clusters_per_index_record;  /* Index block size in clusters. */
        uint8_t        reserved1[3];       /* zero */
        uint64_t   volume_serial_number;   /* Irrelevant (serial number). */
        uint32_t       checksum;           /* Boot sector checksum. */
/*0x54*/uint8_t        bootstrap[426];     /* Irrelevant (boot up code). */
        uint16_t        end_of_sector_marker;   /* End of boot sector magic. Always is
                       0xaa55 in little endian. */
/* sizeof() = 512 (0x200) uint8_ts */
}NTFS_BOOT_SECTOR;

typedef NTFS_BOOT_SECTOR NTFS_DBR ;

// DBR Struct End


// MFT Struct

typedef struct NTFSBootParameterBlock
{
    uint8_t  OEMID[8];//系统标识，必须为NTFS，不属于BPB表，为编程方便纳入
    uint16_t  uint8_tsPerSector;//每扇区字节数，大多数磁盘为0x200
    uint8_t  SectorsPerCluster;//每簇扇区数,和磁盘容量及格式化的具体设置有关
    uint16_t  ReservedSectors;//保留扇区数,一般为0
    uint8_t  Zero[3];//总为0
    uint16_t  NoUsed;//NTFS未使用,一般为00
    uint8_t  MediaID;//介质描述符,硬盘为0xF8
    uint16_t  Zero1;//总为0
    uint16_t  SectorsPerTrack;//每道扇区数,对硬盘来说一般是0x3F
    uint16_t  Heads;//硬盘的磁头数
    uint32_t HidenSectors;//指该分区前的隐含扇区数,一般为一个磁道的扇区数:0x3F
    uint32_t NoUsed1;//NTFS未使用,一般为00
    uint32_t NoUsed2;//NTFS未使用,一般为0x00800080
    uint64_t TotalSectors;//该分区总的扇区数
    uint64_t MFTStartCluster;//*MFT表的起始簇号LCN
    uint64_t MFTMirrorStartCluster;//MFT备份表的起始簇号LCN
    uint32_t ClustersPerMFTRecord;//每个MFT记录的簇数
    uint32_t SerialNumber;//该卷的序列号
    uint32_t CheckSum;//效验和
}NTFSBPB;

typedef struct MFT
{
    CHAR szFile[4];//一定是"FILE"
    uint16_t usOffset;//更新序列Us的偏移
    uint16_t UsnSize;//更新序列号USN的大小与数组,包括第一个字节
    CHAR LSN[8];//日志文件序列号LSN
    uint16_t SN;//序列号(SN)
    uint16_t HardLinkNum;//硬连接数
    uint16_t FirstAttributeOffset;//第一个属性的偏移地址
    uint16_t Signal;//0X00表示文件被删除,0X01表示文件正在使用,0X02表示目录被删除,0X03表示该记录为目录
    uint32_t  FileRecordSize;//记录头和属性的总长度,即文件记录的实际长度
    uint32_t  TotalRecordSize;//总共分配给记录的长度
    CHAR  FileIndexNum[8];//基本文件记录中的索引号
    uint16_t NextAttributeNumber;//下一属性ID
    uint16_t Border;//XP中使用,边界
    uint32_t  FileRecordNumber;//XP中使用,本文件记录号

    uint8_t AttribArea[1024-48];//存放属性的内存空间, 一个MFT大小为1K, 这个属性是我自己加的
}MFT;

typedef struct NTFSAttribute
{
    uint32_t Type;//属性类型,参见表1
    uint32_t Length;//属性的长度
    uint8_t  NonResidentFlag; //是否是非常驻属性,1为非常驻,0为常驻
    uint8_t  NameLength;//属性名称长度,如果无属性名称该值为0
    uint16_t  ContentOffset;//属性内容的偏移量
    uint16_t  CompressedFlag;//该文件记录表示的文件数据是否被压缩过
    uint16_t  Identifcator;//识别标志
    union //常驻属性和非常驻属性的公共部分
    {
        struct //如果该属性为场主属性时使用该结构
        {
            uint32_t StreamLength;//属性值的长度,也即属性具体内容的长度
            uint16_t  StreamOffset;//属性内容的偏移量
            uint16_t  IndexFlag;//属性是否被索引项所索引,索引项是一个索引(如目录)的基本组成
        }resident;
        struct //如果该属性为非常驻属性时候使用该结构
        {
            uint64_t StartVCN;//起始的VCN值
            uint64_t LastVCN;//最后的VCN值
            uint16_t  RunListOffset;//运行列表的偏移量
            uint16_t  CompressEngineIndex;//压缩引擎的索引值,指压缩时使用的具体引擎.
            uint32_t Reserved;
            //uint64_t StreamInitSize;//初始大小
            uint64_t StreamAllocSize;//为属性值分配的空间,单位为簇,压缩文件分配值小于实际值
            uint64_t StreamRealSize;//属性值实际使用的空间,单位为簇
            uint64_t StreamCompressedSize;//属性值经过压缩后的大小,如未压缩,其值为实际值
        }NonResident;
    }Common;
}NTFSAttribute;

//0X10属性 :标准信息
typedef struct StandardInfomation
{
    uint64_t FileCreateTime;//文件创建时间
    uint64_t FileLastChangeTime;//文件最后修改时间
    uint64_t MFTChangeTime;//MFT修改时间
    uint64_t FileLastAccessTime;//文件最后访问时间
    uint32_t     TraditionFileAttribute;//传统文件属性
    uint32_t     MaxVersionNum;//最大版本数:为0 表示版本是没有的
    uint32_t     VersionNum;//版本数: 如果便宜24H处为0则此处也为0
    uint32_t     SortID;//分类ID(一个双向的类索引)
    uint32_t     OwnerID;//所有者ID:表示文件的苏有着,是访问文件配额$Quota中$o和$Q索引的关键字,如果为0 则表示没有设置配额
    uint32_t     SaveID;//安全ID 是文件$Secure中$SII索引和$SDS数据流中的关键字,注意不要和安全标识相混淆
    uint64_t QuotaManagement;//配额管理,配额占用情况,他是文件所有流所占用的总字节数,为0 表示为使用磁盘配额
    uint64_t USN;//更新序列号(USN):文件最后的更新序列号,他是进入元数据文件$USNJRNL直接的索引,如果为0,则表示没有USN日志

}StandardInfomation;

//End

//0X20属性
typedef struct AttributeList
{
    uint32_t Type;//类型
    uint16_t  RecordLength;//记录长度
    uint8_t  AttrNameLength;//属性长度
    uint8_t  AttrNameOffset;//属性偏移
    uint64_t StartVCN;//起始VCN(属性常驻时为0)
    uint64_t AttrBaseFileRecordNum;//属性的基本文件记录中的文件参考号
    uint16_t  AttrID;//属性ID

    //2N 属性名(如果有)
}AttributeList;
//End

//0X30属性 :文件名

typedef struct FileNameAttribute
{
    uint64_t ParentFileNum;//父目录文件参考号
    uint64_t FileCreateTime;//文件建立时间
    uint64_t LastChangeTime;//最后修改时间
    uint64_t MftCHangeTime;//MFT改变时间
    uint64_t LastAccessTime;//最后访问时间
    uint64_t FileAllocSize;//文件分配空间大小
    uint64_t FileRealSize;//文件实际大小
    uint32_t     Flag;//标志
    uint32_t     ReparseValue;//重解析值
    uint8_t      FileNameLength;//文件名长度
    uint8_t      FileNameSpace;//文件名命名空间

    //然后是文件名
}FileNameAttribute;

//End


//0X90属性: 文件目录索引

typedef struct IndexRoot
{
    uint32_t AttributeType;//属性类型
    uint32_t Collation;//校对规则
    uint32_t IndexBufSize;//每个索引缓冲区的字节数
    uint8_t  nCluster;//每个缓冲区大小的簇数

    uint8_t Zero[3];//无意义
}IndexRoot;

typedef struct IndexHead
{
    uint32_t FirstIndexOffset;//第一个索引项的偏移
    uint32_t IndexSize;//索引项总大小
    uint32_t IndexAllocSize;//索引项分配大小
    uint8_t Sign;//当为00时候为小索引,为01为大索引

    uint8_t  Zero[3];//无意义
}IndexHead;

typedef struct IndexItem
{
    uint64_t   MFTNum;//MFT参考号
    uint16_t        SizeofIndex;//索引项大小
    uint16_t        FileAttributeSize;//文件属性体大小
    uint16_t        IndexSign;//索引标志,此处为1表示这个索引项包含子节点,为2表示这是最后一个项
    uint16_t        Zero;//无意义
    uint64_t   ParentMFTNumber;//父目录的MFT文件参考号
    uint64_t   FileCreateTime;//文件创建时间
    uint64_t   FileLastChangeTime;//文件最后修改时间
    uint64_t   FileRecordLastChangeTime;//文件记录最后修改时间
    uint64_t   FileLastAccessTime;//文件最后访问时间
    uint64_t   FileAllocSize;//文件分配大小
    uint64_t   FileRealSize;//文件实际大小
    uint64_t   FileSign;//文件标志
    uint8_t        FileNameLength;//文件名长度
    uint8_t        FileNameSpace;//文件名的命名空间

    //0x52后是文件名 长度 2F;
    //然后填充到能被8整除
    //然后是子节点的索引所在的VCN; 8个字节
}IndexItem;

typedef struct IndexStruct
{
    IndexRoot ir;
    IndexHead ih;
    IndexItem ii;
}IndexStruct;

//End


//INDX Struct

typedef struct INDX
{
    CHAR szINDX[4];//一定是"INDX"
    uint16_t usOffset;//更新序号偏移
    uint16_t UsnSize;//更新序列号USN的大小与数组,包括第一个字节
    CHAR LSN[8];//日志文件序列号LSN
    uint64_t VCN;//该索引缓冲在索引分配中的索引VCN
    uint32_t IndexOffset;//索引入口的偏移(相对于0X18)
    uint32_t IndexSize; //索引入口的大小(相对于0X18)
    uint32_t IndexAllocSize;//索引入口分配大小(相对于0X18)
    CHAR  NonPageNode;//非页级节点为1(有子索引)
    CHAR Zero[3];//总是为0
    uint16_t UpdateNum;//更新序列号

    //大小42

    CHAR INDXItemArea[4096-42]; //4K大小的INDX结构

    //2S-2 更新序列排列
}INDX;  //INDX头部 64个字节

typedef struct INDXItem
{
    uint64_t MFTNum;//文件的MFT参考号
    uint16_t      IndxSize;//索引项大小
    uint16_t      FileNameOffset;//文件名偏移
    uint16_t      IndxFlag;//索引标志
    uint16_t      Zero;//填充(到8字节)
    uint64_t ParentMFTNum;//父目录的MFT文件参考号
    uint64_t FileCreateTime;//文件创建时间
    uint64_t LastChangeTime;//最后修改时间
    uint64_t FileRecordLastChange;//文件记录最后修改时间
    uint64_t LastAccessTime;//最后访问时间
    uint64_t AllocSize;//文件分配大小
    uint64_t RealSize;//文件实际大小
    uint64_t FileFlag;//文件标志
    uint8_t      FileNameLength;//文件名长度
    uint8_t      FileNameSpace;//文件名命名空间

    //接下来是文件名 大小为 2倍的文件名长度(Unicode)
    //填充8字节
    //子节点索引缓存的VCN
}INDXItem;

//INDX Struct End

//UsnJrnl:变更日志结构
typedef struct UsnJrnl
{
    uint32_t LogLength;//本日志项长度
    uint16_t  mVersion;//主版本号
    uint16_t  subVersion;//辅版本号
    uint64_t FileNumber;//导致该项产生的文件的文件参考号
    uint64_t ParentDirNumber;//导致该项产生的文件的父目录的文件参考号
    uint64_t Usn;//日志项的USN(更新序列号)
    uint64_t Timestamp;//时间戳
    uint32_t   SignValue;//变更类型的标志值
    uint32_t   SourceInfo;//源信息
    uint32_t   SaveID;//安全ID
    uint32_t   FileAttribute;//文件属性
    uint16_t    FileNameLength;//文件名长度
}UsnJrnl;
//UsnJrnl End

void f_NTFS_MftSet(MFT * mft);//设置MFT更新数组

void f_NTFS_IndxSet(INDX *indx);//设置INDX更新数组

int f_NTFS_Requires(const void* dbr);//判定一个DBR结构是否属于NTFS_DBR,其判定标准来自于WinHex NTFS DBR模板

int f_NTFS_MFT_Requires(const void* mft);//判定一个MFT结构是否合法

int f_NTFS_INDX_Requires(const void* indx);//判定一个INDX结构是否合法

//SET

uint64_t f_NTFS_FindStrfromFILE(FILE_HANDLE f,const char* str,int len);

NTFSAttribute* f_NTFS_GetFirstNTFSAttribute(const MFT * m);

NTFSAttribute* f_NTFS_GetNextNTFSAttribute(NTFSAttribute *na);

NTFSAttribute* f_NTFS_FindAttributeByType(NTFSAttribute* start,uint32_t Type);

wchar_t* f_NTFS_GetMFTFileName(const MFT* m,uint32_t * size,uint8_t filenamespace);

uint64_t f_NTFS_GetParentDirMftIndex(const MFT* m);

//INDX

int f_NTFS_GetINDXfromFILE(FILE_HANDLE f,INDX *indx);

INDXItem * f_NTFS_GetFirstINDXItem(INDX *indx);

INDXItem * f_NTFS_GetNextINDXItem(INDXItem *indx);

//DataRun

uint64_t f_NTFS_GetMftOffsetFromDataRun(uint8_t* DataRun,uint64_t MftNum,uint32_t uint8_t_per_cluster);
