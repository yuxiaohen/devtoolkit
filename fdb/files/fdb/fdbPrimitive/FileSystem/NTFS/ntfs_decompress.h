﻿#pragma once

#include "stdio.h"
#include "string.h"

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;


int f_NTFS_decompress(u8 *dest, const u32 dest_size,u8 *const cb_start, const u32 cb_size);
