#include "ntfs_datarun.h"


int f_NTFS_GetDataRun(uint8_t *buf,uint64_t *offset,uint64_t* clustersize)//读取一个DataRun的节点,并且返回这个节点的长度,通过地址返回偏移值(offset)和簇长度(clustersize)
{
    int offset_len = buf[0] >> 4;
    int clustersize_len = buf[0]&0X0F;

    if(buf[0] == 0)return 0;

    if(offset!=NULL)
    {
        int i = 0;
        *offset = 0;

        if(buf[offset_len + clustersize_len]>=0X80) //判断负号
        {
            *offset = -1;
        }


        for(i=0; i<offset_len; ++i)
        {
            uint8_t c= buf[offset_len + clustersize_len - i];
            *offset <<= 8;
            *offset |= c;
        }
    }

    if(clustersize!=NULL)
    {
        int i = 0;
        *clustersize = 0;

        for(i=0; i<clustersize_len; ++i)
        {
            *clustersize <<= 8;
            *clustersize |= buf[clustersize_len - i];
        }
    }
    return offset_len + clustersize_len + 1;
}

uint64_t f_NTFS_GetClusterOffsetFromDataRun(uint8_t* DataRun,uint64_t ClusterNum) //错误返回-1,传入簇号,算出偏移
{
    uint64_t real_offset=0,offset,size;
    uint32_t run_len = 0;
    uint64_t num = 0;
    uint8_t * RunList = DataRun;

    if(DataRun==NULL)return -1;
    while(run_len = f_NTFS_GetDataRun(RunList,&offset,&size),run_len>0)
    {
        uint64_t nextnum = num + size;
        real_offset += offset;
        if(num<=ClusterNum && ClusterNum<nextnum)
        {
            break;
        }
        num = nextnum;
        RunList += run_len;
    }
    if(run_len<=0)return -1;
    real_offset += (ClusterNum - num);
    return real_offset;
}
