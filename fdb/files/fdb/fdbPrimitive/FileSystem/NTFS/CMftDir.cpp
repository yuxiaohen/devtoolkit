#include "CMftDir.h"
CMftDir::CMftDir()
{
    Init();
    {
        mp_IndxBuf = (INDX*)calloc(1,sizeof(INDX));
        if(mp_IndxBuf==NULL)exit(1);
    }
}

CMftDir::~CMftDir()
{
    free(mp_IndxBuf);
}

void CMftDir::Init()
{
    mp_Index=NULL;
    mp_ii_0XA0=NULL;
    mp_ii_0X90=NULL;
    mp_na0X90 =NULL;
    mp_na0XA0 =NULL;
    mul_ClusterSize =0;
}

bool CMftDir::Set(CMft *mft)
{
    Init();
    if(mft!=NULL)
    {
        mp_Disk = mft->GetNtfsDisk();
        mp_Mft = mft;

        mp_na0X90 = mp_Mft->GetAttr0X90();
        mp_na0XA0 = mp_Mft->GetAttr0XA0();
        mul_ClusterSize = mp_Mft->GetClusterSize();

        return true;
    }
    return false;
}

bool CMftDir::GetFirstChildMftIndex(uint64_t &mftIndex,uint8_t filenamespace) //遍历子目录
{
    if(mp_Disk==NULL)return false;
    if(mp_na0XA0!=NULL)
    {
        if(mp_na0XA0->NonResidentFlag)
        {
            m_RunList_0XA0.ReadDataRunList(DATARUN(mp_na0XA0));
        }
        mp_Index = mp_na0XA0;
    }
    else
    {
        mp_Index = mp_na0X90;
    }

    if(mp_Index)
    {
        if(mp_Index->Type == 0XA0)
        {
            //if(mp_ii_0XA0==NULL)
            {
                mp_ii_0XA0 = NULL;
                uint64_t IndxOffset;
                if(m_RunList_0XA0.GetFirstClusterOffset(IndxOffset)!=(uint32_t)-1)
                {
                    uint64_t RealOffset = IndxOffset * mul_ClusterSize;
                    if(mp_Disk->ReadINDX(mp_IndxBuf,RealOffset) && f_NTFS_INDX_Requires(mp_IndxBuf) )
                    {
                        mp_ii_0XA0 = f_NTFS_GetFirstINDXItem(mp_IndxBuf);
                    }
                }
            }
            if(mp_ii_0XA0!=NULL)
            {
                mftIndex = mp_ii_0XA0->MFTNum;
                if((uint32_t)mftIndex!=0XFFFFFFFF)
                {
                    return true;
                }
            }

            if(mp_na0X90!=NULL)
            {
                mp_Index = mp_na0X90;
            }
        }
        if(mp_Index->Type == 0X90)
        {
            uint32_t AttrLen  =  mp_Index->Length;

            {
                IndexStruct *is = INDEX_STRUCT(mp_Index);
                mp_ii_0X90 = &is->ii;
            }

            if( (uint8_t*)mp_ii_0X90 - (uint8_t*)mp_Index < (signed)AttrLen
                    &&
                    (uint8_t*)NEXT_INDEX_ITEM(mp_ii_0X90) - (uint8_t*)mp_Index < (signed)AttrLen
              )
            {
                mftIndex = mp_ii_0X90 ->MFTNum;
                if((uint32_t)mftIndex!=0XFFFFFFFF)
                {
                    return true;
                }
            }
        }
    }
    return false;
}

bool CMftDir::GetNextChildMftIndex(uint64_t &mftIndex,uint8_t filenamespace) //遍历子目录
{
    if(mp_Disk==NULL)return false;
    if(mp_Index)
    {
        if(mp_Index->Type == 0XA0)
        {
            if(mp_ii_0XA0!=NULL)
            {
                mftIndex = mp_ii_0XA0->MFTNum;
                do
                {
                    mp_ii_0XA0 = f_NTFS_GetNextINDXItem(mp_ii_0XA0);
                }
                while(mp_ii_0XA0!=NULL && mftIndex == mp_ii_0XA0->MFTNum);//获取当前INDX的下一个MftIndex

                if(mp_ii_0XA0==NULL)//如果当前INDX遍历完毕
                {
                    uint64_t IndxOffset;
                    if(m_RunList_0XA0.GetNextClusterOffset(IndxOffset)!=(uint32_t)-1)
                    {
                        uint64_t RealOffset = IndxOffset * mul_ClusterSize;
                        if(mp_Disk->ReadINDX(mp_IndxBuf,RealOffset) && f_NTFS_INDX_Requires(mp_IndxBuf) )
                        {
                            mp_ii_0XA0 = f_NTFS_GetFirstINDXItem(mp_IndxBuf);
                        }
                    }
                }

                if(mp_ii_0XA0!=NULL)
                {
                    mftIndex = mp_ii_0XA0->MFTNum;
                    if((uint32_t)mftIndex!=0XFFFFFFFF)
                    {
                        return true;
                    }
                }
            }

            if(mp_na0X90!=NULL)
            {
                mp_Index = mp_na0X90;
            }
        }
        if(mp_Index->Type == 0X90)
        {
            uint32_t AttrLen  =  mp_Index->Length;
            if(mp_ii_0X90==NULL)
            {
                IndexStruct *is = INDEX_STRUCT(mp_Index);
                mp_ii_0X90 = &is->ii;
            }

            if( (uint8_t*)mp_ii_0X90 - (uint8_t*)mp_Index < (signed)AttrLen
                    &&
                    (uint8_t*)NEXT_INDEX_ITEM(mp_ii_0X90) - (uint8_t*)mp_Index < (signed)AttrLen
              )
            {
                mp_ii_0X90  = NEXT_INDEX_ITEM(mp_ii_0X90);
                mftIndex = mp_ii_0X90 ->MFTNum;
                if((uint32_t)mftIndex!=0XFFFFFFFF)
                {
                    return true;
                }
            }
        }
    }
    return false;
}
