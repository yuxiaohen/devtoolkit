#include "CNtfsFileScanner.h"

CNtfsFileScanner::CNtfsFileScanner(CNtfs* pNtfs):CFileScanner(pNtfs)
{
    Set(pNtfs);
}

int CNtfsFileScanner::Set(CNtfs* pNtfs)
{
    if(pNtfs!=NULL)
    {
        this->mp_Ntfs = pNtfs;
        return 1;
    }
    return 0;
}

uint32_t CNtfsFileScanner::ScanFile(uint32_t StartSector)
{
    if(this->mp_Ntfs!=NULL && this->mp_Ntfs->Usable())
    {
        return this->ScanFragment(0,
        &this->mp_Ntfs->GetMftFragmentList(),
        StartSector,0XFFFFFFFF);
    }
    return 0;
}
uint32_t CNtfsFileScanner::ScanFreeCluster(uint32_t StartSector)
{
    if(this->mp_Ntfs!=NULL && this->mp_Ntfs->Usable())
    {
        CBitMap b;
        if(this->mp_Ntfs->GetBitMap(&b))
        {
            CFragmentList list = b.GetFragmentList(false);
            return this->ScanFragment(0,
            &list,
            StartSector,0XFFFFFFFF);
        }
    }
    return 0;
}
