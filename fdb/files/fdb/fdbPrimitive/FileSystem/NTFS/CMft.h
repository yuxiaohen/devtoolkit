﻿#pragma once

#include "CNtfs.h"
#include "../CFragmentList.h"

#include <string>
using std::wstring;

class LIB_INTERFACE CMft
{
public:
    CMft();
    virtual ~CMft();

public:
    bool Set(MFT *mft = NULL,uint64_t offset = 0,CNtfs* ntfs = NULL);//将mft设置给对象

    bool SetMftByOffset(CFileSystem* ntfs,uint64_t Offset);//通过CNtfs类以及Mft偏移量设置Mft对象

    bool SetMftByIndex(CNtfs* ntfs,uint64_t Index);//通过CNtfs类以及Mft序号设置Mft对象

    bool isDir()const; //判断是否为目录

    bool isDel()const; //判断是否被删除

    //获取文件名,filenamespace 值 : 1 为POSIX 文件名, 2 为 Windows 文件名,  4 为 DOS文件名,可以用按位或相连
    const wchar_t * FileName(uint32_t& size,uint8_t filenamespace)const;

//Get
    CFragmentList  GetFragmentList()const//获取文件碎片列表
    {
        CFragmentList FileRunList;
        uint32_t i;
        for(i=0; i<m_AttrList.size(); ++i)
        {
            if(m_AttrList.at(i)->Type==0X80 && m_AttrList.at(i)->NonResidentFlag)
            {
                FileRunList.ReadDataRunList( DATARUN(m_AttrList.at(i)) );
            }
        }
        return FileRunList;
    }
    const MFT* GetMft()const//获取MFT结构
    {
        return (m_MFTList.size()>0?&m_MFTList.at(0):NULL);
    }

    uint64_t GetMftOffset()const//获取Mft偏移地址
    {
        return mull_MftOffset;
    }

    const vector <NTFSAttribute*>& GetAttrList()const//获取属性列表
    {
        return m_AttrList;
    }

    wstring GetFileName()const
    {
        wstring s;

        uint32_t len;
        const wchar_t * name = FileName(len,3);
        if(name!=NULL)
        {
            s = name;
            s.erase(len);
        }

        return s;
    }

    wstring GetPath()const
    {
        if(m_MFTList.size()>0)
        return PrintPath(&m_MFTList.at(0),mull_MftOffset);
        return wstring(L"");
    }

    CNtfs* GetNtfsDisk()
    {
        return mp_Disk;
    }

    uint64_t GetFileSize()const
    {
        uint64_t FileSize = 0;
        if(mp_na0X80!=NULL)
        {
            if(mp_na0X80->NonResidentFlag)//非常驻
            {
                FileSize = NONRESIDENTSIZE(mp_na0X80);
            }
            else //常驻
            {
                FileSize = RESIDENTSIZE(mp_na0X80);
            }
        }
        return FileSize;
    }

    bool GetStandardInfomation(StandardInfomation*si)
    {
        if(si!=NULL&&mp_na0X10)
        {
            StandardInfomation *psi = STANDARD_FILE_INFO(mp_na0X10);
            *si = *psi;
            return true;
        }
        return false;
    }

    wstring PrintPath(const MFT*m,uint64_t moffset)const;//填入MFT和偏移量 输出路径

    uint32_t GetClusterSize()
    {
        return mul_ClusterSize;
    }

    NTFSAttribute* GetAttr0X10()
    {
        return mp_na0X10;
    }

    NTFSAttribute* GetAttr0X20()
    {
        return mp_na0X20;
    }

    NTFSAttribute* GetAttr0X80()
    {
        return mp_na0X80;
    }

    NTFSAttribute* GetAttr0X90()
    {
        return mp_na0X90;
    }

    NTFSAttribute* GetAttr0XA0()
    {
        return mp_na0XA0;
    }

private:
    bool Config(CNtfs* ntfs = NULL);
    void Init();

private:

    uint32_t     mul_ClusterSize;//每簇字节数
    uint64_t mull_MftOffset;//MFt索引值
    vector <NTFSAttribute*> m_AttrList;//属性列表
    vector <MFT> m_MFTList;//MFT列表,通常情况下他只有一个项,若有0X20(NTFS扩展属性),他可能有多个项

    CNtfs* mp_Disk; //通过CNtfs解析
    NTFSAttribute *mp_na0X10,*mp_na0X20,*mp_na0X80,*mp_na0X90,*mp_na0XA0;//属性
};
