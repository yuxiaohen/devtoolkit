#include "CNtfsFileReader.h"
CNtfsFileReader::CNtfsFileReader(CFileSystem* pFileSystem)
{
    this->SetFileSystem(pFileSystem);
}

CNtfsFileReader::~CNtfsFileReader()
{

}

bool CNtfsFileReader::SetFileSystem(CFileSystem* pFileSystem)
{
    if(pFileSystem!=NULL && pFileSystem->Usable())
    {
        this->m_Reader.Close();
        this->mp_Info = NULL;
        this->mp_Ntfs = pFileSystem;
        return true;
    }
    return false;
}

bool  CNtfsFileReader::Config(SInfoSet* pInfoSet)
{
    return true;
}

bool CNtfsFileReader::FromFile(SFileInfo* pInfo)
{
    if(pInfo!=NULL && pInfo->GetType()==CFileSystem::NTFS)
    {
        this->m_Reader.Close();
        this->mp_Info = (SNtfsFileInfo*)pInfo;
        //读取mft;
        if(this->m_Mft.SetMftByOffset(this->mp_Ntfs,pInfo->GetOffset()))
        {
            if(this->m_Mft.GetAttr0X80()!=NULL)
            {
                if(this->m_Mft.GetAttr0X80()->NonResidentFlag!=0)
                {
                    CFragmentList fList = this->m_Mft.GetFragmentList();
                    uint32_t BytePerCluster = this->mp_Info->GetBytePerCluster();
                    if(this->mp_Ntfs->GetType()==CFileSystem::NTFS)
                    {
                        BytePerCluster = this->mp_Ntfs->GetClusterSize();
                    }
                    this->m_Reader.Set(this->mp_Ntfs->GetDisk(),&fList,BytePerCluster,0,this->mp_Ntfs->GetSectorSize());
                }
                return true;
            }
        }
    }
    return false;
}

uint32_t CNtfsFileReader::ReadBlock(uint32_t Index,uint8_t* OutBuf,uint32_t Size)
{
    if(this->mp_Info==NULL)return 0;
    if(this->m_Mft.GetAttr0X80()!=NULL && this->m_Mft.GetAttr0X80()->NonResidentFlag==0)
    {
        this->mul_ResidentIndex = Index;
        return this->ReadNextBlock(OutBuf,Size);
    }
    else
    {
        return this->m_Reader.ReadSector(Index,OutBuf,Size);
    }
    return 0;
}
uint32_t CNtfsFileReader::ReadNextBlock(uint8_t* OutBuf,uint32_t Size)
{
    if(this->mp_Info==NULL || this->m_Mft.GetAttr0X80()==NULL)return 0;
    if(this->m_Mft.GetAttr0X80()->NonResidentFlag==0)
    {
        uint64_t FileSize = this->mp_Info->GetFileSize();
        uint64_t Start = this->mul_ResidentIndex*this->mp_Ntfs->GetSectorSize();
        if(FileSize<=Start)return 0;

        uint64_t CopySize = Start+this->mp_Ntfs->GetSectorSize()*Size >FileSize ? FileSize - Start:this->mp_Ntfs->GetSectorSize()*Size;
        memcpy(OutBuf,(uint8_t*)GET_ATTRIBUTE(this->m_Mft.GetAttr0X80())+Start,CopySize);
        this->mul_ResidentIndex+=Size;
        return CopySize/this->mp_Ntfs->GetSectorSize() + 1;
    }
    else
    {
        return this->m_Reader.ReadNextSector(OutBuf,Size);
    }
}
