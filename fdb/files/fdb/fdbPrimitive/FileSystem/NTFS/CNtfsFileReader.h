#pragma once

#include "../CFileReader.h"
#include "../CFragmentReader.h"
#include "CMft.h"
#include "SNtfsFileInfoSet.h"

class LIB_INTERFACE CNtfsFileReader:public CFileReader
{
public:
    CNtfsFileReader(CFileSystem* pFileSystem = NULL);
    virtual ~CNtfsFileReader();
    bool SetFileSystem(CFileSystem* pFileSystem);
    bool  Config(SInfoSet* pInfoSet);
    bool FromFile(SFileInfo* pInfo);
    uint32_t ReadBlock(uint32_t Index,uint8_t* OutBuf,uint32_t Size);
    uint32_t ReadNextBlock(uint8_t* OutBuf,uint32_t Size);
private:
    CFileSystem* mp_Ntfs;
    SNtfsFileInfo* mp_Info;
    CMft m_Mft;
    uint32_t mul_ResidentIndex;
    CFragmentReader m_Reader;
};
