#include "ntfs_struct.h"

#ifdef DEBUG
void MftOutput(MFT* ms)
{
    printf("\nFileHead:\n");
    printf("    szFile:                  %s\n",ms->szFile);
    printf("    usOffset:                %d\n",ms->usOffset);
    printf("    UsnSize:                 %d\n",ms->UsnSize);
    printf("    LSN:                     %s\n",ms->LSN);
    printf("    SN:                      %d\n",ms->SN);
    printf("    HardLinkNum:             %d\n",ms->HardLinkNum);
    printf("    FirstAttributeOffset:    %d\n",ms->FirstAttributeOffset);
    printf("    Signal:                  %d\n",ms->Signal);
    printf("    FileRecordSize:          %d\n",ms->FileRecordSize);
    printf("    TotalRecordSize:         %d\n",ms->TotalRecordSize);
    printf("    FileIndexNum:            %s\n",ms->FileIndexNum);
    printf("    NextAttributeNumber:     %d\n",ms->NextAttributeNumber);
    printf("    Border:                  %d\n",ms->Border);
    printf("    FileRecordNumber:        %d\n",ms->FileRecordNumber);
    printf("\n");
}

void NTFSAttriOutput(NTFSAttribute * cna)
{
    printf("\nAttribute:\n");
    printf("    Type:                       x\n",cna->Type);
    printf("    Length:                     %d\n",cna->Length);
    printf("    NonResidentFlag:            %d\n",cna->NonResidentFlag==1);
    printf("    NameLength:                 %d\n",cna->NameLength);
    printf("    ContentOffset:              %d\n",cna->ContentOffset);
    printf("    CompressedFlag:             %d\n",cna->CompressedFlag);
    printf("    Identifcator:               %d\n",cna->Identifcator);

    if(cna->NonResidentFlag)
    {
        printf("    StartVCN:                   %I64u\n",cna->Common.NonResident.StartVCN);
        printf("    LastVCN:                    %I64u\n",cna->Common.NonResident.LastVCN);
        printf("    RunListOffset:              %d\n",cna->Common.NonResident.RunListOffset);
        printf("    CompressEngineIndex:        %d\n",cna->Common.NonResident.CompressEngineIndex);
        printf("    StreamAllocSize:            %I64u\n",cna->Common.NonResident.StreamAllocSize);
        printf("    StreamRealSize:             %I64u\n",cna->Common.NonResident.StreamRealSize);
        printf("    StreamCompressedSize:       %I64u\n",cna->Common.NonResident.StreamCompressedSize);
    }
    else
    {
        printf("    StreamLength:               %d\n",cna->Common.resident.StreamLength);
        printf("    StreamOffset:               %d\n",cna->Common.resident.StreamOffset);
        printf("    IndexFlag:                  %d\n",cna->Common.resident.IndexFlag);
    }
    printf("\n");
}

void StandardInfomationOutput(StandardInfomation* sa)
{
    SYSTEMTIME st,st2;
    FileTimeToSystemTime((FILETIME*)&sa->FileCreateTime,&st2);
    SystemTimeToTzSpecificLocalTime(NULL,&st2,&st);
    printf("FileCreateTime:       %d/%d/%d  %d:%d:%d\n",st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond);
    FileTimeToSystemTime((FILETIME*)&sa->FileLastAccessTime,&st2);
    SystemTimeToTzSpecificLocalTime(NULL,&st2,&st);
    printf("FileLastAccessTime:   %d/%d/%d  %d:%d:%d\n",st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond);
    FileTimeToSystemTime((FILETIME*)&sa->FileLastChangeTime,&st2);
    SystemTimeToTzSpecificLocalTime(NULL,&st2,&st);
    printf("FileLastChangeTime:   %d/%d/%d  %d:%d:%d\n",st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond);
}

void INDXView(INDX* indx)
{
    printf("szINDX:         %s\n",indx->szINDX);
}

void INDXItemView(INDXItem* indx)
{
    printf("MFTNum:         %I64d\n",indx->MFTNum&0XFFFFFFFF);
    printf("Size:           %d\n",indx->IndxSize);
    wprintf(L"FileName:     %.*s\n",indx->FileNameLength,((char*)indx)+sizeof(INDXItem) );
}

void struct_size_view()
{
    fprintf(stdout,"DBR:                    %d\n",sizeof(NTFS_DBR));
    fprintf(stdout,"NTFSBPB:                %d\n",sizeof(NTFSBPB));
    fprintf(stdout,"NTFSAttribute:          %d\n",sizeof(NTFSAttribute));
    //fprintf(stdout,"DPT:                    %d\n",sizeof(DPT_NODE));
    //fprintf(stdout,"MBR:                    %d\n",sizeof(MBR));
    fprintf(stdout,"MFT:                    %d\n",sizeof(MFT));
}
#endif
//////////////////////////////////////////



//操作函数

void f_NTFS_MftSet(MFT * mft) //修改MFT读取时的更新数组
{
    ((char*)mft)[510] = ((char*)mft)[50];
    ((char*)mft)[511] = ((char*)mft)[51];
    ((char*)mft)[510+1*512] = ((char*)mft)[52];
    ((char*)mft)[511+1*512] = ((char*)mft)[53];
}

void f_NTFS_IndxSet(INDX *indx) //修改INDX读取时的更新数组
{
    ((char*)indx)[510]       = ((char*)indx)[42];
    ((char*)indx)[511]       = ((char*)indx)[43];
    ((char*)indx)[510+512*1] = ((char*)indx)[42];
    ((char*)indx)[511+512*1] = ((char*)indx)[43];
    ((char*)indx)[510+512*2] = ((char*)indx)[42];
    ((char*)indx)[511+512*2] = ((char*)indx)[43];
    ((char*)indx)[510+512*3] = ((char*)indx)[42];
    ((char*)indx)[511+512*3] = ((char*)indx)[43];
}

int f_NTFS_Requires(const void* dbr)
{
    return !memcmp(&((const char*)dbr)[0X03],"NTFS ",5) && //NTFS加一个空格
           HEAD_SECTOR_USABLE(dbr);
}

int f_NTFS_MFT_Requires(const void* mft)
{
    NTFSAttribute* na = NULL;
    if(memcmp(&((const char*)mft)[0X00],"FILE0",5)!=0)return 0;
    na = f_NTFS_GetFirstNTFSAttribute((const MFT*)mft);
    if(na==NULL) return 0;
    do
    {
        if((uint8_t*)na - (uint8_t*)mft >= sizeof(MFT))
        {
            return 0;
        }
        if(na->Type%0X10!=0 || (0X01>na->Type/0X10 || na->Type/0X10 >0X10 ) )
        {
            return 0;
        }
        na = f_NTFS_GetNextNTFSAttribute(na);
    }
    while(na!=NULL);
    return 1;
}

int f_NTFS_INDX_Requires(const void* indx)
{
    return !memcmp((const char*)indx,"INDX",4);
}

NTFSAttribute* f_NTFS_GetFirstNTFSAttribute(const MFT * m)//通过MFT获取NTFS第一个属性地址
{
    if(m==NULL)return NULL;
    if(  m->FirstAttributeOffset<=0 || m->FirstAttributeOffset>=sizeof(MFT) ) return NULL;
    return  (NTFSAttribute*) ( ((char*)m) +  m->FirstAttributeOffset ) ;
}

NTFSAttribute* f_NTFS_GetNextNTFSAttribute(NTFSAttribute *na)//通过NTFS属性获得NTFS下一个属性的地址
{
    NTFSAttribute* next = NULL;
    if( na==NULL || na->Type==0XFFFFFFFF )return NULL;//结尾属性无法获取下一个属性
    if(na->Length>=sizeof(MFT))return NULL;//参数错误,溢出
    next = (NTFSAttribute*) ( ((char*)na) + na->Length );
    if(na==next || next->Type==0XFFFFFFFF)return NULL;
    return  next ;
}

NTFSAttribute* f_NTFS_FindAttributeByType(NTFSAttribute* start,uint32_t Type)//从start属性开始继续查找,除非为NULL
{
    NTFSAttribute *na = NULL;
    if(start==NULL)return NULL;
    na = start;
    while(na!=NULL)
    {
        if(na->Type==Type) //查找属性值
        {
            break;
        }
        na = f_NTFS_GetNextNTFSAttribute(na);
    }
    return na;
}

uint64_t f_NTFS_GetParentDirMftIndex(const MFT* m)//获取MFT的父目录MFT
{
    NTFSAttribute * ma30 = f_NTFS_GetFirstNTFSAttribute(m);
    FileNameAttribute *fa = NULL;
    ma30 = f_NTFS_FindAttributeByType(ma30,0X30);

    if(ma30==NULL)return 0;

    fa  =  FILENAMEATTRIBUTE(ma30);
    return fa->ParentFileNum;
}


INDXItem * f_NTFS_GetFirstINDXItem(INDX *indx)//从INDX结构中找到第一个INDXItem结构
{
    if(indx == NULL)return NULL;
    if( !f_NTFS_INDX_Requires(indx) ) return NULL;
    return (INDXItem*) ( ((char*)indx) + 0x18 + indx->IndexOffset );
}

INDXItem * f_NTFS_GetNextINDXItem(INDXItem *indx)//通过INDXItem结构获取下一个INDXItem结构
{
    INDXItem * next = NULL;
    if(indx==NULL)return NULL;
    if(indx->IndxSize <= 16 || indx->IndxSize >= 1024)return NULL;
    next = (INDXItem*) (  ((char*)indx) + indx->IndxSize );
    if(next->IndxSize <= 16)return NULL;
    return next;
}

wchar_t* f_NTFS_GetMFTFileName(const MFT* m,uint32_t * size,uint8_t filenamespace) //指定名字空间,即获取文件名类型,  1 为POSIX 文件名, 2 为 Windows 文件名,  4 为 DOS文件名
{
    wchar_t* name = NULL;
    NTFSAttribute * filename = f_NTFS_GetFirstNTFSAttribute(m);
    while(filename = f_NTFS_FindAttributeByType(filename,0X30),filename!=NULL )//0X30为文件名属性代号
    {
        FileNameAttribute *fa =  (FileNameAttribute*) ( ((char*)filename) + 0X18 );//0X18为常驻属性长度
        uint32_t offset = 0X18 + sizeof(FileNameAttribute);
        uint8_t mode = ( 2*(fa->FileNameSpace&2) ) | ( 2*(fa->FileNameSpace&1) ) | (fa->FileNameSpace == 0);
        //printf("%d\n",mode);
        if(  mode & filenamespace  )
        {
            name = (wchar_t*)  ( ((char*)filename) + offset );
            if(size!=NULL)*size = fa->FileNameLength;
            break;
        }

        filename = f_NTFS_GetNextNTFSAttribute(filename); //下一个属性
    }
    return name;
}

uint64_t f_NTFS_GetMftOffsetFromDataRun(uint8_t* DataRun,uint64_t MftNum,uint32_t uint8_t_per_cluster) //错误返回-1,传入簇号,算出偏移
{
    uint64_t real_offset = f_NTFS_GetClusterOffsetFromDataRun(DataRun,MftNum/(uint8_t_per_cluster/sizeof(MFT)));
    if(real_offset==(uint64_t)-1)return -1;

    real_offset *= uint8_t_per_cluster;
    real_offset += sizeof(MFT) * (MftNum %(uint8_t_per_cluster/sizeof(MFT)));
    return real_offset;
}
