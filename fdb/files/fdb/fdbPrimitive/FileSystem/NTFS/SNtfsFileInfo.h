#pragma once

#include "../SInfo.h"
#include "ntfs_struct.h"

struct SNtfsFileInfo:public SFileInfo//NTFS文件系统MFT记录过大(1KB+),如果存放在内存中过于占用空间 于是只存储偏移量和数个重要信息
{
    //以下的数个属性包含了大部分文件信息,他们来自MFT中0X30 NTFS属性
    struct
    {
        uint64_t Offset;//偏移量
        uint32_t ParentIndex;//父目录项编号
        uint32_t Flag;//文件标识
        uint32_t Signal;//0X00表示文件被删除,0X01表示文件正在使用,0X02表示目录被删除,0X03表示该记录为目录
        uint32_t BytePerCluster; // 簇大小
        uint64_t FileSize;//文件实际大小
        uint64_t FileCreate;//文件创建时间
        uint64_t FileChange;//文件修改时间
        uint64_t FileLastAccess;//文件最后修改时间
    }data;
    wstring FileName;//文件名

    SNtfsFileInfo(){Clear();}

    uint64_t GetOffset()
    {
        return data.Offset;
    }

    uint32_t GetParentIndex()
    {
        return data.ParentIndex;
    }

    bool isDir()
    {
        return (this->data.Flag&0X10000000) &&  (this->data.Signal==0X03||this->data.Signal==0X02);
    }

    bool isDel()
    {
        return  (this->data.Signal==0X00||this->data.Signal==0X02);
    }
    wstring& GetFileName()
    {
        return this->FileName;
    }
    uint64_t GetFileSize()
    {
        return data.FileSize;
    }
    uint32_t GetType()
    {
        return CFileSystem::NTFS;
    }

    uint32_t GetBytePerCluster()
    {
        return data.BytePerCluster;
    }

    void Clear()
    {
        data.Offset = 0;
        data.ParentIndex = -1;
        data.Flag = 0;
        data.Signal = 0;
        data.FileSize = 0;
        data.FileCreate = 0;
        data.FileChange = 0;
        data.FileLastAccess = 0;
        data.BytePerCluster = 0;
        FileName = L"";
    }

    bool Set(MFT *m,uint64_t MftOffset)
    {
        if(f_NTFS_MFT_Requires(m)==0)return false;
        f_NTFS_MftSet(m);
        Clear();
        NTFSAttribute * na_head = f_NTFS_GetFirstNTFSAttribute(m);
        NTFSAttribute * na30 = f_NTFS_FindAttributeByType(na_head,0X30);//查找0X30属性
        if(na30!=NULL)//是合法的MFT并且具有0X30属性
        {
            FileNameAttribute * fa = FILENAMEATTRIBUTE(na30);
            this->data.FileSize = fa->FileRealSize;
            this->data.FileChange = fa->LastChangeTime;
            this->data.FileCreate = fa->FileCreateTime;
            this->data.FileLastAccess = fa->LastAccessTime;
            this->data.Flag = fa->Flag;
            this->data.Offset = MftOffset;
            this->data.Signal = m->Signal;
            this->data.ParentIndex = (uint32_t)fa->ParentFileNum;

            uint32_t len;
            wchar_t * name = f_NTFS_GetMFTFileName(m,&len,1|2);//获取POSIX文件名和Windows文件名
            if(name!=NULL)
            {
                this->FileName.assign(name,len);
            }

            NTFSAttribute* na80 = f_NTFS_FindAttributeByType(na_head,0X80);
            if(na80!=NULL)
            {
                if(na80->NonResidentFlag!=0)
                {
                    //设置Bytepercluster
                    {
                        uint64_t LastVcn = na80->Common.NonResident.LastVCN;
                        if(LastVcn+1>0)
                        {
                            this->data.BytePerCluster = na80->Common.NonResident.StreamAllocSize/(LastVcn+1);
                        }
                    }
                    this->data.FileSize = NONRESIDENTSIZE(na80);
                }
                else
                {
                    this->data.FileSize = RESIDENTSIZE(na80);
                }
            }
            return true;
        }
        else
        {
            this->data.ParentIndex = -1;
        }
        Clear();
        return false;
    }
};
