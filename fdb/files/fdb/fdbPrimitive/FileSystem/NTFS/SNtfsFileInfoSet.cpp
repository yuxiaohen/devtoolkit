#include "SNtfsFileInfoSet.h"

bool SNtfsFileInfoSet::AddInfo(SInfo* pInfo)
{
    if(pInfo!=NULL && pInfo->GetType()==CFileSystem::NTFS)
    {
        this->m_InfoList.push_back(*(SNtfsFileInfo*)pInfo);
        return true;
    }
    return false;
}
uint32_t SNtfsFileInfoSet::ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize)
{
    uint32_t Total = 0;
    MFT*  pMft = (MFT*)pBlock;
    while(Total+sizeof(MFT)<=BlockSize)
    {
        if(this->m_NtfsInfo.Set(pMft,Offset+Total) || ScanFailedMft )
        {
            this->AddInfo(&this->m_NtfsInfo);
        }
        this->m_NtfsInfo.Clear();
        ++pMft;
        Total += sizeof(MFT);
    }
    return Total;
}
uint32_t SNtfsFileInfoSet::GetType()
{
    return CFileSystem::NTFS;
}
uint32_t SNtfsFileInfoSet::size()
{
    return this->m_InfoList.size();
}
SInfo* SNtfsFileInfoSet::at(uint32_t index)
{
    if(index>=this->m_InfoList.size())
    {
        return NULL;
    }
    return &this->m_InfoList.at(index);
}
void SNtfsFileInfoSet::clear()
{
    this->m_InfoList.clear();
}
