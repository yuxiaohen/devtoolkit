#include "CNtfs.h"
#include "CMft.h"
#include "CNtfsFileReader.h"
#include "CNtfsFileScanner.h"

CNtfs::CNtfs(CFileSystem* pFs):CFileSystem(pFs)
{
    mull_Mftoffset = 0;
    mull_TotalMFT = 0;
}

CNtfs::~CNtfs()
{
}


int CNtfs::ConfigByMft(uint64_t MftOffset, MFT* mft)
{
    this->SetClusterSize(0);
    if(!this->Usable())return 0;

    MFT *pMft = mft;
    MFT MftBuf;

    if(pMft==NULL)
    {
        //若未传入MFT结构 则分配空间供读取,并且设置读取指针
        pMft = &MftBuf;
        this->SetPoint(MftOffset,CIO::START_POS);
    }

    if(
        mft!=NULL  //自行设置MFT
        ||  this->Read(pMft,sizeof(MFT))>0 //或者读取MFT头部记录
    )
    {
        if(!f_NTFS_MFT_Requires(pMft)) //判断MFT结构是否正确
        {
            return -1;
        }

        f_NTFS_MftSet(pMft);//修正更新序列号
        m_MftDataRun.ClearList();
        mull_TotalMFT = 0;
        NTFSAttribute * ma = f_NTFS_GetFirstNTFSAttribute(pMft);
        NTFSAttribute * ma80 = f_NTFS_FindAttributeByType(ma,0X80);
        if(ma80!=NULL && ma80->NonResidentFlag)
        {
            m_MftDataRun.ReadDataRunList(DATARUN(ma80));
            //获得$MFT文件在磁盘上偏移量
            if(m_MftDataRun.GetTotalFrag()>0)
            {
                mull_Mftoffset = m_MftDataRun.GetOffset(0);
            }
            //计算簇大小
            if(this->GetClusterSize()==0)
            {
                uint64_t LastVcn = ma80->Common.NonResident.LastVCN;
                if(LastVcn+1>0)
                {
                    this->SetClusterSize( ma80->Common.NonResident.StreamAllocSize/(LastVcn+1) );
                }
            }
            mull_TotalMFT =  ma80->Common.NonResident.StreamRealSize/sizeof(MFT);
            return 1;
        }
        return -1;
    }
    return 0;
}

    CFileScanner* CNtfs::newCFileScannerObject()
    {
	return new CNtfsFileScanner(this);
    }
    CFileReader* CNtfs::newCFileReaderObject(SInfoSet* pInfoSet)
    {
	CFileReader * pFileReader = new CNtfsFileReader(this);
	if(pFileReader!=NULL&&pInfoSet!=NULL)
        {
            if(!pFileReader->Config(pInfoSet))
            {
                delete pFileReader;
                pFileReader = NULL;
            }
        }
        return pFileReader;
    }

int CNtfs::Config(uint64_t DbrOffset ,const void * dbr)
{
    if(!this->Usable())return 0;

    this->SetClusterSize(0);
    mull_Mftoffset = 0;
    mull_TotalMFT = 0;
    m_MftDataRun.ClearList();

    NTFS_DBR * pDbr=(NTFS_DBR *)dbr;
    static NTFS_DBR DbrBuf;
    if(pDbr==NULL)
    {
        //若未传入dbr结构 则分配空间供读取
        pDbr = &DbrBuf;
    }

    this->SetPoint(DbrOffset,1);
    if(
        (  dbr!=NULL  // 否则从磁盘中读取DBR
           || this->Read(pDbr,sizeof(NTFS_DBR))>0 //默认从磁盘头部读取,如果需要读取备份DBR,只能直接传入DBR
        )
    )
    {
        if(!f_NTFS_Requires(pDbr))
        {
            return 0;
        }
        this->SetClusterSize( BYTE_PER_CLUSTER(*pDbr) );
        this->SetSectorSize(pDbr->bpb.bytes_per_sector);
       this->SetPartitionSize(pDbr->number_of_sectors * pDbr->bpb.bytes_per_sector);
        int Ret = ConfigByMft(MFT_OFFSET(*pDbr));
        if(Ret == -1) //如果Mft结构不正确,读取备份MFT结构
        {
            Ret = ConfigByMft(MFTMIRR_OFFSET(*pDbr));
        }
        return Ret;
    }
    return 0;
}


bool CNtfs::GetMftOffsetByIndex(uint64_t index,uint64_t& offset)const
{
    if(index>=mull_TotalMFT || this->GetClusterSize()==0)return false;
    uint64_t cluster_index = index * sizeof(MFT) / this->GetClusterSize();
    if(m_MftDataRun.GetClusterOffsetByIndex(cluster_index,offset))
    {
        offset *= this->GetClusterSize();
        if( this->GetClusterSize() > sizeof(MFT) )
        {
            offset += sizeof(MFT) * ( index % (this->GetClusterSize() / sizeof(MFT)) );
        }
        return true;
    }
    return false;
}
/*
bool CNtfs::GetMftByOffset(uint64_t offset,MFT* mft)
{
    if(!this->Usable()||mft==NULL)return false;

    this->SetPoint(offset,CIO::START_POS);

    bool Ret =  this->Read(mft,sizeof(MFT))>0;

    return Ret;
}


bool CNtfs::GetMftByIndex(uint64_t index,MFT* mft)
{
    uint64_t offset;
    bool Ret (GetMftOffsetByIndex(index,offset) && GetMftByOffset(offset,mft) );
    return Ret;
}
*/


int CNtfs::ReadMFT(MFT*buf,uint64_t offset)
{
    if(!this->Usable())return 0;
    this->SetPoint(offset,CIO::START_POS);
    int Ret =   this->Read(buf,sizeof(MFT))>0 ;
    f_NTFS_MftSet(buf);
    return Ret;
}

int CNtfs::ReadINDX(INDX*buf,uint64_t offset)
{
    if(!this->Usable())return 0;
    this->SetPoint(offset,CIO::START_POS);
    int Ret =  this->Read(buf,sizeof(INDX))>0 ;
    f_NTFS_IndxSet(buf);
    return Ret;
}

bool CNtfs::GetBitMap(CBitMap* bitmap)
{
    if(bitmap!=NULL)
    {
        CMft Mft_Bitmap;
        if(Mft_Bitmap.SetMftByIndex(this,6))//第6个MFT为$BitMap
        {
            uint64_t filesize = Mft_Bitmap.GetFileSize();
            CFragmentList rl = Mft_Bitmap.GetFragmentList();
            uint32_t n = rl.GetTotalFrag();
            if(filesize>0 && n>0 && this->GetSectorSize()>0)
            {
                bitmap->CleanBitMap();

                uint32_t nSector = 128;
                uint32_t BufferSize = sizeof(uint8_t)*this->GetSectorSize()*nSector;
                uint8_t *pBuf = (uint8_t*)malloc(BufferSize);
                if(pBuf==NULL)exit(1);

                CFragmentReader fr;
                fr.Set(this->GetDisk(),&rl,this->GetClusterSize(),(uint64_t)0,this->GetSectorSize());
                uint64_t TotalRead = 0;
                uint32_t nRead = fr.ReadSector(0,pBuf,nSector);
                while(nRead >0)
                {
                    if(TotalRead + BufferSize > filesize)//已经读取完毕
                    {
                        bitmap->ReadBitMap(pBuf,(uint32_t)(filesize-TotalRead) );
                        break;
                    }
                    else
                    {
                        bitmap->ReadBitMap(pBuf,BufferSize);
                    }
                    TotalRead += BufferSize;
                    nRead = fr.ReadNextSector(pBuf,nSector);
                }
                free(pBuf);

                return true;
            }
        }
    }
    return false;
}

//读取卷标
wstring CNtfs::GetVolumeName()
{
    wstring wstrVolume;
    if(this->Usable())
    {
        this->SetPoint(mull_Mftoffset+3*sizeof(MFT),CIO::START_POS);
        MFT volume;
        if(  this->Read(&volume,sizeof(volume))>0  )
        {
            f_NTFS_MftSet(&volume);
            NTFSAttribute * ma = f_NTFS_GetFirstNTFSAttribute(&volume);
            NTFSAttribute * ma60 = f_NTFS_FindAttributeByType(ma,0X60);
            if(ma60)
            {
                wchar_t* volumename =  (wchar_t*)GET_ATTRIBUTE(ma60);
                wstrVolume.assign(volumename,RESIDENTSIZE(ma60)/2);
            }
        }
    }
    return wstrVolume;
}
