#include "CMft.h"

CMft::CMft()
{
    Init();
    mull_MftOffset = 0;
    mul_ClusterSize = 0;
}

CMft::~CMft()
{

}

void CMft::Init()
{
    mp_Disk = NULL;

    mp_na0X10=NULL;
    mp_na0X20=NULL;
    mp_na0X80=NULL;
    mp_na0X90=NULL;
    mp_na0XA0=NULL;
}

const wchar_t * CMft::FileName(uint32_t& size,uint8_t filenamespace)const//获取文件名
{
    wchar_t *filename =NULL;
    uint32_t i;
    for(i=0; i<m_AttrList.size(); ++i)
    {
        NTFSAttribute * na = m_AttrList.at(i);
        FileNameAttribute * fa = FILENAMEATTRIBUTE(na);
        uint8_t mode = ( 2*(fa->FileNameSpace&2) ) | ( 2*(fa->FileNameSpace&1) ) | (fa->FileNameSpace == 0);
        if(na->Type==0X30 && (mode & filenamespace) )  //代表文件名的属性
        {
            filename = FILE_NAME(na);
            size = fa->FileNameLength;
            break;
        }
    }
    return filename;
}

bool CMft::Config(CNtfs* ntfs)
{
    if(m_MFTList.size()<=0)return false; //在没有设置MFT结构的时候无法进行配置

    m_AttrList.clear();
    //m_RunList_0XA0.ClearList();

    Init();

    NTFSAttribute *na_head = f_NTFS_GetFirstNTFSAttribute(&m_MFTList.at(0));
    NTFSAttribute *na = na_head;

    //if(na_head==NULL) return false;

    while(na!=NULL)
    {
        if(na->Type==0X20)
        {
            mp_na0X20 = na; //0X20H扩展项
        }

        m_AttrList.push_back(na);

        na = f_NTFS_GetNextNTFSAttribute(na);
    }
//*/

    if(ntfs!=NULL) //使用CNtfs对象进行配置
    {
        mp_Disk = ntfs;

        mul_ClusterSize = mp_Disk->GetClusterSize();

        if( mp_na0X20!=NULL && mul_ClusterSize>0) //存在扩展项
        {
            m_AttrList.clear();

            na = mp_na0X20;

            uint32_t nAttr = na->Length;
            uint8_t* AttributeBuffer = NULL;
            AttributeList * al;//扩展项地址
            if(na->NonResidentFlag)//如扩展项非常驻,把扩展MFT目录项放入新开内存中解析,否则直接在MFT中解析
            {
                uint8_t* DataRun_0X20= DATARUN(na);
                nAttr = (uint32_t)na->Common.NonResident.StreamRealSize;
                uint32_t total_copy = 0;
                AttributeBuffer = (uint8_t*)calloc(nAttr + mul_ClusterSize,sizeof(uint8_t));
                if(AttributeBuffer==NULL)exit(1);
                uint64_t t_offset,t_clustersize,first_offset =0;
                uint32_t len,run_len = 0;
                while(len = f_NTFS_GetDataRun(DataRun_0X20+run_len,&t_offset,&t_clustersize),len>0 && total_copy<nAttr)
                {
                    run_len += len;
                    first_offset += t_offset;
                    uint64_t real_offset = first_offset * mul_ClusterSize;
                    mp_Disk->SetPoint(real_offset,CIO::START_POS);
                    if( mp_Disk->Read(AttributeBuffer+total_copy,mul_ClusterSize*t_clustersize)<=0)
                    {
                        free(AttributeBuffer);
                        return false;
                    }
                    total_copy += mul_ClusterSize*t_clustersize;
                }
                al = (AttributeList*)AttributeBuffer;
            }
            else
            {
                al = ATTRIBUTE_LIST( na);
            }
            AttributeList * start = al;

            uint64_t last_offset = 0;
            while( (uint8_t*)al - (uint8_t*)start < (signed)nAttr  )//逐个获取扩展MFT,放入Mft结构列表
            {
                uint64_t mft_offset;
                mp_Disk->GetMftOffsetByIndex((uint32_t)al->AttrBaseFileRecordNum,mft_offset);
                if( (uint32_t)last_offset!=(uint32_t)mft_offset )
                {
                    last_offset = mft_offset;
                    MFT mft;
                    if( mp_Disk->ReadMFT(&mft,mft_offset) )
                    {
                        if(f_NTFS_MFT_Requires(&mft))
                        {
                            m_MFTList.push_back(mft);
                        }
                    }
                }

                al = (AttributeList*) ( (uint8_t*)al + al->RecordLength );
            }

            //遍历全部的MFT结构,将NTFS属性存放进属性列表
            {
                uint32_t i;
                for(i=0; i<m_MFTList.size(); ++i)
                {
                    NTFSAttribute* t_na_head = f_NTFS_GetFirstNTFSAttribute(&m_MFTList.at(i));
                    NTFSAttribute* t_na = t_na_head;
                    while(t_na!=NULL)
                    {
                        if( (uint8_t*)t_na - (uint8_t*)t_na_head >= (signed)sizeof(MFT) //判断指针越界
                                || !(0X10 <= t_na->Type && t_na->Type <=0X100) || ( t_na->Type%0X10!=0) //判断类型值错误
                          )
                        {
                            break;
                        }

                        m_AttrList.push_back(t_na);
                        t_na = f_NTFS_GetNextNTFSAttribute(t_na);
                    }
                }
            }

            if(AttributeBuffer!=NULL)free(AttributeBuffer);
        }
    }
//*/

    //将NTFS属性0X10,0X80,0X90,0XA0归纳
    {
        uint32_t i;
        for(i=0; i<m_AttrList.size(); ++i)
        {

            if(m_AttrList.at(i)->Type==0XA0)
            {
                //m_RunList_0XA0.ReadList( DATARUN(m_AttrList.at(i)) );
                //mp_Index = m_AttrList.at(i);
                if(mp_na0XA0==NULL)
                {
                    mp_na0XA0=m_AttrList.at(i);
                }
            }
            if(m_AttrList.at(i)->Type==0X90)
            {
                //mp_Index = m_AttrList.at(i);
                if(mp_na0X90==NULL)
                {
                    mp_na0X90=m_AttrList.at(i);
                }
            }
            if(m_AttrList.at(i)->Type==0X80)
            {
                if(mp_na0X80==NULL)
                {
                    mp_na0X80 = m_AttrList.at(i);
                }
            }

            if(m_AttrList.at(i)->Type==0X10)
            {
                if(mp_na0X10==NULL)
                {
                    mp_na0X10 = m_AttrList.at(i);
                }
            }
        }
    }

    if(mp_na0X80!=NULL && mp_na0X80->NonResidentFlag && mul_ClusterSize==0)
    {
        uint64_t LastVcn = mp_na0X80->Common.NonResident.LastVCN;
        if(LastVcn+1>0)
        {
            mul_ClusterSize = mp_na0X80->Common.NonResident.StreamAllocSize/(LastVcn+1);
        }
    }

    return true;
}

bool CMft::Set(MFT *mft,uint64_t offset,CNtfs* ntfs)
{
    //Init();
    mul_ClusterSize = 0;
    mull_MftOffset = offset;

    if(mft!=NULL
            && f_NTFS_MFT_Requires(mft) //判断mft是否可用
      )
    {
        m_MFTList.clear();
        m_MFTList.push_back(*mft);
        f_NTFS_MftSet(&m_MFTList.at(0));
        return this->Config(ntfs);
    } //初始化
    return false;
}

bool CMft::SetMftByOffset(CFileSystem* ntfs,uint64_t Offset)
{
    static MFT mft;

    if(ntfs!=NULL)
    {
        ntfs->SetPoint(Offset,CIO::START_POS);
        if(ntfs->Read(&mft,sizeof(mft))>0)
        {
            if(ntfs->GetType()==CFileSystem::NTFS)
            {
                return this->Set(&mft,Offset,(CNtfs*)ntfs);
            }
            return this->Set(&mft,Offset,NULL);
        }
    }
    return false;
}

bool CMft::SetMftByIndex(CNtfs* ntfs,uint64_t Index)
{
    uint64_t Offset;
    if(ntfs!=NULL && ntfs->GetMftOffsetByIndex(Index,Offset))
    {
        return this->SetMftByOffset(ntfs,Offset);
    }
    return false;
}

bool CMft::isDir()const
{
    if(
        m_MFTList.size()>0 &&
        (m_MFTList.at(0).Signal==0X03 || m_MFTList.at(0).Signal==0X02)
    )
        return true;
    return false;
}

bool CMft::isDel()const
{
    if(
        m_MFTList.size()>0 &&
        (m_MFTList.at(0).Signal==0X00 || m_MFTList.at(0).Signal==0X02)
    )
        return true;
    return false;
}

wstring CMft::PrintPath(const MFT*m,uint64_t moffset)const
{
    wstring path;
    if(mp_Disk==NULL)return path;

    uint32_t len;
    wchar_t * name = f_NTFS_GetMFTFileName(m,&len,1|2);
    wstring current;//当前MFT名字

    if(name)
    {
        current = name;
        current.erase(len);
    }
    else
    {
        return path;
    }

    uint64_t mftnum = (uint32_t)f_NTFS_GetParentDirMftIndex(m);

    if(mftnum==0)
    {
        return path;
    }

//计算Mft偏移

    uint64_t real_offset=-1;
    MFT mm;

    //获取父目录MFT->mm
    if((uint32_t)mftnum==(uint32_t)-1
            || !mp_Disk->GetMftOffsetByIndex(mftnum,real_offset)
            || !mp_Disk->ReadMFT(&mm,real_offset)
            || !f_NTFS_MFT_Requires(&mm)
       )
    {
        path = (L"*");
        path +=  L"/" + current;
        return path;
    }

    if(real_offset!=moffset )//没有到根目录
    {
        //对父目录进行判断
        {
            NTFSAttribute* ma_head = f_NTFS_GetFirstNTFSAttribute(&mm);
            NTFSAttribute* ma = f_NTFS_FindAttributeByType(ma_head,0X30);
            FileNameAttribute *fa = ma!=NULL?FILENAMEATTRIBUTE(ma):NULL;

            if( fa && (fa->Flag&0X10000000) &&  (mm.Signal==0X03||mm.Signal==0X02) )//判断路径是否为目录
            {
                path = PrintPath(&mm,real_offset);
            }
            else
            {
                path = (L"*");//未知目录(该路径不是目录)
            }
        }
        path +=  L"/" + current;
    }
    else
    {
        path = (L".");//到达根目录
    }

    return (path);
}
