﻿#pragma once

#include "ntfs_struct.h"
#include "../CFileSystem.h"
#include "../CBitMap.h"
#include "../CFragmentReader.h"

#include <string>
using std::wstring;

class LIB_INTERFACE CNtfs:public CFileSystem
{
public:
        CNtfs(CFileSystem* pFs = NULL);
virtual ~CNtfs();

private:
        CNtfs(CNtfs& );

public:
    uint32_t GetType()
    {
        return CFileSystem::NTFS;
    }
    virtual CFileScanner* newCFileScannerObject();

    virtual CFileReader* newCFileReaderObject(SInfoSet* pInfoSet = NULL);

    int Config(uint64_t DbrOffset =0,const void * dbr=NULL);

    int ConfigByMft(uint64_t MftOffset, MFT* mft = NULL);//通过MFT配置对象参数

    bool GetMftOffsetByIndex(uint64_t index,uint64_t& offset)const;//offset为输出参数,offset返回值为通过Mft序号获得的偏移量,返回false获取失败

    //bool GetMftByIndex(uint64_t index,MFT* mft);//通过Mft序号获取CMft对象,返回false获取失败

    //bool GetMftByOffset(uint64_t offset,MFT* mft);//通过OFFSET获得CMft对象,返回false获取失败

    int ReadMFT(MFT*buf,uint64_t offset);//读取MFT结构

    int ReadINDX(INDX*buf,uint64_t offset);//读取INDX结构

    static bool DbrRequires(void* dbr)
    {
        return f_NTFS_Requires(dbr);
    }

    uint64_t GetTotalMFT()const //获取Mft总数
    {
        return mull_TotalMFT;
    }

    uint64_t GetMftoffset()const
    {
	return mull_Mftoffset;
    }

    CFragmentList& GetMftFragmentList()
    {
        return m_MftDataRun;
    }

    //读取卷标名
    wstring GetVolumeName();

    //获取BitMap
    bool GetBitMap(CBitMap* bitmap);

private:
    CFragmentList m_MftDataRun;//$MFT文件的文件碎片表

    uint64_t mull_Mftoffset;//$MFT文件在分区内偏移量
    uint64_t mull_TotalMFT;//$MFT文件中的MFT记录总数
};
