#pragma once

#include "../SInfoSet.h"
#include "SNtfsFileInfo.h"

#include <vector>
using std::vector;

struct LIB_INTERFACE SNtfsFileInfoSet:public SInfoSet
{
public:
    SNtfsFileInfoSet()
    {
        this->ScanFailedMft = false;
    }
    bool AddInfo(SInfo* pInfo);
    uint32_t ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize);

    uint32_t size();
    SInfo* at(uint32_t index);
    void clear();

    uint32_t GetType();

    vector <SNtfsFileInfo> m_InfoList;
    SNtfsFileInfo m_NtfsInfo;
    bool ScanFailedMft;
};
