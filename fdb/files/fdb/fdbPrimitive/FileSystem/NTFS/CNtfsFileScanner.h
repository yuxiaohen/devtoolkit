#pragma once

#include "../CFileScanner.h"
#include "CNtfs.h"
#include "SNtfsFileInfoSet.h"

class LIB_INTERFACE CNtfsFileScanner:public CFileScanner
{
public:
    CNtfsFileScanner(CNtfs* pNtfs = NULL);
    int Set(CNtfs* pNtfs);
    uint32_t ScanFile(uint32_t StartSector);
    uint32_t ScanFreeCluster(uint32_t StartSector);
private:
    CNtfs* mp_Ntfs;
};
