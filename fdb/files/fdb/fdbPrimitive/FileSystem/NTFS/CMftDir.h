﻿#pragma once

#include "CMft.h"
#include "../CFragmentList.h"

class LIB_INTERFACE CMftDir
{
public:
    CMftDir();
    virtual ~CMftDir();

private:
    CMftDir(CMftDir&);

public:
    bool Set(CMft *mft);

    bool GetFirstChildMftIndex(uint64_t &mftIndex,uint8_t filenamespace);//获取第一个子文件MFT索引值,如果非目录或者获取失败则返回false

    bool GetNextChildMftIndex(uint64_t &mftIndex,uint8_t filenamespace);//获取下一个子文件的MFT索引值,如果非目录或者获取失败则返回false

private:
    void Init();

private:

    CMft * mp_Mft;
    CNtfs *mp_Disk;

    uint32_t mul_ClusterSize;//字节每簇

    INDX * mp_IndxBuf;//存放INDX结构的指针
    NTFSAttribute *mp_Index;//索引属性
    IndexItem *mp_ii_0X90;//索引根遍历指针
    INDXItem * mp_ii_0XA0;//索引分配遍历指针

    NTFSAttribute *mp_na0X90,*mp_na0XA0;

    CFragmentList m_RunList_0XA0;// A0属性的DataRun;
};
