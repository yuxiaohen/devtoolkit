﻿#pragma once

#include "../sys_func.h"

//DataRun

int f_NTFS_GetDataRun(uint8_t *buf,uint64_t *offset,uint64_t* filesize);

uint64_t f_NTFS_GetClusterOffsetFromDataRun(uint8_t* DataRun,uint64_t ClusterNum);
