#pragma once

#include "sys_func.h"
#include "CFragmentList.h"

#include <vector>
using std::vector;

class LIB_INTERFACE CBitMap
{
public:
    CBitMap()
    {
        CleanBitMap();
    }
public:
    void ReadBitMap(uint8_t* bitmap,uint32_t len);//从缓存读入BitMap , 可重复读入
    void AddOneCluster(bool isUsed);        //添加一个簇,被使用为真,没被使用为假
    void CleanBitMap();                     //清空BitMap

    bool isClusterUsed(uint64_t Cluster);  //如果簇Cluster已被使用,返回真,否则返回假

    CFragmentList GetFragmentList(bool Used);//获取碎片列表,Used为真,获取已使用簇列表,否则获取空闲簇列表

    //获取被占用簇块号
    bool GetFirstUsedCluster(uint64_t &offset);
    bool GetNextUsedCluster(uint64_t &offset);

    //获取空闲簇块号
    bool GetFirstFreeCluster(uint64_t &offset);
    bool GetNextFreeCluster(uint64_t &offset);

    vector<uint64_t>& GetList()
    {
        return m_list;
    }

    uint64_t GetFreeCluster()
    {
        return mull_FreeCluster;
    }

    uint64_t GetUsedCluster()
    {
        return mull_UsedCluster;
    }

private:
    bool mb_isUsed; //当前簇被占用为真
    uint64_t mull_FreeCluster;//空闲簇个数
    uint64_t mull_UsedCluster;//被占用簇个数
    vector<uint64_t>m_list;//此列表储存簇个数,第2n个项为被占用簇个数,2n+1的项为空闲簇个数

//用于遍历被占用簇和空闲簇
    uint64_t mull_FreeOffset;//空闲簇块偏移
    uint64_t mull_UsedOffset;//被占用簇块偏移
    uint32_t mul_nFree;//空闲簇块号
    uint32_t mul_nUsed;//被占用簇块
    uint32_t mul_iFree;//簇在空闲块内编号
    uint32_t mul_iUsed;//簇在被占用块内编号
};
