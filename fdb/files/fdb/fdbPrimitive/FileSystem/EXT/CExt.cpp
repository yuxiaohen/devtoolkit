#include "CExt.h"
#include "CExtFileReader.h"
#include "CExtFileScanner.h"
#include "ext_journal.h"

CFileScanner* CExt::newCFileScannerObject()
{
    return new CExtFileScanner(this);
}

CFileReader* CExt::newCFileReaderObject(SInfoSet* pInfoSet)
{
    CFileReader * pFileReader = new CExtFileReader(this);
    if(pFileReader!=NULL&&pInfoSet!=NULL)
    {
        if(!pFileReader->Config(pInfoSet))
        {
            delete pFileReader;
            pFileReader = NULL;
        }
    }
    return pFileReader;
}
