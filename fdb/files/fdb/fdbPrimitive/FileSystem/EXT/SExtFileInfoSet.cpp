#include "SExtFileInfoSet.h"

bool SExtFileInfoSet::AddInfo(SInfo* pInfo)
{
    if(pInfo!=NULL && pInfo->GetType()==CFileSystem::EXT)
    {
        this->m_InfoList.push_back(*(SExtFileInfo*)pInfo);
        return true;
    }
    return false;
}
uint32_t SExtFileInfoSet::ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize)
{
    uint32_t Total = 0;
    return Total;
}
uint32_t SExtFileInfoSet::GetType()
{
    return CFileSystem::EXT;
}
uint32_t SExtFileInfoSet::size()
{
    return this->m_InfoList.size();
}
SInfo* SExtFileInfoSet::at(uint32_t index)
{
    if(index>=this->m_InfoList.size())
    {
        return NULL;
    }
    return &this->m_InfoList.at(index);
}
void SExtFileInfoSet::clear()
{
    this->m_InfoList.clear();
}

