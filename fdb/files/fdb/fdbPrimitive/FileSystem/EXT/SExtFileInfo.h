#pragma once
#include "ext_diritem.h"
#include "ext_inode.h"
#include "ext4_inode.h"
#include "../SInfo.h"

#pragma pack(push)
#pragma pack(1)
struct SExtFileInfo:public SFileInfo
{
    struct
    {
        ext_diritem item;
        uint64_t Offset;
        uint64_t Size;
        uint32_t LastAccessTime;
        uint32_t InodeChangeTime;
        uint32_t LastChangeTime;
        uint32_t DeleteTime;
        uint32_t Flag;
        uint32_t ParentIndex;
        unsigned short Mode;
    }data;
    wstring filename;
    bool Set(ext_inode* ei)
    {
        if(ei==NULL)return false;
        this->data.LastAccessTime = ei->access_last_time;
        this->data.InodeChangeTime = ei->inode_change_time;
        this->data.LastChangeTime = ei->change_last_time;
        this->data.DeleteTime = ei->delete_time;
        this->data.Flag = ei->flag;
        this->data.Mode = ei->file_mode;
        this->data.Size = ei->total_words_hight32;
        this->data.Size =
                (this->data.Size<<32)
                | ei->total_words_low32;
        return true;
    }
    bool Set(ext4_inode* ei)
    {
        if(ei==NULL)return false;
        this->data.LastAccessTime = ei->i_atime;
        this->data.InodeChangeTime = ei->i_ctime;
        this->data.LastChangeTime = ei->i_mtime;
        this->data.DeleteTime = ei->i_dtime;
        this->data.Flag = ei->i_flags;
        this->data.Mode = ei->i_mode;
        this->data.Size = ei->i_size_high;
        this->data.Size = (this->data.Size<<32) | ei->i_size_lo;
        return true;
    }
    SExtFileInfo()
    {
        memset(&data,0,sizeof(data));
    }
    uint32_t GetInodeIndex()
    {
        return data.item.inode_index;
    }
    uint64_t GetOffset()
    {
        return data.Offset;
    }
    uint32_t GetType()
    {
        return CFileSystem::EXT;
    }
    uint32_t GetParentIndex()
    {
        return data.ParentIndex;
    }
    wstring& GetFileName()
    {
        return filename;
    }
    uint64_t GetFileSize()
    {
        return data.Size;
    }
    bool isDel()
    {
        return  data.DeleteTime!=0;
    }
    bool isDir()
    {
        return (data.Mode&0XF000)==0X4000;
    }
};
#pragma pack(pop)
typedef SExtFileInfo ext_fileinfo;
