#include "CExtFileScanner.h"

CExtFileScanner::CExtFileScanner(CExt* pExt):CFileScanner(pExt)
{
    Set(pExt);
}

int CExtFileScanner::Set(CExt* pExt)
{
    if(pExt!=NULL)
    {
        this->mp_Ext = pExt;
        return 1;
    }
    return 0;
}

uint32_t CExtFileScanner::ScanFile(uint32_t StartSector)
{
    if(this->mp_Ext!=NULL && this->mp_Ext->GetDisk()->Usable())
    {
        vector<ext_fileinfo> infolist;
        this->Start();
        ext_fileinfo info;
        info.data.item.inode_index = 0;
        if(this->mp_Ext->GetType()==CFileSystem::EXT2_3)
        {
            info.data.item.inode_index = 3;
        }
        if(this->mp_Ext->GetType()==CFileSystem::EXT4)
        {
            info.data.item.inode_index = 2;
        }
        info.filename = L"/";
        info.data.ParentIndex = 0;
        infolist.push_back(info);
        uint32_t TotalExtItem = 0;
        uint32_t CurrentItem = infolist.size();
        //层遍历目录树
        while(TotalExtItem<CurrentItem && this->isStart() )
        {
            if(infolist.at(TotalExtItem).filename==L"." || infolist.at(TotalExtItem).filename==L"..")
            {
                ++TotalExtItem;
                continue;
            }
            if(!this->mp_Ext->GetDirItemByExtFileInfo(&infolist.at(TotalExtItem),infolist))
            {
                break;
            }

            {
                int i;
                for(i=CurrentItem;i<infolist.size();++i)
                {
                    infolist.at(i).data.ParentIndex = TotalExtItem;
                }
            }
            ++TotalExtItem;
            CurrentItem = infolist.size();
        }
        {
            int i;
            for(i=0;i<this->size();++i)
            {
                if(this->at(i)->GetType()==CFileSystem::EXT)
                {
                    SExtFileInfoSet* pSet = (SExtFileInfoSet*)(this->at(i));
                    pSet->m_InfoList = infolist;
                }
            }
        }
        this->Stop();
        return TotalExtItem;
    }
    return 0;
}

uint32_t CExtFileScanner::ScanFreeCluster(uint32_t StartSector)
{
    if(this->mp_Ext!=NULL && this->mp_Ext->GetDisk()->Usable())
    {
        vector<ext_fileinfo> infolist;
        this->Start();
        if(this->mp_Ext->GetJournalFileInfo(infolist))
        {
            int i;
            for(i=0;i<this->size();++i)
            {
                if(this->at(i)->GetType()==CFileSystem::EXT)
                {
                    SExtFileInfoSet* pSet = (SExtFileInfoSet*)(this->at(i));
                    pSet->m_JournalList = infolist;
                }
            }
        }
        this->Stop();
    }
    return 0;
}

