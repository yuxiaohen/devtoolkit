#pragma once
#include "CExt.h"

class LIB_INTERFACE CExt4:public CExt
{
public:
    CExt4()
    {
        m_Vb = NULL;
        m_GroupInodeNum = 0;
        mp_Dirbuf = NULL;
        mul_DirbufSize = 0;
        m_JournalOffset = 0;
        m_JournalSize = 0;
        mp_JournalBuf = NULL;
    }
    ~CExt4()
    {
        if(m_Vb!=NULL)
        {
            delete[] m_Vb;
        }
        if(mp_Dirbuf!=NULL)
        {
            delete[] mp_Dirbuf;
        }
        if(mp_JournalBuf!=NULL)
        {
            delete[] mp_JournalBuf;
        }
    }
    uint32_t GetType()
    {
        return CFileSystem::EXT4;
    }
    int Config(uint64_t DbrOffset =0,const void * dbr=NULL);
    uint64_t GetInodeOffsetByIndex(uint32_t Index);
    bool GetInodeByOffset(uint64_t Offset,ext4_inode* out_inode);
    bool GetDirItemByExtFileInfo(ext_fileinfo *pInfo,
                                 vector<ext_fileinfo>&infolist);
    bool GetDirItemByInode(ext4_inode* in_inode,
                           vector<ext_fileinfo>&infolist);
    bool GetDirItemByInodeOffset(uint64_t Offset,
                                 vector<ext_fileinfo>&infolist);
    bool GetDirItemByInodeIndex(uint32_t Index,
                                vector<ext_fileinfo>&infolist);
    CFragmentList GetFragmentList(int inode_index);
    CFragmentList GetFragmentList(ext4_inode* in_inode);
    bool GetJournalFileInfo(vector<ext_fileinfo>&infolist);
    uint8_t *mp_JournalBuf;
private:
    void ReadFragmentByClusterNum(uint64_t cluster,CFragmentList& list);
    void Ext4ExtentResolve(ext4_extent* pee,CFragmentList &list);
private:
    ext_blockgroup * m_Vb;
    uint32_t m_GroupInodeNum;
    uint32_t m_JournalOffset;
    uint32_t m_JournalSize;


    //目录项缓存
    uint8_t *mp_Dirbuf;
    uint32_t mul_DirbufSize;
};
