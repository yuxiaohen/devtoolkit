#include "CExt4.h"

int CExt4::Config(uint64_t DbrOffset,const void* dbr)
{
    if(this->Usable())
    {
        m_GroupInodeNum = 0;
        this->SetPoint(0X400,CIO::START_POS);
        ext_superblock vs;
        if(this->Read(&vs,sizeof(vs))>0)
        {
            if(vs.signature!=0XEF53)return 0;
            if( !(vs.compatibility_read_flag&0X08) )return 0;
            //超级块
            long cluster =  (1<<vs.block_describe)*1024;
            this->SetClusterSize(cluster);
            m_GroupInodeNum = vs.group_inode_num;
            m_JournalOffset = *(long*)(((char*)&vs)+0X120);
            m_JournalSize = *(long*)(((char*)&vs)+0X14C);

            if(this->mp_JournalBuf!=NULL)
            {
                delete []this->mp_JournalBuf;
                this->mp_JournalBuf = NULL;
            }
            this->mp_JournalBuf = new uint8_t[m_JournalSize];
            this->SetPoint(m_JournalOffset*this->GetClusterSize(),CIO::START_POS);
            this->Read(this->mp_JournalBuf,this->m_JournalSize);

            //块组描述表
            if(this->m_Vb!=NULL)
            {
                delete this->m_Vb;
                this->m_Vb = NULL;
            }

            this->SetPoint(this->GetClusterSize(),CIO::START_POS);
            this->m_Vb = new ext_blockgroup[128];
            if(this->Read(this->m_Vb,128*sizeof(ext_blockgroup))>0)
            {
                return 1;
            }
        }
    }
    return 0;
}

bool CExt4::GetJournalFileInfo(vector<ext_fileinfo>&infolist)
{
    ext4_inode *pinode = (ext4_inode*)this->mp_JournalBuf;
    int ninode = this->m_JournalSize / sizeof(ext4_inode);
    int i;
    for(i=0;i<ninode;++i)
    {
        ext_fileinfo info;
        if( (pinode[i].i_mode&0XF000) == 0X8000)
        {
            info.Set(pinode+i);
            info.data.Offset = 0;
            info.data.item.inode_index = i;
            infolist.push_back(info);
        }
    }
    return true;
}


uint64_t CExt4::GetInodeOffsetByIndex(uint32_t Index)
{
    if(this->m_Vb!=NULL && this->m_GroupInodeNum!=0 && Index>0)
    {
        long ngroup = Index / this->m_GroupInodeNum;
        uint64_t inode_offset
                = this->m_Vb[ngroup].group_table_start
                *this->GetClusterSize()
                + (Index % this->m_GroupInodeNum - 1)
                * sizeof(ext4_inode);
        return inode_offset;
    }
    return 0;
}

bool CExt4::GetDirItemByExtFileInfo(ext_fileinfo *pInfo, vector<ext_fileinfo>&infolist)
{
    if(pInfo!=NULL && this->Usable())
    {
        ext4_inode ei;
        uint64_t InodeOffset = this->GetInodeOffsetByIndex(pInfo->GetInodeIndex());
        if(this->GetInodeByOffset(InodeOffset,&ei))
        {
            pInfo->data.Offset = InodeOffset;
            pInfo->Set(&ei);
            this->GetDirItemByInode(&ei,infolist);
            return true;
        }
    }
    return false;
}

bool CExt4::GetInodeByOffset(uint64_t Offset,ext4_inode* out_inode)
{
    if(out_inode==NULL || !this->Usable())return false;
    ext4_inode * inode_buf = new ext4_inode[16];//4096
    this->SetPoint(Offset/this->GetClusterSize()*this->GetClusterSize(),CIO::START_POS);
    if(this->Read(inode_buf,16*sizeof(ext4_inode))>0)
    {
        *out_inode = inode_buf[Offset%this->GetClusterSize()/sizeof(ext4_inode)];
        return true;
    }
    delete []inode_buf;
    return false;
}

bool CExt4::GetDirItemByInode(ext4_inode* in_inode,vector<ext_fileinfo>&infolist)
{
    if(in_inode->i_mode&0X4000)
    {
        uint32_t ClusterSize = this->GetClusterSize();
        if( this->mul_DirbufSize < in_inode->i_size_lo+512 )
        {
            this->mul_DirbufSize = in_inode->i_size_lo+512;
            if(this->mp_Dirbuf!=NULL)
            {
                delete[] mp_Dirbuf;
                mp_Dirbuf = NULL;
            }
            mp_Dirbuf = new uint8_t[this->mul_DirbufSize];
        }
        CFragmentList list = this->GetFragmentList(in_inode);

        CFragmentReader reader;
        reader.Set(this->GetDisk(),&list,this->GetClusterSize());
        reader.ReadSector(0,mp_Dirbuf,in_inode->i_size_lo/512+1);
        int current_index;
        string tmp;
        ext_fileinfo fi;
        tmp.resize(100);
        for(current_index = 0;current_index < in_inode->i_size_lo; current_index+=ClusterSize)
        {
            ext_diritem *pitem = (ext_diritem*)(mp_Dirbuf+current_index);
            do
            {
                fi.data.item = *pitem;
                tmp.assign(pitem->file_name,pitem->name_length);
                StringToWString(tmp,fi.filename);
                if(fi.filename.length()>0)
                    infolist.push_back(fi);
            }
            while(pitem = ext_diritem_get_next(pitem),(uint8_t*)pitem-mp_Dirbuf<in_inode->i_size_lo);
        }
        return true;
    }
    return false;
}

bool CExt4::GetDirItemByInodeOffset(uint64_t Offset,vector<ext_fileinfo>&infolist)
{
    if(!this->Usable())return false;
    bool Ret = false;
    ext4_inode * inode_buf = new ext4_inode[32];//4096
    this->SetPoint(Offset/this->GetClusterSize()*this->GetClusterSize(),CIO::START_POS);
    if(this->Read(inode_buf,32*sizeof(ext4_inode))>0)
    {
        ext4_inode * pinode = &inode_buf[Offset%this->GetClusterSize()/sizeof(ext4_inode)];
        Ret =  GetDirItemByInode(pinode,infolist);
    }
    delete []inode_buf;
    return Ret;
}

bool CExt4::GetDirItemByInodeIndex(uint32_t Index,vector<ext_fileinfo>&infolist)
{
    return GetDirItemByInodeOffset(GetInodeOffsetByIndex(Index),infolist);
}

void CExt4::ReadFragmentByClusterNum(uint64_t cluster,CFragmentList& list)
{
    if(cluster==0)return;
    uint32_t ClusterSize = this->GetClusterSize();
    unsigned long long Offset = cluster*(unsigned long long)ClusterSize;
    uint8_t *buf =  new uint8_t [ClusterSize];
    this->SetPoint(Offset,CIO::START_POS);
    {
        this->Read(buf,ClusterSize);
        this->Ext4ExtentResolve((ext4_extent*)buf,list);
    }
    delete []buf;
}

void CExt4::Ext4ExtentResolve(ext4_extent* pee,CFragmentList &list)
{
    ext4_extent_header *Header = (ext4_extent_header*)pee;
    if(Header->eh_magic!=EXT4_EXT_MAGIC){return;}
    if(Header->eh_depth==0)
    {
        unsigned long i = 0;
        ext4_extent *extent = pee+1;
        while( i<Header->eh_entries)
        {
            uint64_t Offset = extent[i].ee_start_hi;
            Offset = Offset<<32 | extent[i].ee_start_lo;
            unsigned long n = extent[i].ee_len;
            {
                list.AddFragment(Offset,n,n);
            }
            ++i;
        }
    }
    else
    {
        ext4_extent_idx *idx = (ext4_extent_idx*)(pee +1);
        int i = 0;
        while(i<Header->eh_entries)
        {
            uint64_t Offset = idx[i].ei_leaf_hi;
            Offset = Offset<<32 | idx[i].ei_leaf_lo;
            this->ReadFragmentByClusterNum(Offset,list);
            ++i;
        }
    }
}

CFragmentList CExt4::GetFragmentList(ext4_inode* in_inode)
{
    CFragmentList list;
    if(in_inode!=NULL)
    {
        this->Ext4ExtentResolve(in_inode->i_extent,list);
    }
    return list;
}

CFragmentList CExt4::GetFragmentList(int inode_index)
{
    CFragmentList list;
    ext4_inode vi;
    if(this->GetInodeByOffset(this->GetInodeOffsetByIndex(inode_index),&vi))
    {
        list = this->GetFragmentList(&vi);
    }
    return list;
}
