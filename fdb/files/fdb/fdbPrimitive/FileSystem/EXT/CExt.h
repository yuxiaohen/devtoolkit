#pragma once
#include "../CFileSystem.h"
#include "ext_superblock.h"
#include "ext_blockgroup.h"
#include "ext_inode.h"
#include "ext4_inode.h"
#include "SExtFileInfo.h"
#include "../CFragmentList.h"
#include "../CFragmentReader.h"
#include "../string2wstring.h"
#include <vector>
using std::vector;
#include <string>
using std::string;

class LIB_INTERFACE CExt:public CFileSystem
{
public:
    virtual bool GetDirItemByInodeIndex(uint32_t Index,
            vector<ext_fileinfo>&infolist)=0;
    virtual bool GetDirItemByExtFileInfo(ext_fileinfo *pInfo,
		    vector<ext_fileinfo>&infolist)=0;
    virtual bool GetJournalFileInfo(vector<ext_fileinfo>&infolist)=0;

    virtual CFragmentList GetFragmentList(int inode_index)=0;
     
    virtual CFileScanner* newCFileScannerObject();

    virtual CFileReader* newCFileReaderObject(SInfoSet* pInfoSet = NULL);
};
