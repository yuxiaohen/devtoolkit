#pragma once

#include "ext_type.h"

#pragma pack(push)
#pragma pack(1)

typedef struct ext_superblock
{
	ulong node_total;		//0x04
	ulong block_total;		//0x08
	ulong retain_block;		//0x0c
	ulong free_block;		//0x10
	ulong free_node_total;		//0x14
	ulong first_block;		//0x18
	ulong block_describe;		//0x1c
	ulong segment_describe;		//0x20
	ulong group_block_num;		//0x24
	ulong group_segment_num;	//0x28
	ulong group_inode_num;		
	ulong mount_last_time;		//0x2c
	ulong write_last_time;		//0x30
	ushort mount_number;		//0x32
	ushort mount_total_number;		//0x34
	ushort signature;		//0x36
	ushort file_system_state;		//0x38
	ushort error_solve;		//0x3a
	ushort second_version_number;		//0x3c
	ulong check_last_time;		//0x40
	ulong force_interval_time;	//0x44
	ulong operating_system;		//0x48
	ulong version_number;		//0x4c
	ushort user_id_block;		//0x50
	ushort group_id_block;		//0x54
	ulong first_node_abandom;		//0x58
	ushort node_size;		//0x5a
	ushort superblock_place;	//0x5c
	ulong compatibility_flag;		//0x60
	ulong incompatile_flag;		//0x64
	ulong compatibility_read_flag;		//0x68
	uchar uuid[16];		//0x78
	uchar folder_name[16];	//0x88
	uchar mount_path_last[64];	//0xc8
	ulong bitmap_arithmetic;	//oxcc
	uchar file_allot_block;		//0xcd
	uchar directory_allot_block;		//0xce
	ushort ukonwn;		//0xD0
	uchar journal_uuid[16];		//0xE0
	ulong journal_node;		//0xE4
	ulong journal_numner;		//0XE8
	ulong node_number_last;		//0xEE
	ulong hash_first;		//0xF0
	ulong hash_second;		//0XF4
	ulong hash_third;		//0xF8
	ulong hash_fourth;		//0xFE
	ulong hash_versions;		//0x100
	ulong mount_option;		//0x104
	ulong meta_block_frist;		//0x108
	ulong system_create_time;		//0x10E
	uchar journal_node_backup[68];		//0x150
	uchar fix[512-336];
}ext_superblock;
#pragma pack(pop)
