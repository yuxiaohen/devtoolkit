#pragma once

#include "ext_type.h"

#pragma pack(push)
#pragma pack(1)

typedef struct ext_blockgroup
{
	ulong block_group_start;
	ulong group_bitmap_start;
	ulong group_table_start;
	ushort group_free_num;
	ushort group_free_inum;
	ushort group_table_total;
	ushort fill;
	uchar unknown[12];
}ext_bockgroup;

#pragma pack(pop)
