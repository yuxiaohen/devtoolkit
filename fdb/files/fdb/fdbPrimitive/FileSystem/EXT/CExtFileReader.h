#pragma once

#include "CExt4.h"
#include "CExt2_3.h"
#include "../CFileReader.h"
#include "../CFragmentReader.h"
#include "SExtFileInfo.h"

class LIB_INTERFACE CExtFileReader:public CFileReader
{
public:
	CExtFileReader(CFileSystem* pFileSystem = NULL);
	virtual ~CExtFileReader(){}
    	bool SetFileSystem(CFileSystem* pFileSystem);
    	bool Config(SInfoSet *pInfoSet);
    	bool FromFile(SFileInfo* pInfo);
        uint32_t ReadBlock(uint32_t Index,uint8_t* OutBuf,uint32_t Size);
        uint32_t ReadNextBlock(uint8_t* OutBuf,uint32_t Size);

private:
	CFileSystem* mp_Ext;
	SExtFileInfo* mp_Info;
	CFragmentReader m_Reader;
    uint64_t mull_DataOffset;
    uint32_t mul_ClusterSize;
};
