#pragma once

#include "ext_type.h"

#pragma pack(push)
#pragma pack(1)
typedef struct ext_inode
{
	ushort file_mode;		//0x02
	ushort user_id_low16;		//0x04
	ulong total_words_low32;		//0x08
	ulong access_last_time;		//0x0c
	ulong inode_change_time;	//0x10
	ulong change_last_time;		//0x14
	ulong delete_time;		//0x18
	ushort group_id_low16;		//0x1A
	ushort link;		//0x1C
	ulong sectors;		//ox20
	ulong flag;		//0x24
	ulong unknown0;
	ulong first_block_pointer;		//0X28
	ulong second_block_pointer;		//0x2C
	ulong third_block_pointer;		//0x30
	ulong fourth_block_pointer;		//0x34
	ulong fifth_block_pointer;		//0x38
	ulong sixth_block_pointer;		//0x3c
	ulong seventh_block_pointer;		//0x40
	ulong eighth_block_pointer;		//0x44
	ulong ninth_block_pointer;		//0x48
	ulong tenth_block_pointer;		//0x4c
	ulong eleventh_block_pointer;		//0x50
	ulong twelfth_block_pointer;		//0x54
	ulong first_indirect_block_pointer;		//0x58
	ulong second_indirect_block_pointer;	//0x5c
	ulong third_indirect_block_pointer;		//0x60
	ulong generation_number;		//0x64
	ulong extended_attributes_block;		//0x68
	ulong total_words_hight32;		//0x6c
	ulong segment_address;		//0x70
	uchar segment_number;		//0x71
	uchar segement_size;		//0x72
	ushort unknown;		//0x74
	ushort user_id_hight16;		//0x76
	ushort group_id_hight16;	//0x78
	ulong unknown2;		//0x7c
}ext_inode;

#pragma pack(pop)
