#pragma once
typedef unsigned long ulong; 
typedef unsigned short ushort; 
typedef unsigned char uchar;
#include <stdio.h>
#pragma pack(push)
#pragma pack(1)
typedef struct ext_diritem
{
	ulong inode_index;
	ushort item_length;
	uchar name_length;
	uchar file_type;
	char file_name[1];
}ext_diritem;
#pragma pack(pop)

ext_diritem * ext_diritem_get_next(ext_diritem* pitem);
