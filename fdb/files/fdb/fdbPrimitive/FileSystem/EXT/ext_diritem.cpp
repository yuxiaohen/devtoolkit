﻿#include "ext_diritem.h"

ext_diritem * ext_diritem_get_next(ext_diritem* pitem)
{
	if(pitem!=NULL && pitem->inode_index!=0)
	{
		{
			return (ext_diritem*)((char*)pitem+pitem->item_length);
		}
	}
	return NULL;
}
