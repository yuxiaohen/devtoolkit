#include "CExtFileReader.h"
CExtFileReader::CExtFileReader(CFileSystem* pFileSystem)
{
    this->SetFileSystem(pFileSystem);
    this->mp_Info = NULL;
}

bool  CExtFileReader::Config(SInfoSet* pInfoSet)
{
    return true;
}

bool CExtFileReader::SetFileSystem(CFileSystem* pFileSystem)
{
    if(pFileSystem!=NULL && pFileSystem->GetDisk()->Usable()
            && (pFileSystem->GetType()==CFileSystem::EXT2_3
                || pFileSystem->GetType()==CFileSystem::EXT4)
            )
    {
        this->m_Reader.Close();
        this->mp_Info = NULL;
        this->mp_Ext = pFileSystem;
        return true;
    }
    return false;
}


bool  CExtFileReader::FromFile(SFileInfo* pInfo)
{
    if(this->mp_Ext!=NULL
            && pInfo!=NULL
            && (pInfo->GetType()==CFileSystem::EXT)
            )
    {
        this->m_Reader.Close();
        this->mp_Info = (SExtFileInfo*)pInfo;
        SExtFileInfo* pExtInfo = (SExtFileInfo*)pInfo;
        CExt *pExt = (CExt*)this->mp_Ext;
        CFragmentList list;
        if(pExtInfo->GetOffset()==0)
        {
            if(pExt->GetType()==CFileSystem::EXT2_3)
            {
                CExt2_3 *pExt2_3 = (CExt2_3*)pExt;
                ext_inode *pInode = (ext_inode*)pExt2_3->mp_JournalBuf;
                list = pExt2_3->GetFragmentList(&pInode[pExtInfo->GetInodeIndex()]);
            }
            else if(pExt->GetType()==CFileSystem::EXT4)
            {
                CExt4 *pExt4 = (CExt4*)pExt;
                ext4_inode *pInode = (ext4_inode*)pExt4->mp_JournalBuf;
                list = pExt4->GetFragmentList(&pInode[pExtInfo->GetInodeIndex()]);
            }
        }
        else
        {
            list = pExt->GetFragmentList(pExtInfo->GetInodeIndex());
        }
        uint32_t ClusterSize = this->mp_Ext->GetClusterSize();
        uint32_t SectorSize = this->mp_Ext->GetSectorSize();
        m_Reader.Set(this->mp_Ext->GetDisk(),&list,ClusterSize,0,SectorSize);
        return true;
    }
    return false;
}

uint32_t CExtFileReader::ReadBlock(uint32_t Index,uint8_t* OutBuf,uint32_t Size)
{
    if(this->mp_Info==NULL)return 0;

    return this->m_Reader.ReadSector(Index,OutBuf,Size);
}
uint32_t CExtFileReader::ReadNextBlock(uint8_t* OutBuf,uint32_t Size)
{
    if(this->mp_Info==NULL)return 0;

    return this->m_Reader.ReadNextSector(OutBuf,Size);
}
