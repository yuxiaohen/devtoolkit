﻿#include "CExt2_3.h"

int CExt2_3::Config(uint64_t DbrOffset,const void* dbr)
{
    if(this->Usable())
    {
        m_GroupInodeNum = 0;
        this->SetPoint(0X400,CIO::START_POS);
        ext_superblock vs;
        if(this->Read(&vs,sizeof(vs))>0)
        {
            if(vs.signature!=0XEF53)return 0;
            if(vs.compatibility_read_flag&0X08)return 0;
            //超级块
            long cluster =  (1<<vs.block_describe)*1024;
            this->SetClusterSize(cluster);
            m_GroupInodeNum = vs.group_inode_num;
            m_JournalOffset = *(long*)(((char*)&vs)+0X10C);
            m_JournalSize = *(long*)(((char*)&vs)+0X14C);

            if(this->mp_JournalBuf!=NULL)
            {
                delete []this->mp_JournalBuf;
                this->mp_JournalBuf = NULL;
            }
            this->mp_JournalBuf = new uint8_t[this->m_JournalSize];
            this->SetPoint(m_JournalOffset*this->GetClusterSize(),CIO::START_POS);
            this->Read(this->mp_JournalBuf,this->m_JournalSize);

            //块组描述表
            if(this->m_Vb!=NULL)
            {
                delete this->m_Vb;
                this->m_Vb = NULL;
            }

            this->SetPoint(this->GetClusterSize(),CIO::START_POS);
            this->m_Vb = new ext_blockgroup[128];
            if(this->Read(this->m_Vb,128*sizeof(ext_blockgroup))>0)
            {
                return 1;
            }
        }
    }
    return 0;
}

bool CExt2_3::GetJournalFileInfo(vector<ext_fileinfo>&infolist)
{
    ext_inode *pinode = (ext_inode*)this->mp_JournalBuf;
    int ninode = this->m_JournalSize / sizeof(ext_inode);
    int i;
    for(i=0;i<ninode;++i)
    {
        ext_fileinfo info;
        if( (pinode[i].file_mode&0XF000) == 0X8000)
        {
            info.Set(pinode+1);
            info.data.Offset = 0;
            info.data.item.inode_index = i;
            infolist.push_back(info);
        }
    }
    return true;
}

uint64_t CExt2_3::GetInodeOffsetByIndex(uint32_t Index)
{
    if(this->m_Vb!=NULL && this->m_GroupInodeNum!=0 && Index>0)
    {
        long ngroup = Index / this->m_GroupInodeNum;
        uint64_t inode_offset
                = this->m_Vb[ngroup].group_table_start
                *this->GetClusterSize()
                + (Index % this->m_GroupInodeNum - 1)
                *sizeof(ext_inode);
        return inode_offset;
    }
    return 0;
}

bool CExt2_3::GetDirItemByExtFileInfo(ext_fileinfo *pInfo, vector<ext_fileinfo>&infolist)
{
    if(pInfo!=NULL && this->Usable())
    {
        ext_inode ei;
        uint64_t InodeOffset = this->GetInodeOffsetByIndex(pInfo->GetInodeIndex());
        if(this->GetInodeByOffset(InodeOffset,&ei))
        {
            pInfo->data.Offset = InodeOffset;
            pInfo->Set(&ei);
            this->GetDirItemByInode(&ei,infolist);
            return true;
        }
    }
    return false;
}

bool CExt2_3::GetInodeByOffset(uint64_t Offset,ext_inode* out_inode)
{
    if(out_inode==NULL || !this->Usable())return false;
    ext_inode * inode_buf = new ext_inode[32];//4096
    this->SetPoint(Offset/this->GetClusterSize()*this->GetClusterSize(),CIO::START_POS);
    if(this->Read(inode_buf,32*sizeof(ext_inode))>0)
    {
        *out_inode = inode_buf[Offset%this->GetClusterSize()/sizeof(ext_inode)];
        return true;
    }
    delete []inode_buf;
    return false;
}

bool CExt2_3::GetDirItemByInode(ext_inode* in_inode,vector<ext_fileinfo>&infolist)
{
    if(in_inode->file_mode&0X4000)
    {
        uint32_t ClusterSize = this->GetClusterSize();
        if(this->mul_DirbufSize<in_inode->total_words_low32+512)
        {
            this->mul_DirbufSize = in_inode->total_words_low32+512;
            if(this->mp_Dirbuf!=NULL)
            {
                delete[] mp_Dirbuf;
                mp_Dirbuf = NULL;
            }
            mp_Dirbuf = new uint8_t[this->mul_DirbufSize];
        }
        CFragmentList list = this->GetFragmentList(in_inode);

        CFragmentReader reader;
        reader.Set(this->GetDisk(),&list,this->GetClusterSize());
        reader.ReadSector(0,mp_Dirbuf,in_inode->total_words_low32/512+1);
        int current_index;
        string tmp;
        ext_fileinfo fi;
        tmp.resize(100);
        for(current_index = 0;current_index < in_inode->total_words_low32; current_index+=ClusterSize)
        {
            ext_diritem *pitem = (ext_diritem*)(mp_Dirbuf+current_index);
            do
            {
                fi.data.item = *pitem;
                tmp.assign(pitem->file_name,pitem->name_length);
                StringToWString(tmp,fi.filename);
                if(fi.filename.length()>0)
                    infolist.push_back(fi);
            }
            while(pitem = ext_diritem_get_next(pitem),(uint8_t*)pitem-mp_Dirbuf<in_inode->total_words_low32);
        }
        return true;
    }
    return false;
}

bool CExt2_3::GetDirItemByInodeOffset(uint64_t Offset,vector<ext_fileinfo>&infolist)
{
    if(!this->Usable())return false;
    bool Ret = false;
    ext_inode * inode_buf = new ext_inode[32];//4096
    this->SetPoint(Offset/this->GetClusterSize()*this->GetClusterSize(),CIO::START_POS);
    if(this->Read(inode_buf,32*sizeof(ext_inode))>0)
    {
        ext_inode * pinode = &inode_buf[Offset%this->GetClusterSize()/sizeof(ext_inode)];
        Ret = GetDirItemByInode(pinode,infolist);
    }
    delete []inode_buf;
    return Ret;
}

bool CExt2_3::GetDirItemByInodeIndex(uint32_t Index,vector<ext_fileinfo>&infolist)
{
    return GetDirItemByInodeOffset(GetInodeOffsetByIndex(Index),infolist);
}

void CExt2_3::ReadFragmentByClusterNum(int cluster,CFragmentList& list, int level)
{
    if(cluster==0)return;
    if(level==1)
    {
        ReadFragmentByClusterNum(cluster,list);
    }
    else
    {
        unsigned long long Offset = cluster*(unsigned long long)this->GetClusterSize();

        this->SetPoint(Offset,CIO::START_POS);
        {
            unsigned long buf_len = this->GetClusterSize()/sizeof(unsigned long);
            unsigned long *ClusterList =  new unsigned long [buf_len];
            this->Read(ClusterList,this->GetClusterSize());
            int i;
            for(i=0;i<buf_len;++i)
            {
                if(ClusterList[i]==0)break;
                this->ReadFragmentByClusterNum(ClusterList[i],list,level-1);
            }
            delete []ClusterList;
        }
    }
}

void CExt2_3::ReadFragmentByClusterNum(int cluster,CFragmentList& list)
{
    if(cluster==0)return;
    unsigned long long Offset = cluster*(unsigned long long)this->GetClusterSize();
    unsigned long buf_len = this->GetClusterSize()/sizeof(unsigned long);
    unsigned long *ClusterList =  new unsigned long [buf_len];
    this->SetPoint(Offset,CIO::START_POS);
    {
        this->Read(ClusterList,this->GetClusterSize());
        int i = 1;
        while(1)
        {
            unsigned long FirstCluster =  ClusterList[i-1];
            unsigned long n = 1;
            while( (ClusterList[i-1]) == (ClusterList[i])-1 && i<=buf_len)
            {
                ++i;
                ++n;
            }

            if(FirstCluster!=0)
            {
                list.AddFragment(FirstCluster,n,n);
            }
            ++i;
            if((ClusterList[i])==0 || i>buf_len)
            {
                break;
            }
        }
    }
    delete []ClusterList;
}

CFragmentList CExt2_3::GetFragmentList(ext_inode* in_inode)
{
    CFragmentList list;
    if(in_inode!=NULL)
    {
        unsigned long *ClusterList = NULL;
        ClusterList = (unsigned long*)&in_inode->first_block_pointer;

        int i = 1;
        while(i < 12)
        {
            unsigned long FirstCluster =  ClusterList[i-1];
            unsigned long n = 1;
            while( (ClusterList[i-1]) == (ClusterList[i])-1 && i < 12)
            {
                ++i;
                ++n;
            }

            if(FirstCluster!=0)
            {
                list.AddFragment(FirstCluster,n,n);
            }
            ++i;
        }
        this->ReadFragmentByClusterNum(in_inode->first_indirect_block_pointer,list,1);
        this->ReadFragmentByClusterNum(in_inode->second_indirect_block_pointer,list,2);
        this->ReadFragmentByClusterNum(in_inode->third_indirect_block_pointer,list,3);
    }
    return list;
}

CFragmentList CExt2_3::GetFragmentList(int inode_index)
{
    CFragmentList list;
    ext_inode vi;
    if(this->GetInodeByOffset(this->GetInodeOffsetByIndex(inode_index),&vi))
    {
        list = this->GetFragmentList(&vi);
    }
    return list;
}
