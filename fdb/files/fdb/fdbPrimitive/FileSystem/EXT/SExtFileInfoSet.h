#pragma once

#include "../SInfoSet.h"
#include "SExtFileInfo.h"

#include <vector>
using std::vector;

struct LIB_INTERFACE SExtFileInfoSet:public SInfoSet
{
public:
    SExtFileInfoSet(){};
    bool AddInfo(SInfo* pInfo);
    uint32_t ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize);

    uint32_t size();
    SInfo* at(uint32_t index);
    void clear();
    uint32_t GetType();
	
    vector<SExtFileInfo> m_InfoList;
    vector<SExtFileInfo> m_JournalList;
};
