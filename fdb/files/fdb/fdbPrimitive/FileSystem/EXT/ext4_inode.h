#pragma once

#define EXT4_NDIR_BLOCKS                12
#define EXT4_IND_BLOCK                  EXT4_NDIR_BLOCKS
#define EXT4_DIND_BLOCK                 (EXT4_IND_BLOCK + 1)
#define EXT4_TIND_BLOCK                 (EXT4_DIND_BLOCK + 1)
#define EXT4_N_BLOCKS                   (EXT4_TIND_BLOCK + 1)


typedef unsigned short __le16;
typedef unsigned long  __le32;
typedef unsigned short __u16;
typedef unsigned long  __u32;

#pragma pack(push)
#pragma pack(1)

#define EXT4_EXT_MAGIC		(0xf30a)
typedef struct ext4_extent_header {
    __le16	eh_magic;	/* probably will support different formats */
    __le16	eh_entries;	/* number of valid entries */
    __le16	eh_max;		/* capacity of store in entries */
    __le16	eh_depth;	/* has tree real underlying blocks? */
    __le32	eh_generation;	/* generation of the tree */
}ext4_extent_header;

struct ext4_extent_idx {
    __le32	ei_block;	/* index covers logical blocks from 'block' */
    __le32	ei_leaf_lo;	/* pointer to the physical block of the next *
                 * level. leaf or next index could be there */
    __le16	ei_leaf_hi;	/* high 16 bits of physical block */
    __u16	ei_unused;
};

typedef struct ext4_extent
{
	__le32	ee_block;	/* first logical block extent covers */
	__le16	ee_len;		/* number of blocks covered by extent */
	__le16	ee_start_hi;	/* high 16 bits of physical block */
	__le32	ee_start_lo;	/* low 32 bits of physical block */
}ext4_extent;

typedef struct ext4_inode 
{
         __le16  i_mode;         /* File mode */
         __le16  i_uid;          /* Low 16 bits of Owner Uid */
         __le32  i_size_lo;      /* Size in bytes */
         __le32  i_atime;        /* Access time */
         __le32  i_ctime;        /* Inode Change time */
         __le32  i_mtime;        /* Modification time */
         __le32  i_dtime;        /* Deletion Time */
         __le16  i_gid;          /* Low 16 bits of Group Id */
         __le16  i_links_count;  /* Links count */
         __le32  i_blocks_lo;    /* Blocks count */
         __le32  i_flags;        /* File flags */
         union {
                 struct {
                         __le32  l_i_version;
                 } linux1;
                 struct {
                         __u32  h_i_translator;
                 } hurd1;
                 struct {
                         __u32  m_i_reserved1;
                 } masix1;
         } osd1;                         /* OS dependent 1 */
         ext4_extent  i_extent[5];/* Pointers to blocks */
         __le32  i_generation;   /* File version (for NFS) */
         __le32  i_file_acl_lo;  /* File ACL */
         __le32  i_size_high;
         __le32  i_obso_faddr;   /* Obsoleted fragment address */
         union {
                 struct {
                         __le16  l_i_blocks_high; /* were l_i_reserved1 */
                         __le16  l_i_file_acl_high;
                         __le16  l_i_uid_high;   /* these 2 fields */
                         __le16  l_i_gid_high;   /* were reserved2[0] */
                         __le16  l_i_checksum_lo;/* crc32c(uuid+inum+inode) LE */
                         __le16  l_i_reserved;
                 } linux2;
                 struct {
                         __le16  h_i_reserved1;  /* Obsoleted fragment number/size which are removed in ext4 */
                         __u16   h_i_mode_high;
                         __u16   h_i_uid_high;
                         __u16   h_i_gid_high;
                         __u32   h_i_author;
                 } hurd2;
                 struct {
                         __le16  h_i_reserved1;  /* Obsoleted fragment number/size which are removed in ext4 */
                         __le16  m_i_file_acl_high;
                         __u32   m_i_reserved2[2];
                 } masix2;
         } osd2;                         /* OS dependent 2 */
         __le16  i_extra_isize;
         __le16  i_checksum_hi;  /* crc32c(uuid+inum+inode) BE */
         __le32  i_ctime_extra;  /* extra Change time      (nsec << 2 | epoch) */
         __le32  i_mtime_extra;  /* extra Modification time(nsec << 2 | epoch) */
         __le32  i_atime_extra;  /* extra Access time      (nsec << 2 | epoch) */
         __le32  i_crtime;       /* File Creation time */
         __le32  i_crtime_extra; /* extra FileCreationtime (nsec << 2 | epoch) */
         __le32  i_version_hi;   /* high 32 bits for 64-bit version */
	 char 	 nouse[100];
}ext4_inode;

#pragma pack(pop)
