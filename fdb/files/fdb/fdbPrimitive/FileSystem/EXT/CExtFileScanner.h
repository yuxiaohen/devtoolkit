#pragma once

#include "../CFileScanner.h"
#include "CExt2_3.h"
#include "SExtFileInfoSet.h"

class LIB_INTERFACE CExtFileScanner:public CFileScanner
{
public:
    CExtFileScanner(CExt* pExt = NULL);
    int Set(CExt* pNtfs);
    uint32_t ScanFile(uint32_t StartSector);
    uint32_t ScanFreeCluster(uint32_t StartSector);
private:
    CExt* mp_Ext;
};


