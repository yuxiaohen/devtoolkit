#pragma once
#include <string>
#include <windows.h>

bool StringToWString(const std::string &str,std::wstring &wstr);

bool WStringToString(const std::wstring &wstr,std::string &str);

