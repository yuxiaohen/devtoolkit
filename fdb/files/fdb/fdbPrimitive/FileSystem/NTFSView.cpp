#include "FileSystem.h"
#include "locale.h"
int main(int argc, char** argv)
{
	setlocale(LC_ALL,"");
	if(argc<=1)return 0;
	CNtfs a;
	if(a.Open(argv[1])>0 && a.Config())
	{
		CFileScanner * s = a.newCFileScannerObject();
		SNtfsFileInfoSet NtfsSet;
		s->AddInfoSet(&NtfsSet);
		if(s->ScanFile(0))
		{
			int i;
			for(i=0;i<NtfsSet.size();++i)
			{
                SFileInfo* ss = (SFileInfo*)NtfsSet.at(i);
                wprintf(L"%s : %d\n",ss->GetFileName().c_str(),ss->GetParentIndex());
			}
		}
		delete s;
	}
	return 0;
}
