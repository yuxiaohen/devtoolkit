﻿#include "CFragmentReader.h"
CFragmentReader::CFragmentReader()
{
    mp_Disk = NULL;
    mp_DecryptBuf = NULL;
    mul_DecryptBufSize = 0;
}

CFragmentReader::~CFragmentReader()
{
    if(mp_DecryptBuf!=NULL)
    {
        free(mp_DecryptBuf);
    }
}

bool CFragmentReader::Set(CIO *pDisk, CFragmentList* pRunList,uint32_t uint8_tPerCluster,uint64_t ClusterStartOffset,uint32_t SectorSize)
{
    if(pDisk==NULL || pRunList==NULL || SectorSize ==0 || uint8_tPerCluster ==0 )
    {
        return false;
    }

    mp_Disk = pDisk;
    mul_DecryptFragNum = -1;
    mul_uint8_tPerCluster = uint8_tPerCluster;
    mul_SectorSize = SectorSize;
    m_runlist = *pRunList;
    mull_ClusterStartOffset = ClusterStartOffset;

    return true;
}

void CFragmentReader::Close()
{
    this->mp_Disk = NULL;
}

uint32_t CFragmentReader::ReadSector(uint32_t SectorIndex,uint8_t* outputbuf,uint32_t n)
{
    if(outputbuf!=NULL && mp_Disk!=NULL && mp_Disk->Usable() && n > 0)
    {
        uint32_t TotalSector = m_runlist.GetTotalCluster()*mul_uint8_tPerCluster/mul_SectorSize;
        if(SectorIndex < TotalSector)//判断有无超出总扇区数
        {
            mul_CurFragIndex = 0;
            mul_CurSectorIndexInFrag = SectorIndex;

            //当前碎片扇区总数
            uint32_t CurFragSectorNum = m_runlist.GetDecompressNum(mul_CurFragIndex)*mul_uint8_tPerCluster/mul_SectorSize;

            while(mul_CurSectorIndexInFrag>=CurFragSectorNum )//如果超出,跳转到下一个碎片
            {
                mul_CurSectorIndexInFrag-=CurFragSectorNum;
                ++mul_CurFragIndex;
                if(mul_CurFragIndex>=m_runlist.GetTotalFrag())//若碎片查找完毕,退出
                {
                    return 0;
                }

                //计算下一个碎片的扇区总数
                CurFragSectorNum = m_runlist.GetDecompressNum(mul_CurFragIndex)*mul_uint8_tPerCluster/mul_SectorSize;
            }
            return ReadNextSector(outputbuf,n);
        }
    }
    return 0;
}

uint32_t CFragmentReader::ReadNextSector(uint8_t* outputbuf,uint32_t n)
{
    if(outputbuf!=NULL && mp_Disk!=NULL && mp_Disk->Usable() && n > 0)
    {
        //判断碎片序号是否在总碎片范围内
        if(mul_CurFragIndex < m_runlist.GetTotalFrag())
        {
            uint32_t nReadSector = 0;
            while(1)
            {
                uint32_t Ret = ReadSectorInFrag(mul_CurFragIndex,mul_CurSectorIndexInFrag,
                                       outputbuf+nReadSector*mul_SectorSize,n-nReadSector);
                if(Ret==0||Ret==(uint32_t)-1)
                {
                    return Ret;
                }
                nReadSector += Ret;
                mul_CurSectorIndexInFrag+=Ret;

                //当前碎片扇区总数
                uint32_t CurFragSectorNum = m_runlist.GetDecompressNum(mul_CurFragIndex)*mul_uint8_tPerCluster/mul_SectorSize;

                if(mul_CurSectorIndexInFrag >= CurFragSectorNum)//若当前碎片读取完毕,切换到下一个碎片
                {
                    ++mul_CurFragIndex;
                    mul_CurSectorIndexInFrag = 0;
                }

                if(nReadSector >= n)//全部读取完毕
                {
                    break;
                }

                if(mul_CurFragIndex>=m_runlist.GetTotalFrag())//读取到末尾
                {
                    break;
                }
            }
            return nReadSector;
        }
    }
    return 0;
}

uint32_t CFragmentReader::ReadSectorInFrag(uint32_t FragIndex,uint32_t SectorIndexInFrag,uint8_t* outputbuf,uint32_t n)
{
    if(outputbuf!=NULL && mp_Disk!=NULL && mp_Disk->Usable() && n > 0)
    {
        //判断碎片序号是否在碎片范围内
        if(FragIndex<m_runlist.GetTotalFrag())
        {
            uint64_t Offset = m_runlist.GetOffset(FragIndex);//当前碎片簇序号
            uint64_t ClusterSize = m_runlist.GetClusterNum(FragIndex);//簇个数
            uint64_t DecompressSize = m_runlist.GetDecompressNum(FragIndex);//簇解压后个数

            //当前碎片总共扇区个数
            uint64_t TotalSector = DecompressSize * mul_uint8_tPerCluster / mul_SectorSize;

            if(SectorIndexInFrag<TotalSector)
            {
                uint32_t TotalRead = n;
                if(SectorIndexInFrag + TotalRead > TotalSector)
                {
                    TotalRead = TotalSector - SectorIndexInFrag;
                }

                uint32_t Ret=0;
                if(ClusterSize == DecompressSize)//未被压缩
                {
                    mp_Disk->SetPoint(mull_ClusterStartOffset + Offset*mul_uint8_tPerCluster+SectorIndexInFrag*mul_SectorSize,CIO::START_POS);
                    Ret = mp_Disk->Read(outputbuf,TotalRead*mul_SectorSize);
                    if(Ret==0 || Ret==(uint32_t)-1)
                    {
                        return Ret;
                    }
                }
                else
                {
                    if(FragIndex!=mul_DecryptFragNum)
                    {
                        mul_DecryptBufSize = (uint32_t)DecompressSize*mul_uint8_tPerCluster;
                        if(mp_DecryptBuf==NULL)
                        {
                            mp_DecryptBuf = (uint8_t*)malloc(sizeof(uint8_t)*mul_DecryptBufSize);
                        }
                        else
                        {
                            mp_DecryptBuf = (uint8_t*)realloc(mp_DecryptBuf,sizeof(uint8_t)*mul_DecryptBufSize);
                        }
                        if(mp_DecryptBuf==NULL)exit(1);

                        uint8_t* buf = (uint8_t*)malloc(sizeof(uint8_t)*ClusterSize*mul_uint8_tPerCluster);
                        if(buf==NULL)exit(1);

                        //读取整个被压缩碎片
                        mp_Disk->SetPoint(mull_ClusterStartOffset + Offset*mul_uint8_tPerCluster,CIO::START_POS);
                        Ret = mp_Disk->Read(buf,ClusterSize*mul_uint8_tPerCluster);

                        if(Ret==0)
                        {
                            free(buf);
                            return 0;
                        }
                        Ret = CFragmentList::Decompress(mp_DecryptBuf,mul_DecryptBufSize,buf,ClusterSize*mul_uint8_tPerCluster);
                        free(buf);
                        if(Ret==0 || Ret==(uint32_t)-1)
                        {
                            ;//return 0;
                        }
                        mul_DecryptFragNum = FragIndex;
                    }

                    {
                        memcpy(outputbuf,mp_DecryptBuf+SectorIndexInFrag*mul_SectorSize,TotalRead*mul_SectorSize);
                    }
                }

                return TotalRead;
            }
        }
    }
    return -1;
}

uint32_t CFragmentReader::GetTotalSector()
{
    uint32_t Total = 0;
    {
        Total = m_runlist.GetTotalCluster()*mul_uint8_tPerCluster/mul_SectorSize;
    }
    return Total;
}
