#include "FileSystem.h"

int main(int argc, char ** argv)
{
    if(argc<5)return 0;
    setlocale(LC_ALL,"");

    CMft cmft;
    MFT mft;
    CDisk disk;
    CNtfs nd;
    DWORDLONG dl = 0;
    DWORDLONG mftValue;
    DWORDLONG BaseOffset = 0;
    int Ret;
    if(argv[1][0]!='-')return 0;

    if(sscanf(argv[3],"%I64d",&mftValue)<=0)return 0;
    Ret = disk.Open(argv[2]);
    sscanf(argv[4],"%I64X",&BaseOffset);


    Ret = nd.Set(&disk,BaseOffset);
    Ret = nd.Config();
    if(!Ret)return 0;
    printf("BytePerCluster:%d\nMftOffset:%X\n",nd.GetClusterSize(),nd.GetMftoffset());
    wprintf(L"VolumeName:%s\n",nd.GetVolumeName().c_str());


    CBitMap bm;
    Ret = nd.GetBitMap(&bm);

     if(0)
    {
        CFragmentList a = bm.GetFragmentList(false);
        DWORD i;
        for(i=0;i<a.GetTotalFrag();++i)
        {
            printf("%I64X,%I64d\n",a.GetOffset(i),a.GetClusterNum(i));
        }
        return 0;
    }

    if(0)
    {

        DWORDLONG dl;
        Ret = bm.GetFirstFreeCluster(dl);
        while(Ret)
        {
            printf("used: %I64d\n",dl);
            Ret = bm.GetNextFreeCluster(dl);
        }
        return 0;
    }


    Ret = nd.GetMftOffsetByIndex(mftValue,dl);

    disk.SetPoint(dl,1);
    //Ret = disk.Read(&mft,sizeof(mft));
    //printf("%d\n",Ret);

    printf("Offset:%I64X\n",dl);



    if(Ret==0)return 0;
    Ret = cmft.SetMftByOffset(&nd,dl);
    //printf("%d\n",Ret);

    //printf("%d\n",Ret);
    if(!Ret)return 0;


    if(argv[1][1]=='v')
    {
        {
            DWORD len;
            wprintf(L"%c%s\n",(cmft.isDir()?'+':'-'),cmft.GetPath().c_str());
            //wchar_t *name = cmft.FileName(len,3);
            //wprintf(L"%c%.*s <%I64X,%d>\n",(cmft.isDir()?'+':'-'),len,name,dl,(DWORD)mftValue);
        }
        CMftDir cmftdir;
        Ret = cmftdir.Set(&cmft);
        Ret = cmftdir.GetFirstChildMftIndex(dl,1|2);
        while(Ret)
        {
            {
                if( !(mftValue==(DWORD)dl || 0==(DWORD)dl || 5==(DWORD)dl)  )
                {
                    CMft cmft2;
		    cmft2.SetMftByIndex(&nd,(DWORD)dl);

                    DWORDLONG offset;
                    nd.GetMftOffsetByIndex((DWORD)dl,offset);

                    /*
                    disk.SetPoint(offset,1);
                    disk.Read(&mft,sizeof(mft));
                    cmft2.Set(4096,mft,&disk);
                    */

                    DWORD len;
                    //wchar_t * name = cmft2.FileName(len,3);
                    wprintf(L"|%c%s <%I64X,%d> Size:%I64d\n",(cmft2.isDir()?'+':'-'),cmft2.GetFileName().c_str(),offset,(DWORD)dl,cmft2.GetFileSize());
                }
                Ret = cmftdir.GetNextChildMftIndex(dl,1|2);
            }

        }
    }
    else if(argv[1][1]=='c')
    {
         CFragmentList plist = cmft.GetFragmentList();
         DWORD n = plist.GetTotalFrag();
         DWORD i;
         for(i=0;i<n;++i)
         {
             printf("%5d: offset:0X%010I64X, clusternum:%8I64d, Decompress: %8I64d\n",i+1,plist.GetOffset(i),plist.GetClusterNum(i),plist.GetDecompressNum(i));
         }
         if(n==0)
         {
             printf("No RunList\n");
         }
    }
    else if(argv[1][1]=='l')
    {
        int Ret = 0;
        DWORDLONG i = mftValue;
        //DWORDLONG n = nd.GetTotalMFT();
        do
        {
            //if(cmft.isDel() || cmft.GetFileName().find(L".chk",0)!=-1)
            wprintf(L"%I64X,%I64d %s\n",dl,i,cmft.GetPath().c_str());
            ++i;
            Ret = nd.GetMftOffsetByIndex(i,dl);
            cmft.SetMftByOffset(&nd,dl);
        }
        while(Ret);
    }
    else if(argv[1][1]=='d' && argc>=6)
    {
        remove(argv[5]);
        CFileOperate fo;
        if(fo.Create(argv[5])<=0)return 0;
        DWORDLONG Ret = 0;

        CFragmentList list = cmft.GetFragmentList();
        CFragmentReader nr;
        nr.Set(&disk,&list,nd.GetClusterSize());
        DWORD TotalSector = nr.GetTotalSector();
        DWORDLONG TotalSize = cmft.GetFileSize();
        printf("TotalSector:%d,Size %I64d\n",TotalSector,TotalSize);

        BYTE *buf =  new BYTE[(unsigned int)512*1024];

        Ret = nr.ReadSector(0,buf,1024);
        while((Ret!=-1 && Ret>0) && TotalSize > 0)
        {
            putchar('#');
            {
                DWORD nWrite = 512*1024;
                if(nWrite>TotalSize)
                {
                    nWrite = (DWORD)TotalSize;
                }
                TotalSize -= nWrite;
                fo.Write(buf,nWrite);
            }
            Ret =nr.ReadNextSector(buf,1024);
        }
        delete[] buf;
        if(TotalSize>0)
            printf("\nError!!\n");
        else
        printf("\nComplete!!\n");
/*
        if(Ret>0 && Ret!=-1)
            printf("\nTotal %I64d,Dump File Success\n",Ret);
        else
            printf("\nDump File Error\n");
*/
    }
    return 0;
}
