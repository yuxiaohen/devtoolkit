#pragma once

#include "sys_func.h"

#define SECTOR_SIZE 512

#define DBR_SIZE 512

#define HEAD_SECTOR_USABLE(x) (((uint8_t*)(x))[510]==0X55 && ((uint8_t*)(x))[511]==0XAA) //判定MagicNum是否正确

// DPT Struct
typedef struct DPT
{
    uint8_t State; //分区状态，0=未激活，0x80=激活（注意此项）；
    uint8_t StartHead; //分区起始磁头号；
    uint16_t StartSC; //分区起始扇区和柱面号，底字节的底6位为扇区号，高2位为柱面号的第9，10位，高字节为柱面号的低8位；
    uint8_t Type;  //分区类型，如0x0B=FAT32，0x83=Linux等，00表示此项未用；
    uint8_t EndHead; //分区结束磁头号；
    uint16_t EndSC;//分区结束扇区和柱面号，定义同前；
    uint32_t Relative;//在线性寻址方式下的分区相对扇区地址（对于基本分区即为绝对地址）
    uint32_t Sectors;//分区大小（总扇区数）。
}DPT;
// DPT Struct End

// MBR Struct

typedef struct MBR
{
    uint8_t mbr_code[446];
    DPT dpt[4];
    uint16_t magic_num;
}MBR;

typedef MBR EBR;//EBR的结构和MBR一样的

//MBR End
