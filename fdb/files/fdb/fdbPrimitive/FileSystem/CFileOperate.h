﻿#pragma once

#include <fdb/fdb.h>
//不支持多线程

class LIB_INTERFACE CFileOperate : public CIO
{
public:
    CFileOperate(FILE_HANDLE fh = INVALID_FILEHANDLE_VALUE,uint64_t Offset = 0);
    virtual ~CFileOperate();
    CFileOperate(CFileOperate& disk);
    CFileOperate& operator=(CFileOperate& disk);

    int Set(CFileOperate *disk);//通过另一个CFileOperate对象设置本身
    int Set(CFileOperate *disk,uint64_t BaseOffset);//通过另一个CFileOperate对象设置本身,同时设置基础偏移量

public:
    int Open(const char* filename,bool ReadOnly=true, bool OpenAlways=false); //打开

    void Close(); //关闭

    //注意 当直接打开磁盘时候 读写字节len 需要为512的整数倍,否则会报错
    int Read(void* buf, uint32_t len);
    int Write(const void* buf, uint32_t len);

    uint64_t GetFileSize();//获取文件大小

    uint64_t SetPoint(uint64_t offset,int mode);//设置读取指针,他的返回值不可做为是否设置成功的依据!!!!

    void SetBaseOffset(uint64_t offset)//设置基础偏移量 (在磁盘句柄中读取分区数据)
    {
        mull_BaseOffset  = offset;
    }

    uint64_t GetBaseOffset()//获取当前的基础偏移量
    {
        return mull_BaseOffset;
    }

    FILE_HANDLE GetFileHandle()//直接获取文件句柄
    {
        return m_fh;
    }

    bool Usable()//当文件可读时返回真
    {
        return !INVALID_FILEHANDLE(m_fh);
    }

private:
    uint32_t *mp_fhQuoteCount;//文件句柄引用计数
    FILE_HANDLE m_fh;//句柄,不解释
    uint64_t mull_BaseOffset;//基础偏移量,作用是在打开的硬盘句柄中通过分区偏移量读取磁盘分区
};

