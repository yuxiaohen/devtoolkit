#pragma once

#include "CIO.h"
#include "drive_struct.h"

class CFileScanner;
class CFileReader;
struct SInfoSet;

class LIB_INTERFACE CFileSystem
{
public:
    CFileSystem(CFileSystem *pFs = NULL)
    {
        this->mp_Disk = NULL;
        mul_ClusterSize = 0;
        mul_SectorSize = SECTOR_SIZE;
        mull_PartitionSize = 0;
        this->SetFileSystem(pFs);
    }

    virtual ~CFileSystem(){}

    bool Usable()
    {
        return this->GetDisk()!=NULL && this->GetDisk()->Usable();
    }
    int Set(CIO *disk)
    {
        if(disk!=NULL && disk->Usable())
        {
            this->mp_Disk = disk;
            return 1;
        }
        return 0;
    }
    void Close()
    {
        this->mp_Disk = NULL;
    }
    int Set(CIO *disk,uint64_t Offset)
    {
        if(disk!=NULL && disk->Usable())
        {
            this->mp_Disk = disk;
            this->mp_Disk->SetBaseOffset(Offset);
            return 1;
        }
        return 0;
    }
    CIO * GetDisk()
    {
        return mp_Disk;
    }
    int Read(void* buf, uint32_t len)
    {
        if(this->GetDisk()!=NULL)
        {
            return this->GetDisk()->Read(buf,len);
        }
        return 0;
    }
    int Write(const void* buf, uint32_t len)
    {
        if(this->GetDisk()!=NULL)
        {
            return this->GetDisk()->Write(buf,len);
        }
        return 0;
    }

    uint64_t GetFileSize()
    {
        if(this->GetDisk()!=NULL)
        {
            return this->GetDisk()->GetFileSize();
        }
        return 0;
    }

    int SetPoint(uint64_t offset,int mode)
    {
        if(this->GetDisk()!=NULL)
        {
            return this->GetDisk()->SetPoint(offset,mode);
        }
        return 0;
    }
    virtual int Config(uint64_t DbrOffset =0,const void * dbr=NULL){return 0;}//配置dbr 需设置disk,返回值大于0 正常,为0 ,读取硬盘失败, 为-1 设置DBR失败
    enum SYSTEM_TYPE
    {RAW=0,PARTITION,NTFS,FAT16,FAT32,EXFAT,EXT,EXT2_3,EXT4,HFSPLUS};
    virtual uint32_t GetType(){return CFileSystem::RAW;}
    static const char* GetSystemNameByType(uint32_t Type)
    {
        const char* SystemName = NULL;
        const char* SystemNameList[] = {"RAW","PARTITION","NTFS","FAT16","FAT32","EXFAT","EXT","EXT2_3","EXT4","HFSPLUS"};
        long  ListLen = sizeof(SystemNameList)/sizeof(*SystemNameList);
        if(0<=Type && Type<ListLen)
        {
            SystemName = SystemNameList[Type];
        }
        return SystemName;
    }

    int SetFileSystem(CFileSystem* pFileSystem)
    {
        if(pFileSystem==NULL || pFileSystem==this) return 0;
        this->SetClusterSize(pFileSystem->GetClusterSize());
        this->SetSectorSize(pFileSystem->GetSectorSize());
        this->SetPartitionSize(pFileSystem->GetPartitionSize());
        this->mp_Disk = pFileSystem->mp_Disk;
        return 1;
    }

    virtual CFileReader *newCFileReaderObject(SInfoSet* pInfoSet = NULL);

    virtual CFileScanner* newCFileScannerObject();

    void        SetClusterSize(uint32_t n)
    {
        mul_ClusterSize = n;
    }

    uint32_t   GetClusterSize()const
    {
        return mul_ClusterSize;
    }

    void        SetSectorSize(uint32_t n)
    {
        mul_SectorSize = n;
    }

    uint32_t   GetSectorSize()const
    {
        return mul_SectorSize;
    }

    uint64_t GetPartitionSize()
    {
        return this->mull_PartitionSize;
    }

    void SetPartitionSize(uint64_t n)
    {
        this->mull_PartitionSize = n;
    }

private:
    CIO *mp_Disk;
    uint32_t     mul_SectorSize;//扇区大小
    uint32_t     mul_ClusterSize;//簇大小
    uint64_t mull_PartitionSize;//分区大小
};
