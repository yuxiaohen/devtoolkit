#pragma once

#include "SInfo.h"
/*
 *SInfoSet类用来提供对SInfo结构及其子结构的管理
 */
struct SInfoSet
{
public:
    //添加一个SInfo结构
    virtual bool AddInfo(SInfo* pInfo)=0;
    //通过数据块来添加多个SInfo结构
    virtual uint32_t ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize)=0;

    //Set中存放的SInfo个数
    virtual uint32_t size()=0;
    //得到index位置的SInfo
    virtual SInfo* at(uint32_t index)=0;
    //清除整个Set
    virtual void clear()=0;

    //返回类型
    virtual uint32_t GetType()=0;
};
