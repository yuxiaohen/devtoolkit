#include "CBitMap.h"

void CBitMap::CleanBitMap()
{
    mb_isUsed = true;
    mull_FreeCluster = 0;
    mull_UsedCluster = 0;
    m_list.clear();
    m_list.push_back(0);

    mul_iFree = 0;
    mul_iUsed = 0;
    mul_nFree = 0;
    mul_nUsed = 0;
    mull_FreeOffset = 0;
    mull_UsedOffset = 0;
}

void CBitMap::ReadBitMap(uint8_t* bitmap,uint32_t len)
{
    uint32_t i;
    for(i=0; i<len; ++i)
    {
        uint8_t c = bitmap[i];
        uint32_t ii;
        for(ii=0;ii<8;++ii)
        {
            AddOneCluster(c&1);
            c>>=1; //右移一位
        }
    }
}

void CBitMap::AddOneCluster(bool isUsed)
{
    isUsed?++mull_UsedCluster:++mull_FreeCluster;
    if(mb_isUsed != isUsed)
    {
        mb_isUsed = isUsed;
        m_list.push_back(0);
    }

    {
        uint32_t n = m_list.size();
        ++m_list.at(n-1);
    }
}

bool CBitMap::isClusterUsed(uint64_t Cluster)
{
    if(Cluster>mull_UsedCluster + mull_FreeCluster)return false;

    uint32_t i;
    uint32_t n = m_list.size();
    for(i=0; i<n; ++i)
    {
        if(Cluster<m_list.at(i))
        {
            break;
        }
        Cluster -= m_list.at(i);
    }
    return i%2==0;
}

bool CBitMap::GetFirstUsedCluster(uint64_t &offset)
{
    mul_iUsed = 0;
    mul_nUsed = 0;
    mull_UsedOffset = 0;

    return GetNextUsedCluster(offset);
}

bool CBitMap::GetNextUsedCluster(uint64_t &offset)
{
    while(mul_nUsed<m_list.size())
    {
        if(mul_iUsed>=m_list.at(mul_nUsed))//当前块遍历完毕
        {
            if(mul_nUsed+2<m_list.size())//移动到下一个簇块,当这个簇块可用时,设置偏移量
            {
                mull_UsedOffset += m_list.at(mul_nUsed);
                mull_UsedOffset += m_list.at(mul_nUsed+1);
            }
            mul_nUsed+=2;
            mul_iUsed = 0;
        }
        else
        {
            offset = mull_UsedOffset+mul_iUsed;
            ++mul_iUsed;
            return true;
        }
    }
    return false;
}

bool CBitMap::GetFirstFreeCluster(uint64_t &offset)
{
    if(m_list.size()<=0)return false;

    mul_iFree = 0;
    mul_nFree = 1;
    mull_FreeOffset = m_list.at(0);

    return GetNextFreeCluster(offset);
}

bool CBitMap::GetNextFreeCluster(uint64_t &offset)
{
    while(mul_nFree<m_list.size())
    {
        if(mul_iFree>=m_list.at(mul_nFree))//当前块遍历完毕
        {
            if(mul_nFree+2<m_list.size())//移动到下一个簇块,当这个簇块可用时,设置偏移量
            {
                mull_FreeOffset += m_list.at(mul_nFree);
                mull_FreeOffset += m_list.at(mul_nFree+1);
            }
            mul_nFree+=2;
            mul_iFree =0;
        }
        else
        {
            offset = mull_FreeOffset+mul_iFree;
            ++mul_iFree;
            return true;
        }
    }
    return false;
}

CFragmentList CBitMap::GetFragmentList(bool Used)
{
    CFragmentList fl;
    uint64_t CurrentClusterIndex = 0;
    uint32_t i;
    for(i=0;i<this->m_list.size();++i)
    {
        if( (i%2==0) == Used && (this->m_list.at(i)>0) )
        {
            fl.AddFragment(CurrentClusterIndex,this->m_list.at(i),this->m_list.at(i));
        }
        CurrentClusterIndex += this->m_list.at(i);
    }
    return fl;
}
