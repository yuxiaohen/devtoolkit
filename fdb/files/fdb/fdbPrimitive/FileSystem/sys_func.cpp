﻿#include "sys_func.h"

#ifdef SYS_WINDOWS //系统相关函数
FILE_HANDLE LIB_INTERFACE f_SYS_OpenFile(const char* path, uint32_t ReadOnly, uint32_t OpenAlways)//打开文件
{
    FILE_HANDLE fh = CreateFileA(path,
                                GENERIC_READ|(ReadOnly==0?GENERIC_WRITE:0),
                                FILE_SHARE_READ|FILE_SHARE_WRITE,
                                NULL,
                                (OpenAlways==0?OPEN_EXISTING:OPEN_ALWAYS),
                                0,
                                NULL
                               );
    return fh;
}

void LIB_INTERFACE f_SYS_CloseFile(FILE_HANDLE f)
{
    CloseHandle(f);
}

uint32_t LIB_INTERFACE f_SYS_ReadFile(FILE_HANDLE fh,void* buf, uint32_t len) //读取文件
{
    uint32_t total = 0;
    if(!ReadFile(fh,buf,len,(DWORD*)&total,NULL))return -1;
    return total;
}

uint32_t LIB_INTERFACE f_SYS_WriteFile(FILE_HANDLE fh,const void* buf, uint32_t len) //写文件
{
    uint32_t total = 0;
    if(!WriteFile(fh,buf,len,(DWORD*)&total,NULL))return -1;
    return total;
}

uint64_t LIB_INTERFACE f_SYS_SetPoint(FILE_HANDLE fh,uint64_t offset,int mode) //设置文件指针位置
{
    uint32_t low = offset&(-1);
    uint32_t high = offset >> 32;
    int set = 0;
    uint64_t ret = 0;
    if(START_POS==mode)
    {
        set = FILE_BEGIN;
    }
    else if(END_POS==mode)
    {
        set = FILE_END;
    }
    else if(CURRENT_POS==mode)
    {
        set = FILE_CURRENT;
    }

    low = SetFilePointer(fh,low,(LONG*)&high,set);
    if(HFILE_ERROR==low)
    {
        return low;
    }
    ret = high;
    ret <<=32;
    ret |= low;
    return ret;
}

//获取文件大小
uint64_t LIB_INTERFACE f_SYS_GetFileSize(FILE_HANDLE fh)
{
    uint64_t Size = 0;
    uint32_t High=0,Low=0;
    Low = GetFileSize(fh,(DWORD*)&High);
    if(Low != INVALID_FILE_SIZE)
    {
        Size = High;
        Size <<= 32;
        Size |= Low;
    }
    return Size;
}

#endif
