<<<<<<< HEAD
//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Copyright (C) 2011 Vicente J. Botet Escriba
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

// <boost/thread/shared_mutex.hpp>

// template <class Mutex> class upgrade_lock;

// upgrade_lock(upgrade_lock const&) = del
=======
#pragma once

#include "../CFileReader.h"
#include "../CFragmentReader.h"
#include "SHfsFileInfoSet.h"

class LIB_INTERFACE CHfsPlusFileReader:public CFileReader
{
public:
    CHfsPlusFileReader(CFileSystem* pFileSystem = NULL)
    {
        SetFileSystem(pFileSystem);
    }
    virtual ~CHfsPlusFileReader(){}
    bool SetFileSystem(CFileSystem* pFileSystem);
    bool  Config(SInfoSet* pInfoSet);
    bool FromFile(SFileInfo* pInfo);
    uint32_t ReadBlock(uint32_t Index,uint8_t* OutBuf,uint32_t Size);
    uint32_t ReadNextBlock(uint8_t* OutBuf,uint32_t Size);
private:
    CFileSystem* mp_HfsPlus;
    SHfsFileInfo* mp_Info;
    CFragmentReader m_Reader;
};
>>>>>>> a9d47686722050a38fed7e884bf851a3947e531f
