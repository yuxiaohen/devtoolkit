#pragma once

#include "../SInfoSet.h"
#include "SHfsFileInfo.h"

#include <vector>
using std::vector;

struct LIB_INTERFACE SHfsFileInfoSet:public SInfoSet
{
public:
    SHfsFileInfoSet()
    {
    }
    bool AddInfo(SInfo* pInfo);
    uint32_t ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize);

    uint32_t size();
    SInfo* at(uint32_t index);
    void clear();

    uint32_t GetType()
    {
        return CFileSystem::HFSPLUS;
    }

    vector <SHfsFileInfo> m_InfoList;
    vector <SHfsFileInfo> m_JournalInfoList;
    SHfsFileInfo m_HfsInfo;
};
