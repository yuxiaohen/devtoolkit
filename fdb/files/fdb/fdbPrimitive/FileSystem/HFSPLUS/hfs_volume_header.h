#pragma once
typedef unsigned long ulong; 
typedef unsigned long long ulonglong;
typedef unsigned short ushort;
typedef unsigned char uchar;


#pragma pack(push)
#pragma pack(1)

#pragma pack(push)
#pragma pack(1)

typedef struct BlockStruct
{
    ulong StartBlock;
    ulong BlockCount;
}BlockStruct;

typedef struct hfs_extent_overflow
{
//盘区溢出文件的关键字结构
    ushort key_lenght;		//关键字长度
    uchar fork_type;		//分支类型
    uchar reserve;			//保留
    ulong file_CNID;		//文件的CNID
    ulong VBN;				//起始虚拟号（VBN）

//盘区溢出文件	数据的结构
//    ulong volume_header_chunk_1;			//0x10
//    ulong volume_header_chunk_1_number;
//    ulong volume_header_chunk_2;
//    ulong volume_header_chunk_2_number;
//    ulong volume_header_chunk_3;
//    ulong volume_header_chunk_3_number;
//    ulong volume_header_chunk_4;
//    ulong volume_header_chunk_4_number;
//    ulong volume_header_chunk_5;
//    ulong volume_header_chunk_5_number;
//    ulong volume_header_chunk_6;
//    ulong volume_header_chunk_6_number;
//    ulong volume_header_chunk_7;
//    ulong volume_header_chunk_7_number;
//    ulong volume_header_chunk_8;
//    ulong volume_header_chunk_8_number;
                                            //0x4F
    BlockStruct chunk[8];
}hfs_extent_overflow;


#pragma pack(pop)

//编录文件信息
typedef struct hfs_volume_header_catalog
{
	uchar total_size[8];		//0x00
	ulong clump_size;			//0x08
	ulong total_blocks;			//0x0C
//	ulong volume_header_chunk_1;			//0x10
//	ulong volume_header_chunk_1_number;		//0x14
//	ulong volume_header_chunk_2;			//0x18
//	ulong volume_header_chunk_2_number;		//0x1C
//	ulong volume_header_chunk_3;			//0x20
//	ulong volume_header_chunk_3_number;		//0x24
//	ulong volume_header_chunk_4;			//0x28
//	ulong volume_header_chunk_4_number;		//0x2C
//	ulong volume_header_chunk_5;			//0x30
//	ulong volume_header_chunk_5_number;		//0x34
//	ulong volume_header_chunk_6;			//0x38
//	ulong volume_header_chunk_6_number;		//0x3C
//	ulong volume_header_chunk_7;			//0x40
//	ulong volume_header_chunk_7_number;		//0x44
//	ulong volume_header_chunk_8;			//0x48
//    ulong volume_header_chunk_8_number;		//0x4C
    BlockStruct chunk[8];
}hfs_volume_header_catalog;

//盘区溢出文件信息
typedef struct hfs_volume_header_extents_file
{
	uchar total_size[8];		//0x00
	ulong clump_size;			//0x08
	ulong total_blocks;			//0x0C
//	ulong volume_header_chunk_1;			//0x10
//	ulong volume_header_chunk_1_number;		//0x14
//	ulong volume_header_chunk_2;			//0x18
//	ulong volume_header_chunk_2_number;		//0x1C
//	ulong volume_header_chunk_3;			//0x20
//	ulong volume_header_chunk_3_number;		//0x24
//	ulong volume_header_chunk_4;			//0x28
//	ulong volume_header_chunk_4_number;		//0x2C
//	ulong volume_header_chunk_5;			//0x30
//	ulong volume_header_chunk_5_number;		//0x34
//	ulong volume_header_chunk_6;			//0x38
//	ulong volume_header_chunk_6_number;		//0x3C
//	ulong volume_header_chunk_7;			//0x40
//	ulong volume_header_chunk_7_number;		//0x44
//	ulong volume_header_chunk_8;			//0x48
//	ulong volume_header_chunk_8_number;		//0x4C
    BlockStruct chunk[8];
}hfs_volume_header_extents_file;

typedef struct hfs_volume_header
{
	ushort volume_header_signature;       	  //0x00
	ushort volume_header_versions;		  //0x02
	ulong volume_header_attribute;		  //0x04
	ulong volume_header_loaded_version;	  //0x08
	ulong volume_header_log;			  //0x0C
	ulong volume_header_creat_time;		  //0x10
	ulong volume_header_modification_time;//0x14
	ulong volume_header_backup_time;	  //0x18
	ulong voulume_header_check_time;	  //0x1C
    ulong volume_header_file_number;	  //0x20
	ulong volume_header_directory_number; //0x24
	ulong volume_header_chunk_bytes;	  //0x28
	ulong volume_header_total_chunk;	  //0x2C
	ulong volume_header_free_chunk;		  //0x30
	ulong volume_header_next_block;		  //0x34
	ulong volume_header_resource_size; 	  //0x38
	ulong volume_header_data_size;		  //0x3C
	ulong Volume_header_catalog_id;		  //0x40
	ulong volume_header_write_number;	  //0x44
	uchar volume_header_document_bitmap[8];  //0x48
	uchar volume_header_finderinfo[32];   //0x50
	uchar volume_header_alloc_file[80];   //0x70
	struct hfs_volume_header_extents_file volume_header_extents_file; //0xC0
	struct hfs_volume_header_catalog catalog_file_information; //0x110
	uchar volume_header_attribute_file[80];//0x160
	uchar volume_header_start_file[80];	   //0x1B0
	
	
}hfs_volume_header;

//节点描述符
typedef struct hfs_leaf_node_descriptor
{
    ulong next_node_num;		//0x00
    ulong last_node_num;		//0x04
    uchar node_type;			//0x08
    uchar node_hight;			//0x09
    ushort node_num;			//0x0A
    ushort reserve;				//0x0C
}hfs_leaf_node_descriptor;

//节点的关键字
//typedef struct hfs_leaf_node_folder
//{
//    ushort key_lenght;		//关键字长度
//    //关键字
//    ulong father_ID;		//父目录的ID
//    ushort node_num;		//节点的字符数
//    //uchar node_name[2*node_num];	//节点名
//}hfs_leaf_node_folder;

typedef struct hfs_catalog_key_head
{
    ushort key_length;
    ulong parent_id;
    ushort node_namelen;
    ushort file_name[1];
}hfs_catalog_key_head;


//叶节点文件夹
typedef struct hfs_leaf_node_folder
{
    ushort file_type;				//0x00
    ushort sign;					//0x02
    ulong folder_number;			//0x04
    ulong folder_CNID;				//0x08
    ulong folder_create_time;		//0x0C
    ulong folder_modification_time; //0x10
    ulong attribute_modification_time;//0x14
    ulong last_access_time;			//0x18
    ulong last_backup_time;			//0x1C
    uchar folder_jurisdiction[16];	//0x20
    uchar user_information[16];		//0x30
    uchar FinderInfo[16];			//0x40
    ulong document_name;			//0x50
    ulong reserve;					//0x54

}hfs_leaf_node_folder;

//文件数据分支信息
typedef struct hfs_leaf_node_file_data
{
    uint64_t total_size;		//0x00
    ulong clump_size;			//0x08
    ulong total_blocks;			//0x0C
//    ulong volume_header_chunk_1;			//0x10
//    ulong volume_header_chunk_1_number;		//0x14
//    ulong volume_header_chunk_2;			//0x18
//    ulong volume_header_chunk_2_number;		//0x1C
//    ulong volume_header_chunk_3;			//0x20
//    ulong volume_header_chunk_3_number;		//0x24
//    ulong volume_header_chunk_4;			//0x28
//    ulong volume_header_chunk_4_number;		//0x2C
//    ulong volume_header_chunk_5;			//0x30
//    ulong volume_header_chunk_5_number;		//0x34
//    ulong volume_header_chunk_6;			//0x38
//    ulong volume_header_chunk_6_number;		//0x3C
//    ulong volume_header_chunk_7;			//0x40
//    ulong volume_header_chunk_7_number;		//0x44
//    ulong volume_header_chunk_8;			//0x48
//    ulong volume_header_chunk_8_number;		//0x4C
    BlockStruct chunk[8];
}hfs_leaf_node_file_data;

//文件资源分支信息
typedef struct hfs_leaf_node_file_resource
{
    uint64_t total_size;		//0x00
    ulong clump_size;			//0x08
    ulong total_blocks;		//0x0C
//    ulong volume_header_chunk_1;			//0x10
//    ulong volume_header_chunk_1_number;		//0x14
//    ulong volume_header_chunk_2;			//0x18
//    ulong volume_header_chunk_2_number;		//0x1C
//    ulong volume_header_chunk_3;			//0x20
//    ulong volume_header_chunk_3_number;		//0x24
//    ulong volume_header_chunk_4;			//0x28
//    ulong volume_header_chunk_4_number;		//0x2C
//    ulong volume_header_chunk_5;			//0x30
//    ulong volume_header_chunk_5_number;		//0x34
//    ulong volume_header_chunk_6;			//0x38
//    ulong volume_header_chunk_6_number;		//0x3C
//    ulong volume_header_chunk_7;			//0x40
//    ulong volume_header_chunk_7_number;		//0x44
//    ulong volume_header_chunk_8;			//0x48
//    ulong volume_header_chunk_8_number;		//0x4C
    BlockStruct chunk[8];
}hfs_leaf_node_file_resource;

//叶节点文件记录结构
typedef struct hfs_leaf_node_file
{
    ushort file_type;				//0x00
    ushort sign;					//0x02
    ulong folder_number;			//0x04
    ulong folder_CNID;				//0x08
    ulong folder_create_time;		//0x0C
    ulong folder_modification_time; //0x10
    ulong attribute_modification_time;//0x14
    ulong last_access_time;			//0x18
    ulong last_backup_time;			//0x1C
    uchar folder_jurisdiction[16];	//0x20
    uchar user_information[16];		//0x30
    uchar FinderInfo[16];			//0x40
    ulong document_name;			//0x50
    ulong reserve;					//0x54

    struct hfs_leaf_node_file_data leaf_node_file_data;

    struct hfs_leaf_node_file_resource leaf_node_file_resource;

}hfs_leaf_node_file;



//链接记录
typedef struct hfs_leaf_node_link
{
    //关键字
    ushort key_lenght;			//0x00	       关键字长度为N  始终6个字节
    ulong link_CNID;			//0x02 		   当前文件夹的CNID
    ushort link_node_number;	//0x06        节点名的字符数  始终为0


    ushort type;				//0x00
    ushort reserve;				//0x02
    ulong father_catalog;		//0x04
    ushort node_characters_number;	//0x08   节点名字符数（N）
    //uchar node_name[2*node_characters_number];	//0x0A  长度2N

}hfs_leaf_node_link;


typedef struct hfs_index_node_descriptor
{
    //节点描述符
    ulong next_node_num;		//0x00
    ulong last_node_num;		//0x04
    uchar node_type;			//0x08
    uchar node_hight;			//0x09
    ushort node_num;			//0x0A
    ushort reserve;				//0x0C

}hfs_index_node_descriptor;


typedef struct hfs_head_node
{
    //节点描述符
    ulong next_node_num;		//0x00
    ulong last_node_num;		//0x04
    uchar node_type;			//0x08
    uchar node_hight;			//0x09
    ushort node_num;			//0x0A
    ushort reserve;				//0x0C
    //头结点
    //头记录
    ushort head_node_hight;		//0x00
    ulong head_node_num;		//0x02
    ulong head_node_leaf_num;	//0x06
    ulong head_node_leaf_frist; //0x0A
    ulong head_node_leaf_last;	//0x0E
    ushort head_node_size;		//0x12   //节点大小
    ushort head_node_key_lenght;//0x14
    ulong head_node_total_size; //0x16
    ulong head_node_free_size;	//0x1A
    ushort head_node_reserve;	//0x1E
    ulong head_node_clumpsize;	//0x20
    uchar head_node_type;		//0x24
    uchar head_node_reserve2;	//0x25
    ulong head_node_attribute;	//0x26
    uchar reserve3[64];			//0x2A-0X69
    //保留记录
    uchar keep_records[128];	//128字节
    uchar reserved[264];
    //位图记录
//	uchar bitmap_record[head_node_size -256 ];		//位图节点  //与节点大小有关系

//	//头结点的节点记录起始偏移量列表
//	ushort node_record_offset;				//节点偏移量列表记录
//	ushort bitmap_record_offset;			//位图记录
//	ushort keep_record_offset;				//保留记录
//	ushort head_record_offset;				//头记录

}hfs_head_node;

typedef struct block_info
{
    uchar journal_list_header_block_num[16];	//0x00	扇区号
    ulong journal_list_header_block_size;		//0x08	字节拷贝数
    ulong journal_list_header_block_next;		//0x0c	阻止列表的第一个元素（在磁盘上，数据已经没有了意义）
}block_info;


typedef struct hfs_journal_list_header
{
    ushort journal_list_header_max_blocks;	//0X00	块的最大数量
    ushort journal_list_header_num_blocks;	//0X02	块信息的数量
    ulong journal_list_header_bytes_used;	//0X04	此块的字节数
    ulong journal_list_header_checksum;		//0X08	块类表校验和
    ulong journal_list_header_pad;			//0X0c	保留

    struct block_info journal_list_header_binfo[1];

}hfs_journal_list_header;

typedef struct hfs_journal_info_block
{
    ulong journal_info_block_flags;		//0x00  标志
    ulong journal_info_signatrue[8];	//0X04  签名 保留
    uchar journal_info_offset[8];		//0x24  从设备起始字节到日志头的开始偏移量
    uchar journal_info_size[8];			//0x2c	该日志的字节数，（日志表头和日志缓冲区）
    ulong journal_info_reserved[32];		//0x34	保留

}hfs_journal_info_block;

typedef struct hfs_journal_header
{
    ulong journal_header_magic;   		//0x00	用来验证头的完整性0x4a4e4c78
    ulong journal_header_endian;		//0x04  用来验证头的完整性0×12345678
    ulonglong journal_header_start;		//0x08	包含从日志头开始字节到第一个交易起始位置的偏移
    ulonglong journal_header_end;		//0x10	包含从日志头开始字节到最后一个交易结束位置的偏移量
    ulonglong journal_header_size;		//0x18	该日志的大小，以字节为单位
    ulong journal_header_blhdr_size;	//0x20	block_list_header大小的字节单位
    ulong journal_header_checksum;		//0x24	该日志的校验和
    ulong journal_header_jhdr_size;		//0x28	本日志头的大小，以字节为单位

}hfs_journal_header;

#pragma pack(pop)
