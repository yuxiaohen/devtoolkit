#pragma once
#include "../CFileSystem.h"
#include "SHfsFileInfo.h"

#include <vector>
using std::vector;

unsigned short be16(unsigned short n );

unsigned long be32(unsigned long n );

unsigned long long be64(unsigned long long n );

class LIB_INTERFACE CHfsPlus:public CFileSystem
{
public:
    int Config(uint64_t DbrOffset =0,const void * dbr=NULL);
    static int GetNode(uint8_t *buf, int len,vector <SHfsFileInfo>& list);
    int  GetJournalNode(vector <SHfsFileInfo>& list);
    uint64_t GetNodeByIndex(uint32_t index);
    int GetAllNode(vector <SHfsFileInfo>& list);
    uint32_t GetType()
    {
        return CFileSystem::HFSPLUS;
    }
    virtual CFileScanner* newCFileScannerObject();

    virtual CFileReader* newCFileReaderObject(SInfoSet* pInfoSet = NULL);
private:
    hfs_volume_header_catalog mCatalog;
    hfs_volume_header_extents_file mExtents;
    uint32_t mNodeSize;
    uint32_t mRootNum;
    uint32_t mJournalinfoblock;
};
