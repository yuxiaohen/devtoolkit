#pragma once

#include "../CFileScanner.h"
#include "CHfsPlus.h"
#include "SHfsFileInfoSet.h"

class LIB_INTERFACE CHfsPlusFileScanner:public CFileScanner
{
public:
    CHfsPlusFileScanner(CHfsPlus* pHfsPlus = NULL);
    int Set(CHfsPlus* pHfsPlus);
    uint32_t ScanFile(uint32_t StartSector);
    uint32_t ScanFreeCluster(uint32_t StartSector);
private:
    CHfsPlus* mp_HfsPlus;
};
