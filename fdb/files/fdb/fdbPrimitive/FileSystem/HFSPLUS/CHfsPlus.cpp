#include "CHfsPlus.h"
#include "CHfsPlusFileReader.h"
#include "CHfsPlusFileScanner.h"

unsigned short be16(unsigned short n )
{
    unsigned char * pn = (unsigned char *)&n;
    pn[0]^=pn[1]^=pn[0]^=pn[1];
    return n;
}

unsigned long be32(unsigned long n )
{
    unsigned char * pn = (unsigned char *)&n;
    pn[0]^=pn[3]^=pn[0]^=pn[3];
    pn[2]^=pn[1]^=pn[2]^=pn[1];
    return n;
}

unsigned long long be64(unsigned long long n )
{
    unsigned char * pn = (unsigned char *)&n;
    pn[0]^=pn[7]^=pn[0]^=pn[7];
    pn[1]^=pn[6]^=pn[1]^=pn[6];
    pn[2]^=pn[5]^=pn[2]^=pn[5];
    pn[3]^=pn[4]^=pn[3]^=pn[4];
    return n;
}

CFileScanner* CHfsPlus::newCFileScannerObject()
{
    return new CHfsPlusFileScanner(this);
}

CFileReader* CHfsPlus::newCFileReaderObject(SInfoSet* pInfoSet)
{
    return new CHfsPlusFileReader(this);
}

int CHfsPlus::Config(DWORDLONG DbrOffset,const void* dbr)
{
    hfs_volume_header hvh;
    if(this->Usable())
    {
        this->SetPoint(0X400,CIO::START_POS);
        if(this->Read(&hvh,sizeof(hvh))>0)
        {
            if(hvh.volume_header_signature==0X5848)
            {
                this->SetClusterSize
                        (be32(hvh.volume_header_chunk_bytes));
                this->mCatalog = hvh.catalog_file_information;
                this->mExtents = hvh.volume_header_extents_file;
                this->mJournalinfoblock = be32(hvh.volume_header_log);
                uint64_t CatalogOffset = be32(this->mCatalog.chunk[0].StartBlock) * (this->GetClusterSize());
                static hfs_head_node  head_node;
                this->SetPoint(CatalogOffset,CIO::START_POS);
                if(this->Read(&head_node,sizeof(head_node))>0)
                {
                    this->mNodeSize = be16(head_node.head_node_size);
                    this->mRootNum = be32(head_node.head_node_num);
                    if(this->mNodeSize!=0 && this->mRootNum!=0 )
                    {
                        return 1;
                    }
                }
            }
        }
        else
        {
            this->Close();
        }
    }
    return 0;
}

int CHfsPlus::GetNode(uint8_t *buf, int len, vector <SHfsFileInfo>& list)
{
    if(buf==NULL || len <=0 )return 0;
    uint16_t *ReverseList = (uint16_t*)buf+len/sizeof(uint16_t);
    hfs_leaf_node_descriptor * node_descriptor = (hfs_leaf_node_descriptor *)buf;
    uint32_t NumRecords = node_descriptor->node_num;
    NumRecords = be16(NumRecords);

    SHfsFileInfo Info;
    int i;
    for(i=0;i<NumRecords;++i)
    {
        hfs_catalog_key_head *node_head = (hfs_catalog_key_head *)(buf+be16(ReverseList[-i-1]));
        if(Info.Set(node_head))
        {
            list.push_back(Info);
        }
    }
    return 1;
}

uint64_t CHfsPlus::GetNodeByIndex(uint32_t index)
{
    if(!this->Usable())return 0;

    int i;
    int index_in_arr = index;

    for(i=0;i<8;++i)
    {
        if(this->mCatalog.chunk[i].BlockCount < index_in_arr)
        {
            index_in_arr -= this->mCatalog.chunk[i].BlockCount;
        }
        else
        {
            break;
        }
    }
    uint64_t offset = ((uint64_t)be32(this->mCatalog.chunk[i].StartBlock)) * this->GetClusterSize();
    offset += this->mNodeSize * index_in_arr;
    return offset;
}

int CHfsPlus::GetJournalNode(vector <SHfsFileInfo>& list)
{
    if(!this->Usable()) return 0;
    uint64_t journalBlockOffset = ((uint64_t)this->mJournalinfoblock)*this->GetClusterSize();
    uint8_t buf[512]={0};
    this->SetPoint(journalBlockOffset,CIO::START_POS);
    if(this->Read(buf,512)<=0)return 0;
    uint64_t journalOffset = be64(*(uint64_t*)(buf+0X24));
    //uint64_t journalSize = be64(*(uint64_t*)(buf+0X2C));
    this->SetPoint(journalOffset,CIO::START_POS);
    if(this->Read(buf,512)<=0)return 0;
    hfs_journal_header* journal_header= (hfs_journal_header*)buf;
    if(journal_header->journal_header_magic != 0x4a4e4c78) return 0; //签名检测
    uint64_t start_offset = (journal_header->journal_header_start);
    uint64_t end_offset = (journal_header->journal_header_end);
    uint64_t journal_size = (journal_header->journal_header_size);
    //uint64_t head_size = (journal_header->journal_header_blhdr_size);
    uint32_t record_start = (journal_header->journal_header_jhdr_size);
    uint8_t *Journal_buf = new uint8_t[journal_size];
    if(start_offset<end_offset)
    {
          this->SetPoint(journalOffset+record_start,CIO::START_POS);
          this->Read(Journal_buf,journal_size-record_start);
    }
    else
    {
         this->SetPoint(journalOffset+start_offset,CIO::START_POS);
         this->Read(Journal_buf,journal_size - start_offset);
         this->SetPoint(journalOffset+record_start,CIO::START_POS);
        this->Read(Journal_buf + journal_size - start_offset,end_offset - record_start);
    }
    if(this->mNodeSize!=0)
    {
        int nNode = journal_size / this->mNodeSize;
        int i;
        for(i=0;i<nNode;++i)
        {
            uint8_t* node_buf = Journal_buf+i*this->mNodeSize;
            if(node_buf[8]==0XFF && node_buf[9]==0X01)
            {
                this->GetNode(node_buf,this->mNodeSize,list);
            }
        }
    }

    delete[] Journal_buf;
    return 1;
}

int CHfsPlus::GetAllNode(vector <SHfsFileInfo>& list)
{
    if(this->Usable())
    {
        vector<SHfsFileInfo>index_list;
        uint8_t * buf = new uint8_t[this->mNodeSize];
        uint64_t offset = GetNodeByIndex(this->mRootNum);
        this->SetPoint(offset,CIO::START_POS);
        this->Read(buf,this->mNodeSize);
        if( ((hfs_index_node_descriptor*)buf)->node_type!=0XFF  )
        {
            this->GetNode(buf,this->mNodeSize,index_list);
        }
        else
        {
            this->GetNode(buf,this->mNodeSize,list);
        }

        int i=0;
        while(i<index_list.size())
        {
            if( index_list.at(i).data.RecordType==0)
            {
                uint32_t NextNode = index_list.at(i).data.Flags;
                if(NextNode!=0)
                {
                    offset = GetNodeByIndex(NextNode);
                    this->SetPoint(offset,CIO::START_POS);
                    this->Read(buf,this->mNodeSize);
                    if( ((hfs_index_node_descriptor*)buf)->node_type!=0XFF  )
                    {
                        this->GetNode(buf,this->mNodeSize,index_list);
                    }
                    else
                    {
                        this->GetNode(buf,this->mNodeSize,list);
                    }
                }
            }
            ++i;
        }
        delete []buf;
        return 1;
    }
    return 0;
}
