<<<<<<< HEAD
/* Boost test/overflow.cpp
 * test if extended precision exponent does not disturb interval computation
 *
 * Copyright 2002-2003 Guillaume Melquiond
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or
 * copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#include <boost/numeric/interval.hpp>
#include <boost/test/minimal.hpp>
#include "bugs.hpp"

template<class I>
void test_one(typename I::base_type x, typename I::base_type f) {
  I y = x;
  typename I::base_type g = 1 / f;
  const int nb = 10000;
  for(int i = 0; i < nb; i++) y *= f;
  for(int i = 0; i < nb; i++) y *= g;
  BOOST_CHECK(in(x, y));
# ifdef __BORLANDC__
  ::detail::ignore_unused_variable_warning(nb);
# endif
}

tem
=======
#include "FileSystem.h"
#include "locale.h"

int main()
{
	setlocale(LC_ALL,"");
	CBitlockerDisk a;
	int ret = a.LoadDll("disk.dll");
	printf("Ret:%d\n",ret);
	a.SetBaseOffset(0X538249DC00);
	a.SetPassword("119581-155133-356466-047729-548031-616088-207086-646085");
	int op = a.Open("\\\\.\\PHYSICALDRIVE0");
	printf("Op:%d\n",op);
	CNtfs ntfs;
	ntfs.Set(&a);
	ret = ntfs.Config();
	printf("ret:%d\n",ret);
	CFileScanner * s = ntfs.newCFileScannerObject();
	if(s==NULL)return 0;
	SNtfsFileInfoSet fileinfo;
	s->AddInfoSet(&fileinfo);
	s->ScanFile(0);
	printf("%d\n",fileinfo.size());
	int i;
	for(i=0;i<fileinfo.size();++i)
	{
		wprintf(L"%s\n",((SFileInfo*)fileinfo.at(i))->GetFileName().c_str());
	}
	return 0;
}
>>>>>>> a9d47686722050a38fed7e884bf851a3947e531f
