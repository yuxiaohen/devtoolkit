#pragma once

#include "CIO.h"
#include "CFragmentList.h"

class LIB_INTERFACE CFragmentReader
{
public:
    CFragmentReader();
    virtual ~CFragmentReader();

public:
    //为读取文件设置磁盘,文件碎片表,设置簇大小,簇起始偏移(FAT系列),扇区大小(不同硬盘规格)
    bool Set(CIO* pDisk,CFragmentList* pRunList,uint32_t uint8_tPerCluster,uint64_t ClusterStartOffset=0,uint32_t SectorSize = SECTOR_SIZE);
    void Close();
    uint32_t ReadSector(uint32_t SectorIndex,uint8_t* outputbuf,uint32_t n);//通过扇区序号 读取n个扇区(512字节)
    uint32_t ReadNextSector(uint8_t* outputbuf,uint32_t n);//读取下n个扇区(ReadSector读取的扇区的下一个扇区)

    //在一块文件碎片内 通过扇区序号 读取n个扇区(512字节),返回值为已经读取的扇区个数
    uint32_t ReadSectorInFrag(uint32_t FragIndex,uint32_t SectorIndexInFrag,uint8_t* outputbuf,uint32_t n);
    uint32_t GetTotalSector();

private:
    CFragmentList m_runlist;
    uint32_t mul_uint8_tPerCluster;
    uint32_t mul_SectorSize;
    CIO* mp_Disk;
    uint8_t * mp_DecryptBuf;//解压缓存区
    uint32_t mul_DecryptBufSize;//解压缓存大小
    uint32_t mul_DecryptFragNum;//解压缓存代表的文件碎片编号

    uint32_t mul_CurSectorIndexInFrag;//当前文件碎片扇区序号
    uint32_t mul_CurFragIndex;//当前文件碎片号

    uint64_t mull_ClusterStartOffset;
};
