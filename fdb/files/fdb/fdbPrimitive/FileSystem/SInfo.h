﻿#pragma once

#include <string>
using std::wstring;

#include "CFileSystem.h"

struct SInfo
{
    virtual uint64_t GetOffset()=0;//获取偏移量
    virtual uint32_t GetType()=0;//获取类型
};

struct SFileInfo:public SInfo
{
    virtual uint32_t GetParentIndex()=0;//获取父目录项编号
    virtual wstring& GetFileName()=0;//获取文件名
    virtual uint64_t GetFileSize()=0;//获取文件大小
    virtual bool isDel()=0;//判断是否删除
    virtual bool isDir()=0;//判断是否目录
};
