#include "sys_func.h"

#ifdef FDB_FUNC

FILE_HANDLE LIB_INTERFACE f_SYS_OpenFile(const char* path, uint32_t ReadOnly, uint32_t OpenAlways)
{
    FILE_HANDLE f =  FDBfIOSystemDevice_new_not_open(path);
    if(FDBfIOSystemDevice_open(f,ReadOnly?FDBEAccessFlag_ReadOnly:FDBEAccessFlag_ReadWrite,
    OpenAlways?FDBEOpenMethod_AlwaysOpen:FDBEOpenMethod_OpenExisting)!=0)
    {
        FDBfIOSystemDevice_delete(&f);
        f=NULL;
    }
    return f;
}

void LIB_INTERFACE f_SYS_CloseFile(FILE_HANDLE f)
{
    FDBfIOSystemDevice_delete(&f);
}

uint32_t LIB_INTERFACE f_SYS_ReadFile(FILE_HANDLE fh,void* buf, uint32_t len)
{
    return FDBfIOSystemDevice_read(fh,buf,len);
}

uint32_t LIB_INTERFACE f_SYS_WriteFile(FILE_HANDLE fh,const void* buf, uint32_t len)
{
    return FDBfIOSystemDevice_write(fh,buf,len);
}

uint64_t LIB_INTERFACE f_SYS_SetPoint(FILE_HANDLE fh,uint64_t offset,int mode)
{
    return FDBfIOSystemDevice_seek(fh,offset,mode);
}

uint64_t LIB_INTERFACE f_SYS_GetFileSize(FILE_HANDLE fh)
{
    return FDBfIOSystemDevice_size(fh);
}

#endif
