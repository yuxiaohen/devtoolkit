#include "CFragmentList.h"

int CFragmentList::Decompress(uint8_t *dest, uint32_t dest_size,uint8_t* const cb_start, uint32_t cb_size)
{
    return f_NTFS_decompress(dest,dest_size,cb_start,cb_size);
}

void CFragmentList::AddFragment(uint64_t ClusterIndex,uint64_t ClusterLength,uint64_t DecompressLength)
{
    if(DecompressLength==0)DecompressLength = ClusterLength;
    m_OffsetList.push_back(ClusterIndex);
    m_ClusterList.push_back(ClusterLength);
    m_DecompressList.push_back(DecompressLength);
    mull_TotalCluster += DecompressLength;
}

void CFragmentList::RemoveFragment(uint32_t n)
{
    if(n<this->GetTotalFrag())
    {
        mull_TotalCluster -= m_DecompressList.at(n);
        m_OffsetList.erase( m_OffsetList.begin()+n);
        m_ClusterList.erase( m_ClusterList.begin()+n);
        m_DecompressList.erase( m_DecompressList.begin()+n);
    }
}

void CFragmentList::ReadDataRunList(uint8_t* RunList)
{
    if(RunList==NULL)return;
    uint8_t *List = RunList;
    uint64_t current_offset=0,offset,clusternum;
    uint32_t run_len;
    while(run_len = f_NTFS_GetDataRun(List,&offset,&clusternum),run_len>0)
    {
        List += run_len;

        if(offset==0)//当遇到RunList中的压缩数据段
        {
            uint32_t n = m_ClusterList.size();
            if(n>0)
            {
                uint64_t precompress_cluster = 0X10;//最小为解压缩后空间预留出16个簇来存放解压缩后数据
                while(clusternum >= precompress_cluster)//计算数据被压缩前大小
                {
                    precompress_cluster <<= 1;
                }
                uint32_t compress_size = precompress_cluster - clusternum;

                if(m_ClusterList.at(n-1)>compress_size)//文件碎片部分被压缩
                {
                    m_ClusterList.at(n-1) -= compress_size;
                    m_DecompressList.at(n-1) -= compress_size;
                    mull_TotalCluster -= compress_size;
                    uint64_t encrypt_offset = m_OffsetList.at(n-1) + m_ClusterList.at(n-1);
                    AddFragment(encrypt_offset,compress_size,precompress_cluster);
                }
                else if(m_ClusterList.at(n-1)==compress_size)//文件碎片全部被压缩
                {
                    mull_TotalCluster += precompress_cluster - compress_size;
                    m_DecompressList.at(n-1) = precompress_cluster;
                }
            }
        }
        else //非压缩数据段
        {
            current_offset += offset;

            AddFragment(current_offset,clusternum,clusternum);
        }
    }
}

void CFragmentList::ClearList()
{
    m_OffsetList.clear();
    m_ClusterList.clear();
    m_DecompressList.clear();
    mull_TotalCluster = 0;
}


uint32_t CFragmentList::GetFirstClusterOffset(uint64_t &offset)
{
    if(m_OffsetList.size()>0)
    {
        mul_CurrentFrag = 0;
        mull_CurrentOffset = m_OffsetList.at(mul_CurrentFrag);
        offset = mull_CurrentOffset;
        return mul_CurrentFrag;
    }
    return -1;
}

uint32_t CFragmentList::GetNextClusterOffset(uint64_t &offset)
{
    if(m_OffsetList.size()>0 && mul_CurrentFrag<m_OffsetList.size() )
    {
        offset = mull_CurrentOffset;
        ++mull_CurrentOffset;
        if( mull_CurrentOffset >= m_OffsetList.at(mul_CurrentFrag) + m_ClusterList.at(mul_CurrentFrag) )
        {
            ++mul_CurrentFrag;
            if(mul_CurrentFrag<m_OffsetList.size())
                mull_CurrentOffset = m_OffsetList.at(mul_CurrentFrag);
        }

        return mul_CurrentFrag;
    }
    return -1;
}

bool CFragmentList::GetClusterOffsetByIndex(uint64_t index,uint64_t &offset)const
{
    if(m_OffsetList.size()>0 && index>=0)
    {
        uint64_t s = index;
        uint32_t i;
        for(i=0; i<m_ClusterList.size(); ++i)
        {
            if(s<m_ClusterList.at(i))
            {
                offset = s + m_OffsetList.at(i);
                break;
            }
            s -= m_ClusterList.at(i);
        }
        if(i<m_ClusterList.size())
            return true;
    }
    return false;
}
