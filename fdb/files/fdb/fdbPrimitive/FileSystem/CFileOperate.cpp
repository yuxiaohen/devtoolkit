﻿#include "CFileOperate.h"

CFileOperate::CFileOperate(FILE_HANDLE fh,uint64_t Offset)
{
    m_fh = fh;
    mull_BaseOffset = Offset;
    mp_fhQuoteCount = NULL;
}

CFileOperate::CFileOperate(CFileOperate& disk)
{
    Set(&disk);
}

int CFileOperate::Set(CFileOperate *disk)//对象间拷贝调用此函数
{
    if(disk!=NULL && disk!=this)
    {
        Close();
        m_fh = disk->m_fh;
        mull_BaseOffset = disk->mull_BaseOffset;

        if(disk->mp_fhQuoteCount==NULL) //若不存在引用计数器,则创建引用计数器
        {
            disk->mp_fhQuoteCount = new uint32_t;
            if(disk->mp_fhQuoteCount==NULL)return 0;
            *(disk->mp_fhQuoteCount) = 1;
        }
        mp_fhQuoteCount = disk->mp_fhQuoteCount;
        ++(*mp_fhQuoteCount);//引用计数加一
        return 1;
    }
    return 0;
}

int CFileOperate::Set(CFileOperate *disk,uint64_t BaseOffset)//通过其他的CFileOperate对象和基础偏移量设置本对象
{
    if(Set(disk))
    {
        mull_BaseOffset = BaseOffset;
        return 1;
    }
    return 0;
}

void CFileOperate::Close() //关闭文件函数,当引用计数不为0 的时候 只减少引用计数而不关闭句柄
{
    if(!INVALID_FILEHANDLE(m_fh))
    {
        if(mp_fhQuoteCount!=NULL)
        {
            //当存在引用计数执行这个分支
            --*mp_fhQuoteCount;//引用计数减一
            if(*mp_fhQuoteCount==0)//减到零了直接关句柄
            {
                delete mp_fhQuoteCount;
                mp_fhQuoteCount = NULL;
                f_SYS_CloseFile(m_fh);
            }
        }
        else
        {
            //不存在引用计数直接关闭句柄
            f_SYS_CloseFile(m_fh);
        }

        m_fh = INVALID_FILEHANDLE_VALUE;
    }
}

//*


CFileOperate& CFileOperate::operator=(CFileOperate& disk)
{
    Set(&disk);
    return *this;
}

//*/

CFileOperate::~CFileOperate()
{
    Close();
}

int CFileOperate::Open(const char* filename,bool ReadOnly,bool OpenAlways)
{
    Close();
    m_fh = f_SYS_OpenFile(filename,ReadOnly,OpenAlways);
    return !INVALID_FILEHANDLE(m_fh);

}

int CFileOperate::Read(void* buf,uint32_t len)
{
    return f_SYS_ReadFile(m_fh,buf,len);
}

int CFileOperate::Write(const void* buf,uint32_t len)
{
    return f_SYS_WriteFile(m_fh,buf,len);
}

uint64_t CFileOperate::SetPoint(uint64_t offset,int mode)
{
    return f_SYS_SetPoint(m_fh,offset + mull_BaseOffset,mode);
}

uint64_t CFileOperate::GetFileSize()
{
    return f_SYS_GetFileSize(m_fh);
}
