#include "CPartition.h"

CPartition::CPartition()
{
    mul_SectorSize = SECTOR_SIZE;
    mp_Disk = NULL;
}

CPartition::~CPartition()
{
    ;
}

const char* CPartition::GetPartitionTypeName(uint8_t type)
{
    const char* TypeName = "RAW";
    switch(type)
    {
        case 0X05:
        case 0X0F:TypeName =  "Extend";break;
        case 0X85:TypeName =  "Linux Extend";break;
        case 0X04:
        case 0X06:
        case 0X0E:
        case 0X16:
        case 0X14:TypeName = "FAT16";break;
        case 0X0B:
        case 0X0C:
        case 0X1B:
        case 0X1C:TypeName = "FAT32";break;
        case 0X07:
        case 0X17:TypeName = "NTFS";break;
        case 0X83:TypeName = "EXT";break;
        case 0XAF:TypeName = "HFSPLUS";break;
        default:break;
    }
    return TypeName;
}

bool CPartition::IsExternPartition(uint8_t type)
{
    return
        (         type == 0X05    //0X05 为Extend
               || type == 0X0F    //0X0F为Windows 95 Extend,0X85H 为Linux Extend
               || type == 0X85    //0X85H 为Linux Extend
        );
}

bool CPartition::IsFat16Partition(uint8_t type)
{
    return
        (
                type == 0X04    //FAT16小于32M
            ||  type == 0X06    //FAT16大于32M
            ||  type == 0X0E    //Win95  FAT16
            ||  type == 0X16    //Hidden FAT16
            ||  type == 0X14    //Hidden FAT16 小于32M
         );
}

//Exfat和Fat32标识符一样
bool CPartition::IsFat32Partition(uint8_t type)
{
    return
        (
                type == 0X0B    //Win95  FAT32
            ||  type == 0X0C    //Win95  FAT32
            ||  type == 0X1B    //Hidden FAT32
            ||  type == 0X1C    //Hidden FAT32 partition (using LBA-mode INT 13 extension)
         );
}

bool CPartition::IsNtfsPartition(uint8_t type)
{
    return
        (
                type == 0X07    //HPFS/NTFS
            ||  type == 0X17    //Hidden HPFS/NTFS
        );
}

bool CPartition::IsWindowsPartition(uint8_t type)
{
    return
        (
                IsFat16Partition(type)
            ||  IsFat32Partition(type)
            ||  IsNtfsPartition(type)
        );
}


int CPartition::GetExtPartition(vector<SPartition> *plist,uint64_t offset)
{
    if(plist!=NULL && this->mp_Disk!=NULL && this->mp_Disk->Usable() && mul_SectorSize>0 )
    {
        uint64_t BaseOffset = offset;
        SPartition sp;
        static EBR ebr;
        while( (this->mp_Disk->SetPoint(offset,CIO::START_POS),1)
              && this->mp_Disk->Read(&ebr,sizeof(ebr))
              && HEAD_SECTOR_USABLE((uint8_t*)&ebr)
              )
        {
            if(ebr.dpt[0].Type!=0)
            {
                {
                    sp.dpt = ebr.dpt[0];
                    sp.BaseOffset = offset;
                    plist->push_back(sp);
                }
            }
            else
            {
                break;
            }

            if(ebr.dpt[1].Type!=0)
            {
                //Ext信息
                if(IsExternPartition(ebr.dpt[1].Type))
                {
                    if(offset >= BaseOffset + PARTITION_OFFSET(ebr.dpt[1],this->mul_SectorSize))//一定程度上避免逻辑锁
                    {
                        return -1;
                    }
                    //跳转到下一个扩展分区数据
                    offset = BaseOffset + PARTITION_OFFSET(ebr.dpt[1],this->mul_SectorSize);
                }
            }
            else
            {
                return 1;
            }

        }
    }
    return 0;
}

int CPartition::GetPartition(vector<SPartition> *plist,MBR* pmbr)
{
    if(plist!=NULL && this->mp_Disk!=NULL && this->mp_Disk->Usable() && mul_SectorSize>0 )
    {
        SPartition sp;
        static MBR mbr;
        if (
                (
                    ( pmbr!=NULL && ( (mbr = *pmbr),1 ) ) //若指定了MBR,则直接使用该MBR
                ||  ( (this->mp_Disk->SetPoint(0,CIO::START_POS),1 ) && this->mp_Disk->Read(&mbr,sizeof(mbr))>0) //没有指定MBR,从硬盘头部读取MBR
                 )
                &&   HEAD_SECTOR_USABLE(&mbr) //判定是否是正确的MBR
           )
        {
            //主分区4个
            uint32_t i;
            for(i=0; i<4; ++i)
            {
                if(mbr.dpt[i].Type!=0)//Type为0未使用
                {
                     //把分区信息添加入vector
                    {
                        sp.dpt = mbr.dpt[i];
                        sp.BaseOffset = 0;
                        plist->push_back(sp);
                    }

                    if(IsExternPartition(mbr.dpt[i].Type))
                    {
                        //读取扩展分区数据
                        int Ret = GetExtPartition(plist,PARTITION_OFFSET(mbr.dpt[i],this->mul_SectorSize));
                        if(Ret<=0)
                        {
                            return Ret;
                        }
                    }
                }
            }
            return 1;
        }
    }
    return 1;
}
