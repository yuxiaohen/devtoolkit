#pragma once

#include "../SInfoSet.h"
#include "SPartitionInfo.h"

#include <vector>
using std::vector;

struct LIB_INTERFACE SPartitionInfoSet:public SInfoSet
{
public:
    SPartitionInfoSet();
    bool AddInfo(SInfo* pInfo);
    uint32_t ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize);
    uint32_t GetType(){return CFileSystem::PARTITION;}
    uint32_t size();
    SInfo* at(uint32_t index);
    void clear();

    vector <SPartitionInfo> m_InfoList;
    SPartitionInfo m_PartitionInfo;
    uint32_t mul_SectorSize;
};
