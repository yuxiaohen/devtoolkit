#ifndef SPARTITIONINFO_H
#define SPARTITIONINFO_H

#include "../SInfo.h"
#include "CPartition.h"

struct LIB_INTERFACE SPartitionInfo:public SInfo//分区结构信息
{
    uint32_t GetType()
    {
        return CFileSystem::PARTITION;
    }

    uint64_t GetOffset()
    {
        return PartitionOffset;
    }

    uint64_t PartitionOffset;//分区在磁盘中起始偏移量
    uint64_t PartitionSize;//分区大小
    uint8_t      PartitionType;//分区类型
};

#endif
