﻿#pragma once

#include "../drive_struct.h"
#include "../CIO.h"
#include "SPartitionInfoSet.h"

#include <vector>
using std::vector;

//代表分区的结构体,里面储存分区的信息,可以据此读取分区
struct SPartition
{
    //DPT结构体来自于 driver_struct.h,为MBR/EBR中的分区信息结构体,里面存储包括分区偏移扇区号的一系列磁盘数据
    DPT dpt;
    //如果是扩展分区,BaseOffset为扩展分区相对于硬盘起始位置的偏移量,如果是主分区 BaseOffset为0
    uint64_t BaseOffset;
};

//通过DPT结构获取分区相对偏移
#define PARTITION_OFFSET(x,sector_size) ( ((uint64_t)(x).Relative) * (sector_size) )

//分区大小
#define PARTITION_SIZE(x,sector_size) ( ((uint64_t)(x).Sectors) * (sector_size) )

//若需要通过SPartition获取分区在磁盘的绝对偏移量
//需要通过BaseOffset加上DPT结构体中储存的分区扇区号乘上扇区字节数来得到分区在磁盘上偏移量
#define ABS_OFFSET(x,sector_size) ( (x).BaseOffset + PARTITION_OFFSET((x).dpt,(sector_size) ) )


//CPartition类 用来打开硬盘或者硬盘镜像
//继承自CDisk,反正都是要读磁盘的,只是CPartition要寻找分区和创建CPartition对象,多了一点东西
class LIB_INTERFACE CPartition
{
public:
    CPartition();
virtual ~CPartition();

public:
    int Set(CIO* pDisk)
    {
        this->mp_Disk = pDisk;
        return 1;
    }
    //通过分区表获取硬盘上所有分区,结果通过vector向量输出
    //如果不指定MBR,则直接从磁盘读取MBR,否则通过MBR解析
    int GetPartition(vector<SPartition> *plist,MBR* pmbr = NULL);//返回值大于0 正常,等于0 读取错误,小于0 磁盘逻辑异常

    //在Offset读取EBR来获取扩展分区的所有分区,添加入向量中
    int GetExtPartition(vector<SPartition> *plist,uint64_t offset);//返回值大于0 正常,等于0 读取错误,小于0 磁盘逻辑异常

    //判断类型是否属于Extern分区
    static bool IsExternPartition(uint8_t type);

    //判断分区类型是否属于FAT16,FAT32,NTFS,EXFAT
    static bool IsWindowsPartition(uint8_t type);

    static bool IsFat16Partition(uint8_t type);

    static bool IsFat32Partition(uint8_t type);

    static bool IsNtfsPartition(uint8_t type);

    static const char* GetPartitionTypeName(uint8_t type);

    void SetSectorSize(uint32_t n)
    {
        mul_SectorSize = n;
    }
private:
    uint32_t mul_SectorSize;
    CIO * mp_Disk;
};
