#include "SPartitionInfoSet.h"
SPartitionInfoSet::SPartitionInfoSet()
{
    this->mul_SectorSize = SECTOR_SIZE;
}

bool SPartitionInfoSet::AddInfo(SInfo* pInfo)
{
    if(pInfo!=NULL && pInfo->GetType()==CFileSystem::PARTITION)
    {
        this->m_InfoList.push_back(*(SPartitionInfo*)pInfo);
        return true;
    }
    return false;
}
uint32_t SPartitionInfoSet::ReadBlock(uint64_t Offset,uint8_t* pBlock,uint32_t BlockSize)
{
    EBR* pEbr = (EBR*)pBlock;
    uint32_t Count = 0;
    uint32_t SectorSize = this->mul_SectorSize;
    while(Count*SectorSize<=BlockSize-SectorSize)//扫描获取到数据块,在数据块检测EBR是否正确
    {
        if(HEAD_SECTOR_USABLE(pEbr))
        {
            uint32_t i;
            for(i=0;i<sizeof(pEbr->mbr_code)/sizeof(uint8_t);++i)
            {
                if(pEbr->mbr_code[i]!='\0')
                {
                    continue;
                }
            }
            for(i=0;i<4;++i)
            {
                if(pEbr->dpt[i].State!=0X80 || pEbr->dpt[i].State!=0X00)
                {
                    continue;
                }

                this->m_PartitionInfo.PartitionType = pEbr->dpt[i].Type;
                this->m_PartitionInfo.PartitionOffset = Offset + Count*SectorSize + PARTITION_OFFSET(pEbr->dpt[i],SectorSize);
                this->m_PartitionInfo.PartitionSize = PARTITION_SIZE(pEbr->dpt[i],SectorSize);
                this->AddInfo(&this->m_PartitionInfo);
            }
        }

        pEbr += 1;
        ++Count;
    }
    return Count*SectorSize;
}

uint32_t SPartitionInfoSet::size()
{
    return this->m_InfoList.size();
}
SInfo* SPartitionInfoSet::at(uint32_t index)
{
    if(index>=this->m_InfoList.size())
    {
        return NULL;
    }
    return &this->m_InfoList.at(index);
}
void SPartitionInfoSet::clear()
{
    this->m_InfoList.clear();
}
