#pragma once

#include "SInfoSet.h"
#include "CFileSystem.h"

class LIB_INTERFACE CFileReader
{
public:
    virtual ~CFileReader(){}
    virtual bool SetFileSystem(CFileSystem* pFileSystem)=0;
    virtual bool Config(SInfoSet* pInfoSet)=0;
    virtual bool FromFile(SFileInfo* pInfo)=0;
    virtual uint32_t ReadBlock(uint32_t Index,uint8_t* OutBuf,uint32_t Size)=0;
    virtual uint32_t ReadNextBlock(uint8_t* OutBuf,uint32_t Size)=0;
};
