#pragma once

#include "sys_func.h"

typedef struct SDosDate
{
    uint8_t Day:5;//(1-31)
    uint8_t Month:4;//(1-12)
    uint8_t Year:7;//Year Offset from 1980, (add 1980 to get actual year)
}SDosDate;

typedef struct SDosTime
{
    uint8_t Second:5;//Second divided by 2
    uint8_t Minute:6;//(0-59)
    uint8_t Hour:5;//(0-23)
}SDosTime;

typedef struct SDosDateTime
{
    SDosDate Date;
    SDosTime Time;
}SDosDateTime;

#define DOSDATE_USABLE(x)  ( (1<=(x).Day && (x).Day<=31) && (1<=(x).Month && (x).Month<=12) )
#define DOSTIME_USABLE(x)  ( (0<=(x).Minute && (x).Minute<=59) && (0<=(x).Hour && (x).Hour<=23) )
