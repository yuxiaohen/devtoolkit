#include "FileSystem.h"

CFileSystem * newCFileSystemObject(CIO* pIO,uint64_t Offset,uint64_t Size)
{
    if(pIO==NULL)return NULL;
    CFileSystem *pFileSystem = NULL;

    pFileSystem = new CNtfs();
    pFileSystem->Set(pIO,Offset);
    if(pFileSystem->Config(0)>0 || pFileSystem->Config(Size)>0)
    {
        return pFileSystem;
    }
    delete pFileSystem;

    pFileSystem = new CFat16();
    pFileSystem->Set(pIO,Offset);
    if(pFileSystem->Config()>0)
    {
        return pFileSystem;
    }
    delete pFileSystem;

    pFileSystem = new CFat32();
    pFileSystem->Set(pIO,Offset);
    if(pFileSystem->Config()>0)
    {
        return pFileSystem;
    }
    delete pFileSystem;

    pFileSystem = new CExFat();
    pFileSystem->Set(pIO,Offset);
    if(pFileSystem->Config()>0)
    {
        return pFileSystem;
    }
    delete pFileSystem;

    pFileSystem = new CExt4();
    pFileSystem->Set(pIO,Offset);
    if(pFileSystem->Config()>0)
    {
        return pFileSystem;
    }
    delete pFileSystem;

    pFileSystem = new CExt2_3();
    pFileSystem->Set(pIO,Offset);
    if(pFileSystem->Config()>0)
    {
        return pFileSystem;
    }
    delete pFileSystem;

    pFileSystem = new CHfsPlus();
    pFileSystem->Set(pIO,Offset);
    if(pFileSystem->Config()>0)
    {
        return pFileSystem;
    }
    delete pFileSystem;

    pFileSystem = new CFileSystem();
    pFileSystem->Set(pIO,Offset);

    return pFileSystem;
}

