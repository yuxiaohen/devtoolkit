#include "free.h"

void FDBfFree(void** p)
{
    if(p && *p)
    {
        free(*p);
        (*p) = NULL;
    }
}
