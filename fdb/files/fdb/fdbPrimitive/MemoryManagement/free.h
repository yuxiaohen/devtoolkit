#ifndef __FDB_FREE_H__2013_08_08__
#define __FDB_FREE_H__2013_08_08__

#include "MemoryManagementlib.h"


#include <stdio.h>
#include <stdlib.h>

FDBInterface void FDBfFree(void** p);

template<typename _T>
void FDBfDeletet(_T** p)
{
    if(p && *p)
    {
        delete *p;
        *p = NULL;
    }
}

template<typename _T>
void FDBfFreet(_T** p)
{
    if(p && *p)
    {
        free(*p);
        *p = NULL;
    }
}

#endif
