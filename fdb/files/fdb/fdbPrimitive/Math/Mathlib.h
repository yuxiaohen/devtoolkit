#ifndef __fdb_math_lib_h_2013_08_30_05_04__
#define __fdb_math_lib_h_2013_08_30_05_04__

#include "../fdbPrimitivelib.h"

#define FDB_MAX(x,y) ( (x)>(y)?(x):(y) )
#define FDB_MIN(x,y) ( (x)<(y)?(x):(y) )

#endif //header
