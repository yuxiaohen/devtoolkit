﻿#pragma once

#include "Mathlib.h"
#include "convert/convert.h"

/*!
 * @brief 求devidend对devisior的进一取整除法值
 *          比如5对3的进一取整值,2
 */
FDBInterface int FDBfMath_cal_ceil_div(const int devidend,const int devisior);

/***********************************************************************
* 计算一个整数最小需要几个字节可以存下,比如一个int64_t i64 = 1,实际上只需要一个字节就可以存下
***********************************************************************/
template<typename _T>
int FDBfMath_cal_num_bytes(const _T& num)
{
	int bytes = 1;
	_T num_copy = num;
	while (1)
	{
		num_copy >>= 8;
		if (num_copy > 0)
		{
			++bytes;
		}
		else {
			break;
		}
	}
	return (bytes);
}
