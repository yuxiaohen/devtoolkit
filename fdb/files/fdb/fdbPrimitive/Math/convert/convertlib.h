﻿#pragma once

#include "../Mathlib.h"

static const union { char c[4]; unsigned long l; } endian_test = {'l','?','?','b'};

#if !defined(ENDIANNESS)
    #define ENDIANNESS ((char)endian_test.l)
#endif
