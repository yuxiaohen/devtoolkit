﻿#ifndef __convert_h__
#define __convert_h__

#include <algorithm>

#include "convertlib.h"

#include "../../Type/Type.h"

#include <string.h>

/*!
 * @brief big-endian buffer convert to a host 64 bit integer value
 * @note 不被重视
 */
FDBInterface int64_t FDBfMath_be_array_to_hosti64(const void* p_be_array,const int i_len);

/*!
 * @brief big-endian buffer convert to a host unsigned 32 bit integer value
 * @note 不被重视
 */
FDBInterface uint32_t FDBfMath_be_array_to_hostu32(const void* p_be_array,const int i_array_len);

/*!
 * @brief big-endian buffer convert to a host unsigned 16 bit integer value
 * @note 不被重视
 */
FDBInterface uint16_t FDBfMath_be_array_to_hostu16(const void* p_be_array,const int i_len);

//都是先补高位,然后移位补低位
/*!
 * @brief 转换大端buffer到本地值
 */
template<typename _T1>
inline void FDBfMath_be_array_to_host_value(const void* p_be_array,const int i_array_len,_T1* p_t_dest)
{
    memset(p_t_dest,0,sizeof(_T1));
    int need_copy_bytes = std::min(i_array_len,(int)sizeof(_T1));
    if(ENDIANNESS == 'l') { //host little-endian
        uint8_t* p_dest = (uint8_t*)p_t_dest;
        for( int i=0;i < need_copy_bytes;++ i )
        {
            p_dest[sizeof(_T1)-1-i] = ((const uint8_t*)p_be_array)[i]; /*对大端buffer来说,前面的(高位)才是优先需要的*/
        }
    } else { //host big-endian
        memcpy( p_t_dest,p_be_array,need_copy_bytes );
    }
    //不足的字节要移位
    int lack_bytes = sizeof(_T1) - i_array_len;
    if(lack_bytes > 0)
    {
        (*p_t_dest) >>= (lack_bytes*8);
    }
}

/*!
 * @brief 转换小端buffer到本地值
 */
template<typename _T1>
inline void FDBfMath_le_array_to_host_value(const void* p_le_array,const int i_array_len,_T1* p_t_dest)
{
    memset(p_t_dest,0,sizeof(_T1));
    int need_copy_bytes = std::min(i_array_len,(int)sizeof(_T1)); //需要拷贝的字节数
    if( ENDIANNESS == 'l' ) { //host littel-endian
        uint8_t* p_dest = (uint8_t*)p_t_dest;
        for(int i=0;i < need_copy_bytes;++ i)
        {
            p_dest[sizeof(_T1)-1-i] = ((const uint8_t*)p_le_array)[i_array_len-1-i];
        }
    } else { //host big-endian
        uint8_t* p_dest = (uint8_t*)p_t_dest;
        for( int i=0;i < need_copy_bytes;++ i )
        {
            p_dest[i] = ((const uint8_t*)p_le_array)[i_array_len-1-i]; /*对于小端buffer来说,后面的(高位)才是优先需要的*/
        }
    }
    //不足的字节要移位
    int lack_bytes = sizeof(_T1) - i_array_len;
    if(lack_bytes > 0)
    {
        (*p_t_dest) >>= (lack_bytes*8);
    }
}

/*!
 * @brief convert a host value fill a big-endian buffer
 */
template<typename _T1>
inline void FDBfMath_host_value_to_be_array(void* p_dest,const _T1 hostValue,int i_bytes_to_convert)
{
    memset(p_dest,0,i_bytes_to_convert);
    if(ENDIANNESS == 'l') { //little-endian
        const uint8_t* p=(const uint8_t*)&hostValue;
        for(int i=0;i<std::min((int)sizeof(hostValue),i_bytes_to_convert);++ i)
        {
            ((uint8_t*)p_dest)[i_bytes_to_convert-1-i] = p[i]; //fill dest low byte to high byte(copy p from low byte to high byte)
        }
    } else  //big-endian
    {
        memcpy(p_dest,&hostValue,std::min((int)sizeof(hostValue),i_bytes_to_convert));
    }
}

/*!
 * @brief 转换一个本地值到小端buffer
 */
template<typename _T1>
inline void FDBfMath_host_value_to_le_array(void* p_dest,const _T1 t_host_value,int i_bytes_to_convert)
{
    memset(p_dest,0,i_bytes_to_convert);
    if(ENDIANNESS == 'l') { //host little-endian
        memcpy(p_dest,&t_host_value,std::min((int)sizeof(t_host_value),i_bytes_to_convert));
    } else { //host big-endian
        const uint8_t* p=(const uint8_t*)&t_host_value;
        for(int i=0;i<std::min((int)sizeof(t_host_value),i_bytes_to_convert);++ i)
        {
            ((uint8_t*)p_dest)[i_bytes_to_convert-1-i] = p[i];
        }
    }
}

#endif //header
