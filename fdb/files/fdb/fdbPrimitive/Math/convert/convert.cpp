﻿#include "convert.h"

int64_t FDBfMath_be_array_to_hosti64(const void* p_be_array,const int i_len)
{
    long long int i64=0;
    if(ENDIANNESS == 'l') {   //little-endian
        char* p=(char*)&i64;
        for(int i2=0;i2<i_len;++ i2) {
            memcpy(p+i2,(const uint8_t*)p_be_array+i_len-1-i2,1);
        }
    } else { //big-endian
        memcpy(&i64,p_be_array,i_len);
    }
    return i64;
}

uint32_t FDBfMath_be_array_to_hostu32(const void* p_be_array, const int i_array_len)
{
    unsigned long u32=0;
    if(ENDIANNESS == 'l') {   //little-endian
        char* p=(char*)&u32;
        for(int i2=0;i2<i_array_len;++ i2) {
            memcpy(p+i2,(const uint8_t*)p_be_array+i_array_len-1-i2,1);
        }
    } else {  //big-endian
        memcpy(&u32,p_be_array,i_array_len);
    }
    return u32;
}
uint16_t FDBfMath_be_array_to_hostu16(const void* p_be_array,const int i_len)
{
    uint16_t u16=0;
    if(ENDIANNESS == 'l') {   //little-endian
        char* p=(char*)&u16;
        for(int i2=0;i2<i_len;++ i2) {
            memcpy(p+i2,(const uint8_t*)p_be_array+i_len-1-i2,1);
        }
    } else { //big-endian
        memcpy(&u16,p_be_array,i_len);
    }
    return u16;
}
