win32 {
    SOURCES += \
        $$PWD/IODevice/Win32IODevice.cpp \
        $$PWD/IODevice/Win32IOSystemDevice.cpp
}
linux {
    SOURCES += \
        $$PWD/IODevice/LinuxIODevice.cpp
}
mac {
    SOURCES += \
        $$PWD/IODevice/DarwinIODevice.cpp
}
linux | mac {
    #why iosystemdevice is common?because
    #getsizethrough path is implemented in
    #iodevice,so iosystemdevice have no this
    #interface
    SOURCES += \
        $$PWD/IODevice/PosixIOSystemDevice.cpp
}

SOURCES += \
    $$PWD/CIO.cpp \
    $$PWD/IOBuffer/IOJITBuffer.cpp \
    $$PWD/IOServicePool/IOServicePool.cpp \
    $$PWD/FFile/FFile.cpp
