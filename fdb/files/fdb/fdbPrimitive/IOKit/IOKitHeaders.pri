linux | mac {
    DEFINES += _FDB_POSIX _LARGEFILE64_SOURCE=1 _FILE_OFFSET_BITS=64
}

win32 {
    HEADERS += \
        $$PWD/IODevice/Win32IOSystemDevice.h \
        $$PWD/IODevice/Win32IODevice.h
}
linux | mac {
    HEADERS += \
        $$PWD/IODevice/PosixIOSystemDevice.h \
        $$PWD/IODevice/PosixIODevice.h
}

HEADERS += \
    $$PWD/IOKit.h \
    $$PWD/IOKitlib.h \
    $$PWD/CIO.h \
    #IODevice
    $$PWD/IODevice/IODevice.h \
    $$PWD/IODevice/IODevicelib.h \
    $$PWD/IODevice/IOSystemDevice.h \
    $$PWD/IODevice/IOAccessibleDevice.h \
    #IO Buffer
    $$PWD/IOBuffer/IOJITBuffer.h \
    #io service pool
    $$PWD/IOServicePool/IOServicePool.h \
    #File
    $$PWD/FFile/FFile.h
