/* 2013-08-16
 * brief: accessible i/o device
 * author:shawhen dsc
 * version: 0.2.0
 *
 * visiable lack:
 *  1.support more platform
 *  2.support async i/o
 *  3.if i/o device is a large file,we can use memory mapping file
 *  4.support other bytes/sector(e.g. 4096),now only 512
 *
 *  LICENSE: LGPL
 */
#ifndef __fdb_io_device_h_2013_08_08_09_59__
#define __fdb_io_device_h_2013_08_08_09_59__

#include "IODevicelib.h"

#include "IOSystemDevice.h"

#if defined(_WIN32)
    #include "Win32IODevice.h"
#elif defined(__APPLE__) || defined(__linux__)
    #include "PosixIODevice.h"
#else
#error "Unsupported platfrom"
#endif

FDBInterface FDBCIODevice *FDBfIODevice_new_not_open(const char* pcDevicePath);
FDBInterface void FDBfIODevice_delete(FDBCIODevice** ppobjIODevice);

/*!
 * @return -1 for failed
 */
FDBInterface int FDBfIODevice_open(FDBCIODevice* pobjIODevice,enum FDBEAccessFlag eAccessFlag,enum FDBEOpenMethod eOpenMethod);

/*!
 *@return -1 for failed.actually read bytes for succeed
 */
FDBInterface int32_t FDBfIODevice_read(FDBCIODevice* pobjIODevice, void* pBuf, const unsigned long u32BytesToRead);

/*!
 *@brief this call should promise read from i64ReadBeginOffset with i32BytesToRead non-terminatable
 *@return -1 for failed.actually read bytes for succeed
 */
FDBInterface int32_t FDBfIODevice_read_from(FDBCIODevice* pobjIODevice, void* pBuf
                                        , const long long int i64ReadBeginOffset
                                        , const unsigned long u32BytesToRead);

/*!
 @return -1 for failed.
 */
FDBInterface int32_t FDBfIODevice_write(FDBCIODevice* pobjIODevice, const void* pBuf, const unsigned long u32BytesToWrite);

FDBInterface int64_t FDBfIODevice_size(FDBCIODevice* pobjIODevice);

/*!
 @return -1 for failed.
 */
FDBInterface int64_t FDBfIODevice_seek(FDBCIODevice* pobjIODevice
                                             , const long long i64Offset
                                             , const unsigned long u32SeekFrom);

/*!
 @return -1 for failed.
 */
FDBInterface int64_t FDBfIODecvice_tell(FDBCIODevice* pobjIODevice);

/*!
 * @return -1 for failed.0 for succeed
 */
FDBInterface int FDBfIODevice_flush(FDBCIODevice* pobjIODevice);


/*!
 * @return specified device path's size,Drive or file
 */
FDBInterface int64_t FDBfIODeviceSizeThroughPath(const char* pcDevicePath);

#endif
