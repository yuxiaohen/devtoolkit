﻿#if defined(_WIN32)

#ifndef __fdb_win32_io_system_device_h_2013_09_29__
#define __fdb_win32_io_system_device_h_2013_09_29__

#include <Windows.h>
#include <stdint.h>

#include "IODevicelib.h"

class FDBInterface FDBCIOSystemDevice {
public:
    HANDLE device;
    int64_t i64_device_size;
    char pc_device_path[1024];

    FDBCIOSystemDevice(const char* pc_device_path)
    {
        this->device = INVALID_HANDLE_VALUE;
        this->i64_device_size = 0;
        memset(this->pc_device_path,0,1024);
        strncpy(this->pc_device_path,pc_device_path,1024);
    }
    ~FDBCIOSystemDevice()
    {
        CloseHandle( this->device );
    }
};

#endif //header

#endif //platform
