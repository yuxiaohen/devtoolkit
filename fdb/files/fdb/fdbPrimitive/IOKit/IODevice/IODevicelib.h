#ifndef __fdb_io_device_lib_h_2013_08_21_12_02__
#define __fdb_io_device_lib_h_2013_08_21_12_02__

#include "../IOKitlib.h"

#define IO_WAIT_TIME_MILLISECONDS 600000

typedef enum FDBEIOInstruction {
    FDBEIOInstruction_Read
    ,FDBEIOInstruction_Write
    ,FDBEIOInstruction_Seek
    ,FDBEIOInstruction_Flush
} FDBEIOInstruction;

typedef enum FDBEIODeviceType {
    FDBEIODeviceType_disk
    ,FDBEIODeviceType_file
} FDBEIODeviceType;

#if defined(__APPLE__)
#define O_LARGEFILE 0x00
#endif

#if defined(_WIN32)
    #include <Windows.h>
    //access flag
    typedef enum FDBEAccessFlag {
        FDBEAccessFlag_AtrributesOnly = 0x00
        ,FDBEAccessFlag_ReadOnly = GENERIC_READ
        ,FDBEAccessFlag_WriteOnly = GENERIC_WRITE
        ,FDBEAccessFlag_ReadWrite = FDBEAccessFlag_ReadOnly | FDBEAccessFlag_WriteOnly
    } FDBEAccessFlag;
    //open method
    /*                  FileExist-Create     FileNotExist-Create
     * OpenExisting      succeed    no         failed       no
     * AlwaysCreateNew   succeed    yes        succeed      yes
     * NotExistCreateNew failed     no         succeed      yes
     * AlwaysOpen        succeed    no         succeed      yes
     */
    typedef enum FDBEOpenMethod {
        FDBEOpenMethod_OpenExisting = OPEN_EXISTING
        ,FDBEOpenMethod_AlwaysCreateNew = CREATE_ALWAYS
        ,FDBEOpenMethod_NotExistCreateNew = CREATE_NEW
        ,FDBEOpenMethod_AlwaysOpen = OPEN_ALWAYS
    } FDBEOpenMethod;
#elif defined(__linux__) || defined(__APPLE__)
    #include <fcntl.h>
    typedef enum FDBEAccessFlag {
        FDBEAccessFlag_AttributesOnly = 0x00 | O_LARGEFILE
        ,FDBEAccessFlag_ReadOnly = O_RDONLY | O_LARGEFILE
        ,FDBEAccessFlag_WriteOnly = O_WRONLY | O_LARGEFILE
        ,FDBEAccessFlag_ReadWrite = O_RDWR | O_LARGEFILE
    } FDBEAccessFlag;
    typedef enum FDBEOpenMethod {
        FDBEOpenMethod_OpenExisting = 0x00
        ,FDBEOpenMethod_AlwaysCreateNew = O_CREAT
        ,FDBEOpenMethod_NotExistCreateNew = O_CREAT | O_EXCL
        ,FDBEOpenMethod_AlwaysOpen = O_CREAT | O_APPEND
    } FDBEOpenMethod;
#else
    #error "Unsupport Platform"
#endif

#endif //header
