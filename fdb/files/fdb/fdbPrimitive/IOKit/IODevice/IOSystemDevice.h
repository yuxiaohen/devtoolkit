/* 2013-09-29
 * 提供原始操作系统IO设备,不加以任何保护措施
 *
 */

#ifndef __fdb_io_system_device_h_2013_09_29__
#define __fdb_io_system_device_h_2013_09_29__

#include "IODevicelib.h"

#if defined(_WIN32)
    #include "Win32IOSystemDevice.h"
#elif defined(__APPLE__) || defined(__linux__)
    #include "PosixIOSystemDevice.h"
#else
    #error "Unsupport Platform"
#endif

FDBInterface FDBCIOSystemDevice *FDBfIOSystemDevice_new_not_open(const char* pc_device_path);
FDBInterface void FDBfIOSystemDevice_delete(FDBCIOSystemDevice** pp_obj_IOSystemDevice);

/*!
 * @return -1 for failed
 */
FDBInterface int FDBfIOSystemDevice_open(FDBCIOSystemDevice* p_obj_IODevice
                                         , enum FDBEAccessFlag e_access_flag
                                         , enum FDBEOpenMethod e_open_method);

/*!
 *@return -1 for failed.actually read bytes for succeed
 */
FDBInterface int32_t FDBfIOSystemDevice_read(FDBCIOSystemDevice* pobjIOSystemDevice
                                             , void* pBuf
                                             , const uint32_t u32BytesToRead);

/*!
 *@brief this call should promise read from i64ReadBeginOffset with i32BytesToRead non-terminatable
 *@return -1 for failed.actually read bytes for succeed
 */
FDBInterface int32_t FDBfIOSystemDevice_read_from(FDBCIOSystemDevice* p_obj_IOSystemDevice
                                                  , void* p_buf
                                                  , const int64_t i64_read_begin_offset
                                                  , const uint32_t u32_bytes_to_read);

/*!
 @return -1 for failed.
 */
FDBInterface int32_t FDBfIOSystemDevice_write(FDBCIOSystemDevice* p_obj_IOSystemDevice
                                              , const void* p_buf
                                              , const uint32_t u32_bytes_to_write);

FDBInterface int64_t FDBfIOSystemDevice_size(FDBCIOSystemDevice* p_obj_IODevice);

/*!
 @return -1 for failed.
 */
FDBInterface int64_t FDBfIOSystemDevice_seek(FDBCIOSystemDevice* p_obj_IOSystemDevice
                                             , const int64_t i64_offset
                                             , const uint32_t u32_seek_from);

/*!
 @return -1 for failed.
 */
FDBInterface int64_t FDBfIOSystemDecvice_tell(FDBCIOSystemDevice* pobjIOSystemDevice);

#endif //header
