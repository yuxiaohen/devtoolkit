#if defined(__linux__) || defined(__APPLE__)

#ifndef __fdb_posix_io_device_h_2013_08_25_19_20__
#define __fdb_posix_io_device_h_2013_08_25_19_20__

#include "IODevicelib.h"

#ifndef Q_MOC_RUN
    #include <boost/thread.hpp>
    #include <boost/interprocess/sync/interprocess_semaphore.hpp>
#endif

#include <unistd.h>
typedef struct FDBCIODevice {
    int device;
    FDBEIODeviceType eIODeviceType;
    long long int i64DeviceSize;

    boost::mutex* pobjInstructLocker;

    boost::thread* pobjWorkThread;
    boost::interprocess::interprocess_semaphore* pobjGoToWorkKick;
    boost::interprocess::interprocess_semaphore* pobjWorkReportKick;

    FDBEIOInstruction eIOInstruction;
    long long int i64ReadWriteOffsetSeekLen; /*read from(or write from or seek distance) offset*/
    unsigned long u32ReadWriteDataLenOrSeekFrom; /*read/write data length or seek from*/
    char* pcReadWriteDestination; /*read into or write from data buffer*/
    long long int i64InstructionResult; /*read/write bytes,or seek result*/

    char* pcDevicePath;
    int bCallForStop;
} FDBCIODevice;

#endif //header

#endif
