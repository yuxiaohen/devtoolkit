#if defined(__APPLE__) || (__linux__)

#ifndef __fdb_posix_io_system_device_h_2013_10_22__
#define __fdb_posix_io_system_device_h_2013_10_22__

#include <unistd.h>
#include <fdb/fdbPrimitive/Type/Type.h>

typedef struct FDBCIOSystemDevice {
    int device;
    int64_t i64_device_size;

    char* pc_device_path;
} FDBCIOSystemDevice;

#endif //header

#endif //platform
