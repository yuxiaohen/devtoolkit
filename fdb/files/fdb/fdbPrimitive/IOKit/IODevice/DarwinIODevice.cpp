#if defined(__APPLE__)

#include "IODevice.h"

#ifndef Q_MOC_RUN
    #include <boost/process.hpp>
    #include <boost/iostreams/device/file_descriptor.hpp>
    #include <boost/iostreams/stream.hpp>
#endif

#include <sys/fcntl.h>

#include <time.h>

#include "../../MemoryManagement/MemoryManagement.h"
#include "../../Exception/Exception.h"

void* fIODeviceWorkThread(void* pParam)
{
    FDBCIODevice* pobjIODevice = (FDBCIODevice*)pParam;
    if(!pobjIODevice)
    {
        return (NULL);
    }

    while(1)
    {
        pobjIODevice->pobjGoToWorkKick->wait();

        if(pobjIODevice->bCallForStop)
        {
            pobjIODevice->pobjWorkReportKick->post();
            return (NULL);
        }
        switch( pobjIODevice->eIOInstruction )
        {
            case FDBEIOInstruction_Read:
            {
                long i32EveryCircleReadBytes = 0;
                unsigned long u32ProperBytesToRead = 0;
                unsigned long u32TotalReadBytes = 0;

                u32ProperBytesToRead
                        = pobjIODevice->i64ReadWriteOffsetSeekLen+pobjIODevice->u32ReadWriteDataLenOrSeekFrom
                            < pobjIODevice->i64DeviceSize ? pobjIODevice->u32ReadWriteDataLenOrSeekFrom
                            : pobjIODevice->i64DeviceSize - pobjIODevice->i64ReadWriteOffsetSeekLen;

                do
                {
                    i32EveryCircleReadBytes = read(pobjIODevice->device
                                                   ,pobjIODevice->pcReadWriteDestination + u32TotalReadBytes
                                                   ,u32ProperBytesToRead - u32TotalReadBytes
                                                   );
                    if(i32EveryCircleReadBytes <= 0)
                    {
                        break;
                    }
                    u32TotalReadBytes += i32EveryCircleReadBytes;
                } while( u32TotalReadBytes < pobjIODevice->u32ReadWriteDataLenOrSeekFrom );
                pobjIODevice->i64InstructionResult = u32TotalReadBytes ? u32TotalReadBytes : -1;
            }break;
            case FDBEIOInstruction_Write:
            {
                long i32EveryCircleWriteBytes = 0;
                unsigned long u32TotalWriteBytes = 0;
                do
                {
                    i32EveryCircleWriteBytes = write(pobjIODevice->device
                                                     ,pobjIODevice->pcReadWriteDestination + u32TotalWriteBytes
                                                     ,pobjIODevice->u32ReadWriteDataLenOrSeekFrom - u32TotalWriteBytes);
                    if(i32EveryCircleWriteBytes <= 0)
                    {
                        break;
                    }
                    u32TotalWriteBytes += i32EveryCircleWriteBytes;
                } while( u32TotalWriteBytes < pobjIODevice->u32ReadWriteDataLenOrSeekFrom );
                pobjIODevice->i64InstructionResult = u32TotalWriteBytes ? u32TotalWriteBytes : -1;
            }break;
            case FDBEIOInstruction_Seek:
            {
                pobjIODevice->i64InstructionResult = lseek(pobjIODevice->device
                      ,pobjIODevice->i64ReadWriteOffsetSeekLen
                      ,pobjIODevice->u32ReadWriteDataLenOrSeekFrom);

            }break;
            case FDBEIOInstruction_Flush:
            {
                pobjIODevice->i64InstructionResult = 1;
            }break;
        } //instruction switch

        //work over,report
        pobjIODevice->pobjWorkReportKick->post();
    } // end while
    return (NULL);
}

FDBCIODevice* FDBfIODevice_new_not_open(const char *pcDevicePath)
{
    FDBCIODevice* pobjIODevice = new FDBCIODevice();
    if(!pobjIODevice)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }
    pobjIODevice->pcDevicePath = (char*)calloc(1,strlen(pcDevicePath)+1);
    if(!pobjIODevice->pcDevicePath)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's path",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's path",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }
    strcpy(pobjIODevice->pcDevicePath,pcDevicePath);
    if( strstr(pobjIODevice->pcDevicePath,"/dev/rdisk") )
    {
        pobjIODevice->eIODeviceType = FDBEIODeviceType_disk;
    } else {
        pobjIODevice->eIODeviceType = FDBEIODeviceType_file;
    }

    /*alloc instruct locker,goto work kick,work report kick,work thread*/
    pobjIODevice->pobjInstructLocker = new boost::mutex();
    if(!pobjIODevice->pobjInstructLocker)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's instruct locker",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's instruct locker",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    pobjIODevice->pobjGoToWorkKick = new boost::interprocess::interprocess_semaphore(0);
    if(!pobjIODevice->pobjGoToWorkKick)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's goto work kick",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's goto work kick",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    pobjIODevice->pobjWorkReportKick = new boost::interprocess::interprocess_semaphore(0);
    if(!pobjIODevice->pobjWorkReportKick)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's work report kick",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's work report kick",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
    if(!pobjIODevice->pobjWorkThread)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's work report kick",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's work report kick",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    return (pobjIODevice);
}

void FDBfIODevice_delete(FDBCIODevice **ppobjIODevice)
{
    if(*ppobjIODevice)
    {
        (*ppobjIODevice)->pobjInstructLocker->lock();

        (*ppobjIODevice)->bCallForStop = 1;
        (*ppobjIODevice)->pobjGoToWorkKick->post();
        (*ppobjIODevice)->pobjWorkReportKick->wait();
        (*ppobjIODevice)->pobjWorkThread->join();
        delete (*ppobjIODevice)->pobjWorkThread;
        delete (*ppobjIODevice)->pobjGoToWorkKick;
        delete (*ppobjIODevice)->pobjWorkReportKick;

        (*ppobjIODevice)->pobjInstructLocker->unlock();
        delete (*ppobjIODevice)->pobjInstructLocker;
        FDBfFree( (void**)(&(*ppobjIODevice)->pcDevicePath) );
        close( (*ppobjIODevice)->device );
        FDBfFree( (void**)ppobjIODevice );
    }
}

int FDBfIODevice_open(FDBCIODevice *pobjIODevice
                      , FDBEAccessFlag eAccessFlag
                      , FDBEOpenMethod eOpenMethod)
{
    if(!pobjIODevice)
    {
        return (-1);
    }
    pobjIODevice->device = open(pobjIODevice->pcDevicePath,eAccessFlag | eOpenMethod);
    if(pobjIODevice->device < 0)
    {
        giFDBErrNum = errno;

        FDBErrStr_THREE_PARAM("%s,error code:%d,path:%s"
                              ,"open device failed"
                              ,giFDBErrNum
                              ,pobjIODevice->pcDevicePath);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE
                           ,"%s,error code:%d,path:%s"
                           ,"open device failed"
                           ,giFDBErrNum
                           ,pobjIODevice->pcDevicePath);
        return (-1);
    }
    //if this iodevice open only for read,i should calculate it's size for once
    if(eAccessFlag == FDBEAccessFlag_ReadOnly)
    {
        pobjIODevice->i64DeviceSize = FDBfIODeviceSizeThroughPath(pobjIODevice->pcDevicePath);
    }

    return (0);
}

int32_t FDBfIODevice_read(FDBCIODevice *pobjIODevice, void *pBuf, const unsigned long u32BytesToRead)
{
    int32_t i32Ret = 0;
    if(!pobjIODevice || !pBuf || pobjIODevice->device < 0)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,read %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,read %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);

        return (-1);
    }
    if(u32BytesToRead == 0)
    {
        return (0);
    }

    pobjIODevice->pobjInstructLocker->lock();

    pobjIODevice->eIOInstruction = FDBEIOInstruction_Read;
    pobjIODevice->pcReadWriteDestination = (char*)pBuf;
    pobjIODevice->u32ReadWriteDataLenOrSeekFrom = u32BytesToRead;

    bool bWaitResult = false;
    long i32ReadBytes = 0;
    boost::system_time objDeadline =
            boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS+u32BytesToRead/(1024*1024));
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitResult = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);

    if(bWaitResult)
    {
        if(i32ReadBytes <= 0)
        {
            giFDBErrNum = errno;
            FDBErrStr_THREE_PARAM("iodevice read error,read %d bytes into 0x%x,error code:%d"
                                  ,u32BytesToRead,pBuf,giFDBErrNum);
            FDBLOG_THREE_PARAM(FDB_LOG_FILE,"iodevice read error,read %d bytes into 0x%x,error code:%d"
                               ,u32BytesToRead,pBuf,giFDBErrNum);
        }
        i32Ret = i32ReadBytes;
    } else {
        //cancel io request...!!!NOT IMPLEMENT!!!
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice read request time_out,restart work thread failed"
                                ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice read request time_out,restart work thread failed"
                             ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_FOUR_PARAM("%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
        i32Ret = -1;
    }

    pobjIODevice->pobjInstructLocker->unlock();

    return (i32Ret);
}

int32_t FDBfIODevice_read_from(FDBCIODevice *pobjIODevice
                           , void *pBuf, const long long i64ReadBeginOffset
                           , const unsigned long u32BytesToRead)
{
    long bWaitRet = 0;
    long i32ReadBytes = 0;
    long i32Ret = 0;
    long long int i64SeekResult = 0;

    if(!pobjIODevice || !pBuf || pobjIODevice->device == 0)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,read %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,read %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);

        return (-1);
    }
    if(u32BytesToRead == 0)
    {
        return (0);
    }

    pobjIODevice->pobjInstructLocker->lock();

    /*seek instruction*/
    pobjIODevice->eIOInstruction = FDBEIOInstruction_Seek;
    pobjIODevice->i64ReadWriteOffsetSeekLen = i64ReadBeginOffset;
    pobjIODevice->u32ReadWriteDataLenOrSeekFrom = SEEK_SET;

    boost::system_time objDeadline = boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS);
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitRet = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);
    i64SeekResult = pobjIODevice->i64InstructionResult;

    if(bWaitRet)
    {
        if(i64SeekResult < 0LL)
        {
            giFDBErrNum = errno;
            FDBErrStr_TWO_PARAM("iodevice seek(read_from) error,seek %lld,error code:%d"
                                ,i64ReadBeginOffset,giFDBErrNum);
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"iodevice seek(read_from) error,seek %lld,error code:%d"
                             ,i64ReadBeginOffset,giFDBErrNum);
            i32Ret = -1;
        }
    } else {
        //cancel io request...!!!NOT IMPLEMENT!!!
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice read request time_out,restart work thread failed"
                                ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice read request time_out,restart work thread failed"
                             ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_FOUR_PARAM("%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
        i32Ret = -1;
    }

    /*read instruction*/
    if(i64SeekResult < 0LL)
    {
        i32Ret = i64SeekResult;
    } else {
        pobjIODevice->eIOInstruction = FDBEIOInstruction_Read;
        pobjIODevice->pcReadWriteDestination = (char*)pBuf;
        pobjIODevice->u32ReadWriteDataLenOrSeekFrom = u32BytesToRead;

        boost::system_time objDeadline = boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS);
        pobjIODevice->pobjGoToWorkKick->post();
        bWaitRet = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);
        i32ReadBytes = pobjIODevice->i64InstructionResult;

        if(bWaitRet)
        {
            if(i32ReadBytes <= 0)
            {
                giFDBErrNum = errno;
                FDBErrStr_THREE_PARAM("iodevice read(read_from) error,read %d bytes into 0x%x,error code:%d"
                                      ,u32BytesToRead,pBuf,giFDBErrNum);
                FDBLOG_THREE_PARAM(FDB_LOG_FILE,"iodevice read(read_from) error,read %d bytes into 0x%x,error code:%d"
                                   ,u32BytesToRead,pBuf,giFDBErrNum);
            }
            i32Ret = i32ReadBytes;
        } else {
            //cancel io request...!!!NOT IMPLEMENT!!!
            pobjIODevice->pobjWorkThread->interrupt();
            delete (pobjIODevice->pobjWorkThread);
            pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
            if( !pobjIODevice->pobjWorkThread )
            {
                giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
                FDBErrStr_ONE_PARAM("%s,when iodevice read request time_out,restart work thread failed"
                                    ,FDB_ERR_MEM_NOT_ENOUGH_STR);
                FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice read request time_out,restart work thread failed"
                                 ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            }
            /*error handle*/
            giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
            FDBErrStr_FOUR_PARAM("%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                                 ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
            FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                              ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
            i32Ret = -1;
        }
    } //seek succeed

    pobjIODevice->pobjInstructLocker->unlock();

    return (i32Ret);
}

int32_t FDBfIODevice_write(FDBCIODevice *pobjIODevice, const void *pBuf, const unsigned long u32BytesToWrite)
{
    long i32Ret = 0;
    if(!pobjIODevice || !pBuf || pobjIODevice->device < 0)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,write %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToWrite,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,write %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToWrite,pBuf,giFDBErrNum);

        return (-1);
    }
    if(u32BytesToWrite == 0)
    {
        return (0);
    }

    pobjIODevice->pobjInstructLocker->lock();

    pobjIODevice->eIOInstruction = FDBEIOInstruction_Write;
    pobjIODevice->pcReadWriteDestination = (char*)pBuf;
    pobjIODevice->u32ReadWriteDataLenOrSeekFrom = u32BytesToWrite;

    bool bWaitResult = false;
    long i32ReadBytes = 0;
    boost::system_time objDeadline =
            boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS+u32BytesToWrite/(1024*1024));
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitResult = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);

    if(bWaitResult)
    {
        if(i32ReadBytes <= 0)
        {
            giFDBErrNum = errno;
            FDBErrStr_THREE_PARAM("iodevice write error,read %d bytes into 0x%x,error code:%d"
                                  ,u32BytesToWrite,pBuf,giFDBErrNum);
            FDBLOG_THREE_PARAM(FDB_LOG_FILE,"iodevice write error,read %d bytes into 0x%x,error code:%d"
                               ,u32BytesToWrite,pBuf,giFDBErrNum);
        }
        i32Ret = i32ReadBytes;
    } else {
        //cancel io request...!!!NOT IMPLEMENT!!!
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice write request time_out,restart work thread failed"
                                ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice write request time_out,restart work thread failed"
                             ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_FOUR_PARAM("%s,iodevice write error,write %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToWrite,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice write error,write %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToWrite,pBuf,giFDBErrNum);
        i32Ret = -1;
    }

    pobjIODevice->pobjInstructLocker->unlock();

    return (i32Ret);
}

long long int FDBfIODevice_seek(FDBCIODevice *pobjIODevice
                                , const long long i64Offset
                                , const unsigned long u32SeekFrom)
{
    long bWaitRet = 0;
    long long int i64SeekResult = 0;
    long long int i64Ret = 0;
    if(!pobjIODevice)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,seek %lld from %d,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,i64Offset,u32SeekFrom,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,seek %lld from %d,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,i64Offset,u32SeekFrom,giFDBErrNum);

        return (-1);
    }
    pobjIODevice->pobjInstructLocker->lock();

    pobjIODevice->eIOInstruction = FDBEIOInstruction_Seek;
    pobjIODevice->i64ReadWriteOffsetSeekLen = i64Offset;
    pobjIODevice->u32ReadWriteDataLenOrSeekFrom = u32SeekFrom;

    boost::system_time objDeadline = boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS);
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitRet = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);
    i64SeekResult = pobjIODevice->i64InstructionResult;

    if(bWaitRet)
    {
        if(i64SeekResult <= 0)
        {
            giFDBErrNum = errno;
                FDBErrStr_THREE_PARAM("iodevice seek error,seek %lld bytes from %d,error code:%d"
                                      ,i64Offset,u32SeekFrom,giFDBErrNum);
                FDBLOG_THREE_PARAM(FDB_LOG_FILE,"iodevice seek error,seek %lld bytes from %d,error code:%d"
                                      ,i64Offset,u32SeekFrom,giFDBErrNum);
        }
        i64Ret = i64SeekResult;
    } else {
        //cancel io request...!!!NOT IMPLEMENT!!!
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice seek request time_out,restart work thread failed"
                                     ,FDB_ERR_MEM_NOT_ENOUGH_STR);
                FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice seek request time_out,restart work thread failed"
                                  ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_FOUR_PARAM("%s,iodevice seek time out,seek %lld bytes from %d,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,i64Offset,u32SeekFrom,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice seek time out,seek %lld bytes from %d,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,i64Offset,u32SeekFrom,giFDBErrNum);
        i64Ret = -1;
    }

    pobjIODevice->pobjInstructLocker->unlock();

    return (i64Ret);
}

long long int FDBfIODecvice_tell(FDBCIODevice *pobjIODevice)
{
    long long int i64FilePointPos = 0;

    if(!pobjIODevice)
    {
        return (-1);
    }

    i64FilePointPos = FDBfIODevice_seek(pobjIODevice,0,SEEK_CUR);

    return (i64FilePointPos);
}

int64_t FDBfIODevice_size(FDBCIODevice *pobjIODevice)
{
    long long int i64DeviceSize = FDBfIODeviceSizeThroughPath(pobjIODevice->pcDevicePath);
    return (i64DeviceSize);
}

int FDBfIODevice_flush(FDBCIODevice *pobjIODevice)
{
    if(!pobjIODevice)
    {
        return (-1);
    }
    return (0);
}

long long int FDBfIODeviceSizeThroughPath(const char *pcDevicePath)
{
    //treat as general file
    int iFile = open(pcDevicePath,O_RDONLY|O_LARGEFILE);
    long long int i64FileEnd = lseek(iFile,0,SEEK_END);

    if(i64FileEnd)
    {
        close(iFile);
        return (i64FileEnd);
    }

    //treat as io driver
    const char* pcDiskSizeTag = "\"Size\" = ";
    char pcChildResult[4096] = {0};
    char pcCommandToExecute[512] = {0};
    long long int i64DeviceSize = 0;

    sprintf(pcCommandToExecute,"ioreg -c IOMedia | grep %s -A10",strstr(pcDevicePath,"disk"));
    FILE* pfChild = popen(pcCommandToExecute,"r");
    fread(pcChildResult,1,4096,pfChild);
    const char* pcSizePos = strstr(pcChildResult,pcDiskSizeTag);
    if(pcSizePos)
    {
        i64DeviceSize = atoll(pcSizePos + strlen(pcDiskSizeTag));
    }
    pclose(pfChild);

    return (i64DeviceSize);
}

#endif
