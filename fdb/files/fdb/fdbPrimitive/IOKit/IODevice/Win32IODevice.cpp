#if defined(_WIN32)

#include <Windows.h>
#include <WinIoCtl.h>

#include <stdio.h>
#include <stdlib.h>

#include "IODevice.h"

#include "../../MemoryManagement/MemoryManagement.h"
#include "../../Exception/Exception.h"

void* fIODeviceWorkThread(void* pParam)
{
    FDBCIODevice* pobjIODevice = (FDBCIODevice*)pParam;
    if(!pobjIODevice)
    {
        return (NULL);
    }

    while(1)
    {
        pobjIODevice->pobjGoToWorkKick->wait();

        if(pobjIODevice->bCallForStop)
        {
            pobjIODevice->pobjWorkReportKick->post();
            return (NULL);
        }
        switch( pobjIODevice->eIOInstruction )
        {
            case FDBEIOInstruction_Read:
            {
                DWORD dwEveryCircleReadBytes = 0;
                int bRet = 0;
                unsigned long u32ProperBytesToRead = 0;
                unsigned long u32TotalReadBytes = 0;

                u32ProperBytesToRead
                        = pobjIODevice->i64ReadWriteOffsetSeekLen+pobjIODevice->u32ReadWriteDataLenOrSeekFrom
                            < pobjIODevice->i64DeviceSize ? pobjIODevice->u32ReadWriteDataLenOrSeekFrom
                            : pobjIODevice->i64DeviceSize - pobjIODevice->i64ReadWriteOffsetSeekLen;
                do
                {
                    bRet= ReadFile(
                                pobjIODevice->device
                                ,pobjIODevice->pcReadWriteDestination + u32TotalReadBytes
                                ,u32ProperBytesToRead - u32TotalReadBytes
                                ,&dwEveryCircleReadBytes
                                ,NULL
                                );
                    if(bRet==FALSE||dwEveryCircleReadBytes == 0)
                    {
                        break;
                    }
                    u32TotalReadBytes += dwEveryCircleReadBytes;
                } while(u32TotalReadBytes < pobjIODevice->u32ReadWriteDataLenOrSeekFrom);
                pobjIODevice->i64InstructionResult = u32TotalReadBytes ? u32TotalReadBytes : -1;
            } break;
            case FDBEIOInstruction_Write:
            {
                DWORD dwEveryCircleWriteBytes = 0;
                int bRet = 0;
                unsigned long u32TotalWriteBytes = 0;
                do
                {
                    bRet= WriteFile(
                                pobjIODevice->device,
                                pobjIODevice->pcReadWriteDestination + u32TotalWriteBytes,
                                pobjIODevice->u32ReadWriteDataLenOrSeekFrom - u32TotalWriteBytes,
                                &dwEveryCircleWriteBytes,
                                NULL
                                );
                    if(bRet==FALSE||dwEveryCircleWriteBytes == 0)
                    {
                        break;
                    }
                    u32TotalWriteBytes += dwEveryCircleWriteBytes;
                } while(u32TotalWriteBytes < pobjIODevice->u32ReadWriteDataLenOrSeekFrom);
                pobjIODevice->i64InstructionResult = u32TotalWriteBytes ? u32TotalWriteBytes : -1;
            } break;
            case FDBEIOInstruction_Seek:
            {
                int bRet = 0;
                LARGE_INTEGER sDistanceToMove,sNewFilePoint;
                int iSeekMethod = pobjIODevice->u32ReadWriteDataLenOrSeekFrom;
                sDistanceToMove.QuadPart = pobjIODevice->i64ReadWriteOffsetSeekLen;

                bRet = SetFilePointerEx(
                            pobjIODevice->device,
                            sDistanceToMove,
                            &sNewFilePoint,
                            iSeekMethod
                            );
                pobjIODevice->i64InstructionResult = bRet ? sNewFilePoint.QuadPart : -1;
            }break;
            case FDBEIOInstruction_Flush:
            {
                int bRet = 0;
                bRet = FlushFileBuffers(pobjIODevice->device);
                pobjIODevice->i64InstructionResult = bRet ? 1 : -1;
            } break;
        } //instruction switch

        //work over,report
        pobjIODevice->pobjWorkReportKick->post();
    }
    return (NULL);
}

FDBCIODevice* FDBfIODevice_new_not_open(const char* pcDevicePath)
{
    FDBCIODevice* pobjIODevice = new FDBCIODevice();

    if(!pobjIODevice)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }
    pobjIODevice->pcDevicePath = (char*)calloc(1,strlen(pcDevicePath)+1);
    if(!pobjIODevice->pcDevicePath)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's path",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's path",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }
    strcpy(pobjIODevice->pcDevicePath,pcDevicePath);

    /*alloc instruct locker,goto work kick,work report kick,work thread*/
    pobjIODevice->pobjInstructLocker = new boost::mutex();
    if(!pobjIODevice->pobjInstructLocker)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's instruct locker",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's instruct locker",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    pobjIODevice->pobjGoToWorkKick = new boost::interprocess::interprocess_semaphore(0);
    if(!pobjIODevice->pobjGoToWorkKick)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's goto work kick",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's goto work kick",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    pobjIODevice->pobjWorkReportKick = new boost::interprocess::interprocess_semaphore(0);
    if(!pobjIODevice->pobjWorkReportKick)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's work report kick",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's work report kick",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
    if(!pobjIODevice->pobjWorkThread)
    {
        FDBfIODevice_delete(&pobjIODevice);

        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice's work report kick",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice's work report kick",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    return (pobjIODevice);
}

void FDBfIODevice_delete(FDBCIODevice **ppobjIODevice)
{
	if(*ppobjIODevice)
	{
		(*ppobjIODevice)->pobjInstructLocker->lock();

		(*ppobjIODevice)->bCallForStop = 1;
        (*ppobjIODevice)->pobjGoToWorkKick->post();
        (*ppobjIODevice)->pobjWorkReportKick->wait();
		(*ppobjIODevice)->pobjWorkThread->join();
		delete (*ppobjIODevice)->pobjWorkThread;
        delete (*ppobjIODevice)->pobjGoToWorkKick;
        delete (*ppobjIODevice)->pobjWorkReportKick;

		(*ppobjIODevice)->pobjInstructLocker->unlock();
		delete (*ppobjIODevice)->pobjInstructLocker;
		FDBfFree( (void**)(&(*ppobjIODevice)->pcDevicePath) );
		CloseHandle( (*ppobjIODevice)->device );
		FDBfFree( (void**)ppobjIODevice );
	}
}

int FDBfIODevice_open(FDBCIODevice *pobjIODevice, enum FDBEAccessFlag eAccessFlag, enum FDBEOpenMethod eOpenMethod)
{
    pobjIODevice->device = CreateFileA(pobjIODevice->pcDevicePath
                                       ,eAccessFlag
                                       ,FILE_SHARE_READ
                                       ,NULL,eOpenMethod
                                       ,0,0);
    if(pobjIODevice->device == INVALID_HANDLE_VALUE)
    {
        giFDBErrNum = GetLastError();

        FDBErrStr_THREE_PARAM("%s,error code:%d,path:%s","open device failed"
                          ,giFDBErrNum,pobjIODevice->pcDevicePath);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE
                          ,"%s,error code:%d,path:%s","open device failed"
                          ,giFDBErrNum,pobjIODevice->pcDevicePath);
        return (-1);
    }
    //if this iodevice open only for read,i should calculate it's size for once
    if(eAccessFlag == FDBEAccessFlag_ReadOnly)
    {
        pobjIODevice->i64DeviceSize = FDBfIODeviceSizeThroughPath(pobjIODevice->pcDevicePath);
    }

    return (0);
}

int32_t FDBfIODevice_read(FDBCIODevice *pobjIODevice, void* pBuf, const unsigned long u32BytesToRead)
{
    int32_t i32Ret = 0;
    if(!pobjIODevice || !pBuf || pobjIODevice->device == 0 )
	{
		giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
		FDBErrStr_FOUR_PARAM("%s,read %d bytes into 0x%x,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);
		FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,read %d bytes into 0x%x,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);

		return (-1);
	}
    if(u32BytesToRead == 0)
    {
        return (0);
    }

    pobjIODevice->pobjInstructLocker->lock();

    pobjIODevice->eIOInstruction = FDBEIOInstruction_Read;
    pobjIODevice->pcReadWriteDestination = (char*)pBuf;
    pobjIODevice->u32ReadWriteDataLenOrSeekFrom = u32BytesToRead;

    bool bWaitResult = false;
    long i32ReadBytes = 0;
    boost::system_time objDeadline =
            boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS+u32BytesToRead/(1024*1024));
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitResult = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);

    if(bWaitResult)
    {
        if(i32ReadBytes <= 0)
        {
            giFDBErrNum = errno;
            FDBErrStr_THREE_PARAM("iodevice read error,read %d bytes into 0x%x,error code:%d"
                                  ,u32BytesToRead,pBuf,giFDBErrNum);
            FDBLOG_THREE_PARAM(FDB_LOG_FILE,"iodevice read error,read %d bytes into 0x%x,error code:%d"
                               ,u32BytesToRead,pBuf,giFDBErrNum);
        }
        i32Ret = i32ReadBytes;
    } else {
        //cancel io request
		//***BECAUSE XP DOESN'T SUPPORT CANCEL IO,I DISABLE IT NOW FOR TEMPORARY
        //CancelSynchronousIo(pobjIODevice->pobjWorkThread->native_handle());
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice read request time_out,restart work thread failed"
                                ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice read request time_out,restart work thread failed"
                             ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_FOUR_PARAM("%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
        i32Ret = -1;
    }

    pobjIODevice->pobjInstructLocker->unlock();

    return (i32Ret);
}

int32_t FDBfIODevice_read_from(FDBCIODevice* pobjIODevice
						,void* pBuf 
						,const long long int i64ReadBeginOffset 
                        ,const unsigned long u32BytesToRead)
{
    int bWaitRet = 0;
    int32_t i32ReadBytes = 0;
    int32_t i32Ret = 0;
    int64_t i64SeekResult = 0;

    if(!pobjIODevice || !pBuf || pobjIODevice->device == 0)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,read %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,read %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);

        return (-1);
    }
    if(u32BytesToRead == 0)
    {
        return (0);
    }

    pobjIODevice->pobjInstructLocker->lock();

    /*seek instruction*/
    pobjIODevice->eIOInstruction = FDBEIOInstruction_Seek;
    pobjIODevice->i64ReadWriteOffsetSeekLen = i64ReadBeginOffset;
    pobjIODevice->u32ReadWriteDataLenOrSeekFrom = SEEK_SET;

    boost::system_time objDeadline = boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS);
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitRet = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);
    i64SeekResult = pobjIODevice->i64InstructionResult;

    if(bWaitRet)
    {
        if(i64SeekResult < 0LL)
        {
            giFDBErrNum = errno;
            FDBErrStr_TWO_PARAM("iodevice seek(read_from) error,seek %lld,error code:%d"
                                ,i64ReadBeginOffset,giFDBErrNum);
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"iodevice seek(read_from) error,seek %lld,error code:%d"
                             ,i64ReadBeginOffset,giFDBErrNum);
            i32Ret = -1;
        }

    } else {
        //cancel io request
		//***BECAUSE XP DOESN'T SUPPORT CANCEL IO,I DISABLE IT NOW FOR TEMPORARY
        //CancelSynchronousIo(pobjIODevice->pobjWorkThread->native_handle());
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice read request time_out,restart work thread failed"
                                ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice read request time_out,restart work thread failed"
                             ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_FOUR_PARAM("%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
        i32Ret = -1;
    }

    /*read instruction*/
    if(i64SeekResult < 0LL)
    {
        i32Ret = i64SeekResult;
    } else {
        pobjIODevice->eIOInstruction = FDBEIOInstruction_Read;
        pobjIODevice->pcReadWriteDestination = (char*)pBuf;
        pobjIODevice->u32ReadWriteDataLenOrSeekFrom = u32BytesToRead;

        boost::system_time objDeadline = boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS);
        pobjIODevice->pobjGoToWorkKick->post();
        bWaitRet = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);
        i32ReadBytes = pobjIODevice->i64InstructionResult;

        if(bWaitRet)
        {
            if(i32ReadBytes <= 0)
            {
                giFDBErrNum = errno;
                FDBErrStr_THREE_PARAM("iodevice read(read_from) error,read %d bytes into 0x%x,error code:%d"
                                      ,u32BytesToRead,pBuf,giFDBErrNum);
                FDBLOG_THREE_PARAM(FDB_LOG_FILE,"iodevice read(read_from) error,read %d bytes into 0x%x,error code:%d"
                                   ,u32BytesToRead,pBuf,giFDBErrNum);
            }
            i32Ret = i32ReadBytes;
        } else {
            //cancel io request
			//***BECAUSE XP DOESN'T SUPPORT CANCEL IO,I DISABLE IT NOW FOR TEMPORARY
			//CancelSynchronousIo(pobjIODevice->pobjWorkThread->native_handle());
            pobjIODevice->pobjWorkThread->interrupt();
            delete (pobjIODevice->pobjWorkThread);
            pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
            if( !pobjIODevice->pobjWorkThread )
            {
                giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
                FDBErrStr_ONE_PARAM("%s,when iodevice read request time_out,restart work thread failed"
                                    ,FDB_ERR_MEM_NOT_ENOUGH_STR);
                FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice read request time_out,restart work thread failed"
                                 ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            }
            /*error handle*/
            giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
            FDBErrStr_FOUR_PARAM("%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                                 ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
            FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice read error,read %d bytes into 0x%x,error code:%d"
                              ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToRead,pBuf,giFDBErrNum);
            i32Ret = -1;
        }
    } //seek succeed

    pobjIODevice->pobjInstructLocker->unlock();

    return (i32Ret);
}

int32_t FDBfIODevice_write(FDBCIODevice *pobjIODevice, const void *pBuf, const unsigned long u32BytesToWrite)
{
    int32_t i32Ret = 0;
    if(!pobjIODevice || !pBuf || pobjIODevice->device < 0)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,write %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToWrite,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,write %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToWrite,pBuf,giFDBErrNum);

        return (-1);
    }
    if(u32BytesToWrite == 0)
    {
        return (0);
    }

    pobjIODevice->pobjInstructLocker->lock();

    pobjIODevice->eIOInstruction = FDBEIOInstruction_Write;
    pobjIODevice->pcReadWriteDestination = (char*)pBuf;
    pobjIODevice->u32ReadWriteDataLenOrSeekFrom = u32BytesToWrite;

    bool bWaitResult = false;
    long i32ReadBytes = 0;
    boost::system_time objDeadline =
            boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS+u32BytesToWrite/(1024*1024));
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitResult = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);

    if(bWaitResult)
    {
        if(i32ReadBytes <= 0)
        {
            giFDBErrNum = errno;
            FDBErrStr_THREE_PARAM("iodevice write error,read %d bytes into 0x%x,error code:%d"
                                  ,u32BytesToWrite,pBuf,giFDBErrNum);
            FDBLOG_THREE_PARAM(FDB_LOG_FILE,"iodevice write error,read %d bytes into 0x%x,error code:%d"
                               ,u32BytesToWrite,pBuf,giFDBErrNum);
        }
        i32Ret = i32ReadBytes;
    } else {
        //cancel io request
		//***BECAUSE XP DOESN'T SUPPORT CANCEL IO,I DISABLE IT NOW FOR TEMPORARY
        //CancelSynchronousIo(pobjIODevice->pobjWorkThread->native_handle());
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice write request time_out,restart work thread failed"
                                ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice write request time_out,restart work thread failed"
                             ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_FOUR_PARAM("%s,iodevice write error,write %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToWrite,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice write error,write %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,u32BytesToWrite,pBuf,giFDBErrNum);
        i32Ret = -1;
    }

    pobjIODevice->pobjInstructLocker->unlock();

    return (i32Ret);
}

long long int FDBfIODevice_seek(FDBCIODevice *pobjIODevice
                                , const long long i64Offset
                                , const unsigned long u32SeekFrom)
{
    long bWaitRet = 0;
    long long int i64SeekResult = 0;
    long long int i64Ret = 0;
    if(!pobjIODevice)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,seek %lld from %d,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,i64Offset,u32SeekFrom,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,seek %lld from %d,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,i64Offset,u32SeekFrom,giFDBErrNum);

        return (-1);
    }
    pobjIODevice->pobjInstructLocker->lock();

    pobjIODevice->eIOInstruction = FDBEIOInstruction_Seek;
    pobjIODevice->i64ReadWriteOffsetSeekLen = i64Offset;
    pobjIODevice->u32ReadWriteDataLenOrSeekFrom = u32SeekFrom;

    boost::system_time objDeadline = boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS);
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitRet = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);
    i64SeekResult = pobjIODevice->i64InstructionResult;

    if(bWaitRet)
    {
        if(i64SeekResult <= 0)
        {
            giFDBErrNum = errno;
                FDBErrStr_THREE_PARAM("iodevice seek error,seek %lld bytes from %d,error code:%d"
                                      ,i64Offset,u32SeekFrom,giFDBErrNum);
                FDBLOG_THREE_PARAM(FDB_LOG_FILE,"iodevice seek error,seek %lld bytes from %d,error code:%d"
                                      ,i64Offset,u32SeekFrom,giFDBErrNum);
        }
        i64Ret = i64SeekResult;
    } else {
        //cancel io request
		//***BECAUSE XP DOESN'T SUPPORT CANCEL IO,I DISABLE IT NOW FOR TEMPORARY
        //CancelSynchronousIo(pobjIODevice->pobjWorkThread->native_handle());
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice seek request time_out,restart work thread failed"
                                     ,FDB_ERR_MEM_NOT_ENOUGH_STR);
                FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice seek request time_out,restart work thread failed"
                                  ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_FOUR_PARAM("%s,iodevice seek time out,seek %lld bytes from %d,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,i64Offset,u32SeekFrom,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,iodevice seek time out,seek %lld bytes from %d,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,i64Offset,u32SeekFrom,giFDBErrNum);
        i64Ret = -1;
    }

    pobjIODevice->pobjInstructLocker->unlock();

    return (i64Ret);
}

long long int FDBfIODecvice_tell(FDBCIODevice *pobjIODevice)
{
    long long int i64FilePointPos = 0;

    if(!pobjIODevice)
    {
        return (-1);
    }

    i64FilePointPos = FDBfIODevice_seek(pobjIODevice,0,SEEK_CUR);

    return (i64FilePointPos);
}

long long int FDBfIODevice_size(FDBCIODevice *pobjIODevice)
{
	if(pobjIODevice->device == 0)
	{
		giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
		FDBErrStr_TWO_PARAM("%s,get size,error code:%d"
			,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,giFDBErrNum);
		FDBLOG_TWO_PARAM(FDB_LOG_FILE,"%s,get size,error code:%d"
			,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,giFDBErrNum);

		return (-1);
	}
    if(pobjIODevice->device)
    {
        //normal file
		BOOL bRet = FALSE;
        WIN32_FILE_ATTRIBUTE_DATA fad;
        memset(&fad,0,sizeof(WIN32_FILE_ATTRIBUTE_DATA));
		bRet = GetFileAttributesExA(pobjIODevice->pcDevicePath,GetFileExInfoStandard,&fad);
        if( bRet )
        {
            LARGE_INTEGER liFileSize;
            liFileSize.u.HighPart = fad.nFileSizeHigh;
            liFileSize.u.LowPart = fad.nFileSizeLow;

            return (liFileSize.QuadPart);
		} else {
			BOOL bRet = FALSE;
            long long int i64DeviceSize = 0;
            GET_LENGTH_INFORMATION sDeviceLengthInfo;
            DWORD dwReturnSize;
            DeviceIoControl(pobjIODevice->device,
                            IOCTL_DISK_GET_LENGTH_INFO,
                            NULL,
                            0,
                            (LPVOID)&sDeviceLengthInfo,
                            sizeof(GET_LENGTH_INFORMATION),
                            &dwReturnSize,
                            NULL);

            i64DeviceSize = sDeviceLengthInfo.Length.QuadPart;
            if( bRet )
            {
                return (i64DeviceSize);
            } else {
				giFDBErrNum = GetLastError();
				FDBErrStr_ONE_PARAM("get device size failed,error code:%d",giFDBErrNum);
				FDBLOG_ONE_PARAM(FDB_LOG_FILE,"get device size failed,error code:%d",giFDBErrNum);
			}
        }
        return (-1);
    }
    return (-1);
}

int FDBfIODevice_flush(FDBCIODevice *pobjIODevice)
{
    int iRet = 0;
    int iFlushResult = 0;
    if(!pobjIODevice)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_TWO_PARAM("%s,flush,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,giFDBErrNum);
        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"%s,flush,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,giFDBErrNum);

        return (-1);
    }

    pobjIODevice->pobjInstructLocker->lock();

    pobjIODevice->eIOInstruction = FDBEIOInstruction_Flush;

    bool bWaitResult = false;
    boost::system_time objDeadline =
            boost::get_system_time()
            + boost::posix_time::milliseconds(IO_WAIT_TIME_MILLISECONDS);
    pobjIODevice->pobjGoToWorkKick->post();
    bWaitResult = pobjIODevice->pobjWorkReportKick->timed_wait(objDeadline);
    iFlushResult = (int)pobjIODevice->i64InstructionResult;

    if(bWaitResult)
    {
        if(iFlushResult < 0)
        {
            giFDBErrNum = GetLastError();
            FDBErrStr_ONE_PARAM("iodevice flush error,error code:%d"
                                ,giFDBErrNum);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"iodevice flush error,error code:%d"
                             ,giFDBErrNum);
        }
        iRet = iFlushResult;
    } else {
        //cancel io request
		//***BECAUSE XP DOESN'T SUPPORT CANCEL IO,I DISABLE IT NOW FOR TEMPORARY
        //CancelSynchronousIo(pobjIODevice->pobjWorkThread->native_handle());
        pobjIODevice->pobjWorkThread->interrupt();
        delete (pobjIODevice->pobjWorkThread);
        pobjIODevice->pobjWorkThread = new boost::thread(&fIODeviceWorkThread,pobjIODevice);
        if( !pobjIODevice->pobjWorkThread )
        {
            giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
            FDBErrStr_ONE_PARAM("%s,when iodevice seek request time_out,restart work thread failed"
                                ,FDB_ERR_MEM_NOT_ENOUGH_STR);
            FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s,when iodevice seek request time_out,restart work thread failed"
                             ,FDB_ERR_MEM_NOT_ENOUGH_STR);
        }
        /*error handle*/
        giFDBErrNum = FDB_ERR_CALL_TIMED_OUT;
        FDBErrStr_TWO_PARAM("%s,iodevice flush time out,error code:%d"
                             ,FDB_ERR_CALL_TIMED_OUT_STR,giFDBErrNum);
        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"%s,iodevice seek time out,seek %lld bytes from %d,error code:%d"
                          ,FDB_ERR_CALL_TIMED_OUT_STR,giFDBErrNum);
        iRet = -1;
    }

    pobjIODevice->pobjInstructLocker->unlock();

    return (iRet);
}

int64_t FDBfIODeviceSizeThroughPath(const char* pcDevicePath)
{
    //normal file
    BOOL bRet = FALSE;
    WIN32_FILE_ATTRIBUTE_DATA fad;
    memset(&fad,0,sizeof(WIN32_FILE_ATTRIBUTE_DATA));
    bRet = GetFileAttributesExA(pcDevicePath,GetFileExInfoStandard,&fad);
    if( bRet )
    {
        LARGE_INTEGER liFileSize;
        liFileSize.u.HighPart = fad.nFileSizeHigh;
        liFileSize.u.LowPart = fad.nFileSizeLow;

        return (liFileSize.QuadPart);
    } else {
        BOOL bRet = FALSE;
        GET_LENGTH_INFORMATION sDeviceLengthInfo;
        DWORD dwBytesReturn = 0;
        HANDLE hDevice = INVALID_HANDLE_VALUE;

        giFDBErrNum = GetLastError();
        FDBErrStr_TWO_PARAM("get device(%s) as normal file size failed,error code:%d",pcDevicePath,giFDBErrNum);
        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"get device(%s) as normal file size failed,error code:%d",pcDevicePath,giFDBErrNum);

        hDevice = CreateFileA(pcDevicePath,FDBEAccessFlag_ReadOnly,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL
                                     ,FDBEOpenMethod_OpenExisting,0,0);
        if(hDevice == INVALID_HANDLE_VALUE)
        {
            giFDBErrNum = GetLastError();
            FDBErrStr_TWO_PARAM("open device(%s) for get size failed,error code:%d",pcDevicePath,giFDBErrNum);
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"open device(%s) for get size failed,error code:%d",pcDevicePath,giFDBErrNum);
            return (-1);
        }

        //physical drive
        int64_t i64DeviceSize = 0;
        DWORD dwReturnSize;
        bRet = DeviceIoControl(hDevice,
                        IOCTL_DISK_GET_LENGTH_INFO,
                        NULL,
                        0,
                        (LPVOID)&sDeviceLengthInfo,
                        sizeof(GET_LENGTH_INFORMATION),
                        &dwReturnSize,
                        NULL);
        CloseHandle(hDevice);

        i64DeviceSize = sDeviceLengthInfo.Length.QuadPart;
        if( bRet )
        {
            return (i64DeviceSize);
        } else {
            giFDBErrNum = GetLastError();
            FDBErrStr_TWO_PARAM("get device(%s)as physical drive size failed,error code:%d",pcDevicePath,giFDBErrNum);
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"get as physical drive size failed,error code:%d",pcDevicePath,giFDBErrNum);
        }

        //partition
        ULARGE_INTEGER s_available_free_bytes = {0};
        ULARGE_INTEGER s_total_bytes = {0};
        ULARGE_INTEGER s_total_free_bytes = {0};
        bRet = GetDiskFreeSpaceExA(pcDevicePath,&s_available_free_bytes,&s_total_bytes,&s_total_free_bytes);
        i64DeviceSize = s_total_bytes.QuadPart;
        if( bRet )
        {
            return (i64DeviceSize);
        } else {
            giFDBErrNum = GetLastError();
            FDBErrStr_TWO_PARAM("get device(%s)as partition size failed,error code:%d",pcDevicePath,giFDBErrNum);
            FDBLOG_TWO_PARAM(FDB_LOG_FILE,"get as partition size failed,error code:%d",pcDevicePath,giFDBErrNum);
        }
    }
	return (-1);
}

#endif
