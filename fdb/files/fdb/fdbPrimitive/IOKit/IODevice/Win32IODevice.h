/* 2013-08-16
 * brief: win32IODevice prototype,can read bad sector safely
 * author: shawhen dsc
 * version: 0.1.0
 *
 * visiable lack:
 *  1.async i/o
 *
 * LICENSE: $LGPL
 */
#if defined(_WIN32)

#ifndef __fdb_win32_io_device_h_2013_08_16_09_55__
#define __fdb_win32_io_device_h_2013_08_16_09_55__

#include "IODevicelib.h"

#include <Windows.h>

#ifndef Q_MOC_RUN
#include <boost/thread.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#endif

typedef struct FDBCIODevice {
    HANDLE device;
    long long int i64DeviceSize;

    boost::mutex* pobjInstructLocker;

    boost::thread* pobjWorkThread;
    boost::interprocess::interprocess_semaphore* pobjGoToWorkKick;
    boost::interprocess::interprocess_semaphore* pobjWorkReportKick;

    FDBEIOInstruction eIOInstruction;
    long long int i64ReadWriteOffsetSeekLen; /*read from(or write from or seek distance) offset*/
    unsigned long u32ReadWriteDataLenOrSeekFrom; /*read/write data length or seek from*/
    char* pcReadWriteDestination; /*read into or write from data buffer*/
    long long int i64InstructionResult; /*read/write bytes,or seek result*/

    char* pcDevicePath;
    int bCallForStop;
} FDBCIODevice;

#endif //header

#else
#error "Include Error File,please contact developer"
#endif
