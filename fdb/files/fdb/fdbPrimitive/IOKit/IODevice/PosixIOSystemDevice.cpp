#if defined(__APPLE__) || defined(__linux__)

#include "IODevice.h"
#include "IOSystemDevice.h"
#include "PosixIOSystemDevice.h"

#include "../../MemoryManagement/MemoryManagement.h"
#include "../../Exception/Exception.h"

#include <stdlib.h>

#include <unistd.h>
#include <fcntl.h>

FDBCIOSystemDevice* FDBfIOSystemDevice_new_not_open(const char* pc_device_path)
{
    FDBCIOSystemDevice* pobj_IODevice = new FDBCIOSystemDevice();

    if(!pobj_IODevice)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    pobj_IODevice->pc_device_path = strdup(pc_device_path);

    return (pobj_IODevice);
}

void FDBfIOSystemDevice_delete(FDBCIOSystemDevice **pp_obj_IOSystemDevice)
{
    if(*pp_obj_IOSystemDevice)
    {
        FDBfFree( (void**)(&(*pp_obj_IOSystemDevice)->pc_device_path) );
        close( (*pp_obj_IOSystemDevice)->device );
        FDBfFree( (void**)pp_obj_IOSystemDevice );
    }
}

int FDBfIOSystemDevice_open(FDBCIOSystemDevice *p_obj_IODevice
                            , enum FDBEAccessFlag e_access_flag
                            , enum FDBEOpenMethod e_open_method)
{
    p_obj_IODevice->device = open(p_obj_IODevice->pc_device_path,e_access_flag|e_open_method);
    if(p_obj_IODevice->device <= 0)
    {
        giFDBErrNum = errno;

        FDBErrStr_THREE_PARAM("%s,error code:%d,path:%s","open device failed"
                          ,giFDBErrNum,p_obj_IODevice->pc_device_path);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE
                          ,"%s,error code:%d,path:%s","open device failed"
                          ,giFDBErrNum,p_obj_IODevice->pc_device_path);
        return (-1);
    }
    //if this iodevice open only for read,i should calculate it's size for once
    if(e_access_flag == FDBEAccessFlag_ReadOnly)
    {
        p_obj_IODevice->i64_device_size = FDBfIODeviceSizeThroughPath(p_obj_IODevice->pc_device_path);
    }

    return (0);
}

int32_t FDBfIOSystemDevice_read(FDBCIOSystemDevice *pobjIOSystemDevice, void *pBuf, const uint32_t u32BytesToRead)
{
    long i32ReadBytes = 0;
    if(!pobjIOSystemDevice || !pBuf || pobjIOSystemDevice->device == 0 )
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,read %d bytes into 0x%x,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,read %d bytes into 0x%x,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);

        return (-1);
    }
    if(u32BytesToRead == 0)
    {
        return (0);
    }

    i32ReadBytes = read(pobjIOSystemDevice->device,pBuf,u32BytesToRead);

    return (i32ReadBytes);
}

int32_t FDBfIOSystemDevice_read_from(FDBCIOSystemDevice *p_obj_IOSystemDevice
                                  , void *p_buf
                                  , const int64_t i64_read_begin_offset
                                  , const uint32_t u32_bytes_to_read)
{
    int32_t i32_read_bytes = 0;

    int64_t i64_new_file_pos = lseek(p_obj_IOSystemDevice->device,i64_read_begin_offset,SEEK_SET);
    if(i64_new_file_pos != i64_read_begin_offset)
    {
        return (-1);
    }

    i32_read_bytes = read(p_obj_IOSystemDevice->device,p_buf,u32_bytes_to_read);

    return (i32_read_bytes);
}

int32_t FDBfIOSystemDevice_write(FDBCIOSystemDevice *p_obj_IOSystemDevice
                              , const void *p_buf
                              , const uint32_t u32_bytes_to_write)
{
    long i32_bytes_write = 0;
    if(!p_obj_IOSystemDevice || !p_buf || p_obj_IOSystemDevice->device < 0)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,write %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32_bytes_to_write,p_buf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,write %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32_bytes_to_write,p_buf,giFDBErrNum);

        return (-1);
    }

    i32_bytes_write = write(p_obj_IOSystemDevice->device,p_buf,u32_bytes_to_write);

    return (i32_bytes_write);
}

int64_t FDBfIOSystemDevice_seek(FDBCIOSystemDevice *p_obj_IOSystemDevice
                                      , const int64_t i64_offset
                                      , const uint32_t u32_seek_from)
{
    int64_t i64_new_file_pos = lseek(p_obj_IOSystemDevice->device,i64_offset,u32_seek_from);
    if(i64_new_file_pos != i64_offset)
    {
        return (-1);
    }

    return (i64_new_file_pos);
}

int64_t FDBfIOSystemDevice_size(FDBCIOSystemDevice* p_obj_IODevice)
{
    int64_t i64_device_size = FDBfIODeviceSizeThroughPath(p_obj_IODevice->pc_device_path);
    return (i64_device_size);
}

int64_t FDBfIOSystemDecvice_tell(FDBCIOSystemDevice* pobjIOSystemDevice)
{
    long long int i64FilePointPos = 0;

    if(!pobjIOSystemDevice)
    {
        return (-1);
    }

    i64FilePointPos = FDBfIOSystemDevice_seek(pobjIOSystemDevice,0,SEEK_CUR);

    return (i64FilePointPos);
}

#endif
