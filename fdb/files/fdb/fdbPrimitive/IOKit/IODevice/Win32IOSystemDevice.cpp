﻿#if defined(_WIN32)

#include "IODevice.h"
#include "IOSystemDevice.h"
#include "Win32IOSystemDevice.h"

#include "../../MemoryManagement/MemoryManagement.h"
#include "../../Exception/Exception.h"

#include <stdlib.h>

#include <Windows.h>
#include <WinIoCtl.h>

FDBCIOSystemDevice* FDBfIOSystemDevice_new_not_open(const char* pc_device_path)
{
    FDBCIOSystemDevice* pobj_IODevice = new FDBCIOSystemDevice(pc_device_path);

    if(!pobj_IODevice)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when alloc iodevice",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when alloc iodevice",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    return (pobj_IODevice);
}

void FDBfIOSystemDevice_delete(FDBCIOSystemDevice **pp_obj_io_system_device)
{
    if( *pp_obj_io_system_device )
    {
        delete *pp_obj_io_system_device;
        *pp_obj_io_system_device = NULL;
    }
}

int FDBfIOSystemDevice_open(FDBCIOSystemDevice *pobjIODevice
                            , enum FDBEAccessFlag eAccessFlag
                            , enum FDBEOpenMethod eOpenMethod)
{
    pobjIODevice->device = CreateFileA(pobjIODevice->pc_device_path
                                       ,eAccessFlag
                                       ,FILE_SHARE_READ|FILE_SHARE_WRITE
                                       ,NULL,eOpenMethod
                                       ,0,0);
    if(pobjIODevice->device == INVALID_HANDLE_VALUE)
    {
        giFDBErrNum = GetLastError();

        FDBErrStr_THREE_PARAM("%s,error code:%d,path:%s","open device failed"
                          ,giFDBErrNum,pobjIODevice->pc_device_path);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE
                          ,"%s,error code:%d,path:%s","open device failed"
                          ,giFDBErrNum,pobjIODevice->pc_device_path);
        return (-1);
    }
    //if this iodevice open only for read,i should calculate it's size for once
    if(eAccessFlag == FDBEAccessFlag_ReadOnly)
    {
        pobjIODevice->i64_device_size = FDBfIODeviceSizeThroughPath(pobjIODevice->pc_device_path);
    }

    return (0);
}

int32_t FDBfIOSystemDevice_read(FDBCIOSystemDevice *pobjIOSystemDevice, void *pBuf, const uint32_t u32BytesToRead)
{
    long i32ReadBytes = 0;
    if(!pobjIOSystemDevice || !pBuf || pobjIOSystemDevice->device == 0 )
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,read %d bytes into 0x%x,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,read %d bytes into 0x%x,error code:%d"
                            ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToRead,pBuf,giFDBErrNum);

        return (-1);
    }
    if(u32BytesToRead == 0)
    {
        return (0);
    }

    if(ReadFile(pobjIOSystemDevice->device,pBuf,u32BytesToRead,(LPDWORD)&i32ReadBytes,NULL) == FALSE)
    {
        giFDBErrNum = ::GetLastError();
        FDBErrStr_FOUR_PARAM("read from %s, %d bytes into 0x%x,error code:%d"
                            ,pobjIOSystemDevice->pc_device_path,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"read from %s, %d bytes into 0x%x,error code:%d"
                              ,pobjIOSystemDevice->pc_device_path,u32BytesToRead,pBuf,giFDBErrNum);

        return (-1);
    }
    if(i32ReadBytes != u32BytesToRead)
    {
        giFDBErrNum = ::GetLastError();
        FDBErrStr_FOUR_PARAM("read from %s, %d bytes into 0x%x(not enought),error code:%d"
                            ,pobjIOSystemDevice->pc_device_path,u32BytesToRead,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"read %d bytes into 0x%x(not enought),error code:%d"
                             ,pobjIOSystemDevice->pc_device_path,u32BytesToRead,pBuf,giFDBErrNum);
    }

    return (i32ReadBytes);
}

int32_t FDBfIOSystemDevice_read_from(FDBCIOSystemDevice *pobjIOSystemDevice
                                  , void *pBuf
                                  , const int64_t i64ReadBeginOffset
                                  , const uint32_t u32BytesToRead)
{
    int32_t i32ReadBytes = 0;

    static LARGE_INTEGER sDistanceToMove = {0};
    static LARGE_INTEGER sNewFilePos = {0};
    sNewFilePos.QuadPart = 0;
    sDistanceToMove.QuadPart = i64ReadBeginOffset;

    SetFilePointerEx(pobjIOSystemDevice->device,sDistanceToMove,&sNewFilePos,FILE_BEGIN);
    if(sNewFilePos.QuadPart != i64ReadBeginOffset)
    {
        return (-1);
    }

    BOOL b_read_result = ReadFile(pobjIOSystemDevice->device,pBuf,u32BytesToRead,(LPDWORD)&i32ReadBytes,NULL);
    if(b_read_result == FALSE)
    {
        giFDBErrNum = GetLastError();
        FDBErrStr_TWO_PARAM("read file(%s) failed,ec(%d)",pobjIOSystemDevice->pc_device_path,giFDBErrNum);
        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"read file(%s) failed,ec(%d)",pobjIOSystemDevice->pc_device_path,giFDBErrNum);
    }

    return (i32ReadBytes);
}

int32_t FDBfIOSystemDevice_write(FDBCIOSystemDevice *pobjIOSystemDevice
                              , const void *pBuf
                              , const uint32_t u32BytesToWrite)
{
    int32_t i32BytesWrite = 0;
    if(!pobjIOSystemDevice || !pBuf || pobjIOSystemDevice->device < 0)
    {
        giFDBErrNum = FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR;
        FDBErrStr_FOUR_PARAM("%s,write %d bytes into 0x%x,error code:%d"
                             ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToWrite,pBuf,giFDBErrNum);
        FDBLOG_FOUR_PARAM(FDB_LOG_FILE,"%s,write %d bytes into 0x%x,error code:%d"
                          ,FDB_ERR_INVALID_IO_DEVICE_DESCRIPTOR_STR,u32BytesToWrite,pBuf,giFDBErrNum);

        return (-1);
    }

    WriteFile(pobjIOSystemDevice->device,pBuf,u32BytesToWrite,(LPDWORD)&i32BytesWrite,NULL);

    return (i32BytesWrite);
}

int64_t FDBfIOSystemDevice_seek(FDBCIOSystemDevice *pobjIOSystemDevice
                                      , const int64_t i64Offset
                                      , const uint32_t u32SeekFrom)
{
    static LARGE_INTEGER sDistanceToMove = {0};
    static LARGE_INTEGER sNewFilePos = {0};
    sNewFilePos.QuadPart = 0;
    sDistanceToMove.QuadPart = i64Offset;

    SetFilePointerEx(pobjIOSystemDevice->device,sDistanceToMove,&sNewFilePos,u32SeekFrom);
    if(sNewFilePos.QuadPart != i64Offset)
    {
        return (-1);
    }

    return (sNewFilePos.QuadPart);
}

long long int FDBfIOSystemDevice_size(FDBCIOSystemDevice* pobjIODevice)
{
    long long int i64DeviceSize = 0;
    GET_LENGTH_INFORMATION sDeviceLengthInfo;
    DWORD dwReturnSize;
    DeviceIoControl(pobjIODevice->device,
                    IOCTL_DISK_GET_LENGTH_INFO,
                    NULL,
                    0,
                    (LPVOID)&sDeviceLengthInfo,
                    sizeof(GET_LENGTH_INFORMATION),
                    &dwReturnSize,
                    NULL);

    i64DeviceSize = sDeviceLengthInfo.Length.QuadPart;

    return (i64DeviceSize);
}

int64_t FDBfIOSystemDecvice_tell(FDBCIOSystemDevice* pobjIOSystemDevice)
{
    long long int i64FilePointPos = 0;

    if(!pobjIOSystemDevice)
    {
        return (-1);
    }

    i64FilePointPos = FDBfIOSystemDevice_seek(pobjIOSystemDevice,0,SEEK_CUR);

    return (i64FilePointPos);
}

#endif
