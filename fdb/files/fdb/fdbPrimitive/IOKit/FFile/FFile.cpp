﻿#include "FFile.h"

#include "../IODevice/IODevice.h"

FFile::FFile(const char* pc_path)
: CIO(pc_path)
{
    this->m_p_obj_io_system_device = FDBfIOSystemDevice_new_not_open(pc_path);
    this->m_io_locker = new boost::mutex();
    this->m_opened = false;
}
FFile::~FFile()
{
    Close();
    delete this->m_io_locker;
}

int FFile::Open(FDBEAccessFlag e_access_flag, FDBEOpenMethod e_open_method)
{
    boost::mutex::scoped_lock lock(*this->m_io_locker);
    if(this->m_opened)
    {
        return (0);
    }
    int code = FDBfIOSystemDevice_open(this->m_p_obj_io_system_device,e_access_flag,e_open_method);
    if(code != -1)
    {
        this->m_opened = true;
    }
    return (code);
}

void FFile::Close()
{
    boost::mutex::scoped_lock lock(*this->m_io_locker);
    FDBfIOSystemDevice_delete(&this->m_p_obj_io_system_device);
}

int FFile::Read(void *buf, uint32_t len)
{
    this->m_io_bytes += len;
    boost::mutex::scoped_lock lock(*this->m_io_locker);
    return ( FDBfIOSystemDevice_read(this->m_p_obj_io_system_device,buf,len) );
}
int FFile::ReadFrom(void *buf, uint32_t len, int64_t from_begin)
{
    this->m_io_bytes += len;
    boost::mutex::scoped_lock lock(*this->m_io_locker);
    int64_t new_pos = FDBfIOSystemDevice_seek(this->m_p_obj_io_system_device
                                           ,from_begin+this->m_base_offset
                                           ,SEEK_SET);
    if(new_pos != from_begin)
    {
        return (-1);
    }
    return (FDBfIOSystemDevice_read(this->m_p_obj_io_system_device,buf,len));
}

int FFile::Write(const void *buf, uint32_t len)
{
    this->m_io_bytes += len;
    boost::mutex::scoped_lock lock(*this->m_io_locker);
    return ( FDBfIOSystemDevice_write(this->m_p_obj_io_system_device,buf,len) );
}



int64_t FFile::SetPoint(int64_t offset, int mode)
{
    boost::mutex::scoped_lock lock(*this->m_io_locker);
    return ( FDBfIOSystemDevice_seek(this->m_p_obj_io_system_device
                                     ,offset+this->m_base_offset
                                     ,mode) );
}

int64_t FFile::GetDeviceCapacity()
{
    boost::mutex::scoped_lock lock(*this->m_io_locker);
    return ( FDBfIODeviceSizeThroughPath(this->m_p_obj_io_system_device->pc_device_path) );
}
