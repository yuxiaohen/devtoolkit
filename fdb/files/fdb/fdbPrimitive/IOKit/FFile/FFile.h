﻿#pragma once

#include "../CIO.h"
#include "../IODevice/IOSystemDevice.h"

#ifndef Q_MOC_RUN
#include <boost/thread.hpp>
#endif

class FDBInterface FFile : public CIO {
private:
    boost::mutex* m_io_locker;
    FDBCIOSystemDevice* m_p_obj_io_system_device;
    bool m_opened;
public:
    FFile(const char *pc_path);
    virtual ~FFile();
    virtual int Open(FDBEAccessFlag e_access_flag,FDBEOpenMethod e_open_method);
    virtual void Close();

    virtual int Read(void *buf, uint32_t len);
    virtual int ReadFrom(void* buf,uint32_t len,int64_t from_begin);
    virtual int Write(const void *buf, uint32_t len);

    virtual int64_t SetPoint(int64_t offset, int mode);

    virtual bool Usable() { return (true); };

    virtual int64_t GetDeviceCapacity();
};
