﻿#include "CIO.h"
#include "FFile/FFile.h"

CIO *FDBfIOFactoryInterface(const char* pc_path)
{
    CIO* ret = NULL;
    if(strstr(pc_path,"bitlocker:/"))
    {
        return (NULL);
    } else {
        FFile* cio = new FFile(pc_path);
        ret = dynamic_cast<CIO*>(cio);
    }
    return (ret);
}

void CIO::SetDeviceCapacity(const int64_t size)
{
    this->m_dev_size = size;
}
