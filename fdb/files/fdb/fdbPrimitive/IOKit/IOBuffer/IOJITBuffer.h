/* 2013-08-03
 * brief: disk i/o ring buffer
 * author:shawhen
 * version: 0.0.2
 *
 * visible lack:
 *  1.support 4096 bytes per sector disk.
 *
 * LICENSE: LGPL
 */
#ifndef __fdb_io_ring_buffer_h_2013_08_03__
#define __fdb_io_ring_buffer_h_2013_08_03__

#include "../IOKitlib.h"
#include "../IODevice/IODevice.h"

/*don't manipulate this roughly,use provided API*/
typedef struct FDBCIOJITBuffer{
    char* pcInternalBuffer;
    int iInternalBufferSize;

    FDBCIOSystemDevice* pobjIODevice;

    long long int i64CachedOffsetBegin;
    long long int i64CachedOffsetEnd;
    int iCacheCursorBegin;
    int iCacheCursorEnd;

    long long int i64Wanted;

    boost::thread* pobjFillingThread;

    boost::interprocess::interprocess_semaphore* pobjGotoCacheKick;
    boost::interprocess::interprocess_semaphore* pobjHaveCacheKick;

    int bCallStop;
} FDBCIOJITBuffer;

/*!
 * @brief: I think most people will cache from sector zero,so i will not provide
 * more parameter to enslave stack
 * @param iInternalBufferSize ring buffer size,multiply of 512(n * 512)
 * @return NULL for failed(global error detect).
 */
FDBInterface FDBCIOJITBuffer* FDBfIOJITBuffer_new(const char* pDiskPath,const int iInternalBufferSize);
FDBInterface void FDBfIOJITBuffer_delete(FDBCIOJITBuffer** ppobjIOJITBuffer);

/*!
 * @brief all i/o operation in bytes
 * @return actually get bytes,-1 for error
 */
FDBInterface int FDBfIOJITBuffer_get(FDBCIOJITBuffer* pobjIOJITBuffer
                                      , void *pBuf
                                      , const long long int i64Offset
                                      , const long i32Length);


#endif //__disk_io_ring_buffer_h_2013_08_03__
