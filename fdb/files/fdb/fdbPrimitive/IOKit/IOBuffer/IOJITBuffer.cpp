#include "IOJITBuffer.h"

#include <stdlib.h>
#include <string.h>

#include "../../MemoryManagement/MemoryManagement.h"
#include "../../Exception/Exception.h"

void* fJITBufferFiller(void* pArg)
{
    FDBCIOJITBuffer* pobjIOJITBuffer = (FDBCIOJITBuffer*)pArg;
    long long int i64Offset = 0;
    int iReadRet = 0;
	int iReusableMemoryBytes = 0;
	int iReusableMemoryOffset = 0;

    while(1)
    {
        pobjIOJITBuffer->pobjGotoCacheKick->wait();

        if(pobjIOJITBuffer->bCallStop)
        {
            return (NULL);
        }

		iReusableMemoryBytes = 0;
		iReusableMemoryOffset = 0;
		//maybe we can use last time cache memory?
		if(pobjIOJITBuffer->i64Wanted < pobjIOJITBuffer->i64CachedOffsetEnd 
			&& pobjIOJITBuffer->i64Wanted > pobjIOJITBuffer->i64CachedOffsetBegin) //yes
		{
			iReusableMemoryBytes = pobjIOJITBuffer->i64CachedOffsetEnd - pobjIOJITBuffer->i64Wanted;
			iReusableMemoryOffset = pobjIOJITBuffer->i64Wanted-pobjIOJITBuffer->i64CachedOffsetBegin;
            if(pobjIOJITBuffer->i64Wanted != pobjIOJITBuffer->i64CachedOffsetBegin)
            {
                memmove(pobjIOJITBuffer->pcInternalBuffer
                        ,pobjIOJITBuffer->pcInternalBuffer + iReusableMemoryOffset
                        ,iReusableMemoryBytes);
            }
		}
        if(iReusableMemoryBytes < pobjIOJITBuffer->iInternalBufferSize)
        {
            i64Offset = pobjIOJITBuffer->i64Wanted;
            iReadRet = FDBfIOSystemDevice_read_from(pobjIOJITBuffer->pobjIODevice
                                         ,pobjIOJITBuffer->pcInternalBuffer+iReusableMemoryBytes
                                         ,i64Offset+iReusableMemoryBytes
                                         ,pobjIOJITBuffer->iInternalBufferSize-iReusableMemoryBytes);
            if( iReadRet < 0 )
            {
                pobjIOJITBuffer->i64CachedOffsetBegin = 0;
                pobjIOJITBuffer->i64CachedOffsetEnd = 0;
                pobjIOJITBuffer->iCacheCursorBegin = 0;
                pobjIOJITBuffer->iCacheCursorEnd = 0;
            } else { //i/oed
                pobjIOJITBuffer->iCacheCursorBegin = 0;
                pobjIOJITBuffer->iCacheCursorEnd = iReusableMemoryBytes+iReadRet;
                pobjIOJITBuffer->i64CachedOffsetBegin = i64Offset;
                pobjIOJITBuffer->i64CachedOffsetEnd = i64Offset + pobjIOJITBuffer->iCacheCursorEnd;
            }
        }
        pobjIOJITBuffer->pobjHaveCacheKick->post();
    } //while
    return (NULL);
}

FDBCIOJITBuffer *FDBfIOJITBuffer_new(const char *pcDiskPath, const int iInternalBufferSize)
{
	int iRet = 0;
    FDBCIOJITBuffer* pobjIOJITBuffer = NULL;
    if( (iInternalBufferSize % 512) )
    {
        giFDBErrNum = FDB_ERR_PARAMETER_IS_INCORRECT;
        FDBErrStr_THREE_PARAM("%s when new io ring buffer(path:%s,internal buffer size:%d)"
                              ,FDB_ERR_PARAMETER_IS_INCORRECT_STR
                              ,pcDiskPath,iInternalBufferSize);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new io ring buffer(path:%s,internal buffer size:%d)"
                           ,FDB_ERR_PARAMETER_IS_INCORRECT_STR
                           ,pcDiskPath,iInternalBufferSize);

        return (NULL);
    }
    pobjIOJITBuffer = new FDBCIOJITBuffer();
    if(!pobjIOJITBuffer)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new io ring buffer(path:%s,internal buffer size:%d)"
                              ,FDB_ERR_MEM_NOT_ENOUGH_STR
                              ,pcDiskPath,iInternalBufferSize);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new io ring buffer(path:%s,internal buffer size:%d)"
                           ,FDB_ERR_MEM_NOT_ENOUGH_STR
                           ,pcDiskPath,iInternalBufferSize);

        return (NULL);
    }

    pobjIOJITBuffer->iInternalBufferSize = iInternalBufferSize;
    pobjIOJITBuffer->pcInternalBuffer = (char*)malloc(iInternalBufferSize);
    if(!pobjIOJITBuffer->pcInternalBuffer)
    {
        FDBfIOJITBuffer_delete(&pobjIOJITBuffer);
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new io ring buffer's internalbuffer(path:%s,internal buffer size:%d)"
                              ,FDB_ERR_MEM_NOT_ENOUGH_STR
                              ,pcDiskPath,iInternalBufferSize);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new io ring buffer's internalbuffer(path:%s,internal buffer size:%d)"
                           ,FDB_ERR_MEM_NOT_ENOUGH_STR
                           ,pcDiskPath,iInternalBufferSize);

        return (NULL);
    }

    //internal i/o device
    pobjIOJITBuffer->pobjIODevice = FDBfIOSystemDevice_new_not_open(pcDiskPath);
    iRet = FDBfIOSystemDevice_open(pobjIOJITBuffer->pobjIODevice,FDBEAccessFlag_ReadOnly,FDBEOpenMethod_OpenExisting);
    if(iRet != 0)
    {
        FDBfIOJITBuffer_delete(&pobjIOJITBuffer);
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new io jit buffer's iodevice(path:%s,internal buffer size:%d)"
                              ,FDB_ERR_MEM_NOT_ENOUGH_STR
                              ,pcDiskPath,iInternalBufferSize);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new io jit buffer's iodevice(path:%s,internal buffer size:%d)"
                           ,FDB_ERR_MEM_NOT_ENOUGH_STR
                           ,pcDiskPath,iInternalBufferSize);

        return (NULL);
    }

    /*CREATE KICK AND THREAD*/
    pobjIOJITBuffer->pobjGotoCacheKick = new boost::interprocess::interprocess_semaphore(1);
    if(!pobjIOJITBuffer->pobjGotoCacheKick)
    {
        FDBfIOJITBuffer_delete(&pobjIOJITBuffer);
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new io jit buffer's goto cache kick(path:%s,internal buffer size:%d)"
                              ,FDB_ERR_MEM_NOT_ENOUGH_STR
                              ,pcDiskPath,iInternalBufferSize);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new io jit buffer's goto cache kick(path:%s,internal buffer size:%d)"
                           ,FDB_ERR_MEM_NOT_ENOUGH_STR
                           ,pcDiskPath,iInternalBufferSize);

        return (NULL);
    }

    pobjIOJITBuffer->pobjHaveCacheKick = new boost::interprocess::interprocess_semaphore(0);
    if(!pobjIOJITBuffer->pobjHaveCacheKick)
    {
        FDBfIOJITBuffer_delete(&pobjIOJITBuffer);
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new io jit buffer's have cache kick(path:%s,internal buffer size:%d)"
                              ,FDB_ERR_MEM_NOT_ENOUGH_STR
                              ,pcDiskPath,iInternalBufferSize);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new io jit buffer's have cache kick(path:%s,internal buffer size:%d)"
                           ,FDB_ERR_MEM_NOT_ENOUGH_STR
                           ,pcDiskPath,iInternalBufferSize);

        return (NULL);
    }

    pobjIOJITBuffer->pobjFillingThread = new boost::thread(&fJITBufferFiller,pobjIOJITBuffer);
    if(!pobjIOJITBuffer->pobjFillingThread)
    {
        FDBfIOJITBuffer_delete(&pobjIOJITBuffer);
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new io ring buffer's filling thread(path:%s,internal buffer size:%d)"
                              ,FDB_ERR_MEM_NOT_ENOUGH_STR
                              ,pcDiskPath,iInternalBufferSize);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new io ring buffer's filling thread(path:%s,internal buffer size:%d)"
                           ,FDB_ERR_MEM_NOT_ENOUGH_STR
                           ,pcDiskPath,iInternalBufferSize);

        return (NULL);
    }

    return (pobjIOJITBuffer);
}

void FDBfIOJITBuffer_delete(FDBCIOJITBuffer **ppobjIOJITBuffer)
{
	if(!*ppobjIOJITBuffer)
	{
		return ;
	}

    (*ppobjIOJITBuffer)->bCallStop = 1;
    //give filler thread a kick
    if( (*ppobjIOJITBuffer)->pobjGotoCacheKick )
    {
        (*ppobjIOJITBuffer)->pobjGotoCacheKick->post();
    }

    if( (*ppobjIOJITBuffer)->pobjFillingThread )
    {
        (*ppobjIOJITBuffer)->pobjFillingThread->join();
        delete (*ppobjIOJITBuffer)->pobjFillingThread;
    }

    FDBfFree( (void**)&(*ppobjIOJITBuffer)->pcInternalBuffer );
    FDBfIOSystemDevice_delete( &(*ppobjIOJITBuffer)->pobjIODevice );

    if( (*ppobjIOJITBuffer)->pobjGotoCacheKick )
    {
        delete (*ppobjIOJITBuffer)->pobjGotoCacheKick;
    }
    if( (*ppobjIOJITBuffer)->pobjHaveCacheKick )
    {
        delete (*ppobjIOJITBuffer)->pobjHaveCacheKick;
    }

    delete (*ppobjIOJITBuffer);
}

int FDBfIOJITBuffer_get(FDBCIOJITBuffer *pobjIOJITBuffer
                         , void* pBuf
                         , const long long i64Offset
                         , const long i32Length)
{
    long i32RemainBytes = i32Length;
	long i32CacheMatchedBytes = 0;
    int iRet = 0;

    if(!pobjIOJITBuffer || i32Length < 0 || i64Offset < 0)
	{
		return (-1);
	}


    pobjIOJITBuffer->pobjHaveCacheKick->wait();

    if( i64Offset >= pobjIOJITBuffer->i64CachedOffsetBegin
            && i64Offset <= pobjIOJITBuffer->i64CachedOffsetEnd ) //shot cache
    {
        if(i64Offset + i32Length <= pobjIOJITBuffer->i64CachedOffsetEnd) //entire data matched
        {
            memcpy(pBuf
                   ,pobjIOJITBuffer->pcInternalBuffer+(i64Offset-pobjIOJITBuffer->i64CachedOffsetBegin)
                   ,i32Length);
            i32RemainBytes = 0;
        } else { //section matched
            i32CacheMatchedBytes = (pobjIOJITBuffer->i64CachedOffsetEnd-i64Offset);
            memcpy(pBuf
                   ,pobjIOJITBuffer->pcInternalBuffer+(i64Offset-pobjIOJITBuffer->i64CachedOffsetBegin)
                   ,i32CacheMatchedBytes);
            i32RemainBytes = i32Length - i32CacheMatchedBytes;
        }
    }

	//deal with remain need i/o section
	if(i32RemainBytes > 0)
    {
        iRet = FDBfIOSystemDevice_read_from(pobjIOJITBuffer->pobjIODevice
                                      ,(char*)pBuf+i32CacheMatchedBytes
                                      ,i64Offset+i32CacheMatchedBytes,i32RemainBytes);
		if(iRet > 0)
		{
			i32RemainBytes -= iRet;
		}
	}

    //leave wanted and give filling thread a kick
    pobjIOJITBuffer->i64Wanted = i64Offset + i32Length;

    pobjIOJITBuffer->pobjGotoCacheKick->post();

	return (i32Length - i32RemainBytes);
}
