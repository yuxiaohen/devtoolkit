﻿#pragma once

#include "../Type/int.h"

#include "IODevice/IODevicelib.h"

#include <memory>
#include <string>

using namespace std;

/*!
 * @brief i/o设备接口
 */
class FDBInterface CIO {
protected:
    string m_dev_path;
    int64_t m_dev_size;
    int64_t m_base_offset;

    int64_t m_io_bytes;
public:
    CIO(const char* pc_io_device_path)
    {
        this->m_dev_path = std::string(pc_io_device_path);
        this->m_dev_size = this->m_base_offset = this->m_io_bytes = 0;
    }
    virtual ~CIO()
    {
    }

    virtual int64_t ioBytes()
    {
        return (this->m_io_bytes);
    }

    /*!
     * @brief 返回设备io的路径
     * @return
     */
    std::string DevPath()
    {
        return (this->m_dev_path);
    }

    /*!
     * @brief 打开i/o设备
     * @param 访问权限标志
     * @param 打开方式
     * @return 返回错误码:-1失败,0成功
     */
    virtual int Open(FDBEAccessFlag e_access_flag,FDBEOpenMethod e_open_method)=0; //打开

    virtual void Close()=0; //关闭

    /*!
     * @brief 读写i/o设备
     * @return 返回i/o的字节数
     */
    virtual int Read(void* buf, uint32_t len)=0;
    virtual int ReadFrom(void* buf,uint32_t len,int64_t from_begin)=0;
    virtual int Write(const void* buf, uint32_t len)=0;

    /*!
     * @brief 设置i/o设备的位置
     * @param mode 与c89兼容的偏移标志(SEEK_SET,SEEK_CUR,SEEK_END)
     * @return 返回设置后i/o设备的新的位置,-1失败
     */
    virtual int64_t SetPoint(int64_t offset,int mode)=0;

    /*!
     * @brief 设置一个固定的i/o基础偏移
     * @note 主要为了应对解析一个全局的i/o设备内的一个小的部分,比如:
     *          i/o设备为磁盘,要解析500偏移处开始的一个文件系统
     *          文件系统内指示的偏移值全部为相对于文件系统开始处的偏移值,即文件系统
     *          内指示的100偏移,实际上在磁盘内为600偏移,故可做此用
     */
    virtual void SetBaseOffset(int64_t offset)
    {
        this->m_base_offset = offset;
    }

    virtual int64_t GetBaseOffset()
    {
        return (this->m_base_offset);
    }

    /*!
     * @brief 当文件可用时返回真
     */
    virtual bool Usable()=0;

    /*!
     * @brief 返回设备容量(比如磁盘大小,文件大小,usb大小)
     */
    virtual int64_t GetDeviceCapacity()=0;//获取文件大小
    virtual void SetDeviceCapacity(const int64_t size); //设置文件大小
};

/*!
 * @brief 创建一个i/o设备
 * @param pc_path locale编码的设备路径
 * @return 一个只构造了的i/o设备
 * @note 目前支援的设备类型前缀有
 *          1.无前缀,比如c:/demo.iso,/etc/profile...
 *          2.android手机,android:/
 *          3.bitlocker分区,bitlocker:/
 */
FDBInterface CIO* FDBfIOFactoryInterface(const char* pc_path);
