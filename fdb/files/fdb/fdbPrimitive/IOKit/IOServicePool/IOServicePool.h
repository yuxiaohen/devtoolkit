/* file: IOServicePool.h
 *
 * This file is part of FullDataBox(FDB) library
 *
 * copyright 2012-2013 ShangHai JiaYu Co.,
 *
 *
 * author: shawhen
 *
 * version: 2013-07-05 init
 */
#ifndef __fdb_io_service_h_2013_08_29_14_33__
#define __fdb_io_service_h_2013_08_29_14_33__

#ifndef Q_MOC_RUN
    #include <boost/asio.hpp>
    #include <boost/thread.hpp>
    #include <boost/atomic.hpp>
#endif

//std
#include <vector>

#include "../IOKitlib.h"

typedef struct FDBCIOServicePool {
    std::vector<boost::shared_ptr<boost::asio::io_service> > m_vecspIOServices;
    std::vector<boost::shared_ptr<boost::asio::io_service::work> > m_vecspIOServiceWorks;
    int iNextIOService;
    boost::atomic<bool> mbatc_AllStopped; //reset this value MUST be true
} FDBCIOServicePool;

/*!
 * \brief create a io service pool
 * \return NULL for failed
 */
FDBInterface FDBCIOServicePool* FDBfIOServicePool_new_not_run(const int iPoolSize = -1);

/*!
 * @brief drive a io service pool run,and block
 */
FDBInterface void FDBfIOServicePool_run(FDBCIOServicePool* pobjIOServicePool);

FDBInterface void FDBfIOServicePool_stop(FDBCIOServicePool* pobjIOServicePool);

FDBInterface boost::shared_ptr<boost::asio::io_service>
                        FDBfIOServicePool_get_io_service(FDBCIOServicePool* pobjIOServicePool);

FDBInterface void FDBfIOServicePool_delete(FDBCIOServicePool** ppobjIOServicePool);

#endif //header
