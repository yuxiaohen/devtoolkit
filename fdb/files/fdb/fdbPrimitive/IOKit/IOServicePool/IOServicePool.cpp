#include "IOServicePool.h"

#include "../../MemoryManagement/MemoryManagement.h"

FDBCIOServicePool* FDBfIOServicePool_new_not_run(const int iPoolSize/*=-1*/)
{
    FDBCIOServicePool* pobjIOServicePool = new FDBCIOServicePool();
    if(!pobjIOServicePool)
    {
        return (NULL);
    }

    int iDecidePoolSize = iPoolSize;
    if(iPoolSize<=0) //set pool size by hardware
    {
        iDecidePoolSize = std::max(boost::thread::hardware_concurrency(),(unsigned)1);
    }

    for(int i=0;i < iDecidePoolSize;++ i)
    {
        boost::shared_ptr<boost::asio::io_service> spIOService(new boost::asio::io_service());
        boost::shared_ptr<boost::asio::io_service::work>
                spIOServiceWork(new boost::asio::io_service::work(*spIOService));
        pobjIOServicePool->m_vecspIOServices.push_back(spIOService);
        pobjIOServicePool->m_vecspIOServiceWorks.push_back(spIOServiceWork);
    }

    pobjIOServicePool->iNextIOService = 0;
    pobjIOServicePool->mbatc_AllStopped = true;

    return (pobjIOServicePool);
}

void FDBfIOServicePool_run(FDBCIOServicePool *pobjIOServicePool)
{
    if(!pobjIOServicePool)
    {
        return ;
    }
    if(pobjIOServicePool->mbatc_AllStopped)
    {
        boost::thread_group tgIOService;
        for(int i=0;i < pobjIOServicePool->m_vecspIOServices.size();++ i)
        {
            pobjIOServicePool->m_vecspIOServices[i]->reset();
            tgIOService.create_thread(boost::bind(&boost::asio::io_service::run
                                                  ,pobjIOServicePool->m_vecspIOServices[i]));
        }
        pobjIOServicePool->mbatc_AllStopped = false;
        tgIOService.join_all();
    }
}

void FDBfIOServicePool_stop(FDBCIOServicePool *pobjIOServicePool)
{
    if(!pobjIOServicePool)
    {
        return ;
    }
    for(int i=0;i < pobjIOServicePool->m_vecspIOServices.size();++ i)
    {
        pobjIOServicePool->m_vecspIOServices[i]->stop();
    }
    pobjIOServicePool->mbatc_AllStopped = true;
}

boost::shared_ptr<boost::asio::io_service>
            FDBfIOServicePool_get_io_service(FDBCIOServicePool *pobjIOServicePool)
{
    if(!pobjIOServicePool)
    {
        return (boost::shared_ptr<boost::asio::io_service>());
    }
    boost::shared_ptr<boost::asio::io_service> spResult;
    spResult = pobjIOServicePool->m_vecspIOServices[pobjIOServicePool->iNextIOService++];
    pobjIOServicePool->iNextIOService
            = pobjIOServicePool->iNextIOService % pobjIOServicePool->m_vecspIOServices.size();
    return (spResult);
}

void FDBfIOServicePool_delete(FDBCIOServicePool **ppobjIOServicePool)
{
    if(*ppobjIOServicePool)
    {
        FDBfIOServicePool_stop(*ppobjIOServicePool);
        delete (*ppobjIOServicePool);
        *ppobjIOServicePool = NULL;
    }
}
