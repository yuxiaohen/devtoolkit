#ifndef __fdb_int_h_2013_09_01_13_01__
#define __fdb_int_h_2013_09_01_13_01__

#if defined(_WIN32) || defined(__APPLE__)
	#include <stdint.h>
#elif defined(__linux__)
    #if (__GNUC__ <= 4) && (__GNUC_MINOR__ <= 4)
        #include <stdint.h>
    #else
        #include <stdint-gcc.h>
    #endif
#else
	#error "Unsupport Platform"
#endif

#endif //header
