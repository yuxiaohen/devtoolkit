DEFINES += _FDB_POSIX

HEADERS += \
    $$PWD/Unix.h \
    $$PWD/Unixlib.h \
    #PseudoTerminal
    $$PWD/PseudoTerminal/PseudoTerminal.h \
    $$PWD/PseudoTerminal/PseudoTerminallib.h \
    $$PWD/PseudoTerminal/LinuxPseudoTerminal.h \
    $$PWD/PseudoTerminal/ApplePseudoTerminal.h
