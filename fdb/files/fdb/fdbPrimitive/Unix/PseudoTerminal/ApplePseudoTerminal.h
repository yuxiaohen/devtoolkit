#if defined(__APPLE__)

#ifndef __fdb_apple_pseudo_terminal_h_2013_09_12_05_35__
#define __fdb_apple_pseudo_terminal_h_2013_09_12_05_35__

typedef struct FDBCPseudoTerminal {
    int iChildPID;
    int iMasterPTY;
    int iSlavePTY;
} FDBCPseudoTerminal;

#endif //header

#endif
