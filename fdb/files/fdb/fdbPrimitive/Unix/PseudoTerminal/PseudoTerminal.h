#ifndef __fdb_pseudo_terminal_h_2013_08_26_10_35__
#define __fdb_pseudo_terminal_h_2013_08_26_10_35__

#include "PseudoTerminallib.h"

#ifndef Q_MOC_RUN
    #include <boost/asio.hpp>
#endif

extern boost::asio::io_service g_PseudoTerminalIOService;

#if defined(__linux__)
    #include "LinuxPseudoTerminal.h"
#elif defined(__APPLE__)
    #include "ApplePseudoTerminal.h"
#else
    #error "Unsupport platform"
#endif

FDBInterface FDBCPseudoTerminal* FDBPseudoTerminal_new();
FDBInterface void FDBPseudoTerminal_delete(FDBCPseudoTerminal** ppobjPseudoTerminal);

/*!
 * \brief execute pcPathToExecute with args
 * \return 0 for succeed,-1 for failed
 */
FDBInterface int FDBPseudoTerminal_execute(FDBCPseudoTerminal* pobjPseudoTerminal
                                            , const char* pcPathToExecute
                                            , char *ppcArgs[] = NULL);

/*!
 * \brief read response info from pseudo terminal master pty
 * \return actually read response info length(bytes),-1 for failed
 */
FDBInterface int FDBPseudoTerminal_response_info(FDBCPseudoTerminal* pobjPseudoTerminal
                                                 ,void* pResponseInfoBuffer
                                                 ,const long i32BufferLen);

/*!
 * \brief type into pseudo terminal
 */
FDBInterface int FDBPseudoTerminal_type(FDBCPseudoTerminal* pobjPseudoTerminal
                                        ,const char* pcBuf
                                        ,const long i32BufLen);

#endif //header
