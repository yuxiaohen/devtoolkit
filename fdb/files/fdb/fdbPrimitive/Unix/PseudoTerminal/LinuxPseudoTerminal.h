#if defined(__linux__)

#ifndef __fdb_linux_pseudo_terminal_h_2013_08_26_10_36__
#define __fdb_linux_pseudo_terminal_h_2013_08_26_10_36__

#ifndef Q_MOC_RUN
    #include <boost/asio.hpp>
    #include <boost/process.hpp>
    #include <boost/iostreams/device/file_descriptor.hpp>
#endif

typedef struct FDBCPseudoTerminal {
    int iChildPID;
    int iMasterPTY;
    int iSlavePTY;
} FDBCPseudoTerminal;

#endif //header

#endif
