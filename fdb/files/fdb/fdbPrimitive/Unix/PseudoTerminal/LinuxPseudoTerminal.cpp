#if defined(__linux__)

#include "PseudoTerminal.h"
#include "LinuxPseudoTerminal.h"

#ifndef Q_MOC_RUN
    #include <boost/process.hpp>
    #include <boost/iostreams/device/file_descriptor.hpp>
    #include <boost/iostreams/stream.hpp>
#endif

#include "../../MemoryManagement/MemoryManagement.h"

FDBCPseudoTerminal* FDBPseudoTerminal_new()
{
    int iRet = 0;
    int iMasterPTY = -1;
    int iSlavePTY = -1;
    char* pcSlavePTYName = NULL;
    FDBCPseudoTerminal* pobjPseudoTerminal = (FDBCPseudoTerminal*)calloc(1,sizeof(FDBCPseudoTerminal));
    if(!pobjPseudoTerminal)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when new pseudo terminal",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when new pseudo terminal",FDB_ERR_MEM_NOT_ENOUGH_STR);

        return (NULL);
    }

    iMasterPTY = posix_openpt(O_RDWR);
    if(iMasterPTY < 0)
    {
        giFDBErrNum = errno;
        FDBErrStr_ONE_PARAM("failed when posix_openpt,error code: %d",giFDBErrNum);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"failed when posix_openpt,error code: %d",giFDBErrNum);

        FDBPseudoTerminal_delete(&pobjPseudoTerminal);
        return (NULL);
    }

    iRet = grantpt(iMasterPTY);
    if(iRet < 0)
    {
        giFDBErrNum = errno;
        FDBErrStr_ONE_PARAM("failed when grantpt,error code: %d",giFDBErrNum);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"failed when grantpt,error code: %d",giFDBErrNum);

        FDBPseudoTerminal_delete(&pobjPseudoTerminal);
        return (NULL);
    }
    iRet = unlockpt(iMasterPTY);
    if(iRet < 0)
    {
        giFDBErrNum = errno;
        FDBErrStr_ONE_PARAM("failed when unlockpt,error code: %d",giFDBErrNum);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"failed when unlockpt,error code: %d",giFDBErrNum);

        FDBPseudoTerminal_delete(&pobjPseudoTerminal);
        return (NULL);
    }

    pcSlavePTYName = ptsname(iMasterPTY);
    if(!pcSlavePTYName)
    {
        giFDBErrNum = errno;
        FDBErrStr_TWO_PARAM("failed when get slave pty's name(ptsname(%d)),error code: %d"
                            ,iMasterPTY,giFDBErrNum);
        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"failed when get slave pty's name(ptsname(%d)),error code: %d"
                         ,iMasterPTY,giFDBErrNum);

        FDBPseudoTerminal_delete(&pobjPseudoTerminal);
        return (NULL);
    }
    iSlavePTY = open(pcSlavePTYName,O_RDWR);
    if(iSlavePTY < 0)
    {
        giFDBErrNum = errno;
        FDBErrStr_TWO_PARAM("failed when open(slave:%s),error code: %d"
                            ,pcSlavePTYName,giFDBErrNum);
        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"failed when open(slave:%s),error code: %d"
                         ,pcSlavePTYName,giFDBErrNum);

        FDBPseudoTerminal_delete(&pobjPseudoTerminal);
        return (NULL);
    }

    pobjPseudoTerminal->iMasterPTY = iMasterPTY;
    pobjPseudoTerminal->iSlavePTY = iSlavePTY;
//    pobjPseudoTerminal->pobjSinkSlavePTY=new boost::iostreams::file_descriptor_sink(iSlavePTY
//                                                                                    ,boost::iostreams::close_handle);
//    pobjPseudoTerminal->pobjSourceSlavePTY=new boost::iostreams::file_descriptor_source(iSlavePTY
//                                                                                        ,boost::iostreams::close_handle);
//    pobjPseudoTerminal->childProcess = boost::process::execute( boost::process::initializers::run_exe("/bin/su")
//                             ,boost::process::initializers::bind_stdout(*pobjPseudoTerminal->pobjSinkSlavePTY)
//                             ,boost::process::initializers::bind_stderr(*pobjPseudoTerminal->pobjSinkSlavePTY)
//                             ,boost::process::initializers::bind_stdin(*pobjPseudoTerminal->pobjSourceSlavePTY) );

//    char pBuf[1000] = {0};
//    int iBytesRead = 0;
//    iBytesRead = read(iMasterPTY,pBuf,5);
//    printf("read msg: %s | bytes read: %d\r\n",pBuf,iBytesRead);

    return (pobjPseudoTerminal);
}

void FDBPseudoTerminal_delete(FDBCPseudoTerminal **ppobjPseudoTerminal)
{
    if(*ppobjPseudoTerminal)
    {
        FDBfFree( (void**)ppobjPseudoTerminal );
    }
}

int FDBPseudoTerminal_execute(FDBCPseudoTerminal* pobjPseudoTerminal
                               , const char* pcPathToExecute
                               , char *ppcArgs[]/* = NULL*/)
{
    if(!pobjPseudoTerminal)
    {
        return (-1);
    }

    int iChildPID = fork();
    if(iChildPID)
    {
        pobjPseudoTerminal->iChildPID = iChildPID;
    } else {
        struct termios slave_orig_term_settings; // Saved terminal settings
        struct termios new_term_settings; // Current terminal settings

        // Child

        // Close the master side of the PTY
        close(pobjPseudoTerminal->iMasterPTY);

        // Save the default parameters of the slave side of the PTY
        tcgetattr(pobjPseudoTerminal->iSlavePTY, &slave_orig_term_settings);

        // Set raw mode on the slave side of the PTY
        new_term_settings = slave_orig_term_settings;
        cfmakeraw (&new_term_settings);
        tcsetattr (pobjPseudoTerminal->iSlavePTY, TCSANOW, &new_term_settings);

        // The slave side of the PTY becomes the standard input and outputs of the child process
        close(0); // Close standard input (current terminal)
        close(1); // Close standard output (current terminal)
        close(2); // Close standard error (current terminal)

        dup(pobjPseudoTerminal->iSlavePTY); // PTY becomes standard input (0)
        dup(pobjPseudoTerminal->iSlavePTY); // PTY becomes standard output (1)
        dup(pobjPseudoTerminal->iSlavePTY); // PTY becomes standard error (2)

        //        close(fds);

        // Make the current process a new session leader
        setsid();

        // As the child is a session leader, set the controlling terminal to be the slave side of the PTY
        // (Mandatory for programs like the shell to make them manage correctly their outputs)
        ioctl(0, TIOCSCTTY, 1);

        char pInputBuffer[4096] = {0};
        int iBytesRead = 0;
//        while (1)
//        {
//            iBytesRead = read(pobjPseudoTerminal->iSlavePTY, pInputBuffer, sizeof(pInputBuffer) - 1);

//            if (iBytesRead > 0)
//            {
//                // Replace the terminating \n by a NUL to display it as a string
//                pInputBuffer[iBytesRead - 1] = '\0';

//                printf("Child received instruction: '%s'\n", pcPathToExecute);
                int iRet = execl(pcPathToExecute,"",NULL);
//                boost::iostreams::file_descriptor_sink objSlavePTY(fds,boost::iostreams::never_close_handle);
//                boost::process::child child = boost::process::execute( boost::process::initializers::run_exe(input)
//                                                                       ,boost::process::initializers::bind_stdout(objSlavePTY)
//                                                                       ,boost::process::initializers::bind_stderr(objSlavePTY) );

//                printf("return %d,error code:%d\r\n",iRet,errno);
//            }
//            else
//            {
//                break;
//            }
//        } // End while
    }

//    if(pobjPseudoTerminal->childProcess.pid)
//    {
//        boost::process::wait_for_exit( pobjPseudoTerminal->childProcess );
//    }

//    std::vector<std::string> vecArgs;
//    if(ppcArgs)
//    {
//        for(int i=0;ppcArgs[i] != NULL;++ i)
//        {
//            char* p = ppcArgs[i];
//            vecArgs.push_back( std::string( p ) );
//        }
//    }

//    if(vecArgs.size())
//    {
//        pobjPseudoTerminal->childProcess = boost::process::execute( boost::process::initializers::run_exe(pcPathToExecute)
//                                                                    ,boost::process::initializers::set_args(vecArgs)
//                                                                    ,boost::process::initializers::bind_stdout(*pobjPseudoTerminal->pobjSinkSlavePTY)
//                                                                    ,boost::process::initializers::bind_stderr(*pobjPseudoTerminal->pobjSinkSlavePTY)
//                                                                    ,boost::process::initializers::bind_stdin(*pobjPseudoTerminal->pobjSourceSlavePTY)
//                                                                  );
//    } else {
//        pobjPseudoTerminal->childProcess = boost::process::execute( boost::process::initializers::run_exe(pcPathToExecute)
//                                                                    ,boost::process::initializers::bind_stdout(*pobjPseudoTerminal->pobjSinkSlavePTY)
//                                                                    ,boost::process::initializers::bind_stderr(*pobjPseudoTerminal->pobjSinkSlavePTY)
//                                                                    ,boost::process::initializers::bind_stdin(*pobjPseudoTerminal->pobjSourceSlavePTY)
//                                                                  );
//    }
//    return (0);
}

int FDBPseudoTerminal_response_info(FDBCPseudoTerminal* pobjPseudoTerminal
                                    ,void* pResponseInfoBuffer
                                    ,const long i32BufferLen)
{
    if(!pobjPseudoTerminal)
    {
        return (-1);
    }

    if(pobjPseudoTerminal->iChildPID)
    {
        int iBytesRead = read(pobjPseudoTerminal->iMasterPTY,pResponseInfoBuffer,i32BufferLen);
        printf("bytes read: %d\r\n",iBytesRead);
        return (iBytesRead);
    }
    return (-1);
}

int FDBPseudoTerminal_type(FDBCPseudoTerminal *pobjPseudoTerminal
                           , const char *pcBuf
                           , const long i32BufLen)
{
    if(!pobjPseudoTerminal)
    {
        return (-1);
    }

    if(pobjPseudoTerminal->iChildPID)
    {
        int iBytesWrite = write(pobjPseudoTerminal->iMasterPTY,pcBuf,i32BufLen);
        printf("bytes typed: %d\r\n",iBytesWrite);
        return (iBytesWrite);
    }

    return (-1);
}

#endif
