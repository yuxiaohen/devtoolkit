#ifndef __fdb_uri_lib_h_2013_11_04_05_53__
#define __fdb_uri_lib_h_2013_11_04_05_53__

#include "../Networklib.h"

typedef enum FDBEURISchema {
    FDBEURISchema_Unspecified = 0
    ,FDBEURISchema_HTTP = 1
} FDBEURISchema;

/* URI -> http://www.baidu.com/demo/png.html#thumbnail
 * schema   -> http
 * host     -> www.baidu.com
 * path     -> /demo/png.html
 * anchor   -> thumbnail
 */
typedef struct FDBCURI {
    char pacSchema[10];
    char* pcHostAddr;
    char* pcPath;
    char* pcAnchor;
} FDBCURI;

#endif //header
