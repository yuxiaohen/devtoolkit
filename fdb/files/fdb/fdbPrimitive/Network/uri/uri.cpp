#include "uri.h"

#include <stdlib.h>

#include "../../MemoryManagement/MemoryManagement.h"
#include "../../Exception/Exception.h"

FDBCURI* FDBfURI_new()
{
    FDBCURI* pobjURI = (FDBCURI*)malloc(sizeof(FDBCURI));
    if(!pobjURI)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_ONE_PARAM("%s when new URI",FDB_ERR_MEM_NOT_ENOUGH_STR);
        FDBLOG_ONE_PARAM(FDB_LOG_FILE,"%s when new URI",FDB_ERR_MEM_NOT_ENOUGH_STR);
        return (NULL);
    }
    memset(pobjURI,0,sizeof(FDBCURI));

    return (pobjURI);
}

void FDBfURI_delete(FDBCURI **pp_obj_uri)
{
    FDBfFree( (void**)pp_obj_uri);
}

int FDBfURI_setSchema(FDBCURI* p_obj_uri, const FDBEURISchema e_uri_schema)
{
    if(!p_obj_uri)
	{
		return (-1);
	}

    switch(e_uri_schema)
    {
        case FDBEURISchema_HTTP:
        {
            strcpy(p_obj_uri->pacSchema,"http");
        }break;
        default:
        {
            return (-1);
        }break;
    }

    return (0);
}

int FDBfURI_setHostAddr(FDBCURI *p_obj_uri, const char *p_host_addr)
{
    if(!p_obj_uri)
	{
		return (-1);
	}

    FDBfFree( (void**)&p_obj_uri->pcHostAddr);
    p_obj_uri->pcHostAddr = (char*)malloc(strlen(p_host_addr)+1);
    if(!p_obj_uri->pcHostAddr)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new URI(%x)'s host addr to (%s)",FDB_ERR_MEM_NOT_ENOUGH_STR,p_obj_uri,p_host_addr);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new URI(%x)'s host addr to (%s)",FDB_ERR_MEM_NOT_ENOUGH_STR,p_obj_uri,p_host_addr);

        return (-1);
    }
    strcpy(p_obj_uri->pcHostAddr,p_host_addr);

    return (0);
}

int FDBfURI_setPath(FDBCURI *pobjURI, const char *p_path)
{
    FDBfFree( (void**)&pobjURI->pcPath);
    pobjURI->pcPath = (char*)malloc(strlen(p_path)+1);
    if(!pobjURI->pcPath)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new URI(%x)'s path to (%s)",FDB_ERR_MEM_NOT_ENOUGH_STR,pobjURI,p_path);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new URI(%x)'s path to (%s)",FDB_ERR_MEM_NOT_ENOUGH_STR,pobjURI,p_path);

        return (-1);
    }
    strcpy(pobjURI->pcPath,p_path);

    return (0);
}

int FDBfURI_setAnchor(FDBCURI* p_obj_uri, const char* p_anchor)
{
    FDBfFree( (void**)&p_obj_uri->pcAnchor);
    p_obj_uri->pcAnchor = (char*)malloc(strlen(p_anchor)+1);
    if(!p_obj_uri->pcAnchor)
    {
        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
        FDBErrStr_THREE_PARAM("%s when new URI(%x)'s anchor to (%s)",FDB_ERR_MEM_NOT_ENOUGH_STR,p_obj_uri,p_anchor);
        FDBLOG_THREE_PARAM(FDB_LOG_FILE,"%s when new URI(%x)'s anchor to (%s)",FDB_ERR_MEM_NOT_ENOUGH_STR,p_obj_uri,p_anchor);

        return (-1);
    }
    strcpy(p_obj_uri->pcAnchor,p_anchor);

    return (0);
}
