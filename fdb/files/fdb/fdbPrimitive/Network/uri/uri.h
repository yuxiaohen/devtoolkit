#ifndef __fdb_uri_h_2013_11_04_05_55__
#define __fdb_uri_h_2013_11_04_05_55__

#include "urilib.h"

FDBInterface FDBCURI* FDBfURI_new();
//FDBCURI* FDBfCURI_new_from_string(const char* pcURIString);
FDBInterface void FDBfURI_delete(FDBCURI** pp_obj_uri);

/*!
 * @brief FDBfCURI_setSchema
 * @param pobjURI
 * @param eURISchema
 * @return -1 for failed.0 for succeed
 */
FDBInterface int FDBfURI_setSchema(FDBCURI *p_obj_uri, const FDBEURISchema e_uri_schema);

/*!
 * @brief set uri's host addr
 * @return -1 for failed.0 for succeed
 */
FDBInterface int FDBfURI_setHostAddr(FDBCURI* p_obj_uri,const char* p_host_addr);

/*!
 * @brief set uri's path
 * @return -1 for failed,0 for succeed
 */
FDBInterface int FDBfURI_setPath(FDBCURI* p_obj_uri,const char* p_path);

FDBInterface int FDBfURI_setAnchor(FDBCURI* p_obj_uri,const char* p_anchor);

#endif //header
