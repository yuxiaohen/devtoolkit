#include "http.h"

#include <stdlib.h>

#include "../../../Text/Text.h"

#include <QDebug>

const char* p_err_str = NULL;
int i_err_offset = -1;
char* gp_uri_regex = "([a-zA-Z]{1,})://((\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3})|([a-zA-Z0-9.-]{1,}))(:(\\d{1,10})){0,1}(/[a-zA-Z._-]{0,}){0,}";
pcre* gps_uri_regex = pcre_compile(gp_uri_regex,0,&p_err_str,&i_err_offset,NULL);

FDBCHttp::FDBCHttp()
{
    this->m_io_service = boost::shared_ptr<io_service>(new io_service());
    this->m_sock = boost::shared_ptr<ip::tcp::socket>(new ip::tcp::socket(*this->m_io_service));
    this->m_buf_size = 40960;
    this->m_buf = (char*)calloc(1,this->m_buf_size);
    this->m_match_result_num = 30;
    this->m_match_results = (int*)calloc(this->m_match_result_num,sizeof(int));
    this->m_e_header = EHTTPHeader_null;
}

void FDBCHttp::slEnableHeader(EHTTPHeader e_header)
{
    this->m_e_header = (EHTTPHeader)(this->m_e_header | e_header);
}

pair<string,string> FDBCHttp::slGet(const char *url)
{
    boost::mutex::scoped_lock lock(this->m_locker);
    //截取域名或ip部份
    int url_len = strlen(url);
    int uri_parse_result = pcre_exec(gps_uri_regex,NULL,url,url_len,0,0,this->m_match_results,this->m_match_result_num);
    if(uri_parse_result < 0)
    {
        return (make_pair("",""));
    }
    //[2,3]: schema,[4,5]: host,[12,13]: port,[14,15]: path
    string path("/");
    string host;
    string port("80");
    if(this->m_match_results[4] >= 0 && this->m_match_results[4] <= url_len)
    {
        host = string(url+this->m_match_results[4],this->m_match_results[5]-this->m_match_results[4]);
    }
    if(this->m_match_results[12] >= 0 && this->m_match_results[12] <= url_len)
    {
        port = string(url+this->m_match_results[12],this->m_match_results[13]-this->m_match_results[12]);
    }
    if(this->m_match_results[14] >= 0 && this->m_match_results[14] <= url_len)
    {
        path = string(url+this->m_match_results[14],this->m_match_results[15]-this->m_match_results[14]);
    }
    try {
        ip::tcp::resolver resolver(*this->m_io_service);
        ip::tcp::resolver::query query(host.data(),port);
        ip::tcp::resolver::iterator ite_begin = resolver.resolve(query);
        ip::tcp::resolver::iterator ite_end;
        while( ite_begin != ite_end )
        {
            this->m_sock->connect( *ite_begin );

            //construct get buffer,such as:GET / HTTP/1.1 \r\nHost: 192.168.1.30\r\nConnection: Close\r\n\r\n
            if(this->m_e_header & EHTTPHeader_host)
            {
                sprintf(this->m_buf,"GET %s HTTP/1.1\r\nHost: %s\r\n\r\n",path.data(),host.data());
            }
            write(*this->m_sock,buffer(this->m_buf,strlen(this->m_buf)));
            boost::asio::streambuf response_buf;
            auto response_header_len = read_until(*this->m_sock,response_buf,"\r\n\r\n");
            istream response_stream(&response_buf);
            response_stream.read(this->m_buf,response_header_len);
            int content_len = 0;
            char* content_len_pos = strstr(this->m_buf,"Content-Length: ");
            if(content_len_pos)
            {
                sscanf(content_len_pos,"Content-Length: %d",&content_len);
            }
            if(content_len > 0)
            {
                char* p_content = (char*)malloc(content_len);
                response_stream.read(p_content,content_len);
                pair<string,string> response = make_pair(string(this->m_buf,response_header_len)
                                                         ,string(p_content,content_len));
                free(p_content);

                this->m_sock->close();
                return (response);
            } else {
                this->m_sock->close();
                ++ ite_begin;
            }
        }
    } catch(boost::system::system_error ec) {
        qDebug() << ec.what();
        return (make_pair("",""));
    }
    return (make_pair("",""));
}

//long long int FDBfGetHTTPContentLength(const char* pcBuf,int iBufLen)
//{
//	int iRet = 0;
//    char* pcContentLengthMarkPos = NULL;
//    long long int i64ContentLength = 0;
//    if(!pcBuf || !iBufLen)
//    {
//        return (-1);
//    }
//    //found Content-Length: pos
//    pcContentLengthMarkPos = FDBfStrstr(pcBuf,iBufLen,"Content-Length:");
//    if(!pcContentLengthMarkPos)
//    {
//        return (-1);
//    }

//    iRet = sscanf(pcContentLengthMarkPos+strlen("Content-Length: "),"%lld",&i64ContentLength);
//    return (i64ContentLength);
//}

//char* FDBfFindHTTPContentBegin(const char *pBuf, const int iBufLen)
//{
//    char* pcContentBegin = NULL;
//    //pBuf contains \n\r
//    pcContentBegin = FDBfStrstr(pBuf,iBufLen,"\n\r");

//    return (pcContentBegin+3);
//}

//int FDBfHTTPDownload(const char *pcURL, const char *pcStoreDirectory)
//{
//    FDBCSocket* pobjDownloadSocket = FDBfSocket_new(FDBEInternetFamily_IPV4,FDBESocketType_TCP,FDBESocketProtocol_Auto);

//    FDBCURI* pobjURI = FDBfURI_new();
//    FDBfURI_setSchema(pobjURI,FDBEURISchema_HTTP);
//    FDBfURI_setHostAddr(pobjURI,"192.168.1.31");
//    FDBfURI_setPath(pobjURI,"/demo.exe");
//    FDBfHTTPDownload_through_exist_socket(pobjDownloadSocket,pobjURI,pcStoreDirectory);

//    FDBfURI_delete(&pobjURI);
//    FDBfSocket_delete(&pobjDownloadSocket);

//	return (0);
//}

//int FDBfHTTPDownload_through_exist_socket(FDBCSocket* pobjSocket,const FDBCURI* pobjURI, const char* pcStoreDirectory)
//{
//    int iRet = 0;
//    char* pcResponseInfoBuf = NULL;
//	long long int i64ContentLength = 0;
//    long long int i64WrittenContentBytes = 0;
//    char* pcContentBegin = NULL; /*not new,peek in pcResponseInfoBuf*/
//    char* pcDownloadFileName = NULL;
//    char* pcDownloadFileStorePath = NULL;
//    FDBCIODevice* pobjDownloadFile = NULL;
//    long long int i64FullLoadWrittenTime = 0; //once write HTTP_RESPONSE_INFO_BUFFER_LENGTH bytes time
//    long long int i64FullLoadWrittenTimeCursor = 0;
//	iRet = FDBfSocket_connect(pobjSocket,pobjURI->pcHostAddr,80);
//	if(iRet<0)
//	{
//		return (-1);
//	}
//    iRet = FDBfHTTPRequest_through_exist_socket(pobjSocket
//                                                ,pobjURI,FDBEHTTPCommand_GET
//                                                ,FDBEHTTPConnectionMethod_Close);
//    if(iRet<0)
//    {
//        return (iRet);
//    }
//    // ^
//    // | send download request normal

//    pcResponseInfoBuf = (char*)calloc(1,HTTP_RESPONSE_INFO_BUFFER_LENGTH);
//    if(!pcResponseInfoBuf)
//    {
//        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
//        FDBErrStr_TWO_PARAM("%s,when new http response info buffer(len:%d)"
//                            ,FDB_ERR_MEM_NOT_ENOUGH_STR,HTTP_RESPONSE_INFO_BUFFER_LENGTH);
//        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"%s,when new http response info buffer(len:%d)"
//                         ,FDB_ERR_MEM_NOT_ENOUGH_STR,HTTP_RESPONSE_INFO_BUFFER_LENGTH);

//        return (-1);
//    }
//    iRet = FDBfSocket_recv(pobjSocket,pcResponseInfoBuf,HTTP_RESPONSE_INFO_BUFFER_LENGTH,FDBESocketIOFlag_No_Set);
//    if(iRet<0)
//    {
//        return (iRet);
//    }

//    //found content length and begin
//	i64ContentLength = FDBfGetHTTPContentLength(pcResponseInfoBuf,HTTP_RESPONSE_INFO_BUFFER_LENGTH);
//    pcContentBegin = FDBfFindHTTPContentBegin(pcResponseInfoBuf,HTTP_RESPONSE_INFO_BUFFER_LENGTH);

//    pcDownloadFileName = FDBfPickupFileNameFromPath(pobjURI->pcPath);
//    pcDownloadFileStorePath = (char*)calloc(strlen(pcStoreDirectory)+strlen(pcDownloadFileName)+2,1);
//    if( pcStoreDirectory[ strlen(pcStoreDirectory)-1 ] == '/')
//    {
//        sprintf(pcDownloadFileStorePath,"%s%s",pcStoreDirectory,pcDownloadFileName);
//    } else {
//        sprintf(pcDownloadFileStorePath,"%s/%s",pcStoreDirectory,pcDownloadFileName);
//    }

//    pobjDownloadFile = FDBfIODevice_new_not_open(pcDownloadFileStorePath);
//    if(!pobjDownloadFile)
//    {
//        return (-1);
//    }
//    iRet = FDBfIODevice_open(pobjDownloadFile,FDBEAccessFlag_WriteOnly,FDBEOpenMethod_AlwaysCreateNew);
//    if(iRet<0)
//    {
//        return (-1);
//    }
//    iRet = FDBfIODevice_write(pobjDownloadFile,pcContentBegin
//                              ,HTTP_RESPONSE_INFO_BUFFER_LENGTH - (pcContentBegin-pcResponseInfoBuf) );
//    if(iRet<0)
//    {
//        return (-1);
//    }
//    i64WrittenContentBytes += iRet;

//    i64FullLoadWrittenTime = (i64ContentLength-i64WrittenContentBytes) / HTTP_RESPONSE_INFO_BUFFER_LENGTH;
//    for(i64FullLoadWrittenTimeCursor = 0;i64FullLoadWrittenTimeCursor<i64FullLoadWrittenTime;++ i64FullLoadWrittenTimeCursor)
//    {
//        iRet = FDBfSocket_recv(pobjSocket,pcResponseInfoBuf,HTTP_RESPONSE_INFO_BUFFER_LENGTH,FDBESocketIOFlag_No_Set);
//        if(iRet<0)
//        {
//            return (-1);
//        }
//		if(iRet != HTTP_RESPONSE_INFO_BUFFER_LENGTH)
//		{
//            return (-1);
//		}
//        iRet = FDBfIODevice_write(pobjDownloadFile,pcResponseInfoBuf,iRet);
//        if(iRet<0)
//        {
//            return (-1);
//        }
//        i64WrittenContentBytes += iRet;
//    }
//    //recv remain content
//	while(i64WrittenContentBytes<i64ContentLength)
//	{
//		iRet = FDBfSocket_recv(pobjSocket,pcResponseInfoBuf,(int)(i64ContentLength-i64WrittenContentBytes),FDBESocketIOFlag_No_Set);
//		if(iRet<0)
//		{
//			return (-1);
//		}
//		iRet = FDBfIODevice_write(pobjDownloadFile,pcResponseInfoBuf,(int)(i64ContentLength-i64WrittenContentBytes));
//		if(iRet<0)
//		{
//			return (-1);
//		}
//		i64WrittenContentBytes += iRet;
//	}

//    FDBfIODevice_delete(&pobjDownloadFile);
//    FDBfFree( (void**)&pcDownloadFileStorePath);
//    FDBfFree( (void**)&pcResponseInfoBuf);
//    FDBfFree( (void**)&pcDownloadFileName);

//    return (0);
//}

//int FDBfHTTPRequest_through_exist_socket(FDBCSocket* pobjSocket
//                                         ,const FDBCURI* pobjURI,FDBEHTTPCommand eHTTPCommand
//                                         ,const FDBEHTTPConnectionMethod eHTTPConnectionMethod)
//{
//    char* pcRequestBuf = NULL;
//    int iRequestBufLen = 0;
//    int iRet = 0;

//    //construct request buffer,such as:GET / HTTP/1.1 \r\nHost: 192.168.1.30\r\nConnection: Close\r\n\r\n
//    switch(eHTTPCommand)
//    {
//        case FDBEHTTPCommand_GET:
//        {
//            const char* pcRequestFormatBuf = "GET %s HTTP/1.1 \r\nHost: %s\r\nAccept:*/*\r\nUser-Agent:Mozilla/4.0 (compatible; MSIE 5.00; Windows 98)\r\nConnection: %s\r\n\r\n";
//            iRequestBufLen = ( strlen(pcRequestFormatBuf) + strlen(pobjURI->pcPath)
//                               + strlen(pobjURI->pcHostAddr) + 10 );
//            pcRequestBuf = (char*)malloc(iRequestBufLen);
//            if(!pcRequestBuf)
//            {
//                giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
//                FDBErrStr_TWO_PARAM("%s,when new http request buffer(len:%d)",FDB_ERR_MEM_NOT_ENOUGH_STR,iRequestBufLen);
//                FDBLOG_TWO_PARAM(FDB_LOG_FILE,"%s,when new http request buffer(len:%d)"
//                                 ,FDB_ERR_MEM_NOT_ENOUGH_STR,iRequestBufLen);

//                return (-1);
//            }
//            switch(eHTTPConnectionMethod)
//            {
//                case FDBEHTTPConnectionMethod_Close:
//                {
//                    sprintf(pcRequestBuf,pcRequestFormatBuf,pobjURI->pcPath,pobjURI->pcHostAddr,"Close");
//                }break;
//                case FDBEHTTPConnectionMethod_Persist:
//                {
//                    sprintf(pcRequestBuf,pcRequestFormatBuf,pobjURI->pcPath,pobjURI->pcHostAddr,"Keep-Alive");
//                }break;
//                default:
//                {
//                    return (-1);
//                }break;
//            }
//        }break;
//        default:
//        {
//            return (-1);
//        }break;
//    }

//    //send request buffer use socket
//    iRet = FDBfSocket_send(pobjSocket,pcRequestBuf,strlen(pcRequestBuf),FDBESocketIOFlag_No_Set);
//    return (iRet);
//}
