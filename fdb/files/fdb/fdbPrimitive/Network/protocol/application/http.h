#ifndef __fdb_http_h_2013_11_04_06_00__
#define __fdb_http_h_2013_11_04_06_00__

#include "httplib.h"

#include "../../uri/uri.h"

#include <QObject>

#include <string>

#if !defined(Q_MOC_RUN)
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#endif

#include <pcre/pcre.h>

using namespace boost::asio;

using namespace std;

extern char* gp_uri_regex;
extern pcre* gps_uri_regex;

/*!
 * \brief The FDBCHttp class
 * @note 線程安全
 */
class FDBCHttp : public QObject{
    Q_OBJECT
public:
    typedef enum EHTTPHeader {
        EHTTPHeader_null = 0x00
        ,EHTTPHeader_host = 0x01
    } EHTTPHeader;
    FDBCHttp();
private:
    boost::mutex m_locker;
    boost::shared_ptr<io_service> m_io_service;
    boost::shared_ptr<ip::tcp::socket> m_sock;
    char* m_buf;
    int m_buf_size;
    int m_match_result_num;
    int* m_match_results;
    EHTTPHeader m_e_header;
public:
public slots:
    /*一次get请求,<头,内容>*/
    pair<string, string> slGet(const char* url);
    void slEnableHeader(EHTTPHeader e_header);
};

///*!
// * @brief get http content length,\r\nContent-Length: 44\r\n
// * @return -1 for failed(global error detect),other succeed
// * @note Content-Length is represented in http response,and display in decimal
// */
//FDBInterface int64_t FDBfGetHTTPContentLength(const char* pcBuf,int iBufLen);

///*!
// * @brief   find http content begin pos,always,it is a empty CRLF.
// * @return  NULL for failed
// * @note now,the \n\r flag must contain is one buffer,it means that \n\r cannot be truncated
// */
//FDBInterface char* FDBfFindHTTPContentBegin(const char* pBuf,const int iBufLen);

///*!
// * @return 0 for succeed,-1 for failed
// */
//FDBInterface int FDBfHTTPDownload(const char* pcURL,const char* pcStoreDirectory);

///*!
// * @brief use GET method download file through http protocol use exist socket
// * @return -1 for failed(global error detect),0 for succeed
// */
//FDBInterface int FDBfHTTPDownload_through_exist_socket(FDBCSocket* pobjSocket
//                                                       , const FDBCURI* pobjURI, const char* pcStoreDirectory);

///*!
// * @brief send http request through exist socket
// * @param
// * @return -1 for failed(global error detect),0 for succeed
// */
//FDBInterface int FDBfHTTPRequest_through_exist_socket(FDBCSocket* pobjSocket
//                                                      , const FDBCURI* pobjURI, FDBEHTTPCommand eHTTPCommand
//                                                      , const FDBEHTTPConnectionMethod eHTTPConnectionMethod);

#endif //header
