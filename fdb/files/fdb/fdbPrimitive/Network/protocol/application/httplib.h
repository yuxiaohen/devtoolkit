#ifndef __fdb_http_lib_h_2013_11_04_06_00__
#define __fdb_http_lib_h_2013_11_04_06_00__

#include "Applicationlib.h"

#define FDB_HTTP_VERSION_1_1 1.1
#define FDB_HTTP_VERSION_1_0 1.0

#define HTTP_RESPONSE_INFO_BUFFER_LENGTH    409600

typedef enum FDBEHTTPCommand {
    FDBEHTTPCommand_Unspecified = 0
    ,FDBEHTTPCommand_GET = 1
    ,FDBEHTTPCommand_POST = 2
    ,FDBEHTTPCommand_PUT = 3
    ,FDBEHTTPCommand_DELETE = 4
} FDBEHTTPCommand;

typedef enum FDBEHTTPConnectionMethod {
    FDBEHTTPConnectionMethod_Close = 0
    ,FDBEHTTPConnectionMethod_Persist = 1
} FDBEHTTPConnectionMethod;

typedef struct FDBSHTTPPDU {
    FDBEHTTPCommand eHTTPCommand;
    float rfHTTPVersion;

} FDBSHTTPPDU;

#endif //header
