HEADERS += \
    $$PWD/Network.h \
    $$PWD/Networklib.h \
    $$PWD/uri/uri.h \
    #socket
#    $$PWD/socket/socket.h \
#    $$PWD/socket/socketlib.h \
#    $$PWD/socket/Win32Socket.h \
    #protocol
    $$PWD/protocol/Protocol.h \
    $$PWD/protocol/Protocollib.h \
        #application layer
        $$PWD/protocol/application/Application.h \
        $$PWD/protocol/application/Applicationlib.h \
        $$PWD/protocol/application/http.h
