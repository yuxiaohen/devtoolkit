#include "fdb_mysql.h"

FDBCMysqlResult::FDBCMysqlResult(MYSQL *p_s_handler)
{
    this->m_p_s_result = mysql_store_result(p_s_handler);
    if(this->m_p_s_result == NULL)
    {
        return ;
    }
    this->m_pp_result_row = mysql_fetch_row(this->m_p_s_result);
    if(this->m_pp_result_row == NULL)
    {
        return ;
    }
    this->m_p_ul_result_column_len = mysql_fetch_lengths(this->m_p_s_result);

    this->i_column_num = mysql_num_fields(this->m_p_s_result);
    this->i_row_num = mysql_num_rows(this->m_p_s_result);
    MYSQL_FIELD* p_result_filed = NULL;
    for( int i=0;i < this->i_column_num;++ i )
    {
        p_result_filed = mysql_fetch_field(this->m_p_s_result);
        if(p_result_filed)
        {
            this->m_map_filed_name_to_index.insert(std::pair<std::string,int>(std::string(p_result_filed->name),i));
        }
    }
}

int
FDBCMysqlResult::mfColumnCount()
{
    return ( this->m_map_filed_name_to_index.size() );
}

std::pair<char *, int>
FDBCMysqlResult::mfGetResultByColumnName(const char* pc_column_name)
{
    std::map<std::string,int>::iterator ite = this->m_map_filed_name_to_index.find(pc_column_name);
    if( ite == this->m_map_filed_name_to_index.end())
    {
        return ( std::pair<char*,int>((char*)NULL,0) );
    }
    int i_column_index = this->m_map_filed_name_to_index.find(pc_column_name)->second;

    return ( std::pair<char*,int>(this->m_pp_result_row[i_column_index],this->m_p_ul_result_column_len[i_column_index]) );
}

std::pair<char *, int>
FDBCMysqlResult::mfGetResultByColumnIndex(const int i_column_index)
{
    if(i_column_index < this->i_row_num)
    {
        return ( std::pair<char*,int>(this->m_pp_result_row[i_column_index],this->m_p_ul_result_column_len[i_column_index]) );
    } else {
        return ( std::pair<char*,int>((char*)NULL,0) );
    }
}

FDBCMysqlDBConnector::FDBCMysqlDBConnector()
{
    MYSQL* p_s_mysql_handler = mysql_init(&this->m_s_handler);
    if(p_s_mysql_handler == NULL)
    {
        printf("mysql init failed\r\n");
    }
}

int
FDBCMysqlDBConnector::mfConnect(const char* pc_host,const uint16_t u16_host_port,const char* pc_user,const char* pc_passwd,const char* pc_db_name)
{
    MYSQL* p_s_mysql_handler = mysql_real_connect(&this->m_s_handler
                                                  ,pc_host
                                                  ,pc_user
                                                  ,pc_passwd
                                                  ,pc_db_name
                                                  ,u16_host_port
                                                  ,NULL,0);
    if(p_s_mysql_handler == NULL)
    {
        return (-1);
    }
    return (0);
}

boost::shared_ptr<FDBCMysqlResult>
FDBCMysqlDBConnector::mfExeSql(const void* p_sql,const uint32_t u32_sql_len)
{
    int i_query_result = mysql_real_query(&this->m_s_handler,(char*)p_sql,u32_sql_len);
    if(i_query_result != 0)
    {
        printf("query error,reason: %s\r\n",mysql_error(&this->m_s_handler));
        return (boost::shared_ptr<FDBCMysqlResult>());
    }

    boost::shared_ptr<FDBCMysqlResult> sp_result(new FDBCMysqlResult(&this->m_s_handler));
    return (sp_result);
}

int
FDBCMysqlDBConnector::mfEscapeString(void* p_dest,const void* p_from,const uint32_t u32_source_len)
{
    uint32_t u32_len = 0;
    u32_len = mysql_real_escape_string(&this->m_s_handler,(char*)p_dest,(const char*)p_from,u32_source_len);
    return (u32_len);
}