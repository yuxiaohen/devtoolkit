#ifndef __fdb_database_mysql_h_2013_11_19_05_44__
#define __fdb_database_mysql_h_2013_11_19_05_44__

#include <boost/shared_ptr.hpp>

extern "C" {
    #include <mysql.h>
}

#include <utility>
#include <map>
#include <string>

#include "../../../fdbPrimitive/Type/Type.h"

class FDBInterface FDBCMysqlResult {
private:
    MYSQL_RES* m_p_s_result; //结果集
    MYSQL_ROW m_pp_result_row; //当前结果的一行
    unsigned long* m_p_ul_result_column_len; //一行结果的每一列的数据长度

    std::map<std::string,int> m_map_filed_name_to_index;
public:
    int i_column_num;
    int i_row_num;
    FDBCMysqlResult(MYSQL* p_s_handler);

    /*!
     * @brief 获得结果集列数,为0时说明没有结果
     * @return 非负数
     */
    int
    mfColumnCount();

    /*!
     * @brief mfGetResultByColumnName 通过列名获得列值
     * @param pc_column_name 列名
     * @return <列值,长度>,<NULL,0>失败
     */
    std::pair<char*,int>
    mfGetResultByColumnName(const char* pc_column_name);

    /*!
     * @brief mfGetResultByColumnIndex 通过列索引获得列值
     * @param i_column_index 列索引
     * @return <列值,长度>,<NULL,0>失败
     */
    std::pair<char*,int>
    mfGetResultByColumnIndex(const int i_column_index);

    ~FDBCMysqlResult()
    {
        mysql_free_result(this->m_p_s_result);
    }
};

class FDBInterface FDBCMysqlDBConnector {
private:
    MYSQL m_s_handler;

public:
    FDBCMysqlDBConnector();
    ~FDBCMysqlDBConnector()
    {
        mysql_close(&m_s_handler);
    }

    /*!
     * @brief 连接远程数据库
     * @return 0成功,-1失败
     */
    int mfConnect(const char* pc_host,const uint16_t u16_host_port,const char* pc_user,const char* pc_passwd,const char* pc_db_name);

    /*!
     * @brief 执行sql语句
     */
    boost::shared_ptr<FDBCMysqlResult> mfExeSql(const void *p_sql, const uint32_t u32_sql_len);

    /*!
     * @brief 转义二进制数据
     */
    int mfEscapeString(void* p_dest,const void* p_from,const uint32_t u32_source_len);
};

#endif //header
