﻿#include "Base64.h"

//***********************************************Base64 加解密模块***********************************************//

//将6位bit字节转换为Base64字符
static char base64_string[]= {"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"};

//将Base64字符转换为6位bit字节,若非Base64字符,返回-1
static int base64_decode_char(char c)
{
    int i;
    if(c=='\0'||c=='=')return -1;
    for(i=0; i<sizeof(base64_string)/sizeof(*base64_string); ++i)
    {
        if(c==base64_string[i])
        {
            return i;
        }
    }
    return -1;
}

/*Base64编码:
    对in中数据进行base64编码,结果放入out中
    [out]参数out:指向存放编码后Base64编码的缓冲区
    [in] 参数in:指向存放待编码的数据缓冲区
    [in] 参数in_len:表示待编码数据的长度
    return: 返回Base64编码长度
*/
int base64_encode(char* out, const char* in,int in_len)
{
    int i_out = 0;
    int i_bit = 0;
    int i_in = 0;
    out[i_out] = 0 ;
    for (i_in = 0; i_in < in_len; ++i_in)//对输入buf进行遍历
    {
        char c = in[i_in];
        int char_bit = 8;
        while(--char_bit>=0)
        {
            out[i_out] |= ((c&0X80)!=0);//最高位放入
            ++i_bit;
            c <<= 1;

            if( (i_bit)%6==0 )//某个字节已经填满
            {
                out[i_out] = base64_string[out[i_out]];
                ++i_out;//跳转到下一个字节
                out[i_out] = 0 ;//清零
            }
            else //未被填满 继续左移
            {
                out[i_out] <<= 1;
            }
        }
    }

    //不足3n个字符,进行末尾处理
    if( ( (i_out)%4)!=0 )
    {
        //最末字符的处理
        while( (++i_bit)%6!=0 )
        {
            out[i_out] <<= 1;
        }

        out[i_out] = base64_string[out[i_out]];

        //填 '=' 符号
        while( ( (i_out)%4)!=0 )
        {
            ++i_out;
            out[i_out] = '=';
        }
    }
    return i_out;
}

/*Base64解码:
    对in中数据进行base64解码,结果放入out中
    [out]参数out:指向存放解码后数据的缓冲区
    [in] 参数in:指向存放Base64编码的数据缓冲区
    [in] 参数in_len:表示Base64编码数据的长度
    return: 返回解码后数据的长度
*/
int base64_decode(char* out, const char* in,int in_len)
{
    int i_out = 0;
    int i_bit = 0;
    int i_in = 0;
    out[i_out] = 0 ;
    for (i_in = 0; i_in < in_len; ++i_in)//对输入buf进行遍历
    {
        char c = base64_decode_char(in[i_in]);
        int char_bit = 6;
	if(c==-1)return i_out; //遇到结束符 '\0' 或者 '\='
        while(--char_bit>=0)
        {
            out[i_out] |= ((c&0X20)!=0);//最高位放入
            ++i_bit;
            c <<= 1;

            if( (i_bit)%8==0 )//某个字节已经填满
            {
                ++i_out;//跳转到下一个字节
                out[i_out] = 0 ;
                continue;
            }
            else //未被填满 继续左移
            {
                out[i_out] <<= 1;
            }
        }
    }
    return i_out;
}
//***********************************************Base64 加解密模块结束***********************************************//
