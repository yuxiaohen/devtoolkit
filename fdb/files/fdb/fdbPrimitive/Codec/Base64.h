﻿#ifndef __fdb_base_64_h_2013_08_20_12_22__
#define __fdb_base_64_h_2013_08_20_12_22__

#include "Codeclib.h"

FDBInterface int base64_encode(char* out, const char* in,int in_len);

FDBInterface int base64_decode(char* out, const char* in,int in_len);

#endif
