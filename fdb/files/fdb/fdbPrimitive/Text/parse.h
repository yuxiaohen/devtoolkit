﻿/*******************************************************************
 *  FDB - Full Data Box -- FullDataSoft Company Foudation Library
 *
 *  author:shawhen
 *
 *  parse.h -- function unit:
 *      \f1.遵从某一个键值对的格式提取出key对应的值,格式比如key:Value|
            |--比如从"age:18|length:175|sex:male|"
            |--中提取sex或者age等等对应的值
           !!NOTE:support only one special spliter
 *
 *
 * version: 2013-07-06 | shawhen | reject qt
 *******************************************************************/
#ifndef __fdb_parse_h_2013_08_29_10_14__
#define __fdb_parse_h_2013_08_29_10_14__

#include "Textlib.h"

/*!
 * @brief f1:the key-and store in strContent with some fixed format,such as key:value|
 *          通过指定一种键值对存储格式,从字符串中提取键对应的值
 * @param p_KV_format 键值对存储格式,指定的分隔符在p_content中作为普通字符时应该使用\进行转义,当然,这包括\本身
            //Key=Value-
            //Key:Value|
            //Key:Val\|ue|
 * @return 值是在p_content中的位置,不要释放,失败时返回NULL
 */

FDBInterface const char *FDBfText_parse_value_by_key_match_format(const char* pc_content
                                                                  , const char* pc_key
                                                                  , unsigned long *p_u32_value_length
                                                                  , const int i_from
                                                                  , const char* pc_KV_format/* = "Key:Value|"*/
                                                                  );

#endif //header
