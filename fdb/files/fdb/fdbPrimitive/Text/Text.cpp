﻿#include "Text.h"

#include <stdlib.h>
#include <string.h>

#include <ctype.h>

//#include "../Exception/Exception.h"

char* FDBfText_strstr(const char* pcTotal, const int iTotalLen, const char* pcSection,const int section_len)
{
    int iSectionLen;
    if(section_len < 0)
    {
        iSectionLen = strlen(pcSection);
    } else {
        iSectionLen = section_len;
    }
    int iMatchedNumber = 0;
    int iTotalCursor = 0;

    if(!iSectionLen)
    {
        return ((char*)pcTotal);
    }


    for(iTotalCursor = 0;iTotalCursor<iTotalLen && iMatchedNumber != iSectionLen;)
    {
        if(pcTotal[iTotalCursor] == pcSection[iMatchedNumber])
        {
            ++ iMatchedNumber;
            ++ iTotalCursor;
        } else {
            iTotalCursor = iTotalCursor - iMatchedNumber + 1;
            iMatchedNumber = 0;
        }
    }

    if(iMatchedNumber == iSectionLen)
    {
        return (char*)(pcTotal+iTotalCursor-iSectionLen);
    }

    return (NULL);
}

int64_t
FDBfText_str_to_i64(const char *pcBuf)
{
    long long int i64Ret = 0;
    int iBufferCursor = 0;

    if(!pcBuf)
    {
        return (0);
    }

    for(iBufferCursor = strlen(pcBuf) - 1;iBufferCursor>=0;-- iBufferCursor)
    {
        if( isalnum(pcBuf[iBufferCursor]) )
        {
            i64Ret *= 10;
            i64Ret += pcBuf[iBufferCursor];
        }
    }

    if(pcBuf[0] == '-')
    {
        i64Ret |= 0x8000000000000000;
    }


    return (i64Ret);
}

char* 
FDBfText_pickup_filename_from_path(const char* pcPath)
{
    int iFileNameLen = 0;
    int iPathCursor = 0;
    char* pcFileName = NULL;
    int iPathLen = strlen(pcPath);

    for(iPathCursor = iPathLen-1;iPathCursor>=0;-- iPathCursor)
    {
        if(pcPath[iPathCursor]=='/' || pcPath[iPathCursor]=='\\')
        {
            break;
        }
        ++ iFileNameLen;
    }

    pcFileName = (char*)calloc(iFileNameLen+1,1);
//    if(!pcFileName)
//    {
//        giFDBErrNum = FDB_ERR_MEM_NOT_ENOUGH;
//        FDBErrStr_TWO_PARAM("%s when new file name buffer(length:%d)",FDB_ERR_MEM_NOT_ENOUGH_STR,iFileNameLen);
//        FDBLOG_TWO_PARAM(FDB_LOG_FILE,"%s when new file name buffer(length:%d)",FDB_ERR_MEM_NOT_ENOUGH_STR,iFileNameLen);

//        return (NULL);
//    }
    strcpy(pcFileName,pcPath+iPathLen-iFileNameLen);

    return (pcFileName);

}
