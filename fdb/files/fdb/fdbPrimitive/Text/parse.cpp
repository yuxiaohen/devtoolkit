#include "parse.h"

#include <stdlib.h>
#include <string.h>

#include "../MemoryManagement/MemoryManagement.h"
#include "../Type/Type.h"

const char*  FDBfText_parse_value_by_key_match_format(const char* pc_content
                                      ,const char* pc_key
                                      ,unsigned long* p_u32_value_length
                                      ,const int i_from/*=0*/
                                      ,const char* pc_KV_format/* = "Key:Value|"*/
                                      )
{
    const uint32_t u32_key_len = strlen(pc_key); //原始key的长度
    char pc_search_key[1024] = {0}; //真正供搜索的key
    char* pc_key_pos = NULL; //key开始的地方

    const char* pc_KV_end_pos = pc_KV_format + strlen(pc_KV_format) - 1; //键值对结束符的开始位置
    const char* pc_search_value_begin_pos = NULL; //在content中搜索value开始的位置
    const char* pc_value_begin_pos_in_conent = NULL; //content中值开始的位置
    const char* pc_value_end_pos_in_content = NULL; //content中值结束的位置

    //初始化供搜索的key
    if(pc_search_key != NULL)
    {
        const char* pc_value_begin_pos = strstr(pc_KV_format,"Value"); //value开始的地方
        if(pc_value_begin_pos == NULL)
        {
            return (NULL);
        }
        memset(pc_search_key,0,1024);
        strncpy( pc_search_key,pc_KV_format+3,pc_value_begin_pos-(pc_KV_format+3) );
    }

    //找到content中key开始的位置
    pc_key_pos = strstr((char*)pc_content+i_from,pc_search_key);
    if(pc_key_pos == NULL)
    {
        return (NULL);
    }

    pc_value_begin_pos_in_conent = pc_key_pos + u32_key_len+1;
    pc_search_value_begin_pos = pc_key_pos + u32_key_len;
    //寻找值结束符的位置
    do {
        pc_value_end_pos_in_content = strstr(pc_search_value_begin_pos,pc_KV_end_pos);
        if(pc_value_end_pos_in_content == NULL)
        {
            return (NULL);
        }
        pc_search_value_begin_pos = pc_value_end_pos_in_content + 1;
    } while( *(pc_value_end_pos_in_content-1)=='\\' && *(pc_value_end_pos_in_content-2)!='\\' );

    if( !pc_value_end_pos_in_content )
    {
        return (NULL);
    }

    //值的长度
    *p_u32_value_length = pc_value_end_pos_in_content - pc_value_begin_pos_in_conent;

    return (pc_value_begin_pos_in_conent);
}
