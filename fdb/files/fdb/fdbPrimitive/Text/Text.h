﻿#ifndef __fdb_text_h_2013_08_29_10_00__
#define __fdb_text_h_2013_08_29_10_00__

#include "parse.h"
#include "Textlib.h"
#include "../Type/Type.h"

#include <string.h>
#include <string>
#include <ctype.h>
#include <stdlib.h>

using namespace std;

/*!
 * @brief search substr in specified buffer not end up with '\0',this means,caller must specify total buffer length
 * @return NULL for failed
 * @note return value can not be free,it is a value inside pcTotal
 */
FDBInterface char*
FDBfText_strstr(const char* pcTotal, const int iTotalLen, const char* pcSection, const int section_len = -1);

/*!
 * @brief ascii to long long int (不被重视,可用sscanf+%llu...实现)
 * @return + or - ascii number value
 */
FDBInterface int64_t 
FDBfText_str_to_i64(const char* pcBuf);

/*!
 * @brief 从一个字符串中析取出第一个连贯的数字堆,比如adb123d,会拿出123,mmcblk12,会拿出12
 * @note 暂不搞正负
 */
template<typename _Tdest>
void FDBfText_pick_up_num(_Tdest* p_dest,const char* p_ascii_text,int i_ascii_text_len = -1)
{
    int text_len = (i_ascii_text_len < 0 ? strlen(p_ascii_text) : i_ascii_text_len);
    for(int i=0;i<text_len;++ i)
    {
        if( isdigit((unsigned char)p_ascii_text[i]) )
        {
            FDBfText_ascii_to_integer(p_dest,p_ascii_text+i,text_len-i);
            break;
        }
    }
}

template<typename _Tdest>
void FDBfText_ascii_to_integer(_Tdest* p_dest,const char* p_ascii_text,int i_ascii_text_len = -1)
{
    memset(p_dest,0,sizeof(_Tdest));
    i_ascii_text_len = i_ascii_text_len==-1?strlen(p_ascii_text):i_ascii_text_len;
    //从左到右找到最后一个合法数字字符
    int i_first_invalid_digit_char = 0;
        //第一位允许是符号位
    if(p_ascii_text[0] == '+' || p_ascii_text[0] == '-')
    {
        i_first_invalid_digit_char = 1;
    }
    for(;i_first_invalid_digit_char<i_ascii_text_len;++ i_first_invalid_digit_char)
    {
        if( !isdigit((unsigned char)p_ascii_text[i_first_invalid_digit_char]) )
        {
            break;
        }
    }

    //单独处理符号位
    int i_sign = 1;
    int i_begin_cal_pos = 0;
    if(p_ascii_text[0] == '-')
    {
        i_sign = -1;
        i_begin_cal_pos = 1;
    } else if(p_ascii_text[0] == '+') {
        i_begin_cal_pos = 1;
    }

    for(;i_begin_cal_pos <= i_first_invalid_digit_char-1;++ i_begin_cal_pos)
    {
        (*p_dest) *= 10;
        (*p_dest) += (p_ascii_text[i_begin_cal_pos]-'0');
    }
    (*p_dest) *= i_sign;
}

/*!
 * @return 返回转换后的整型所占的字节数
 */
template<typename _Tsrc>
string FDBfText_integer_to_ascii(const _Tsrc p_src)
{
    //看看src有需要几位字符存储
    int i_src_len = 0;
    char pc_text[4096] = {0};
    _Tsrc src_copy = (p_src);
    for(;i_src_len<4096;++ i_src_len)
    {
        if(src_copy > 0)
        {
            pc_text[4096-1-i_src_len] = src_copy % 10 + '0'; //取出个位
            src_copy /= 10; //去掉末位
        } else {
            break;
        }
    }

    return string(pc_text+4096-1-(i_src_len-1),i_src_len);
}

/*!
 * @brief pick up file name form a path,such as /d/demo/demo2/demo.txt -> demo.txt
 * @return path suffix:file name
 * @note caller must free return value manually
 */
FDBInterface char*
FDBfText_pickup_filename_from_path(const char* pcPath);

#endif //header
