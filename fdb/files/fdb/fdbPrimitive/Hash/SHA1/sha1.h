﻿#ifndef __fdb_sha1_h_2013_08_30_05_30__
#define __fdb_sha1_h_2013_08_30_05_30__

#include "../../IOKit/IOKit.h"

#include <stdlib.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct SHA1Context
{
	unsigned Message_Digest[5]; /* Message Digest (output)          */

	unsigned Length_Low;        /* Message length in bits           */
	unsigned Length_High;       /* Message length in bits           */

	unsigned char Message_Block[64]; /* 512-bit message blocks      */
	int Message_Block_Index;    /* Index into message block array   */

	int Computed;               /* Is the digest computed?          */
	int Corrupted;              /* Is the message digest corruped?  */
} SHA1Context;

void SHA1Input64(SHA1Context *context,char* cBuf);
void SHA1Reset(SHA1Context *);
int SHA1Result(SHA1Context *);
void SHA1Input( SHA1Context *, const unsigned char *, unsigned);
int FDBfGetFileSHA1(const char* pcPath, char* SHA1);

#if defined(__cplusplus)
}
#endif

#endif //header
