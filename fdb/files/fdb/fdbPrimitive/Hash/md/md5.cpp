#include "md5.h"

#include <openssl/md5.h>

#include <stdlib.h>

char *FDBfHash_md5(const void* p_origin_buffer, const uint32_t u32_origin_buffer_len)
{
    char* digest = (char*)calloc(1,33);
    unsigned char md[16] = {0};
    MD5((const unsigned char*)p_origin_buffer,u32_origin_buffer_len,md);
    sprintf(digest,"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x"
            ,md[0],md[1],md[2],md[3],md[4],md[5],md[6],md[7]
            ,md[8],md[9],md[10],md[11],md[12],md[13],md[14],md[15]);
    return (digest);
}
