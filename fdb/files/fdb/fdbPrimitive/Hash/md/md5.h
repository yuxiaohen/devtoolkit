﻿#ifndef __fdb_hash_h_2013_11_07_05_29__
#define __fdb_hash_h_2013_11_07_05_29__

#include "../Hashlib.h"

#include "../../Type/Type.h"

#ifndef Q_MOC_RUN
    #include <boost/shared_ptr.hpp>
#endif

#include <vector>

/*!
 * @brief 计算md5,32字节十进进制字符串
 * @return md5值,请在外部释放....
 */
FDBInterface char*
FDBfHash_md5(const void* p_origin_buffer, const uint32_t u32_origin_buffer_len);

#endif //header
