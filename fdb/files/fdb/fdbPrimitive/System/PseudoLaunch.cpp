#if defined(__APPLE__)

#include "PseudoLaunch.h"
#include "../IOKit/IOKit.h"

#include <list>

//launch other program as root,use sudo require password
int main(int argc,char* argv[],char* env[])
{
    int iRet = 0;
    FDBCIODevice* pobjIODevice = FDBfIODevice_new_not_open("PseudoLaunch.dat");
    iRet = FDBfIODevice_open(pobjIODevice,FDBEAccessFlag_ReadOnly,FDBEOpenMethod_OpenExisting);
    assert(!iRet);
    long long int i64AppToLaunchSize = FDBfIODevice_size(pobjIODevice);
    char* pcAppToLaunch = (char*)malloc(i64AppToLaunchSize);
    FDBfIODevice_read_from(pobjIODevice,pcAppToLaunch,0,i64AppToLaunchSize);

    iRet = execl("/usr/bin/sudo","/bin/ls","-al",NULL);
    if(iRet < 0)
    {
        printf("start program(%s) failed,error code: %d\r\n",pcAppToLaunch,errno);
        return (-1);
    }

    return (0);
}

#endif
