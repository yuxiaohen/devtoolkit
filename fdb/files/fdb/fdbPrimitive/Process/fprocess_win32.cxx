﻿#if defined(_WIN32)

#include "fprocess.h"

#include <Windows.h>

#include <fdb/fdb.h>

#include <fcntl.h>
#include <io.h>

typedef struct FProcessOpaque {
    uint32_t process_id;
    HANDLE process_handle;
    HANDLE process_read_pipe;
    HANDLE process_error_read_pipe;
    HANDLE process_write_pipe;
    ~FProcessOpaque()
    {
        TerminateProcess(this->process_handle,0);
        CloseHandle(process_read_pipe);
        CloseHandle(process_error_read_pipe);
        CloseHandle(process_write_pipe);
    }
} FProcessOpaque;

FProcess::FProcess()
{
    this->m_opaque = new FProcessOpaque();
}

bool FProcess::start(const string command_with_args)
{
    SECURITY_ATTRIBUTES security_attibutes;
    security_attibutes.nLength = sizeof(SECURITY_ATTRIBUTES);
    security_attibutes.lpSecurityDescriptor = NULL;
    security_attibutes.bInheritHandle = TRUE;
    PROCESS_INFORMATION process_information;
    ZeroMemory(&process_information,sizeof(PROCESS_INFORMATION));
    STARTUPINFOA startup_info;

    HANDLE process_handle = NULL;
    uint32_t process_id = 0;

    //初始化通信管道
    HANDLE parent_write_to_child_pipe = INVALID_HANDLE_VALUE;
    HANDLE parent_read_from_child_pipe = INVALID_HANDLE_VALUE;
    HANDLE child_read_from_parent_pipe = INVALID_HANDLE_VALUE;
    HANDLE child_write_to_parent_pipe = INVALID_HANDLE_VALUE;
    HANDLE child_error_write_to_parent_pipe = INVALID_HANDLE_VALUE;
    HANDLE parent_error_read_from_child_pipe = INVALID_HANDLE_VALUE;
    auto result = CreatePipe(&parent_read_from_child_pipe,&child_write_to_parent_pipe
                             ,&security_attibutes,0);
    if(result == FALSE)
    {
        return (false);
    }
    result = CreatePipe(&child_read_from_parent_pipe,&parent_write_to_child_pipe
                        ,&security_attibutes,0);
    if(result == FALSE)
    {
        CloseHandle(parent_read_from_child_pipe);
        return (false);
    }
    result = CreatePipe(&parent_error_read_from_child_pipe,&child_error_write_to_parent_pipe
                        ,&security_attibutes,0);
    if(result == FALSE)
    {
        CloseHandle(parent_read_from_child_pipe);
        CloseHandle(child_read_from_parent_pipe);
        return (false);
    }
    //启动参数
    ZeroMemory(&startup_info,sizeof(STARTUPINFOA));
    startup_info.cb = sizeof(STARTUPINFOA);
//    startup_info.wShowWindow = SW_HIDE;
    startup_info.wShowWindow = SW_SHOW;
    startup_info.dwFlags = STARTF_USESTDHANDLES|STARTF_USESHOWWINDOW;
    startup_info.hStdInput = child_read_from_parent_pipe;
    startup_info.hStdOutput = child_write_to_parent_pipe;
    DuplicateHandle(GetCurrentProcess(),child_error_write_to_parent_pipe
                    ,GetCurrentProcess(),&startup_info.hStdError
                    ,DUPLICATE_SAME_ACCESS,TRUE,0);
    //启动
    auto start_result = CreateProcessA(NULL,(char*)command_with_args.data()
                                       ,NULL,NULL,TRUE,0,NULL,NULL
                                       ,&startup_info,&process_information);
    if(start_result == FALSE)
    {
        auto error = ::GetLastError();
        CloseHandle(parent_read_from_child_pipe);
        CloseHandle(parent_write_to_child_pipe);
        CloseHandle(child_read_from_parent_pipe);
        CloseHandle(child_write_to_parent_pipe);
        CloseHandle(child_error_write_to_parent_pipe);
        CloseHandle(parent_error_read_from_child_pipe);
        return (false);
    }
    process_handle = process_information.hProcess;
    CloseHandle(process_information.hThread);
    process_id = process_information.dwProcessId;
//    CloseHandle(child_read_from_parent_pipe);
//    CloseHandle(child_write_to_parent_pipe);

    FProcessOpaque* win32_opaque = (FProcessOpaque*)this->m_opaque;
    win32_opaque->process_handle = process_handle;
    win32_opaque->process_id = process_id;
    win32_opaque->process_write_pipe = parent_write_to_child_pipe;
    win32_opaque->process_read_pipe = parent_read_from_child_pipe;
    return (true);
}

int FProcess::processId()
{
    FProcessOpaque* win32_opaque = (FProcessOpaque*)this->m_opaque;
    return (win32_opaque->process_id);
}

string FProcess::readAll()
{
    FProcessOpaque* win32_opaque = (FProcessOpaque*)this->m_opaque;
    char buf[4096] = {0};

    string result;
    int total_bytes_read = 0;
    while(1)
    {
        int bytes_read = 0;
        auto read_result = ReadFile(win32_opaque->process_read_pipe
                                    ,buf,4096
                                    ,(LPDWORD)&bytes_read
                                    ,NULL);
        if(read_result == FALSE || bytes_read <= 0)
        {
            break;
        }
        result.resize(total_bytes_read+bytes_read);
        memcpy((char*)result.data()+total_bytes_read,buf,bytes_read);
        total_bytes_read += bytes_read;
        if(bytes_read != 4096)
        { //非满...但是个bug..万一某一次真是刚好4096读完了,就悲剧了
            break;
        }
    }
    return (result);
}

int FProcess::read(void* buf,const int len)
{
    FProcessOpaque* win32_opaque = (FProcessOpaque*)this->m_opaque;
    int bytes_read = 0;
    auto result = ReadFile(win32_opaque->process_read_pipe
                            ,buf,len
                            ,(LPDWORD)&bytes_read
                            ,NULL);
    if (result == FALSE)
    {
        return (-1);
    }
    //
    return (bytes_read);
}

int FProcess::write(const void *buf, const int len)
{
    FProcessOpaque* win32_opaque = (FProcessOpaque*)this->m_opaque;

    int total_bytes_write = 0;
    while( total_bytes_write < len )
    {
        int bytes_write = 0;
        auto write_result = WriteFile(win32_opaque->process_write_pipe
                                      ,(const char*)buf+total_bytes_write,len-total_bytes_write
                                      ,(LPDWORD)&bytes_write
                                      ,NULL);
        if(write_result == FALSE || bytes_write <= 0)
        {
            break;
        }
        total_bytes_write += bytes_write;
    }
    FlushFileBuffers(win32_opaque->process_write_pipe);
    return (total_bytes_write);
}

FProcess::~FProcess()
{
    FProcessOpaque* win32_opaque = (FProcessOpaque*)this->m_opaque;
    delete this->m_opaque;
}

#endif
