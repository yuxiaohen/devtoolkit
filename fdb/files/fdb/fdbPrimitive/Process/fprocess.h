﻿#pragma once

#include <string>

using namespace std;

class FProcess {
private:
    void* m_opaque;
public:
    FProcess();
    ~FProcess();
    bool start(const string command_with_args);
    int processId();
    string readAll();
    int read(void *buf, const int len);
    int write(const void* buf,const int len);
};
