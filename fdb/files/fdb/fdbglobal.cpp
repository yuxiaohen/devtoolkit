#pragma init_seg(".CRT$XCB")

#include "fdbglobal.h"

#include <functional>

set<const QMetaObject*>* gp_reflect_set = new set<const QMetaObject*>();
map<const string, const QMetaObject*>* gp_reflect_map = new map<const string, const QMetaObject*>();
