#pragma once

#include "fdblib.h"

#include <QMetaObject>

#ifndef Q_MOC_RUN
#include <boost/thread.hpp>
#endif

#include <set>
#include <map>
#include <string>
using namespace std;

extern FDBInterface set<const QMetaObject*>* gp_reflect_set;
extern FDBInterface map<const string,const QMetaObject*>* gp_reflect_map;
//extern FDBInterface pair<set<const QMetaObject*>::iterator,bool> g_pair_insert_result;

#define F_DEC_REFLECT(_T) \
    static auto _T##insert_result = gp_reflect_set->insert(&_T::staticMetaObject);

inline FDBInterface QObject* F_USE_REFLECT(const char* class_name)
{
    static boost::mutex* mutex = new boost::mutex();
    static int b_initialized = 0;
    QObject* ret = nullptr;
    mutex->lock();
    if (b_initialized == 0)
    {
        for (auto ite_set = gp_reflect_set->begin()
            ; ite_set != gp_reflect_set->end()
            ; ++ite_set)
        {
            gp_reflect_map->insert(make_pair((*ite_set)->className(),*ite_set));
        }
        b_initialized = 1;
    }
    auto ite = gp_reflect_map->find(class_name);
    if(ite != gp_reflect_map->end())
    {
        if (ite->second)
        {
            ret = ite->second->newInstance();
        }
    }
    mutex->unlock();
    return (ret);
}
