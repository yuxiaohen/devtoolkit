CONFIG += qt
QT += gui core widgets webkit webkitwidgets

INCLUDEPATH += $$PWD/files

#include($$PWD/files/fdb/fdbLogic/Qt/qtquick2applicationviewer/qtquick2applicationviewer.pri)

include($$PWD/FDBUIComponent.pri)

#暂时还不是太稳定或者健全的东西放这
HEADERS += \
#    $$PWD/files/fdb/fdbLogic/Qt/FDBCWizard.h \
#    $$PWD/files/fdb/fdbLogic/Qt/FDBCWizardPage.h \
    $$PWD/files/fdb/fdbPrimitive/Process/fprocess.h

SOURCES += \
#    $$PWD/files/fdb/fdbLogic/Qt/FDBCWizard.cxx \
#    $$PWD/files/fdb/fdbLogic/Qt/FDBCWizardPage.cxx \
    $$PWD/files/fdb/fdbPrimitive/Process/fprocess_win32.cxx

linux | mac {
    DEFINES += _FDB_POSIX _LARGEFILE64_SOURCE=1 _FILE_OFFSET_BITS=64
}

include(x:/boost/boost.pri)
include(x:/serialization/jsonite/jsonite.pri)
include(x:/openssl/openssl.pri)
include(x:/regex/regex.pri)
win32 {
    DEFINES += NOMINMAX
    LIBS += -lDbghelp
    win32-msvc2010 {
      contains(DEFINES,WIN64) { #amd64
          QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/amd64/msvc/2010
          CONFIG(debug,debug|release) { #debug
              LIBS += -lFDBd.2010.dll
          } else { #release
              LIBS += -lFDB.2010.dll
          }
      } else { #i386
          QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/i386/msvc/2010
          CONFIG(debug,debug|release) { #debug
              LIBS += -lFDBd.2010.dll
          } else { #release
              LIBS += -lFDB.2010.dll
          }
      }
    }
    win32-msvc2013 {
        QMAKE_LFLAGS += /LIBPATH:$$PWD/lib/win32/i386/msvc/2013
        CONFIG(debug,debug|release) {
            LIBS += -lFDBd.2013.dll
        } else {
            LIBS += -lFDB.2013.dll
        }
    }
}

mac {
    macx-clang {
        CONFIG(x86,x86|x86_64) { #i386
            QMAKE_LFLAGS += -L$$PWD/lib/mac/clang/amd64/static
        } else { #amd64
            QMAKE_LFLAGS += -L$$PWD/lib/mac/clang/amd64/static
            CONFIG(debug,debug|release) { #debug
                message("link fdb on mac clang amd64 debug mode")
                LIBS += -lFDBd
            } else {
                message("link fdb on mac clang amd64 release mode")
                LIBS += -lFDB
            }
        }
    }
}
