message("           ----------------->link fdb from source")

CONFIG += qt
QT += core concurrent gui widgets webkitwidgets

win32 {
    DEFINES += NOMINMAX
    include(x:/boost/boost.pri)
    include(x:/openssl/openssl.pri)
    include(x:/serialization/jsonite/jsonite.pri)
    include($$PWD/FDBUIComponent.pri)
    include(x:/zlib/zlib.pri)
    include(x:/regex/regex.pri)
    include(x:/smartdog/smartdog.pri)
}

DEFINES += LINK_FDB_FROM_SOURCE

include($$PWD/fdbHeaders.pri)
include($$PWD/fdbSources.pri)

win32-msvc* {
    QMAKE_LFLAGS += /DEBUG
}

message("           <-----------------link fdb from source")
